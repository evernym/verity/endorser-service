import DevEnvironment.DebianRepo
import DevEnvironmentTasks._
import Lightbend._
import SharedLibrary.{StandardLib, sharedLibraries, sharedLibrariesDir}
import Utils._
import sbtassembly.AssemblyKeys.assemblyMergeStrategy
import sbtassembly.MergeStrategy

name := "endorser"

scalaVersion := "2.13.8"
jdkExpectedVersion := "11"

val majorNum = "0"
val minorNum = "1"
val patchNum = "0"

envRepos := Seq(DebianRepo(
  "https://repo.corp.evernym.com/deb",
  "evernym-agency-dev-ubuntu",
  "main",
  "954B 4A2B 453F 834B 6962  7B5F 5A45 7C93 E812 1A0A",
  "https://repo.corp.evernym.com/repo.corp.evenym.com-sig.key"
))

// Library Versions
val akkaVersion            = "2.6.19"
val akkaHttpVersion        = "10.2.9"
val akkaPersistenceJdbc    = "5.0.4"
val akkaMgtVer             = "1.1.3"
val alpakkaKafka           = "3.0.0"
val alpakkaStream          = "3.0.4"
val jacksonDatabindVersion = "2.11.4"
val slickVersion           = "3.3.3"
val logbackVersion         = "1.2.11"
val logbackJacksonVersion  = "0.1.5"
val scalaLoggingVersion    = "3.9.5"
val mysqlJdbcVersion       = "8.0.29"
val cloudEventsVersion     = "2.3.0"
val vdrtoolsVer            = "0.8.5"
val vdrtoolsWrapperVer     = "0.8.5"

val cinnamonVer            = "2.17.0"

val scalaTestVersion       = "3.2.12"
val mockitoVer             = "1.17.7"
val h2Version              = "2.1.214"

DevEnvironmentTasks.init
Version.init(majorNum, minorNum, patchNum)
K8sTasks.init(vdrtoolsWrapperVer)
Lightbend.init
SharedLibrary.init

sharedLibraries :=  Seq(StandardLib("libvdrtools", vdrtoolsVer))

lightbendCinnamonVer := cinnamonVer

agentJars := lightbendCinnamonAgentJar.value
resolvers ++= lightbendResolvers.value


ThisBuild / scapegoatVersion := "1.4.14"

// Run in a separate JVM, to make sure sbt waits until all threads have
// finished before returning.
// If you want to keep the application running while executing other
// sbt tasks, consider https://github.com/spray/sbt-revolver/
fork := true

scalacOptions := Seq(
  "-target:11",
  "-feature",
  "-unchecked",
  "-deprecation",
  "-encoding",
  "utf8",
  "-Xfatal-warnings",
)

// This prevents problems with noexec on the system temporary directory
javaOptions += s"-Djna.tmpdir=${target.value.getAbsolutePath}"
// Adds shard libraries dir to JNA source path for libs like vdrtools
javaOptions += s"-Djna.library.path=${sharedLibrariesDir.value.getAbsolutePath}"

ThisBuild / Test / testOptions += Tests.Argument(
  TestFrameworks.ScalaTest,
  "-oDF",
  "-u", (target.value / "test-reports").toString
)  // summarized test output

libraryDependencies ++= Seq(
  "commons-codec"               % "commons-codec"             % "1.15",
  "com.typesafe.akka"          %% "akka-actor-typed"          % akkaVersion,
  "com.typesafe.akka"          %% "akka-stream"               % akkaVersion,
  "com.typesafe.akka"          %% "akka-stream-kafka"         % alpakkaKafka,
  "com.lightbend.akka"         %% "akka-stream-alpakka-s3"    % alpakkaStream,
  "com.lightbend.akka"         %% "akka-stream-alpakka-file"  % alpakkaStream,

  "com.typesafe.akka"          %% "akka-persistence-typed"     % akkaVersion,
  "com.lightbend.akka"         %% "akka-persistence-jdbc"      % akkaPersistenceJdbc,
  "com.typesafe.akka"          %% "akka-serialization-jackson" % akkaVersion,
  "com.fasterxml.jackson.core" %  "jackson-databind"           % jacksonDatabindVersion,
  "mysql"                      % "mysql-connector-java"        % mysqlJdbcVersion
    exclude("com.google.protobuf", "protobuf-java"),
  "com.typesafe.slick"         %% "slick"                      % slickVersion,
  "com.typesafe.slick"         %% "slick-hikaricp"             % slickVersion,

  "com.typesafe.akka"          %% "akka-cluster-typed"          % akkaVersion,
  "com.typesafe.akka"          %% "akka-cluster-sharding-typed" % akkaVersion,

  "com.lightbend.akka.management" %% "akka-management"                   % akkaMgtVer,
  "com.lightbend.akka.management" %% "akka-management-cluster-http"      % akkaMgtVer,
  "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap" % akkaMgtVer,
  "com.typesafe.akka"             %% "akka-discovery"                    % akkaVersion,
  "com.lightbend.akka.discovery"  %% "akka-discovery-kubernetes-api"     % akkaMgtVer,

  "com.typesafe.akka"          %% "akka-http"                % akkaHttpVersion,
  "com.typesafe.akka"          %% "akka-http-spray-json"     % akkaHttpVersion,
  "com.typesafe.akka"          %% "akka-http-jackson"        % akkaHttpVersion,
  "com.typesafe.akka"          %% "akka-http-xml"            % akkaHttpVersion,

  "ch.qos.logback"              % "logback-classic"          % logbackVersion,
  "ch.qos.logback.contrib"      % "logback-json-classic"     % logbackJacksonVersion,
  "ch.qos.logback.contrib"      % "logback-jackson"          % logbackJacksonVersion,
  "com.typesafe.scala-logging" %% "scala-logging"            % scalaLoggingVersion,

  "com.evernym.vdrtools"        % "vdr-tools"                % vdrtoolsWrapperVer,

  "io.cloudevents"              % "cloudevents-core"         % cloudEventsVersion,
  "io.cloudevents"              % "cloudevents-json-jackson" % cloudEventsVersion,

// TESTING Dependencies
  "com.typesafe.akka"          %% "akka-actor-testkit-typed" % akkaVersion      % Test,
  "com.typesafe.akka"          %% "akka-persistence-testkit" % akkaVersion      % Test,
  "com.typesafe.akka"          %% "akka-http-testkit"        % akkaHttpVersion  % Test,
  "org.mockito"                %% "mockito-scala-scalatest"  % mockitoVer       % Test,

  "org.scalatest"              %% "scalatest"                % scalaTestVersion % Test,

  "com.h2database"             % "h2"                        % h2Version        % Test,
)
libraryDependencies ++= lightbendDeps.value

 assembly / assemblyMergeStrategy := {
   case PathList(ps @ _*) if ps.last equals "module-info.class"      => MergeStrategy.concat
   case PathList("reference.conf")                                   => referenceConfMerge()
   case PathList("cinnamon-reference.conf")                          => MergeStrategy.concat
   case PathList("version.conf")                                     => MergeStrategy.concat
   case s                                                            => MergeStrategy.defaultMergeStrategy(s)
}

