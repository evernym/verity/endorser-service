The Endorser Service submits transactions to an underlying verifiable data registry (VDR). It is designed to be part of [Evernym Verity](https://gitlab.com/evernym/verity/verity/-/blob/main/CHANGELOG.md).

# Release notes - Endorser Service 0.1.0 - released TBD
Included in Verity 2.18.0
Includes VDR Tools 0.8.5

This is the initial release.

## Features
* [VE-1436] Automate Sovrin Network writes using an Evernym Endorser
  * Coordination with other services using Kafka / Confluent for event streaming
  * Publish logs to DataDog
  * Logs have adequate information for billing
  * Deployment with Terraform and Helm
  * Supports the MySQL wallet in VDR Tools
  * Host transactions on an external location (like S3)
  * Works with fully qualified DIDs
  * Support Key rotation commands
  * Supports Sovrin BuilderNet, StagingNet, and MainNet
