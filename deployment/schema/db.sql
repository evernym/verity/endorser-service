CREATE SCHEMA IF NOT EXISTS akka_persistence;
USE akka_persistence;

CREATE TABLE IF NOT EXISTS event_journal(
    ordering SERIAL,
    deleted BOOLEAN DEFAULT false NOT NULL,
    persistence_id VARCHAR(255) NOT NULL,
    sequence_number BIGINT NOT NULL,
    writer TEXT NOT NULL,
    write_timestamp BIGINT NOT NULL,
    adapter_manifest TEXT NOT NULL,
    event_payload BLOB NOT NULL,
    event_ser_id INTEGER NOT NULL,
    event_ser_manifest TEXT NOT NULL,
    meta_payload BLOB,
    meta_ser_id INTEGER,meta_ser_manifest TEXT,
    PRIMARY KEY(persistence_id,sequence_number)
);

CREATE TABLE IF NOT EXISTS event_tag (
    event_id BIGINT UNSIGNED NOT NULL,
    tag VARCHAR(255) NOT NULL,
    PRIMARY KEY(event_id, tag),
    FOREIGN KEY (event_id)
        REFERENCES event_journal(ordering)
        ON DELETE CASCADE
    );

CREATE TABLE IF NOT EXISTS snapshot (
    persistence_id VARCHAR(255) NOT NULL,
    sequence_number BIGINT NOT NULL,
    created BIGINT NOT NULL,
    snapshot_ser_id INTEGER NOT NULL,
    snapshot_ser_manifest TEXT NOT NULL,
    snapshot_payload BLOB NOT NULL,
    meta_ser_id INTEGER,
    meta_ser_manifest TEXT,
    meta_payload BLOB,
  PRIMARY KEY (persistence_id, sequence_number));

CREATE SCHEMA IF NOT EXISTS wallet;
USE wallet;

CREATE TABLE IF NOT EXISTS wallets (
	    id BIGINT(20) NOT NULL AUTO_INCREMENT,
	    name VARCHAR(1024) NOT NULL,
	    metadata VARCHAR(10240) NOT NULL,
	    PRIMARY KEY (id),
	    UNIQUE KEY wallet_name (name)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS items (
	    id BIGINT(20) NOT NULL AUTO_INCREMENT,
	    wallet_id BIGINT(20) NOT NULL,
	    type VARCHAR(128) NOT NULL,
	    name VARCHAR(1024) NOT NULL,
	    value LONGBLOB NOT NULL,
	    tags JSON NOT NULL,
	    PRIMARY KEY (id),
	    UNIQUE KEY ux_items_wallet_id_type_name (wallet_id, type, name),
	    CONSTRAINT fk_items_wallet_id FOREIGN KEY (wallet_id)
		REFERENCES wallets (id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;
