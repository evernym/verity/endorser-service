# # Creates AWS ACM SSL certs for Verity apps FQDNs
# module "aws-acm" {
#   source  = "terraform-aws-modules/acm/aws"
#   version = "3.4.1"

#   for_each = {
#     for k, v in var.install_apps_cfg : k => v if try(v.acm.domain_name != null, false)
#   }

#   providers                 = { aws = aws }
#   domain_name               = each.value.acm.domain_name
#   subject_alternative_names = try(each.value.acm.san, [])
#   zone_id                   = data.aws_route53_zone.app[each.key].zone_id
#   wait_for_validation       = true
#   tags = {
#     service = each.key
#   }
# }
