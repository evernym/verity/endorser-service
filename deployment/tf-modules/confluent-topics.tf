locals {
  kafka_topics = [
    "public.event.ssi.endorsement",
    "public.event.ssi.endorser",
    "public.event.ssi.publisher",
    "public.request.endorsement",
    "private.cmd.ssi.endorser"
  ]
}

resource "confluent_kafka_topic" "kafka_topics" {
  for_each = toset(local.kafka_topics)

  kafka_cluster {
    id = var.confluent_cloud_cluster_id
  }
  topic_name         = each.key
  partitions_count   = 6
  http_endpoint      = var.confluent_cloud_cluster_http_endpoint
  config = {
    "cleanup.policy"    = "delete"
    "max.message.bytes" = "2097164"
    "retention.ms"      = "-1"
  }
  credentials {
    key    = var.confluent_cloud_api_key
    secret = var.confluent_cloud_api_secret
  }
}
