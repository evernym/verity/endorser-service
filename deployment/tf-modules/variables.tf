variable "aws_shared_cred_files" {
  description = "AWS credentials file location"
  type        = string
}

variable "aws_profile" {
  description = "AWS profile that will be used to create EKS resources"
}

variable "aws_region" {
  description = "AWS region for aws_profile"
}

variable "environment_name" {
  description = "Name of EKS environment (e.g. team1). Mostly used to create unique AWS resource names"
  type        = string
}

variable "gitlab_environment_name" {
  description = "Name of EKS environment as seen by gitlab. Mostly used for access to vault secrets"
  type        = string
}

variable "owner_email" {
  type = string
}

variable "cidr_block" {
  description = "CIDR block of the EKS VPC"
  type        = string
}

variable "rds_allowed_cidrs" {
  description = "list of extra cidr blocks allowed to access RDS endpoints"
  type        = list(string)
  default     = []
}

variable "install_apps_cfg" {
  description = "Map of endorser apps to install with their config options"
  type        = any
}

variable "confluent_cloud_api_key" {
  description = "Kafka API key"
  sensitive = true
}

variable "confluent_cloud_api_secret" {
  description = "Kafka API secret"
  sensitive = true
}

variable "confluent_environment" {
  description = "Environment name used by Confluent"
  type = string
}

variable "confluent_cloud_cluster_id" {
  description = "Confluent Cluster ID"
  type = string
}

variable "confluent_cloud_cluster_http_endpoint" {
  description = "Confluent Cluster HTTP endpoint"
  type = string
}
