data "aws_route53_zone" "app" {
  for_each = {
    for k, v in var.install_apps_cfg : k => v if try(v.acm.zone_name != null, false)
  }
  name         = each.value.acm.zone_name # "pdev.evernym.com"
  private_zone = false
}

data "aws_vpc" "selected" {
  cidr_block = var.cidr_block
}

data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.selected.id]
  }

  tags = {
    Network = "private"
  }
}
