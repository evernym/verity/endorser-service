locals {
  vault_data = {
    infrastructure = {
      RDS_USER       = "root"
      RDS_PASSWORD   = try(module.rds_cluster["endorser_appdb"].cluster_master_password, null)
      RDS_READ_HOST  = try(coalesce(module.rds_cluster["endorser_appdb"].cluster_reader_endpoint, module.rds_cluster["endorser_appdb"].cluster_endpoint), null)
      RDS_WRITE_HOST = try(module.rds_cluster["endorser_appdb"].cluster_endpoint, null)
    }
    app-database = {
      ENDORSER__DB__S3__ACCESS_KEY_ID         = try(aws_iam_access_key.access_key["endorser"].id, null)
      ENDORSER__DB__S3__SECRET_ACCESS_KEY     = try(aws_iam_access_key.access_key["endorser"].secret, null)
      ENDORSER__DB__S3__BUCKET_NAME           = module.s3_bucket["endorser-transaction-storage"].s3_bucket_id
      ENDORSER__DB__S3__DEFAULT_REGION        = module.s3_bucket["endorser-transaction-storage"].s3_bucket_region
      ENDORSER__DB__MYSQL__USER           = "endorser"
      ENDORSER__DB__MYSQL__PASSWORD       = random_password.endorser_appdb_password.result
      ENDORSER__DB__MYSQL__READ_HOST      = try(coalesce(module.rds_cluster["endorser_appdb"].cluster_reader_endpoint, module.rds_cluster["endorser_appdb"].cluster_endpoint), null)
      ENDORSER__DB__MYSQL__WRITE_HOST     = try(module.rds_cluster["endorser_appdb"].cluster_endpoint, null)
      ENDORSER__SECRET__WALLET_KEY_SALT   = random_password.endorser_wallet_salt.result
    }
  }
}

resource "vault_generic_secret" "infrastructure" {
  for_each = local.vault_data

  path      = "kubernetes_secrets/eks-${var.gitlab_environment_name}/endorser/${each.key}"
  data_json = jsonencode(local.vault_data[each.key])
}

resource "random_password" "endorser_appdb_password" {
  length           = 30
  special          = true
  override_special = "%^_,.-"
}

resource "random_password" "endorser_wallet_salt" {
  length           = 128
  special          = false
}
