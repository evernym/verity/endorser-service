#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#RDS Cluster
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
locals {
  rds_databases = ["appdb"]

  create_rds_apps = {
    for pair in setproduct(keys(var.install_apps_cfg), local.rds_databases) :
    join("_", [pair[0], pair[1]]) => {
      app  = pair[0],
      type = pair[1]
    }
    if try(
      var.install_apps_cfg[pair[0]]["rds"][pair[1]]["use_existing"] == null,
      true
    )
  }
  #["endorser_appdb" = {app = cas, type = appdb}, "vas_appdb" = {app = vas, type = appdb}...]
  #also makes outputs available via 'module.rds_cluster["endorser_appdb"]'

  existing_rds_apps = {
    for pair in setproduct(keys(var.install_apps_cfg), local.rds_databases) :
    join("_", [pair[0], pair[1]]) => {
      app     = pair[0],
      type    = pair[1],
      db_name = var.install_apps_cfg[pair[0]]["rds"][pair[1]]["use_existing"]
    }
    if try(
      var.install_apps_cfg[pair[0]]["rds"][pair[1]]["use_existing"] != null,
      false
    )
  }

  rds_type = {
    appdb = {
      engine         = "aurora-mysql"
      engine_version = "5.7.mysql_aurora.2.07.1"
      engine_mode    = "serverless"
      instances      = {}
      scaling_configuration = {
        auto_pause     = false
        min_capacity   = 1
        max_capacity   = 16
        timeout_action = "RollbackCapacityChange"
      }
    }
  }
}

data "aws_db_instance" "existing" {
  for_each               = local.existing_rds_apps
  db_instance_identifier = each.value.db_name
}

module "rds_cluster" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "7.1.0"

  for_each = local.create_rds_apps

  name          = join("-", [each.value.app, "eks", replace(var.environment_name, "_", "-"), "rds", each.value.type, "cluster"]) #eg: endorser-eks-team1-rds-appdb-cluster
  database_name = each.value.type
  engine = try(
    var.install_apps_cfg[each.key]["rds"][each.value.type].engine, #eg: install_apps_cfg[cas][rds][appdb][engine]
    local.rds_type[each.value.type].engine
  )
  engine_version = try(
    var.install_apps_cfg[each.key]["rds"][each.value.type].engine_version,
    local.rds_type[each.value.type].engine_version
  )
  engine_mode = try(
    var.install_apps_cfg[each.key]["rds"][each.value.type].engine_mode,
    local.rds_type[each.value.type].engine_mode
  )
  instances = try(
    var.install_apps_cfg[each.key]["rds"][each.value.type].instances,
    local.rds_type[each.value.type].instances
  )
  scaling_configuration = try(
    var.install_apps_cfg[each.key]["rds"][each.value.type].scaling_configuration,
    local.rds_type[each.value.type].scaling_configuration
  )

  vpc_id              = data.aws_vpc.selected.id
  subnets             = data.aws_subnets.private.ids
  allowed_cidr_blocks = distinct(concat([var.cidr_block], var.rds_allowed_cidrs))
  storage_encrypted   = true

  tags = {
    service = each.value.app
  }
  #TODO: create_random_password
}
