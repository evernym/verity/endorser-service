locals {
  default_tags = {
    Creator     = "terraform",
    Environment = var.environment_name,
    env         = var.environment_name,
    Owner       = var.owner_email,
    Expiry      = "indefinite"
  }
}

provider "aws" {
  region                   = var.aws_region                          #"us-west-2"
  shared_credentials_files = [pathexpand(var.aws_shared_cred_files)] #"~/.aws/credentials"
  profile                  = var.aws_profile                         #"dev"

  default_tags {
    tags = local.default_tags
  }
}

provider "vault" {
  # uses VAULT_ADDR and VAULT_TOKEN environment variables
}
