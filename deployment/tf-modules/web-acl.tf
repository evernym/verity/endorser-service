# resource "aws_wafv2_web_acl" "k8s-endorser" {
#   name        = "k8s-endorser-${var.environment_name}-waf"
#   description = "Web Application Firewall for ${var.environment_name} for endroser micro service"
#   scope       = "REGIONAL"

#   default_action {
#     allow {}
#   }

#   rule {
#     name     = "CyberSecurityCloudInc-CyberSecurityCloud-HighSecurityOWASPSet-"
#     priority = 0

#     override_action {
#       none {}
#     }

#     statement {
#       managed_rule_group_statement {
#         name        = "CyberSecurityCloud-HighSecurityOWASPSet-"
#         vendor_name = "Cyber Security Cloud Inc."
#       }
#     }

#     visibility_config {
#       sampled_requests_enabled   = true
#       cloudwatch_metrics_enabled = true
#       metric_name                = "CyberSecurityCloudInc-CyberSecurityCloud-HighSecurityOWASPSet-"
#     }
#   }

#   visibility_config {
#     sampled_requests_enabled   = true
#     cloudwatch_metrics_enabled = true
#     metric_name                = "k8s-endorser-${var.environment_name}-waf"
#   }

#   tags = {
#     service = "waf-regional"
#   }
# }
