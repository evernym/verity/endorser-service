locals {
  s3_buckets = [
    "endorser-transaction-storage"
  ]

  iam_s3_arns = { for app, cfg in var.install_apps_cfg :
    app => flatten([
      for bucket in local.s3_buckets:
        [
          module.s3_bucket[bucket].s3_bucket_arn,
          "${module.s3_bucket[bucket].s3_bucket_arn}/*"
        ]
    ])
  }
}

module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"
  version = "3.1.1"

  for_each = toset(local.s3_buckets)

  bucket = join("-", [each.value, replace(var.environment_name, "_", "-")])
  acl    = "public-read"

  versioning = {
    enabled = false
  }

  attach_policy = true
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": "*",
        "Action": "s3:GetObject",
        "Resource": "arn:aws:s3:::${join("-", [each.value, replace(var.environment_name, "_", "-")])}/*"
      }
    ]
  })

  lifecycle_rule = [{
    id      = "Delete objects after 1 week"
    prefix  = ""
    enabled = true
    expiration = {
      days = 7
    }
  }]
}
