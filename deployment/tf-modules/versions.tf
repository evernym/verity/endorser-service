terraform {
  required_version = ">= 1.2.5"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.42.0"
    }
    confluent = {
      source  = "confluentinc/confluent"
      version = "0.9.0"
    }
  }
}
