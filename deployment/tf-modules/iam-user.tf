locals {
  create_iam_apps = toset([for k, v in var.install_apps_cfg : k if try(v.create_iam, true)])
}

resource "aws_iam_user" "iam_user" {
  for_each = local.create_iam_apps
  name     = join("-", ["eks", var.environment_name, each.key]) #eg: eks-team1-cas
  tags = {
    service = each.key
  }
}

resource "aws_iam_access_key" "access_key" {
  for_each = local.create_iam_apps
  user     = aws_iam_user.iam_user[each.key].name
}

resource "aws_iam_user_policy" "policy" {
  for_each = local.create_iam_apps

  name = join("-", ["eks", var.environment_name, each.key]) #eg: eks-team1-cas
  user = aws_iam_user.iam_user[each.key].name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = [
        "s3:*",
      ]
      Effect   = "Allow"
      Resource = local.iam_s3_arns[each.key]
      }]
  })
}
