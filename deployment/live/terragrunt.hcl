terraform {
  extra_arguments "custom_vars" {
    commands = [
      "apply",
      "plan",
      "destroy",
      "import",
      "push",
      "refresh"
    ]

    required_var_files = [
      "${get_parent_terragrunt_dir()}/terraform_defaults.tfvars",
      "${get_terragrunt_dir()}/vars.tfvars"
    ]
  }
}

inputs = {
  confluent_cloud_api_key = get_env("CONFLUENT_API_KEY", "")
  confluent_cloud_api_secret = get_env("CONFLUENT_API_SECRET", "")
  confluent_cloud_cluster_id = get_env("CONFLUENT_CLUSTER_ID", "")
  confluent_cloud_cluster_http_endpoint = get_env("CONFLUENT_CLUSTER_HTTP_ENDPOINT", "")
}
