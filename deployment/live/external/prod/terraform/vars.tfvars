#AWS variables
aws_shared_cred_files = "~/.aws/credentials"
aws_profile           = "agency"       #change
aws_region            = "eu-central-1" #change

environment_name = "prod" #change (used to construct unique AWS resource names)
confluent_environment = "Production" #"pretty" environment name used by confluent. one of 'Development', 'Staging', 'Demo', or 'Production'

install_apps_cfg = {
  "endorser" = {
  }
} #change

cidr_block = "10.128.16.0/22" #change

rds_allowed_cidrs = [
  #main cidr_block is already allowed
  "10.2.3.134/32", #Corp VPN
  "10.2.3.4/32",   #Bastion
]
