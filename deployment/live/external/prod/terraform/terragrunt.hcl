include {
  path = find_in_parent_folders()
}

remote_state {
  backend = "s3"
  disable_init = tobool(get_env("DISABLE_INIT", "false"))
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket  = "tf-endorser-prod-state-bucket"
    # Disable encryption until the pipeline owns state TODO fix after pipeline built
    #encrypt = true
    
    # TODO what it should be:
    # key     = "${path_relative_to_include()}/terraform.tfstate"
    # need to move this key to the correct location
    key     = "live/external/prod/terraform/terraform.tfstate"
    region  = "eu-central-1"
    profile = "agency" #TODO: This should stay like this for now - this is not read seems like?
  }
}
terraform {
  source = "${get_env("CI_PROJECT_DIR","../../../../..")}//deployment//tf-modules"
}
