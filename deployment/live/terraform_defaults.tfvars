# AWS credentials variables
aws_shared_cred_files = "~/.aws/credentials"
aws_profile            = "dev"
aws_region             = "us-west-2"

owner_email = "te@evernym.com"
