# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.14.0"
  constraints = "3.14.0"
  hashes = [
    "h1:qsXUGJtyWULbPJy6ZnCezOzbsaULhjV7sJTcTKpWuGQ=",
    "h1:xR1R5xT0jLXzRqqkHLh8fQ6MjHSwvw1qajFulebHHlY=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:25be08eb83e94820d72884a02684dd38b8a47544dc5afce1640ba25a94720c6f",
    "zh:2a0b9388a1b7c84afce36df990b4f23de50fb5672538333f46998d14319f213a",
    "zh:2bdf18cbd144326fc99cf507196ad21d099f0360c16786fb543d575d6770a139",
    "zh:487890bc773edb3e42831ce649eb627a8a21da0b8874ae068e204c6fb0e834a3",
    "zh:4e47b4430f9974050188cb09fb9ac4647eb473b04d50165277b290e5e742b6cf",
    "zh:63735a191a39b177eb4513cea9f4592971a017d5d3bf7942b39d955e960c819a",
    "zh:6f89bbc30eabc4ae79af0bda57d7d5c037e68c24130348780673662a4fa2bd10",
    "zh:7d42633babb29f8d86df0e03b1b8738a16f9cba61d3951aca02e32174a11299f",
    "zh:865d20d3e868ac8ece7fe4294eaca9c85945400e043b74003ef01606bcfb8092",
    "zh:a747a4b55f25982896939785acba21ac441c8eaf1b906f1bb1dc7388b4f5350b",
    "zh:b0b2217b2aa97a1aea44d1f58463699663c4bd8832621c36c37617cada8b1ff5",
    "zh:d48bfcf224c21756a35e150a1cb2cf699ba7ebffc575bf4dd2297e918e82da12",
    "zh:fa64065c7f37ce4c9052ba9c78db632e2b55806ca53a969febcaa17d210f9644",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.16.0"
  constraints = "4.16.0"
  hashes = [
    "h1:jmf/sbrsF1//4DG3QdPzbjCNsPUCp66g7ysuyj8QECc=",
    "h1:u1SMSE9I4DL7ODSNbNvNZbOpXm9w6GO34i82YHm/zL0=",
    "zh:0aa204fead7c431796386cc9e73ccda9a185f37e46d4b6475ff3f56ad4f15101",
    "zh:130396f5da1650f4d6949e67ec44e0f408a529b7f76c48701a7bf21ba6949bbc",
    "zh:271bfa95bc1332676a81d3f01ba1b5a188abf26df475ca9f25972c68935b6ee9",
    "zh:51bc600c6c00292c6cb00ca460c555fb2cafd11d5fe9c5dc7d4ce62ec71874f8",
    "zh:6ece49ba67e484777e1588a08b043c186aa896b5189b0a5056eb7838c566f63e",
    "zh:994402e0973b12f2266f2d3ad00f000b2e2f3ee6961631aeab32688c0c4e07fd",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9e9ef874b11dfb1960aa18e1254ee430142cb583f721a2ed44b608ddf652db57",
    "zh:bb1755cd1dd39e0caf98efb1ccb5b03323f77ba13b3f5531bfe28aded7750db0",
    "zh:e5e73ddc1e3d0c0311be90176152c07f0d27af377a95baab72c6f00622461f46",
    "zh:e7fd8313107ab7f63297b8440b0ccf08a7b56a329ae110ad9b6ef51959939a20",
    "zh:f095a3f10331b3a91527822a2a881a6714c2e40ee20a14b3c127340c540e37e5",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.11.0"
  hashes = [
    "h1:lSh/Q5vX73hHL80TtGn2Vrv1UYLzlIRjC+xaCijY4ew=",
    "h1:pJiAJwZKUaoAJ4x+3ONJkwEVkjrwGROCGFgj7noPO58=",
    "zh:143a19dd0ea3b07fc5e3d9231f3c2d01f92894385c98a67327de74c76c715843",
    "zh:1fc757d209e09c3cf7848e4274daa32408c07743698fbed10ee52a4a479b62b6",
    "zh:22dfebd0685749c51a8f765d51a1090a259778960ac1cd4f32021a325b2b9b72",
    "zh:3039b3b76e870cd8fc404cf75a29c66b171c6ba9b6182e131b6ae2ca648ec7c0",
    "zh:3af0a15562fcab4b5684b18802e0239371b2b8ff9197ed069ff4827f795a002b",
    "zh:50aaf20336d1296a73315adb66f7687f75bd5c6b1f93a894b95c75cc142810ec",
    "zh:682064fabff895ec351860b4fe0321290bbbb17c2a410b62c9bea0039400650e",
    "zh:70ac914d5830b3371a2679d8f77cc20c419a6e12925145afae6c977c8eb90934",
    "zh:710aa02cccf7b0f3fb50880d6d2a7a8b8c9435248666616844ba71f74648cddc",
    "zh:88e418118cd5afbdec4984944c7ab36950bf48e8d3e09e090232e55eecfb470b",
    "zh:9cef159377bf23fa331f8724fdc6ce27ad39a217a4bae6df3b1ca408fc643da6",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}
