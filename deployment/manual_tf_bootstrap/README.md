# Purpose
This exists as a place for the source of truth of the permissions needed by the pipeline. It is *NOT* a part of the pipeline, but expected to be run manually by a member of the TE team.

If a new permissions are needed for the pipeline do the following:
1. Create a new branch
1. Add IAM permissions to `tf-deploy-iam.tf`, commit to your branch and raise an MR for review by TE
1. TE will then review and if approved, execute terraform to apply the change for the new permissions

# Execution
The permissions are deployed by TE. The following pre-requistes need to be met first:
1. The terraform binary must be installed
1. You should have access to the IT AWS account, with credentials added as a profile under the 'it' name in your configs. See also [awscli_mfa_login.sh](https://gitlab.corp.evernym.com/te/Ops-tools/-/blob/master/awscli_mfa_login.sh), for an easy script that will allow set up your profiles while doing role assumption from our root account
1. You should have a Gitlab Personal Access token you can use so that terraform can update CICD vars in the project

The terraform uses a single bucket in the IT account (`tf-endorser-iam-manual-state-bucket`) with workspaces.

## The deployment helper script
There is a deployment script next to this readme to help called: `deploy_to.sh`

To run:
1. Change into the directory where the `deploy_to.sh` script is located: `cd deployment/manual_tf_bootstrap/`

The script takes the following positional arguments:
* terraform action - the action to pass to terraform. e.g: `plan`, `apply`, etc...
* aws account - Optional specific account/profile to run the terraform action against

### Examples

Execute a plan on all of the accounts:
```
./deploy_to.sh plan
```

Execute a plan on a specific account/profile:
```
./deploy_to.sh plan dev
```

Apply to a specific account:
```
./deploy_to.sh apply dev
```

## Manual execution
If for some reason you don't want to use the helper script, you can apply terraform manually

This is an example against the "dev" account
```
cd deployment/manual_tf_bootstrap/
read -s -p 'Enter your gitlab token: ' TF_VAR_gitlab_token
export TF_VAR_gitlab_token
terraform init
terraform workspace select devrc
TF_VAR_aws_account_name=dev \
TF_VAR_aws_region=us-west-2 \
TF_VAR_eks_cluster="eks-devrc" \
TF_VAR_environment_name="devrc" \
TF_VAR_iam_env_scope="development/devrc" \
terraform plan
```
