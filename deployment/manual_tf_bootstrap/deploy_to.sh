#!/bin/bash

TF_ACTION="$1"
SPECIFIC_ACCOUNT="$2"
# Format is:
#  aws_account_name    default_region    comma_list_env_names    comma_list_eks_cluster_name    comma_list_cicd_var_scope
AWS_ACCOUNT_PROFILES=(
    'dev us-west-2 devrc,team1 eks-devrc,eks-team1 development/devrc,development/team1'
    'it us-west-2 staging eks-it staging/staging'
    'ps us-west-2 demo eks-demo external/demo'
    'agency eu-central-1 prod eks-prod external/prod'
)

function usage {
    cat <<EOF
Usage: $(basename $0) <terraform action>
  <terraform action> should something like 'plan' or 'apply' etc...
EOF
}

if [ -z "$TF_VAR_gitlab_token" ] ; then
    read -s -p 'Environment variable "TF_VAR_gitlab_token" is not set, please enter it now, as it is required to set the CICD vars: ' TF_VAR_gitlab_token
    export TF_VAR_gitlab_token
fi

echo "Are you ready to execute the '$TF_ACTION' action with terraform?"
read -p "(press enter to continue, or CTRL+C to cancel)"

set -e
if [ ! -d '.terraform' ] ; then
    echo "Initializing terraform"
    terraform init
fi
for account in "${AWS_ACCOUNT_PROFILES[@]}" ; do
    account_split=( $account )
    PROFILE=${account_split[0]}
    DEF_REGION=${account_split[1]}
    ENVS="${account_split[2]}"
    ENVS=( ${ENVS//,/ } )
    EKS_CLUSTER="${account_split[3]}"
    EKS_CLUSTER=( ${EKS_CLUSTER//,/ } )
    CICD_VAR_SCOPE="${account_split[4]}"
    CICD_VAR_SCOPE=( ${CICD_VAR_SCOPE//,/ } )
    if [ ! -z "$SPECIFIC_ACCOUNT" ] ; then
        if [ "$PROFILE" != "$SPECIFIC_ACCOUNT" ] ; then
            echo "Skipping env: '$PROFILE' as a specific env was specified: '$SPECIFIC_ACCOUNT'"
            continue
        fi
    fi
    echo "======= Running terraform for account/workspace: $PROFILE ========"
    for idx in "${!ENVS[@]}" ; do
        env=${ENVS[$idx]}
        eks_cluster=${EKS_CLUSTER[$idx]}
        cicd_var_scope="${CICD_VAR_SCOPE[$idx]}"
        echo "=========== Deploying for Environment: ${env} on eks cluster: ${eks_cluster} cicd_var_scope: ${cicd_var_scope}  ==========="
        terraform workspace "select" "$env"
        TF_VAR_aws_account_name="$PROFILE" \
        TF_VAR_aws_region="$DEF_REGION" \
        TF_VAR_eks_cluster="$eks_cluster" \
        TF_VAR_environment_name="$env" \
        TF_VAR_iam_env_scope="$cicd_var_scope" \
        terraform "$TF_ACTION"
    done
done
echo "Switching back to 'dev' terraform workspace as a precaution"
terraform workspace "select" devrc
