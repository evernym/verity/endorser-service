###- Define needed variables -###
variable "aws_account_name" {
  type = string
  validation {
    condition     = contains(["dev", "it", "ps", "agency"], var.aws_account_name)
    error_message = "Allowed values for aws_account_name are \"dev\", \"it\", \"ps\" or \"agency\"."
  }
}

variable "aws_region" {
  type        = string
  description = "The AWS region to use when connecting to the AWS account"
}

variable "aws_shared_cred_files" {
  type        = string
  description = "The location for there the credentials file is at"
  default     = "~/.aws/credentials"
}

variable "eks_cluster" {
  description = "EKS cluster name to use for creating kubernetes resources"
  type        = string
  validation {
    condition     = contains(["eks-devrc", "eks-team1", "eks-it", "eks-demo", "eks-prod"], var.eks_cluster)
    error_message = "Allowed values for eks_cluster are \"eks-devrc\", \"eks-team1\", \"eks-it\", \"eks-demo\" or \"eks-prod\"."
  }
}

variable "environment_name" {
  description = "Environment name corresponding the deployment of the resources"
  type        = string
  validation {
    condition     = contains(["devrc", "team1", "staging", "demo", "prod"], var.environment_name)
    error_message = "Allowed values for environment_name are \"devrc\", \"team1\", \"staging\", \"demo\" or \"prod\"."
  }
}

variable "gitlab_token" {
  description = "gitlab.com api token for saving the env vars"
  type        = string
}

variable "iam_env_scope" {
  description = "When storing the IAM credentials into gitlab, this defines the scope of the gitlab cicd variable"
  type        = string
  default     = ""
}

locals {
  default_tags = {
    Creator = "terraform",
    Owner   = "te@evernym.com",
    Expiry  = "indefinite"
  }
}

data "aws_caller_identity" "current" {}

###- Main -###
terraform {
  required_version = ">= 0.13"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 4.16.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "= 3.14.0"
    }
  }
  backend "s3" {
    bucket = "tf-endorser-iam-manual-state-bucket"
    # Disable encryption until the pipeline owns state TODO fix after pipeline built
    encrypt                 = true
    skip_metadata_api_check = true
    shared_credentials_file = "~/.aws/credentials"
    key                     = "terraform.tfstate"
    workspace_key_prefix    = "iam_manual_tfstate"
    region                  = "us-west-2"
    profile                 = "it"
  }
}
###- Set up a provider for each aws account we care about -###
# [NOTE] providers have to be static resources, it would be awesome to use
# something like for_each providers to populate region and profile, but nope :-(
# See also: https://github.com/hashicorp/terraform/issues/24476
provider "aws" {
  region                   = var.aws_region
  shared_credentials_files = [pathexpand(var.aws_shared_cred_files)] #"~/.aws/credentials"
  profile                  = var.aws_account_name
  default_tags {
    tags = local.default_tags
  }
}

###- Create the IAM users -###
resource "aws_iam_user" "iam_user" {
  name = join("-", ["tf", var.environment_name, "endorser"]) #eg: tf-dev-endorser
  tags = {
    service = "endorser"
  }
}

resource "aws_iam_user" "iam_user_ro" {
  name = join("-", ["tf", var.environment_name, "endorser", "ro"]) #eg: tf-dev-endorser-ro
  tags = {
    service = "endorser"
  }
}

resource "aws_iam_user" "iam_cicd_ecr" {
  count = var.aws_account_name == "it" ? 1 : 0
  name  = "endorser_cicd"

  tags = {
    service = "endorser"
  }
}

###- Create the IAM credentials -###
resource "aws_iam_access_key" "access_key" {
  user = aws_iam_user.iam_user.name
}

resource "aws_iam_access_key" "access_key_ro" {
  user = aws_iam_user.iam_user_ro.name
}

###- Generate the IAM Policies -###
resource "aws_iam_policy" "rw_scoped_policy" {
  name        = join("-", ["tf", var.environment_name, "endorser", "scopedresources"]) #eg: tf-dev-endorser-scopedresources
  description = "Read write permissions for endorser cicd pipeline that are scoped to specific resources"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "eks:DescribeCluster",
          "iam:CreateUser",
          "iam:CreateAccessKey",
          "iam:DeleteUser",
          "iam:GetUser",
          "iam:GetUserPolicy",
          "iam:ListAccessKeys",
          "iam:ListGroupsForUser",
          "iam:PutUserPolicy",
          "iam:TagUser",
          "rds:AddTagsToResource",
          "rds:CreateDBCluster",
          "rds:CreateDBSubnetGroup",
          "rds:DeleteDBCluster",
          "rds:DeleteDBSubnetGroup",
          "rds:DescribeDBClusters",
          "rds:DescribeDBSubnetGroups",
          "rds:ListTagsForResource",
          "s3:*"
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:eks:${var.aws_region}:${data.aws_caller_identity.current.account_id}:cluster/${var.eks_cluster}",
          "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/eks-${var.environment_name}-endorser",
          "arn:aws:rds:${var.aws_region}:${data.aws_caller_identity.current.account_id}:cluster:endorser-eks-${var.environment_name}-rds-appdb-cluster",
          "arn:aws:rds:${var.aws_region}:${data.aws_caller_identity.current.account_id}:subgrp:endorser-eks-${var.environment_name}-rds-appdb-cluster",
          "arn:aws:s3:::endorser-transaction-storage-${var.environment_name}",
          "arn:aws:s3:::endorser-transaction-storage-${var.environment_name}/*",
          "arn:aws:s3:::tf-endorser-${var.environment_name}-state-bucket",
          "arn:aws:s3:::tf-endorser-${var.environment_name}-state-bucket/*"
        ]
      }
    ]
  })
}

resource "aws_iam_policy" "rw_allresources_policy" {
  name        = join("-", ["tf", var.environment_name, "endorser", "allresources"]) #eg: tf-dev-endorser-allresources
  description = "Read write permissions for endorser cicd pipeline that are NOT scoped to specific resources"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "acm:AddTagsToCertificate",
          "acm:DeleteCertificate",
          "acm:DescribeCertificate",
          "acm:ListTagsForCertificate",
          "acm:RequestCertificate",
          "ec2:AuthorizeSecurityGroupIngress",
          "ec2:CreateSecurityGroup",
          "ec2:CreateTags",
          "ec2:DeleteSecurityGroup",
          "ec2:DescribeAccountAttributes",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DescribeRouteTables",
          "ec2:DescribeSecurityGroups",
          "ec2:DescribeSubnets",
          "ec2:DescribeVpcAttribute",
          "ec2:DescribeVpcs",
          "ec2:RevokeSecurityGroupEgress",
          "route53:ChangeResourceRecordSets",
          "route53:GetChange",
          "route53:GetHostedZone",
          "route53:ListHostedZones",
          "route53:ListResourceRecordSets",
          "route53:ListTagsForResource",
          "s3:ListAllMyBuckets",
          "wafv2:CreateWebACL",
          "wafv2:DeleteWebACL",
          "wafv2:GetWebACL",
          "wafv2:ListTagsForResource",
          "wafv2:TagResource"
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_policy" "read_only" {
  name        = join("-", ["tf", var.environment_name, "endorser", "ro"]) #eg: tf-dev-endorser-ro
  description = "Read permissions for endorser cicd pipeline"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "acm:DescribeCertificate",
          "acm:ListTagsForCertificate",
          "ec2:DescribeAccountAttributes",
          "ec2:DescribeNetworkInterfaces",
          "ec2:DescribeRouteTables",
          "ec2:DescribeSecurityGroups",
          "ec2:DescribeSubnets",
          "ec2:DescribeVpcAttribute",
          "ec2:DescribeVpcs",
          "route53:GetChange",
          "route53:GetHostedZone",
          "route53:ListHostedZones",
          "route53:ListResourceRecordSets",
          "route53:ListTagsForResource",
          "s3:ListAllMyBuckets",
          "wafv2:GetWebACL",
          "wafv2:ListTagsForResource"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "s3:Get*",
          "s3:ListBucket"
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:s3:::endorser-transaction-storage-${var.environment_name}",
          "arn:aws:s3:::endorser-transaction-storage-${var.environment_name}/*",
          "arn:aws:s3:::tf-endorser-${var.environment_name}-state-bucket",
          "arn:aws:s3:::tf-endorser-${var.environment_name}-state-bucket/*"
        ]
      },
      {
        Action = [
          "eks:DescribeCluster"
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:eks:${var.aws_region}:${data.aws_caller_identity.current.account_id}:cluster/${var.eks_cluster}"
        ]
      },
      {
        Action = [
          "rds:DescribeDBClusters",
          "rds:DescribeDBSubnetGroups",
          "rds:ListTagsForResource"
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:rds:${var.aws_region}:${data.aws_caller_identity.current.account_id}:cluster:endorser-eks-${var.environment_name}-rds-appdb-cluster",
          "arn:aws:rds:${var.aws_region}:${data.aws_caller_identity.current.account_id}:subgrp:endorser-eks-${var.environment_name}-rds-appdb-cluster",
        ]
      },
      {
        Action = [
          "iam:GetUser",
          "iam:GetUserPolicy",
          "iam:ListAccessKeys"
        ]
        Effect = "Allow"
        Resource = [
          "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/eks-${var.environment_name}-endorser",
        ]
      }
    ]
  })
}

resource "aws_iam_policy" "endorser_cicd_ecr_policy" {
  count       = var.aws_account_name == "it" ? 1 : 0
  name        = "endorser_cicd_ecr"
  description = "ECR permissions for the Endorser CICD pipelines"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ecr:PutImageTagMutability",
          "ecr:StartImageScan",
          "ecr:DescribeImageReplicationStatus",
          "ecr:ListTagsForResource",
          "ecr:UploadLayerPart",
          "ecr:BatchDeleteImage",
          "ecr:ListImages",
          "ecr:DeleteRepository",
          "ecr:CompleteLayerUpload",
          "ecr:TagResource",
          "ecr:DescribeRepositories",
          "ecr:DeleteRepositoryPolicy",
          "ecr:BatchCheckLayerAvailability",
          "ecr:ReplicateImage",
          "ecr:GetLifecyclePolicy",
          "ecr:PutLifecyclePolicy",
          "ecr:DescribeImageScanFindings",
          "ecr:GetLifecyclePolicyPreview",
          "ecr:CreateRepository",
          "ecr:PutImageScanningConfiguration",
          "ecr:GetDownloadUrlForLayer",
          "ecr:DeleteLifecyclePolicy",
          "ecr:PutImage",
          "ecr:UntagResource",
          "ecr:BatchGetImage",
          "ecr:DescribeImages",
          "ecr:StartLifecyclePolicyPreview",
          "ecr:InitiateLayerUpload",
          "ecr:GetRepositoryPolicy"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:ecr:*:177276529591:repository/endorser"
      },
      {
        Action = [
          "ecr:GetRegistryPolicy",
          "ecr:DescribeRegistry",
          "ecr:GetAuthorizationToken",
          "ecr:DeleteRegistryPolicy",
          "ecr:PutRegistryPolicy",
          "ecr:PutReplicationConfiguration"
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

###- Attach policies to the IAM users -###
resource "aws_iam_user_policy_attachment" "rw_scoped" {
  user       = aws_iam_user.iam_user.name
  policy_arn = aws_iam_policy.rw_scoped_policy.arn
}

resource "aws_iam_user_policy_attachment" "rw_allresources" {
  user       = aws_iam_user.iam_user.name
  policy_arn = aws_iam_policy.rw_allresources_policy.arn
}

resource "aws_iam_user_policy_attachment" "ro_scoped" {
  user       = aws_iam_user.iam_user_ro.name
  policy_arn = aws_iam_policy.read_only.arn
}

resource "aws_iam_user_policy_attachment" "ecr_access" {
  count      = var.aws_account_name == "it" ? 1 : 0
  user       = aws_iam_user.iam_cicd_ecr[0].name
  policy_arn = aws_iam_policy.endorser_cicd_ecr_policy[0].arn
}

###- Update Gitlab CICD Vars -###
provider "gitlab" {
  token = var.gitlab_token
}

resource "gitlab_project_variable" "endorser_repo_cicd_iam" {
  project           = "33567140"
  variable_type     = "file"
  protected         = true
  environment_scope = var.iam_env_scope
  key               = "AWS_CREDENTIALS_FILE"
  value             = <<-EOF
  [${var.aws_account_name}]
  aws_access_key_id = ${aws_iam_access_key.access_key.id}
  aws_secret_access_key = ${aws_iam_access_key.access_key.secret}
  EOF
}
resource "gitlab_project_variable" "endorser_repo_cicd_iam_ro" {
  project           = "33567140"
  variable_type     = "file"
  protected         = false
  environment_scope = var.iam_env_scope
  key               = "AWS_CREDENTIALS_FILE_RO"
  value             = <<-EOF
  [${var.aws_account_name}]
  aws_access_key_id = ${aws_iam_access_key.access_key_ro.id}
  aws_secret_access_key = ${aws_iam_access_key.access_key_ro.secret}
  EOF
}

###- Create S3 bucket for pipeline tf state -###
resource "aws_s3_bucket" "tf_endorser_pipeline_state" {
  bucket = "tf-endorser-${var.environment_name}-state-bucket"
  tags = {
    service = "endorser"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "sse_enable" {
  bucket = aws_s3_bucket.tf_endorser_pipeline_state.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_acl" "pipeine_tfstate_bucket_acl" {
  bucket = aws_s3_bucket.tf_endorser_pipeline_state.id
  acl    = "private"
}

###- Needed kubernetes access -###
data "aws_eks_cluster" "cluster" {
  name = var.eks_cluster
}

data "aws_eks_cluster_auth" "cluster" {
  name = var.eks_cluster
}


provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

resource "kubernetes_namespace" "endorser" {
  metadata {
    name = "endorser-${var.environment_name}"
    labels = {
      "elbv2.k8s.aws/pod-readiness-gate-inject" = "enabled"
    }
  }
}

resource "kubernetes_secret" "vault-tls" {
  metadata {
    name      = "vault-tls-secret"
    namespace = "endorser-${var.environment_name}"
  }

  data = {
    "evernym-root-ca.crt" = file("${path.module}/assets/tls.crt")
  }

  type = "Opaque"

  depends_on = [kubernetes_namespace.endorser]
}

resource "kubernetes_role" "pod-reader-akka-k8s-bootstrap" {
  metadata {
    name      = "pod-reader-akka-k8s-bootstrap"
    namespace = "endorser-${var.environment_name}"
  }

  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
  }

  depends_on = [kubernetes_namespace.endorser]
}

resource "kubernetes_role_binding" "pod-reader-akka-k8s-bootstrap" {
  metadata {
    name      = "pod-reader-akka-k8s-bootstrap"
    namespace = "endorser-${var.environment_name}"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "pod-reader-akka-k8s-bootstrap"
  }

  subject {
    kind      = "User"
    name      = join(":", ["system:serviceaccount", "endorser-${var.environment_name}", "default"])
    api_group = "rbac.authorization.k8s.io"
    namespace = "endorser-${var.environment_name}"
  }

  depends_on = [kubernetes_namespace.endorser]
}
