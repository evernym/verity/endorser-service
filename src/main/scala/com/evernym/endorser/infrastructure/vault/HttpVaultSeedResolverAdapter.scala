package com.evernym.endorser.infrastructure.vault

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.HttpMethods.{GET, POST}
import akka.http.scaladsl.model.StatusCodes.{BadRequest, OK}
import akka.http.scaladsl.model.{HttpMethod, StatusCode}
import com.evernym.endorser.application.resolver.{Seed, VaultSeedResolverPort}
import com.evernym.endorser.infrastructure.vault.HttpVaultSeedResolverAdapter._
import com.evernym.endorser.util.JsonUtil

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.language.postfixOps
import scala.sys.process._
import scala.util.{Failure, Success, Try}



class UnresolvableSeedException(msg: String) extends Exception(msg)
class VaultResolveSecurityViolationException(msg: String) extends Exception(msg)
class VaultResolveNotFoundException(msg: String) extends Exception(msg)

class HttpVaultSeedResolverAdapter(configPath: String, actorSystem: ActorSystem[_]) extends VaultSeedResolverPort{
  private val config = actorSystem.settings.config.getConfig(configPath)

  val vaultEndpoint: String = config.getString("vault-address")

  implicit val system: ActorSystem[_] = actorSystem
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext

  override def resolve(token: String, expectedPath: String): Future[Seed] = {
    callApi(lookupApi(vaultEndpoint), token)
      .map(x => validateLookup(x._1, x._2, expectedPath))
      .flatMap { _ =>
        callApi(unwrapApi(vaultEndpoint), token, method = POST)
          .map(extractSeed)
      }
  }
}

object HttpVaultSeedResolverAdapter {
  def apply(configPath: String, actorSystem: ActorSystem[_]): HttpVaultSeedResolverAdapter =
    new HttpVaultSeedResolverAdapter(configPath, actorSystem)

  def lookupApi(vaultEndpoint: String): String = {
    s"$vaultEndpoint/v1/sys/wrapping/lookup"
  }

  def unwrapApi(vaultEndpoint: String): String = {
    s"$vaultEndpoint/v1/sys/wrapping/unwrap"
  }

  /*
  Using CURL because Java's default Java Secure Socket Extension does not work with
  Evernym self-sign certificates that Vault uses. So we use CURL which uses a different implementation that seem
  to work with the certificates that vault uses. This does mean that this resolver has a dependency to CURL.
   */
  def callApi(vaultEndpoint: String, token: String, method: HttpMethod = GET)
             (implicit executionContext: ExecutionContextExecutor)
  : Future[(StatusCode, String)] = {
    Future {
      Try {
        val cmd = s"""curl -s -w "%{http_code}" -H ""X-Vault-Token": $token" -X ${method.value} $vaultEndpoint"""
        val raw =  cmd !!
        val body = raw.trim.substring(0, raw.length-4)
        val status = StatusCode.int2StatusCode(raw.trim.substring(body.length).toInt)
        (status, raw)
      } match {
        case Success(value) => value
        case Failure(exception) => throw new UnresolvableSeedException(s"Call to CURL failed -- ${exception.getMessage}")
      }

    }
  }

//  def callApi(vaultEndpoint: String, token: String, method: HttpMethod = GET)
//             (implicit system: ActorSystem[_], executionContext: ExecutionContextExecutor)
//  : Future[(StatusCode, String)] = {
//    Http().singleRequest(
//      HttpRequest(
//        uri = vaultEndpoint,
//        headers = Seq(RawHeader("X-Vault-Token", token)),
//        method = method
//      ),
//    ).flatMap{resp =>
//      Unmarshal(resp.entity).to[String].map(text => (resp.status, text))
//    }
//  }

  def validateLookup(status: StatusCode, responseBody: String, expectedPath: String): Boolean = {
    status match {
      case OK =>
        JsonUtil
          .jsonExtractString(responseBody, "/data/creation_path")
          .map(_.contains(expectedPath))
          .getOrElse(throw new VaultResolveSecurityViolationException("Wrapped secret is not for expected path"))

      case BadRequest => handleBadRequest(responseBody)
      case _ => handleOtherRequest(status, responseBody)
    }
  }

  def extractSeed(resp: (StatusCode, String)): Seed = {
    val status = resp._1
    val body = resp._2
    resp._1 match {
      case OK =>
        extractSeedKvmV1(resp._2)
          .orElse(extractSeedKvmV2(resp._2))
          .getOrElse(throw new UnresolvableSeedException("Unable to parse unwrapped secret"))
      case BadRequest => handleBadRequest(body)
      case _ => handleOtherRequest(status, body)
    }
  }

  def extractSeedKvmV1(body: String): Option[Seed] = {
    JsonUtil
      .jsonExtractString(body, "/data/seed")
      .map(Seed)
  }

  def extractSeedKvmV2(body: String): Option[Seed] = {
    JsonUtil
      .jsonExtractString(body, "/data/data/seed")
      .map(Seed)
  }

  def handleOtherRequest(status: StatusCode, body: String): Nothing = {
    throw new UnresolvableSeedException(s"unable to resolve wrapped secret -- " +
      s"status code: $status -- response: $body")
  }

  def handleBadRequest(body: String): Nothing = {
    if (body.contains("wrapping token is not valid or does not exist")) {
      throw new VaultResolveSecurityViolationException("Cannot resolve wrapped secret")
    }
    else {
      throw new UnresolvableSeedException(s"unable to resolve wrapped secret -- " +
        s"status code: $BadRequest -- response: $body")
    }
  }
}