package com.evernym.endorser.infrastructure.resolver

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, ResponseEntity}
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.ports.{TransactionResolverPort, UnresolvableRefException}
import com.evernym.endorser.infrastructure.resolver.AkkaResolverAdapter.mapResponse

import java.io.IOException
import java.net.URI
import java.nio.file.{Files, Paths}
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Try


class AkkaResolverAdapter(configPath: String, actorSystem: ActorSystem[_]) extends TransactionResolverPort {
  private val config = actorSystem.settings.config.getConfig(configPath)

  implicit val system: ActorSystem[_] = actorSystem
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext

  def resolve(ref: TransactionRef): Future[String] = {
    if(ref.startsWith("http")) {
      resolveHttp(ref)
    }
    else if (ref.startsWith("file")) {
      resolveFile(ref)
    }
    else {
      Future.failed(new IOException(s"Unable to resolve ref '$ref' -- not supported ref type"))
    }
  }

  def resolveHttp(ref: TransactionRef): Future[String] = {
    Http().singleRequest(HttpRequest(uri = ref))
      .map(mapResponse(ref))
      .flatMap(Unmarshal(_).to[String])
  }

  def resolveFile(ref: TransactionRef): Future[String] = {
    Try(Paths.get(new URI(ref)))
      .map{path =>
        Future {
          Files.readString(path)
        }
      }.getOrElse(Future.failed(new IOException(s"Unable to read file '$ref'")))
  }
}

object AkkaResolverAdapter {
  def mapResponse(ref: TransactionRef)(resp: HttpResponse): ResponseEntity = {
    resp.status match {
      case OK => resp.entity
      case _ => throw new UnresolvableRefException(
        s"Reference response for '$ref' was not OK: ${resp.toString()}",
      )
    }
  }
}
