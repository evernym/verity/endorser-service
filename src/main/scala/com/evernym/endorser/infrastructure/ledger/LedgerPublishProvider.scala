package com.evernym.endorser.infrastructure.ledger

import akka.actor.typed.ActorSystem
import com.evernym.endorser.application.provider.PublishProvider
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.unknownVDR
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort
import com.evernym.endorser.domain.process.publisher.valueobjects.Status
import com.evernym.endorser.domain.process.publisher.valueobjects.Status.unknown
import com.evernym.endorser.infrastructure.ledger.LedgerPublishProvider.{providerAdapter, resolveLedgerConfig}
import com.evernym.endorser.util.LoggerUtil.logTryFailure
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import org.slf4j.event.Level

import scala.concurrent.Future
import scala.util.Try

class LedgerPublishProvider(configPath: String, actorSystem: ActorSystem[_]) extends PublishProvider {
  private val config = actorSystem.settings.config.getConfig(configPath)
  private val ledgerConfig = resolveLedgerConfig(actorSystem.settings.config)

  override def provide(ledgerPrefix: String): PublishTransactionPort =
    providerAdapter(ledgerPrefix, ledgerConfig, actorSystem)
}

object LedgerPublishProvider {
  val logger: Logger = Logger(LedgerPublishProvider.getClass)

  def resolveLedgerConfig(config: Config): Config = Try(
    config.getConfig("endorser.ledger")
  ).getOrElse(ConfigFactory.empty())

  def providerAdapter(ledgerPrefix: String,
                      ledgerConfig: Config,
                      actorSystem: ActorSystem[_]): PublishTransactionPort = {
    extractLedgerType(ledgerPrefix, ledgerConfig) match {
      case Some(LedgerType("indy", config)) =>
        IndyPublishTransactionAdapter(
          ledgerPrefix,
          config,
          actorSystem
        )
      case Some(LedgerType(typeStr, config)) =>
        logger.warn(s"Unknown ledger type configure -- '$typeStr' is not known")
        unknownVDRAdapter(ledgerPrefix)
      case _ => unknownVDRAdapter(ledgerPrefix)
    }
  }

  def apply(configPath: String, actorSystem: ActorSystem[_]): LedgerPublishProvider =
    new LedgerPublishProvider(configPath, actorSystem)

  def normalizeLedgerName(ledgerPrefix: String): String =
    Option(ledgerPrefix).getOrElse("").replace(":", "_")

  final case class LedgerType(typeString: String, config: Config)
  def extractLedgerType(ledgerPrefix: String, ledgerConfigs: Config): Option[LedgerType] = {
    val normalName = normalizeLedgerName(ledgerPrefix)
    Try(ledgerConfigs.getConfig(normalName))
      .recoverWith(
        logTryFailure(s"Configuration not found for '$ledgerPrefix'")(logger, level = Level.WARN)
      )
      .map { c => LedgerType(c.getString("type"), c)}
      .recoverWith(
        logTryFailure(s"Ledger type was not specified for '$ledgerPrefix'")(logger, level = Level.WARN)
      )
      .toOption
  }

  def unknownVDRAdapter(ledgerPrefix: String): PublishTransactionPort =
    // Unknown ledger Adaptor -- Always fails predicatively
    new PublishTransactionPort() {
      override def publishTransaction(txn: String): Future[Results.Result] = Future.successful(unknownVDR)
      override def connectionStatus(): Status = Status(ledger, unknown)
      override def ledger: Ledger = Ledger(ledgerPrefix)
    }
}
