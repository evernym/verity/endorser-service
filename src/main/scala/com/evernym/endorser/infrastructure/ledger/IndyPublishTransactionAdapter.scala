package com.evernym.endorser.infrastructure.ledger

import akka.actor.typed.ActorSystem
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.rejectedByVDRWithException
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort
import com.evernym.endorser.domain.process.publisher.valueobjects.Status
import com.evernym.endorser.domain.process.publisher.valueobjects.Status.{connected, disconnected}
import com.evernym.endorser.infrastructure.ledger.IndyPublishTransactionAdapter.{handlePublishException, openPool, responseToResult}
import com.evernym.endorser.infrastructure.ledger.LedgerPublishProvider.normalizeLedgerName
import com.evernym.endorser.util.ConfigUtil.default
import com.evernym.endorser.util.JsonUtil
import com.evernym.endorser.util.JsonUtil.simpleMapToJson
import com.evernym.endorser.util.LoggerUtil.{logFutureFailure, logTryFailure}
import com.evernym.endorser.util.TryUtils.getOrThrow
import com.evernym.vdrtools.IndyException
import com.evernym.vdrtools.ledger.{Ledger => IndyLedger}
import com.evernym.vdrtools.pool.Pool
import com.evernym.vdrtools.pool.PoolJSONParameters.CreatePoolLedgerConfigJSONParameter
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import org.slf4j.event.Level

import java.nio.file.Path
import java.util.concurrent.atomic.AtomicReference
import scala.compat.java8.FutureConverters.{toScala => toFuture}
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.chaining.scalaUtilChainingOps
import scala.util.{Failure, Success, Try}

final case class PusherException(msg: String, cause: Throwable) extends Exception(msg, cause)

class IndyPublishTransactionAdapter(ledgerPrefix: String, config: Config, actorSystem: ActorSystem[_])
  extends PublishTransactionPort {
  val logger: Logger = Logger(classOf[IndyPublishTransactionAdapter])

  val poolHandle: AtomicReference[Option[Pool]] = new AtomicReference[Option[Pool]](None)
  implicit val executionContext: ExecutionContext = actorSystem.executionContext

  private def open(): Future[Pool] = {
    openPool(ledgerPrefix, config, poolHandle.get())
      .andThen {
        case Success(handle) =>
          poolHandle.set(Some(handle))
        case Failure(e) =>
          logger.warn("connecting to ledger failed", e)
          poolHandle.get().foreach { handle =>
            Try(
              Await.result(
                toFuture(
                  handle.closePoolLedger()
                ).recoverWith(logFutureFailure("Failed to close pool")(logger)),
                5 seconds
              )
            ).recoverWith(logTryFailure("Awaiting for closing future failed")(logger))
          }
          poolHandle.set(None)
      }
  }

  override def publishTransaction(txn: String): Future[Results.Result] = {
    open()
      .flatMap { handle =>
        toFuture(
          IndyLedger.submitRequest(handle, txn)
        )
      }
      // Deeper consideration should be given to handling errors from Indy
//      .andThen{
//        case Success(_) =>
//        case Failure(e) =>
//          logger.warn("submitting txn failed", e)
//          poolHandle.set(None) // TODO should close pool before setting to none
//      }
      .flatMap(responseToResult(ledgerPrefix))
      .recover(handlePublishException(ledgerPrefix))
  }

  override def connectionStatus(): Status = poolHandle.get() match {
    case Some(p) => Status(ledger, connected)
    case None    => Status(ledger, disconnected)
  }

  override def ledger: Ledger = Ledger(ledgerPrefix)
}

object IndyPublishTransactionAdapter {
  val logger: Logger = Logger(IndyPublishTransactionAdapter.getClass)

  def apply(ledgerPrefix: String,
            config: Config,
            actorSystem: ActorSystem[_]): IndyPublishTransactionAdapter =
    new IndyPublishTransactionAdapter(ledgerPrefix, config, actorSystem)

  def openPool(ledgerPrefix: String,
               config: Config,
               currentPool: Option[Pool])
          (implicit executionContext: ExecutionContext): Future[Pool] = {
    currentPool match {
      case None =>
        val poolName = normalizeLedgerName(ledgerPrefix)
        logger.info(s"Using '$poolName' as pool name for '$ledgerPrefix'")

        createPool(poolName, ledgerPrefix, config)
        .flatMap{ _ =>
          toFuture(
            Pool.openPoolLedger(
              poolName,
              getOrThrow(buildLedgerConfig(ledgerPrefix, config))
            )
          )
        }
        .recoverWith(logFutureFailure(s"Opening connection to pool for '$ledgerPrefix' failed")(logger))
      case Some(p:Pool) => Future.successful(p)
    }
  }

  def resolveGenesis(ledgerPrefix: String, ledgerConfig: Config): Path = {
    Path.of(
      ledgerConfig
        .getString("genesis-txn-file")
    )
    .tap { path =>
      if(!path.toFile.isFile) logger.error(s"Genesis file path '$path' is not accessible file")
      logger.info(s"Using genesis file '$path' for '$ledgerPrefix")
    }
  }

  @SuppressWarnings(Array("VariableShadowing")) // not sure why it sees logger as a shadow variable
  def createPool(poolName: String, ledgerPrefix: String, config: Config)
                (implicit executionContext: ExecutionContext): Future[_] = {
    toFuture{
      Pool.deletePoolLedgerConfig(poolName)
    }
    .recoverWith(
      logFutureFailure("Unable to delete pool -- this is allowed to failed")(logger, level = Level.WARN)
    )
    .recoverWith{
      // Delete allowed to failed, most likely because there was nothing to delete
      case e: IndyException => Future.successful(())
    }
    .flatMap { _ =>
      val path: Path = resolveGenesis(ledgerPrefix, config)
      toFuture(Pool.createPoolLedgerConfig(
        poolName,
        new CreatePoolLedgerConfigJSONParameter(path.toAbsolutePath.toString).toJson
      ))
      .recoverWith(logFutureFailure("Unable to configure pool with vdrtools")(logger))
    }
  }
  @SuppressWarnings(Array("VariableShadowing")) // not sure why it sees logger as a shadow variable
  def buildLedgerConfig(ledgerPrefix: String, config: Config): Try[String] = {
    Try{
      config
    }
    .orElse{
      logger.warn("Building ledger config with out defined config (taking hardcoded defaults)")
      Try(ConfigFactory.empty())
    }
    .map { c =>
      Map(
        "timeout" -> default(20)(c.getInt("config.timeout")),
        "extended_timeout" -> default(60)(c.getInt("config.extended-timeout")),
        "conn_limit" -> default(20)(c.getInt("config.conn-limit")),
        "conn_active_timeout" -> default(20)(c.getInt("config.conn-active-timeout")),
      ).tap{ map =>
        logger.info(s"Using config for $ledgerPrefix -- $map")
      }
    }
    .map(simpleMapToJson)
    .recoverWith(logTryFailure("Unable to build Ledger Config")(logger))
  }

  def handlePublishException(ledgerPrefix: String): PartialFunction[Throwable, Results.Result] = {
    case e: IndyException =>
      logger.error(s"Unable to write to ledger -- for '$ledgerPrefix'", e)
      rejectedByVDRWithException(e)
  }

  def responseToResult(ledgerPrefix: String)(resp: String): Future[Results.Result] = {
    Future.successful{
      JsonUtil.jsonExtractString(resp, "/result/txnMetadata/seqNo") match {
        case Some(_) => Results.writtenToVDR
        case None    =>
          JsonUtil.jsonExtractString(resp, "/reason")
            .map{ reason =>
              logger.warn(s"Unable to publish transaction -- for '$ledgerPrefix' -- $reason")
              Results.rejectedByVDRWithException(new Exception(reason))
            }
            .getOrElse(Results.rejectedByVDR)
      }
    }
  }
}
