package com.evernym.endorser.infrastructure.crypto

import akka.Done
import akka.actor.typed.ActorSystem
import akka.cluster.sharding.ShardRegion.EntityId
import com.evernym.endorser.application.wallet.WalletConfig
import com.evernym.endorser.application.wallet.WalletUtil.{deriveWalletKey, deriveWalletKeySeed}
import com.evernym.endorser.domain.aggregate.endorser.ports.SignerPort
import com.evernym.endorser.infrastructure.crypto.IndySignerAdapter.extractAndTransformNymTxn
import com.evernym.endorser.domain.{DID, Finished}
import com.evernym.endorser.util.JsonUtil.jsonExtractString
import com.evernym.vdrtools.did.{Did, DidJSONParameters}
import com.evernym.vdrtools.ledger.Ledger
import com.evernym.vdrtools.wallet.{Wallet, WalletExistsException}
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger

import scala.collection.mutable
import scala.compat.java8.FutureConverters.CompletionStageOps
import scala.concurrent.duration.{Duration, SECONDS}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try

class IndySignerAdapter(system: ActorSystem[_], entityId: String, walletConfig: WalletConfig) extends SignerPort{
  val logger:Logger = Logger(classOf[IndySignerAdapter])
  implicit val executionContext: ExecutionContext = system.executionContext
  val config: Config = system.settings.config

  private val WALLET_CONFIG: String = walletConfig.buildConfig(entityId)
  private val WALLET_CREDENTIALS: String = walletConfig.buildCredentials(
    deriveWalletKey(
      deriveWalletKeySeed(
        config.getString("endorser.secret.wallet-key-salt"),
        entityId
      )
    ), "RAW")

  private var walletOption: Option[Wallet] = None

  override def prepareTxn(transaction: String, ledgerPrefix: String, endorserDid: Option[DID] = None): Future[String] = {
    // TODO: fill this in with logic to select between different transaction types
    getWallet().flatMap(result =>
      prepareIndyTxn(
        transaction,
        endorserDid
          .map(_.unqualifiedId)
          .getOrElse({
            logger.error(s"ERROR while preparing transaction: Endorser DID required for Indy transaction: $transaction, but no endorserDID was supplied.")
            ""
          }))
    )
  }

  private def prepareIndyTxn(transaction: String, endorserDid: String): Future[String] = {
    getWallet().flatMap(wallet =>
      if(jsonExtractString(transaction, "/operation/type").contains("1")){ // nym transaction type number is 1
        extractAndTransformNymTxn(
          transaction,
          endorserDid,
          wallet,
          logger,
        )
      }
      else {
        Ledger.multiSignRequest(wallet, endorserDid, transaction).toScala
      }
    )
  }

  override def newDID(seed: Option[String] = None): Future[String] = {
    getWallet().flatMap(wallet => {
      val didJSONParameter: String = new DidJSONParameters.CreateAndStoreMyDidJSONParameter(
        null,
        seed.orNull,
        null,
        null).toJson

      Did.createAndStoreMyDid(
        wallet,
        didJSONParameter)
        .toScala
        .map(r =>
          r.getDid
        )
    })
  }

  override def rotateKeyWithSeed(did: String, seed: String): Future[String] = {
    getWallet().flatMap { wallet =>

      val didJSONParameter: String = new DidJSONParameters.CreateAndStoreMyDidJSONParameter(
        null,
        seed,
        null,
        null).toJson

      Did.replaceKeysStart(
        wallet,
        did,
        didJSONParameter
      ).toScala
        .flatMap{verkey =>
          Did.replaceKeysApply(wallet, did).toScala.map(_ => verkey)
        }
    }
  }

  override def close(): Finished = {
    walletOption match {
      case Some(wallet) =>
        Await.result(wallet
          .closeWallet()
          .toScala
          .map(
            result => {
              walletOption = None
              Finished("WalletClosed")
            }
          ),
          Duration(3, SECONDS)
        )
      case empty => Finished("WalletClosed")
    }
  }

  //noinspection AccessorLikeMethodIsEmptyParen
  private def getWallet(): Future[Wallet] = {
    walletOption match {
      case Some(value) => Future.successful(value)
      case none =>
        Wallet.createWallet(
        WALLET_CONFIG,
        WALLET_CREDENTIALS
        )
        .toScala
        .recover{
          case e: WalletExistsException =>
            logger.info(s"ERROR while creating wallet: ${e.getMessage}")
            openWallet.flatMap(
              result => {
                walletOption = Option(result)
                Future.successful(Done)
              }
            )
          case e: Throwable =>
            logger.error(s"ERROR while creating wallet: ${e.getMessage}")
            Future.failed(e)
        }
        .flatMap( result =>
          openWallet.flatMap(
            wallet => {
              walletOption = Option(wallet)
              Future.successful(wallet)
            }
          )
        )
    }
  }

  private def openWallet: Future[Wallet] = {
    Wallet.openWallet(WALLET_CONFIG, WALLET_CREDENTIALS).toScala
  }
}

object IndySignerAdapter {
  def apply(system: ActorSystem[_],
            entityId: EntityId,
            walletConfig: WalletConfig): IndySignerAdapter = {
    new IndySignerAdapter(system, entityId, walletConfig)
  }

  def padLeftZeros(inputString: String, length: Int = 32): String = {
    if (inputString.length >= length) return inputString
    val sb = new mutable.StringBuilder
    while ( {
      sb.length < length - inputString.length
    }) sb.append('0')
    sb.append(inputString)
    sb.toString
  }

  def extractAndTransformNymTxn(transaction: String,
                                endorserDid: String,
                                wallet: Wallet,
                                logger: Logger)
                               (implicit executionContext: ExecutionContext): Future[String] = {
    val fut = for(
      targetDID <- jsonExtractString(transaction, "/operation/dest");
      verkey <- jsonExtractString(transaction, "/operation/verkey")
    ) yield Ledger.buildNymRequest(endorserDid, targetDID, verkey, null, null).toScala
      .flatMap{ txn =>
        val taaFuture = for (
          digest      <- jsonExtractString(transaction, "/taaAcceptance/taaDigest");
          mechanism   <- jsonExtractString(transaction, "/taaAcceptance/mechanism");
          time        <- jsonExtractString(transaction, "/taaAcceptance/time").flatMap{s => Try(s.toLong).toOption}
        ) yield Ledger.appendTxnAuthorAgreementAcceptanceToRequest(txn, null, null, digest, mechanism, time).toScala

        taaFuture
          .getOrElse{
            logger.warn("Unable to attach Transaction Author Agreement - " +
              "Transaction did not have a valid TAA attached")
            Future.successful(txn)
          }
          .flatMap{ txn =>
            Ledger.signRequest(wallet, endorserDid, txn)
              .toScala
          }
      }
    fut.getOrElse(Future.failed(new Exception("Unable to extract DID and/or Verkey")))
  }
}


