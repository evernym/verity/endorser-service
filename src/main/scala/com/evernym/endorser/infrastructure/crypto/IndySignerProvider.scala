package com.evernym.endorser.infrastructure.crypto

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.ShardRegion.EntityId
import com.evernym.endorser.application.provider.SignerProvider
import com.evernym.endorser.application.wallet.WalletConfig
import com.evernym.endorser.application.wallet.WalletUtil.buildWalletConfig
import com.evernym.endorser.domain.aggregate.endorser.ports.SignerPort

class IndySignerProvider(configPath: String, actorSystem: ActorSystem[_]) extends SignerProvider {
  private val config = actorSystem.settings.config.getConfig(configPath)
  private val walletConfig: WalletConfig = buildWalletConfig(config)

  override def provide(entityId: EntityId): SignerPort = {
    IndySignerAdapter(actorSystem, entityId, walletConfig)
  }
}

object IndySignerProvider {
  def apply(configPath: String, system: ActorSystem[_]): SignerProvider = {
    new IndySignerProvider(configPath, system)
  }
}