package com.evernym.endorser.infrastructure.rest.v1

import akka.http.javadsl.marshallers.jackson.Jackson
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.evernym.endorser.application.health.HealthPort
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import scala.language.implicitConversions

object V1Routes {
  private val jacksonMapper = new ObjectMapper()
  jacksonMapper.registerModule(DefaultScalaModule)

  implicit def caseClassMarshaller[T]: ToEntityMarshaller[T] =
    Jackson.marshaller[T](jacksonMapper)


  def userRoutes(health: HealthPort): Route = pathPrefix("v1") {
    pathPrefix("health") {
      get {
        complete(health.report)
      }
    }
  }
}
