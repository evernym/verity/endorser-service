package com.evernym.endorser.infrastructure.rest.v1

import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import com.evernym.endorser.application.health.HealthPort
import com.evernym.endorser.application.initialization.Startable
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger

import scala.concurrent.{ExecutionContext, Future}

class V1Service(health: HealthPort) extends Startable {
  val logger: Logger = Logger(this.getClass)

  override def start(config: Config)
                    (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {
    val interface = config.getString("endorser.http.interface")
    val port = config.getInt("endorser.http.port")

    Http()
      .newServerAt(interface, port)
      .bind(V1Routes.userRoutes(health))
      .map { _ =>
        s"Server online at http://$interface:$port/"
      }
  }

  override def name(): String = "V1 HTTP Routes"
}

object V1Service {
  def apply(health: HealthPort): V1Service = new V1Service(health)
}
