package com.evernym.endorser.infrastructure.observability.logs

import ch.qos.logback.classic
import ch.qos.logback.classic.turbo.TurboFilter
import ch.qos.logback.core.spi.FilterReply
import org.slf4j.Marker

class IgnoreLoggerFilter() extends TurboFilter {
  private val loggerNameContainsSet: Set[String] = Set("nativesqlx.query")//Set.empty[String]

  override def decide(marker: Marker,
                      logger: classic.Logger,
                      level: classic.Level,
                      format: String,
                      params: Array[AnyRef],
                      t: Throwable): FilterReply = {
    Option(logger) match {
      case Some(lgr) if loggerNameContainsSet.exists(lnc => lgr.getName.contains(lnc)) =>
        FilterReply.DENY
      case _ =>
        FilterReply.NEUTRAL
    }
  }
}
