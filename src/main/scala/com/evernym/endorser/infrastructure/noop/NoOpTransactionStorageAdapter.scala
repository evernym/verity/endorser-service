package com.evernym.endorser.infrastructure.noop

import akka.actor.typed.ActorSystem
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorser.ports.TransactionStoragePort
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future

class NoOpTransactionStorageAdapter(configPath: String, actorSystem: ActorSystem[_]) extends TransactionStoragePort{
  private val config = actorSystem.settings.config.getConfig(configPath)
  val logger:Logger = Logger(classOf[NoOpTransactionStorageAdapter])

  override def store(transactionStr: String): Future[TransactionRef] = {
    logger.info(s"NO Operation for host -- transactionStr: $transactionStr")
    Future.successful("https://pastebin.com/raw/c6Bue6L6")
  }
}
