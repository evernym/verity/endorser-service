package com.evernym.endorser.infrastructure.noop

import akka.actor.typed.ActorSystem
import com.evernym.endorser.application.resolver.{Seed, VaultSeedResolverPort}
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future

class NoOpVaultSeedResolverPort(configPath: String, actorSystem: ActorSystem[_]) extends VaultSeedResolverPort{
  private val config = actorSystem.settings.config.getConfig(configPath)

  val logger:Logger = Logger(classOf[NoOpVaultSeedResolverPort])

  override def resolve(token: String, expectedPath: String): Future[Seed] = {
    logger.info(s"No Operation resolve -- token: $token - expectedPath: $expectedPath")
    Future.successful(Seed("e3642918e84041fac1be6aaa45e79286252f731ce003bb55422f2c325d19fcf0"))
  }
}
