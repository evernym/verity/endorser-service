package com.evernym.endorser.infrastructure.noop

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.ShardRegion.EntityId
import com.evernym.endorser.domain.{DID, Finished}
import com.evernym.endorser.domain.aggregate.endorser.ports.SignerPort
import com.evernym.endorser.application.provider.SignerProvider
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future

class NoOpSignerProvider(configPath: String, actorSystem: ActorSystem[_]) extends SignerProvider {
  val logger:Logger = Logger(classOf[NoOpSignerProvider])
  private val config = actorSystem.settings.config.getConfig(configPath)

  override def provide(entityId: EntityId): SignerPort = new SignerPort {
    override def prepareTxn(transaction: String, ledgerPrefix: String, endorserDid: Option[DID] = None): Future[String] = Future.successful{
      logger.info(s"NO Operation for prepareTxn -- entityId: $entityId -- ledgerPrefix: $ledgerPrefix -- " +
        s"transaction: ${transaction.substring(0, 30)} -- endorserDid: $endorserDid")
      transaction
    }

    override def newDID(seed: Option[String] = None): Future[String] = Future.successful{
      logger.info(s"NO Operation for newDID -- entityId: $entityId -- return static 'did:sov:9kDbgfz2x1haHhVJHb8ksd'")
      "did:sov:9kDbgfz2x1haHhVJHb8ksd"
    }

    override def close(): Finished = {
      logger.info("NO Operation for close -- entityId: $entityId ")
      Finished("Done")
    }

    override def rotateKeyWithSeed(did: String, seed: String): Future[String] = Future.successful{
      logger.info(s"NO Operation for rotateKeyWithSeed -- did: $entityId seed: $seed -- return static 'GfWXLeCfU8Hg74V7ZjegdbYK3ACxPCUk1VwXjqZg7TvM'")
      "GfWXLeCfU8Hg74V7ZjegdbYK3ACxPCUk1VwXjqZg7TvM"
    }
  }
}
