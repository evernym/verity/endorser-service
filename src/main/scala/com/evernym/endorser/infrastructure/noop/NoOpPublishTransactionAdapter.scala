package com.evernym.endorser.infrastructure.noop

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort
import com.evernym.endorser.domain.process.publisher.valueobjects.Status
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future

object NoOpPublishTransactionAdapter extends PublishTransactionPort{
  val logger:Logger = Logger(NoOpPublishTransactionAdapter.getClass)

  override def publishTransaction(txn: String): Future[Results.Result] = {
    logger.info(s"NO Operation for publishTransaction -- txn: $txn")
    Future.successful(Results.writtenToVDR)
  }

  override def connectionStatus(): Status = {
    logger.info(s"NO Operation for connectionStatus")
    Status(ledger, Status.connected)
  }

  override def ledger: Ledger = {
    logger.info(s"NO Operation for ledger")
    Ledger("did:noop:")
  }
}
