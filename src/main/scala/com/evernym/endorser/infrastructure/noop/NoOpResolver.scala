package com.evernym.endorser.infrastructure.noop

import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.ports.TransactionResolverPort
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future

object NoOpResolver extends TransactionResolverPort {
  val logger:Logger = Logger(NoOpResolver.getClass)

  def resolve(ref: TransactionRef): Future[String] = {
    logger.info(s"No Operation resolving transaction ref -- ref: $ref")

    Future.successful("")
  }
}
