package com.evernym.endorser.infrastructure.noop

import akka.actor.typed.ActorSystem
import com.evernym.endorser.application.provider.PublishProvider
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort
import com.evernym.endorser.domain.process.publisher.valueobjects.Status
import com.typesafe.scalalogging.Logger

import scala.concurrent.Future

class NoOpPublishProvider(configPath: String, actorSystem: ActorSystem[_]) extends PublishProvider{
  private val config = actorSystem.settings.config.getConfig(configPath)
  val logger:Logger = Logger(classOf[NoOpPublishProvider])

  override def provide(ledgerPrefix: String): PublishTransactionPort = new PublishTransactionPort {


    override def publishTransaction(txn: String): Future[Results.Result] = {
      logger.info(s"NO Operation for publishTransaction -- txn: $txn")
      Future.successful(Results.writtenToVDR)
    }

    override def connectionStatus(): Status = {
      logger.info(s"NO Operation for connectionStatus")
      Status(ledger, Status.connected)
    }

    override def ledger: Ledger = {
      Ledger(ledgerPrefix)
    }
  }
}
