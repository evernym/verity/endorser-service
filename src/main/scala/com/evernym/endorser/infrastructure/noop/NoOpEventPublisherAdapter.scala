package com.evernym.endorser.infrastructure.noop

import com.evernym.endorser.domain.Finished
import com.evernym.endorser.domain.ports.EventPublisherPort
import com.typesafe.scalalogging.Logger

import java.net.URI
import scala.concurrent.Future

object NoOpEventPublisherAdapter extends EventPublisherPort {
  val logger:Logger = Logger(NoOpEventPublisherAdapter.getClass)

  override def publishEvent(topic: String, event: Array[Byte], eventSource: URI, eventType: String, eventId: String): Future[Finished] = {
    logger.info(s"NO Operation for produceEvent -- topic: $topic")
    Future.successful(Finished(eventType))
  }
}
