package com.evernym.endorser.infrastructure.host

import akka.actor.typed.ActorSystem
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorser.ports.TransactionStoragePort

import java.io.IOException
import java.nio.file.Files
import scala.concurrent.Future
import scala.util.Try

class LocalFileTransactionStorageAdapter(configPath: String, actorSystem: ActorSystem[_]) extends TransactionStoragePort {
  private val config = actorSystem.settings.config.getConfig(configPath)

  override def store(transactionStr: String): Future[TransactionRef] = {
    Try(Files.createTempFile("transaction", ".txt"))
      .map(path => Files.writeString(path.toAbsolutePath, transactionStr))
      .map(_.toUri)
      .map(_.toString)
      .map(Future.successful)
      .getOrElse(Future.failed(new IOException("Unable to write transaction to file system")))
  }
}
