package com.evernym.endorser.infrastructure.sharedlib

import com.typesafe.scalalogging.Logger

import java.nio.file.{Path, Paths}
import scala.annotation.tailrec

/*
Facilitates runtime modification of the JNA path

This is used mostly for development and CI/CD pipelines
NOT real application deployments
 */
object JnaPath {
  val logger: Logger = Logger(JnaPath.getClass)
  val sharedLibsDir = "shared-libs"

  // Paths to look for shared lib directories
  val searchPaths: Set[Option[String]] = Set(
    None, // project root directory
  )

  // If application is running in a build or development env (defined as SBT or IntelliJ
  // augment to include non-packaged managed shared libraries (ex libindy)
  def augmentJnaPath(propKey: String = "jna.library.path"): Unit = {
    augmentJnaWithPaths(findJnaPaths(), propKey)
  }

  def augmentJnaWithPaths(paths: Seq[String], propKey: String = "jna.library.path"): Unit = {
    if(isRecognizedCmd()){
      val currentJnaPath = sys.props.get(propKey)
      val filteredPaths = filterJnaPaths(
        Option(paths).getOrElse(Seq.empty),
        currentJnaPath
      )
      applyNewPaths(
        filteredPaths,
        currentJnaPath,
        propKey
      )

      applyTempDir()
    }
  }

  // Set of partial CMD that are recognized that the application
  // is running in SBT or IntelliJ
  val recognizedCmd: Set[String] = Set(
    "jetbrains.plugins.scala",
    "sbt-launch.jar",
    "sbt.ForkMain"
  )

  def isRecognizedCmd(propKey: String = "sun.java.command"): Boolean = {
    // linear search is ok because it only happens a couple of times
    // during startup over a very small set
    val rtn = sys
      .props
      .get(propKey)
      .flatMap(_.split("\\s").toList.headOption)
      .exists{ x =>
        recognizedCmd.exists(x.contains(_))
      }
    if(rtn) logger.info(s"Run command '${sys.props.get("sun.java.command")}' will cause jna path augmentation")
    else logger.debug(s"Run command '${sys.props.get("sun.java.command")}' will NOT cause jna path augmentation")
    rtn
  }

  def applyNewPaths(paths: Seq[String], currentJnaPath: Option[String], propKey: String = "jna.library.path"): Unit = {
    val pathsToAdd = Option(paths)
      .getOrElse(Seq.empty)
      .mkString(":")

    val newPropValue = currentJnaPath match {
      case None => pathsToAdd
      case Some("") => pathsToAdd
      case Some(p) => s"$pathsToAdd:$p"
    }
    sys.props += propKey -> newPropValue
  }

  def applyTempDir(propKey: String = "jna.tmpdir", cwdPropKey: String = "user.dir"): Unit = {
    sys.props
      .get(cwdPropKey)
      .map(Paths.get(_))
      .flatMap(findRootTargetDirectory)
      .foreach{ target =>
        sys.props += propKey -> target.toAbsolutePath.toString
      }
  }

  def filterJnaPaths(paths: Seq[String], currentJnaPath: Option[String]): Seq[String] = {
    val jnaPath = currentJnaPath.getOrElse("")
    Option(paths)
      .getOrElse(Seq.empty)
      .filterNot(jnaPath.contains(_))
  }

  def findJnaPaths(cwdPropKey: String = "user.dir"): Seq[String] = {
    sys.props
      .get(cwdPropKey)
      .map(Paths.get(_))
      .flatMap(findRootTargetDirectory)
      .map { target =>
        searchPaths
          .map(_.map(target.resolve).getOrElse(target))
          .map(_.resolve(s"$sharedLibsDir/libs"))
          .filter(_.toFile.isDirectory)
          .map(_.toAbsolutePath.toString)
          .toSeq
      }.getOrElse(Seq.empty)
  }

  def findRootTargetDirectory(path:Path): Option[Path] = {
    Option(path)
      .flatMap(searchTargetFirst)
      .flatMap(searchTargetLast)
      .map(_.resolve("target"))
  }

  @tailrec
  private def searchTargetFirst(path: Path): Option[Path] = {
    val searchPath = Option(path).map(_.normalize())
    searchPath match {
      case Some(p) if p.resolve("target").toFile.isDirectory => Some(p)
      case Some(p) if Option(p.getParent).isEmpty => None
      case Some(p) => searchTargetFirst(p.getParent)
      case None => None
    }
  }

  @tailrec
  private def searchTargetLast(path: Path): Option[Path] = {
    val searchPath = Option(path.normalize().getParent)
    searchPath match {
      case Some(p) =>
        if(p.resolve("target").toFile.isDirectory){
          searchTargetLast(p)
        }
        else {
          Some(path)
        }
      case None => None
    }
  }
}
