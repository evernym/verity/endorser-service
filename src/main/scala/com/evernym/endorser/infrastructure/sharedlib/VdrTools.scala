package com.evernym.endorser.infrastructure.sharedlib

import akka.actor.typed.ActorSystem
import com.evernym.endorser.application.initialization.Startable
import com.evernym.endorser.infrastructure.sharedlib.JnaPath.augmentJnaPath
import com.evernym.endorser.infrastructure.sharedlib.VdrTools.vdrToolsAvailable
import com.evernym.vdrtools.LibIndy
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger

import java.nio.file.Path
import scala.concurrent.{ExecutionContext, Future}

class VdrTools() extends Startable {
  val logger: Logger = Logger(this.getClass)

  override def start(config: Config)
                    (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {
    initialize()
  }

  // Catching Fatal Exception is intended here to allow processing of the exception by callers
  // since loading VDR Tools is not required in all contexts and therefore not really fatal.
  @SuppressWarnings(Array("CatchFatal"))
  def initialize()(implicit executionContext: ExecutionContext): Future[Boolean] = {
      Future {
        val libVdrToolsDir: Option[Path] = None

        augmentJnaPath()
        logger.info("Loading library")
        try {
          libVdrToolsDir
            .map(p =>
              LibIndy.init(p.toAbsolutePath.toString)
            )
            .orElse{
              Some(LibIndy.api) // Forces the static init() in the LibIndy.java class
            }
        }
        catch {
          case e:NoClassDefFoundError => throw new Exception("Unable to load VDR Tools " +
            "-- Temporary Directory must be executable", e)
          case e: LinkageError => throw new Exception("Unable to load VDR Tools", e)
        }

        vdrToolsAvailable
      }
  }

  override def name(): String = "VDR Tools"

}

object VdrTools {
  def apply(): VdrTools = new VdrTools()

  def vdrToolsAvailable: Boolean = Option(LibIndy.api).isDefined
}
