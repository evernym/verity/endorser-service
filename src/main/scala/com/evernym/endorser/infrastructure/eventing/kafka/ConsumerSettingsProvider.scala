package com.evernym.endorser.infrastructure.eventing.kafka

import akka.actor.typed.ActorSystem
import akka.kafka.{CommitterSettings, ConsumerSettings}
import org.apache.kafka.common.serialization.{ByteArrayDeserializer, StringDeserializer}


final class ConsumerSettingsProvider(actorSystem: ActorSystem[_]) {
  def kafkaConsumerSettings(): ConsumerSettings[String, Array[Byte]] = {
    ConsumerSettings(actorSystem, new StringDeserializer, new ByteArrayDeserializer)
  }

  def kafkaCommitterSettings(): CommitterSettings = {
    CommitterSettings(actorSystem)
  }
}

object ConsumerSettingsProvider {
  def apply(actorSystem: ActorSystem[_]): ConsumerSettingsProvider = {
    new ConsumerSettingsProvider(actorSystem)
  }
}
