package com.evernym.endorser.infrastructure.eventing.file

import akka.actor.typed.ActorSystem
import akka.stream.scaladsl.{FileIO, Source}
import akka.util.ByteString
import com.evernym.endorser.application.eventing.EventSink
import com.evernym.endorser.domain.Finished
import com.evernym.endorser.domain.ports.EventPublisherPort
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger

import java.net.URI
import java.nio.file.Path
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class FileEventSink(configPath: String, actorSystem: ActorSystem[_]) extends EventSink {
  val logger: Logger = Logger(classOf[FileEventSink])
  private val config = actorSystem.settings.config.getConfig(configPath)
  val root: Path = Path.of(config.getString("root-path"))
  implicit val sys: ActorSystem[_] = actorSystem
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext

  override def eventPublisherAdapter: EventPublisherPort =

    (topic: String, event: Array[Byte], eventSource: URI, eventType: String, eventId: String) => writeEvent(topic, event, eventType, eventId)

  def writeEvent(topic: String, event: Array[Byte], eventType: String, eventId: String): Future[Finished] = {
    val topicDir = root.resolve(topic)
    topicDir.toFile.mkdirs()

    Source.single(event)
      .map(ByteString(_))
      .runWith(
        FileIO.toPath(
          topicDir.resolve(s"$eventType.$eventId.evt")
        )
      )
      .map(_ => Finished(eventType))
  }

  override def start(c: Config)(implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[EventSink] =
    Future.successful(this)
}
