package com.evernym.endorser.infrastructure.eventing.file

import akka.actor.typed.ActorSystem
import com.evernym.endorser.application.eventing.{EventHandler, EventSource}
import com.typesafe.scalalogging.Logger

import java.nio.file.Path
import scala.concurrent.{ExecutionContext, Future}
import scala.util.chaining.scalaUtilChainingOps

class FileEventSource(configPath: String, actorSystem: ActorSystem[_]) extends EventSource {
  val logger: Logger = Logger(this.getClass)

  private val config = actorSystem.settings.config.getConfig(configPath)

  val root: Path = Path.of(config.getString("root-path"))

  override def start(eventProcessor: EventHandler)
                    (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {

    Future.successful{
      FileEventing(
        root,
        eventProcessor.topics
          .map(x => x ->
            { bytes: Array[Byte] =>
              eventProcessor.process(x, bytes)
            }
          )
          .pipe(Map.from(_))
      )
    }
  }
}