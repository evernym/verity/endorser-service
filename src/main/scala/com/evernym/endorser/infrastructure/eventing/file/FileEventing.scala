package com.evernym.endorser.infrastructure.eventing.file

import akka.stream.Materializer
import akka.stream.alpakka.file.DirectoryChange
import akka.stream.alpakka.file.scaladsl.DirectoryChangesSource
import akka.stream.scaladsl.FileIO

import java.nio.file.Path
import scala.concurrent.duration._
import scala.language.postfixOps

object FileEventing extends App {
  def apply(root:Path, handleTopics: Map[String, Array[Byte] => Unit])(implicit system: Materializer): Unit = {
    root.toFile.mkdirs()

    // Make topic directories
    handleTopics.foreach{ pair =>
      root.resolve(pair._1).toFile.mkdirs()
    }

    handleTopics
      .map{ handler =>
        val topic = handler._1
        DirectoryChangesSource(root.resolve(topic), 100 milliseconds, 1000)
//          .map{ t => println(t); t}
          .filter{
            case (path, DirectoryChange.Creation) =>
              path.getFileName.toString.endsWith(".evt")
            case _ => false
          }
          .filter (_._1.toFile.isFile)
          .map(_._1)
          .flatMapConcat(FileIO.fromPath(_).reduce((a, b) => a ++ b))
          .map ( _.toArray )
          .runForeach(handler._2)
      }
  }
}
