package com.evernym.endorser.infrastructure.eventing.kafka

import akka.actor.typed.ActorSystem
import akka.kafka.Subscriptions
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.kafka.scaladsl.{Committer, Consumer}
import akka.stream.scaladsl.Sink
import com.evernym.endorser.application.eventing.{EventHandler, EventSource}
import com.typesafe.scalalogging.Logger

import scala.concurrent.{ExecutionContext, Future}

class KafkaEventSource(configPath: String, actorSystem: ActorSystem[_]) extends EventSource{
  private val config = actorSystem.settings.config.getConfig(configPath)
  val logger: Logger = Logger(classOf[KafkaEventSource])

  val settingsProvider: ConsumerSettingsProvider = ConsumerSettingsProvider(actorSystem)

  var controller: Option[DrainingControl[_]] = None

  override def start(eventProcessor: EventHandler)
                    (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {

    val topics = Subscriptions.topics(eventProcessor.topics.toSet)

    controller = Option(
      Consumer.committableSource(
        settingsProvider.kafkaConsumerSettings().withGroupId(actorSys.name),
        topics
      )
      .map { committableMsg =>
          logger.debug(s"committable message received: $committableMsg")
          eventProcessor.process(committableMsg.record.topic(), committableMsg.record.value())
          committableMsg.committableOffset
      }
      .via(Committer.flow(settingsProvider.kafkaCommitterSettings()))
      .toMat(Sink.seq)(DrainingControl.apply)
      .run()
    )
    Future.successful(controller)
  }
}
