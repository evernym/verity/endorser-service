package com.evernym.endorser.infrastructure.eventing.kafka

import akka.actor.typed.ActorSystem
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.scaladsl.Source
import com.evernym.endorser.application.eventing.EventSink
import com.evernym.endorser.domain.Finished
import com.evernym.endorser.domain.ports.EventPublisherPort
import com.typesafe.config.Config
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

import java.net.URI
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

class KafkaEventSink(configPath: String, actorSystem: ActorSystem[_]) extends EventSink {
  private val config: Config = actorSystem.settings.config.getConfig(configPath)
  implicit val sys: ActorSystem[_] = actorSystem
  implicit val executionContext: ExecutionContextExecutor = actorSystem.executionContext

  val producerSettings: ProducerSettings[String, Array[Byte]] = ProducerSettings(actorSystem, new StringSerializer, new ByteArraySerializer)

  val settingsWithProducer: ProducerSettings[String, Array[Byte]] = producerSettings.withProducer(
    producerSettings.createKafkaProducer()
  )

  override def start(startConfig: Config)(implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[EventSink] = {
    Future.successful(this)
  }

  override def eventPublisherAdapter: EventPublisherPort =
    (topic: String, event: Array[Byte], eventSource: URI, eventType: String, eventId: String) => publishEvent(topic, event, eventSource, eventType)

  def publishEvent(topic: String, event: Array[Byte], eventSource: URI, eventType: String): Future[Finished] = {
    Source.single(new ProducerRecord[String, Array[Byte]](topic, eventSource.toASCIIString, event))
      .runWith(Producer.plainSink(settingsWithProducer)).map(_ => Finished(eventType))
  }
}
