package com.evernym.endorser.application

import akka.actor.typed.ActorSystem
import com.typesafe.config.Config

import scala.util.Try

package object discovery {

  /**
   * Looks up and loads a class defined in configuration. Expects there to be a configuration path that points to a
   * configuration object that defines a 'class' attribute with a fully qualified class name at the provided path.
   * The provided class must be constructed with only the config path and the implicit ActorSystem provided to
   * the contractor.
   * @param configPath path to the configuration that points to the configuration for the provider (see: discoverClassName)
   * @param as implicit ActorSystem to used to construct the class
   * @tparam T expected type
   * @return
   */
  @SuppressWarnings(Array("AsInstanceOf"))
  def discover[T](configPath: String)(implicit as: ActorSystem[_]): Try[T] = {
    discoverClassName(as.settings.config, configPath)
      .map{ config =>
        Class
          .forName(config._2)
          .getConstructor(classOf[String], classOf[ActorSystem[_]])
          .newInstance(config._1, as)
          .asInstanceOf[T]
      }
  }

  /**
   * Looks up class name that is define by provider config redirect.
   *
   * example:
   * test.giver = "test.impl.simple-giver"
   * test.impl.simple-giver = {
   *   class = com.example.giver.simple.ImplSimpleGiver
   * }
   *
   * @param config configuration to discover the provider class name
   * @param configPath path to the configuration value to points to configuration for provider. (MUST include class
   *                   attribute with fully qualified class name)
   * @return
   */
  def discoverClassName(config: Config, configPath: String): Try[(String, String)] = {
      redirectPath(config, configPath)
      .map { redirectPath =>
        (redirectPath, config.getConfig(redirectPath))
      }
      .map(pair => (pair._1, pair._2.getString("class")))
  }

  def redirectPath(config: Config, path: String): Try[String] = {
    Try(config.getString(path))
  }

}
