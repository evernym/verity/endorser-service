package com.evernym.endorser.application

package object health {

  final case class HealthReport(version: String) {
    override def toString: String = version
  }

  final case class Version(major: String, minor: String, patch: String, build: String) {
    override def toString: String = s"$major.$minor.$patch.$build"
  }


}
