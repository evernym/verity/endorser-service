package com.evernym.endorser.application.health

import com.evernym.endorser.application.health.BuiltVersion.version

class HealthApplicationAdapter extends HealthPort {
  override def report: HealthReport = HealthReport(version.toString)
}

object HealthApplicationAdapter {
  def apply(): HealthApplicationAdapter = new HealthApplicationAdapter()
}
