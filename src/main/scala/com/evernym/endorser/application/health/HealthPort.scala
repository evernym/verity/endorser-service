package com.evernym.endorser.application.health


trait HealthPort {
  def report: HealthReport
}