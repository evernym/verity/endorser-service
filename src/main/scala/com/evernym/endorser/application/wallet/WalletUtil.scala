package com.evernym.endorser.application.wallet

import com.evernym.endorser.util.ConfigUtil.default
import com.evernym.vdrtools.wallet.Wallet
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger
import org.apache.commons.codec.digest.DigestUtils

import scala.compat.java8.FutureConverters.CompletionStageOps
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, SECONDS}

object WalletUtil {

  val logger: Logger = Logger(WalletUtil.getClass)

  def buildWalletConfig(config: Config): WalletConfig = {
    if (config.getString("wallet-type") == "mysql") {
      val readHost = config.getString("wallet-db-storage.read-host-ip")
      val writeHost = config.getString("wallet-db-storage.write-host-ip")
      val port = config.getInt("wallet-db-storage.host-port")
      val userName = config.getString("wallet-db-storage.credentials-username")
      val password = config.getString("wallet-db-storage.credentials-password")
      val dbName = config.getString("wallet-db-storage.db-name")
      val connectionLimit = default(20)(config.getInt("wallet-db-storage.connection-limit"))
      new MySqlWalletConfig(
        readHost,
        writeHost,
        port,
        userName,
        password,
        dbName,
        Some(connectionLimit),
        Some(CacheSettings(10))
      )
    } else new DefaultWalletConfig
  }

  def deriveWalletKeySeed(salt: String, entityId: String): String = {
    if(salt.length < 64) {
      logger.error("ERROR wallet key secret length less than 64 bytes")
      throw new Exception("ERROR: wallet key secret length less than 64 bytes")
    }

    val hashedEntityId = DigestUtils.sha256(entityId)
    val hashedSalt = DigestUtils.sha256(salt)

    DigestUtils.sha256Hex(hashedSalt.concat(hashedEntityId))
  }

  def deriveWalletKey(seed: String): String = {
    Await.result(Wallet.generateWalletKey(s"{\"seed\":\"$seed\"}").toScala, Duration(2, SECONDS))
  }
}
