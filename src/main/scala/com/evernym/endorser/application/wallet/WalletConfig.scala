package com.evernym.endorser.application.wallet

import com.evernym.endorser.application.wallet.CacheSettings.defaultEntities
import com.evernym.endorser.util.JsonUtil.simpleObjectToJson

trait ToJson {
  def toJson: String
}

final case class StorageCredential(user: String, pass: String)
final case class WalletCredential(key: String,
                            storage_credentials: Option[StorageCredential],
                            key_derivation_method: String) extends ToJson {
  override def toJson: String = simpleObjectToJson(this)
}

final case class CacheSettings(size: Int, entities: List[String] = defaultEntities) extends ToJson{
  override def toJson: String = simpleObjectToJson(this)
}

object CacheSettings {
  private val defaultEntities: List[String] = List(
    "vdrtools::Did",
    "vdrtools::Key",
    "indy::Did",
    "indy::Key"
  )
}

trait WalletConfig {


  def buildConfig(id: String): String

  def buildCredentials(encKey: String, keyDerivationMethod: String): String
}

class DefaultWalletConfig extends WalletConfig {

  override def buildConfig(id: String): String = {
    simpleObjectToJson(
      Map(
        "id" -> id,
        "storage_type" -> "default"
      )
    )
  }

  override def buildCredentials(encKey: String, keyDerivationMethod: String): String = {
    WalletCredential(encKey, None, keyDerivationMethod).toJson
  }
}

class MySqlWalletConfig(readHost: String,
                        writeHost: String,
                        port: Int,
                        userName: String,
                        password: String,
                        dbName: String,
                        connectionLimit: Option[Int],
                        cacheSettings: Option[ToJson] = None) extends WalletConfig {

  val DEFAULT_CONNECTION_LIMIT = 1

  List(readHost, writeHost, port, userName, password, dbName).foreach { i =>
    require(i != null)
  }

  private def buildStorageConfig: Map[String, Any] = Map(
    "db_name" -> dbName,
    "read_host" -> readHost,
    "write_host" -> writeHost,
    "port" -> port,
    "connection_limit" -> connectionLimit.getOrElse(DEFAULT_CONNECTION_LIMIT)
  )

  override def buildConfig(id: String): String = {
    simpleObjectToJson(
      Map(
        "id" -> id,
        "storage_type" -> "mysql",
        "storage_config" -> buildStorageConfig
      ) ++ cacheSettings.map("cache" -> _)
    )
  }

  override def buildCredentials(encKey: String, keyDerivationMethod: String): String = {
    WalletCredential(
      encKey,
      Some(StorageCredential(userName, password)),
      keyDerivationMethod
    ).toJson
  }
}



