package com.evernym.endorser.application.resolver

import scala.concurrent.Future


final case class Seed(seed: String)

trait VaultSeedResolverPort {
  def resolve(token: String, expectedPath: String): Future[Seed]
}
