package com.evernym.endorser.application.endorser

import akka.NotUsed
import akka.actor.ClassicActorSystemProvider
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.HttpEntity.Empty.contentType
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorser.ports.TransactionStoragePort
import com.evernym.endorser.util.ConfigUtil.default
import akka.stream.Attributes
import akka.stream.alpakka.s3._
import akka.stream.alpakka.s3.scaladsl.S3
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.alpakka.s3.BucketAccess.{AccessGranted, NotExists}
import akka.util.ByteString
import com.typesafe.config.Config
import org.apache.commons.lang3.RandomStringUtils

import scala.concurrent.{ExecutionContext, Future}

class S3TransactionStorageAdapter(configPath: String, system: ActorSystem[_]) extends TransactionStoragePort{
  private val configFromPath = system.settings.config.getConfig(configPath)
  private val config = system.settings.config

  implicit val classicActorSystemProvider: ClassicActorSystemProvider = system.classicSystem
  implicit val ec: ExecutionContext = system.executionContext

  lazy val s3Settings: S3Settings = S3Settings(config.getConfig("alpakka.s3"))
  lazy val s3Attrs: Attributes = S3Attributes.settings(s3Settings)

  lazy val bucketName: String = default("")(config.getString("endorser.db.s3.bucket-name"))
  lazy val idLength: Int = default(16)(config.getInt("endorser.db.s3.id-length"))
  
  override def store(transactionStr: String): Future[TransactionRef] = {
    val file: Source[ByteString, NotUsed] = Source.single(ByteString(transactionStr))

    val id = RandomStringUtils.randomAlphanumeric(idLength)

    val s3Sink: Sink[ByteString, Future[MultipartUploadResult]] =
      S3 multipartUpload(bucketName, id, contentType=contentType) withAttributes s3Attrs

    file.runWith(s3Sink).map(x => x.location.toString())
  }
}
