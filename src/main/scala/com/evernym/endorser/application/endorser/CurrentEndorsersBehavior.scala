package com.evernym.endorser.application.endorser

import akka.actor.typed.scaladsl.{ActorContext, Behaviors}
import akka.actor.typed.{ActorSystem, Behavior, PostStop}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, EntityRef}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior}
import com.evernym.endorser.application.endorser.Commands.{AddActiveEndorser, RemoveInactiveEndorser, RouteToActiveEndorser}
import com.evernym.endorser.application.endorser.CurrentEndorsersBehavior.{EndorserId, Source}
import com.evernym.endorser.application.endorser.Events.{EndorserAdded, EndorserRemoved}
import com.evernym.endorser.application.serialization.CborSerializable
import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.aggregate.endorsement.EndorsementBehavior.EndorsementId
import com.evernym.endorser.domain.aggregate.endorser.Commands.{EndorserCommand, PrepareTransaction}
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior.entityKey
import com.evernym.endorser.domain.ledger.Ledger

trait Router {
  def sendToEndorser(cmd: RouteToActiveEndorser, endorserId: EndorserId)(implicit system: ActorSystem[_]): Unit
}
object Router extends Router {
  override def sendToEndorser(cmd: RouteToActiveEndorser,
                              endorserId: EndorserId)
                             (implicit system: ActorSystem[_]): Unit = {
    val sharding: ClusterSharding = ClusterSharding(system)
    val ref: EntityRef[EndorserCommand] = sharding.entityRefFor(entityKey, endorserId)
    ref ! PrepareTransaction(
      cmd.txn,
      cmd.ledger.ledgerPrefix,
      cmd.endorsementId,
      cmd.requestSource
    )
  }
}

sealed trait Commands extends CborSerializable
object Commands {
  final case class AddActiveEndorser(endorserId: EndorserId, ledger: Ledger, endorserDID: Option[DID]) extends Commands
  final case class RemoveInactiveEndorser(endorserId: EndorserId) extends Commands
  final case class RouteToActiveEndorser(endorsementId: EndorsementId, requestSource: Option[Source], txn: String, ledger: Ledger) extends Commands
}

final case class CurrentEndorsers(endorsers: Map[String, EndorserId]) extends CborSerializable

sealed trait Events extends CborSerializable
object Events {
  final case class EndorserAdded(endorserId: EndorserId, ledger: Ledger, endorserDID: Option[DID]) extends Events
  final case class EndorserRemoved(endorserId: EndorserId) extends Events
}

object CurrentEndorsersBehavior {
  type EndorserId = String
  type Source = String

  def apply(persistenceId: PersistenceId, router: Router = Router): Behavior[Commands] = Behaviors.setup{ ctx =>
    EventSourcedBehavior.apply(
      persistenceId,
      CurrentEndorsers(Map.empty),
      commandHandler(ctx, router),
      eventHandler
    )
  }

  def commandHandler(ctx: ActorContext[Commands],
                     router: Router)
                    (state: CurrentEndorsers, cmd: Commands): Effect[Events, CurrentEndorsers]  = {
    implicit val system: ActorSystem[_] = ctx.system
    (state, cmd) match {
      case (_: CurrentEndorsers, cmd: AddActiveEndorser) =>
        Effect.persist(
          EndorserAdded(cmd.endorserId, cmd.ledger, cmd.endorserDID)
        )
      case (_: CurrentEndorsers, cmd: RemoveInactiveEndorser) =>
        Effect.persist(
          EndorserRemoved(cmd.endorserId)
        )
      case (_: CurrentEndorsers, cmd: RouteToActiveEndorser) =>
        Effect.none
          .thenRun { s =>
            findEndorser(s, cmd.ledger)
              .map(router.sendToEndorser(cmd,_))
          }
      case _ => Effect.none
    }
  }


  def eventHandler(state: CurrentEndorsers, event: Events): CurrentEndorsers =
    event match {
      case add: EndorserAdded =>
        state.copy(endorsers = state.endorsers + (add.ledger.ledgerPrefix -> add.endorserId))
      case remove: EndorserRemoved =>
        state.copy(
          endorsers = state.endorsers
            --
            state
              .endorsers
              .filter(_._2 == remove.endorserId)
              .keys
        )
    }

  private def findEndorser(state: CurrentEndorsers, ledger: Ledger): Option[EndorserId] = {
    state
      .endorsers
      .get(ledger.ledgerPrefix)
  }
}
