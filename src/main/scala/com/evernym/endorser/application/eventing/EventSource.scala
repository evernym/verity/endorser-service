package com.evernym.endorser.application.eventing

import akka.actor.typed.ActorSystem

import scala.concurrent.{ExecutionContext, Future}

trait EventHandler {
  def process(topic: String, eventBytes: Array[Byte]): Unit
  def topics: Seq[String]
}

trait EventSource {
  def start(eventProcessor: EventHandler)
           (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any]
}
