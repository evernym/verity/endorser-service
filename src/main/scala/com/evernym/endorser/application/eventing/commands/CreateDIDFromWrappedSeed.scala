package com.evernym.endorser.application.eventing.commands

import com.evernym.endorser.application.endorser.CurrentEndorsersBehavior.EndorserId
import com.evernym.endorser.domain.events.Event
import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object CreateDIDFromWrappedSeed extends Event {
  override val eventType: EventType = "endorser.create-endorser-did.v1"
  val eventTopic: EventTopic = endorserCommandTopic

  final case class DTO(endorserId: EndorserId, ledgerPrefix: String, token: String, expectedPath: String)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}


