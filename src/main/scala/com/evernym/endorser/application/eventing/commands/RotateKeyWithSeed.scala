package com.evernym.endorser.application.eventing.commands

import com.evernym.endorser.application.endorser.CurrentEndorsersBehavior.EndorserId
import com.evernym.endorser.domain.events.Event
import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object RotateKeyWithSeed extends Event {
  override val eventType: EventType = "endorser.rotate-key-with-seed.v1"
  val eventTopic: EventTopic = endorserCommandTopic

  final case class DTO(endorserId: EndorserId, token: String, expectedPath: String)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}


