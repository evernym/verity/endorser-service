package com.evernym.endorser.application.eventing

import com.evernym.endorser.application.adapter.Adapters
import com.evernym.endorser.application.eventing.ApplicationEventHandler.{logUnknownEventType, logUnknownTopic, logger}
import com.evernym.endorser.application.eventing.commands.{CreateDIDFromWrappedSeed, RotateKeyWithSeed, endorserCommandTopic}
import com.evernym.endorser.application.port.{EndorsementPort, EndorserPort, PublisherPort}
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.events._
import com.evernym.endorser.domain.events.endorsement.EndorsementTransactionValidated
import com.evernym.endorser.domain.events.endorser.TransactionPrepared
import com.evernym.endorser.domain.events.publisher.{PublisherTransactionWritten, PublisherWriteFailed}
import com.evernym.endorser.domain.events.request.RequestEndorsement
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.util.LoggerUtil.logTryFailure
import com.typesafe.scalalogging.Logger
import io.cloudevents.CloudEvent
import org.slf4j.event.Level

import scala.util.Try

class ApplicationEventHandler(adapters: Adapters) extends EventHandler {
  private val endorsementPort: EndorsementPort = adapters.endorsement
  private val endorserPort: EndorserPort = adapters.endorser
  private val publisherPort: PublisherPort = adapters.publisher


  override def topics: Seq[String] = Seq(
    requestEndorsementTopic,
    endorsementTopic,
    endorserTopic,
    publisherTopic,
    endorserCommandTopic
  )

  override def process(topic: String, eventBytes: Array[Byte]): Unit = {
    deserializeEvent(topic, eventBytes)
      .foreach{ event =>
        topic match {
          case t if t == requestEndorsementTopic => processRequestEndorsementTopic(t, event, event.getType)
          case t if t == endorsementTopic => processEndorsementTopic(t, event, event.getType)
          case t if t == endorserTopic => processEndorserTopic(t, event, event.getType)
          case t if t == publisherTopic => processPublisherTopic(t, event, event.getType)
          case t if t == endorserCommandTopic => processCommandTopic(t, event, event.getType)
          case t => logUnknownTopic(t, event)
        }
      }
  }

  def processCommandTopic(topic: String, event: CloudEvent, eventType: String): Unit = {
    eventType match {
      case CreateDIDFromWrappedSeed.eventType =>
        CreateDIDFromWrappedSeed.deserializeData(event)
          .foreach{ data =>
            endorserPort.createEndorserDIDWithSeed(
              data.endorserId,
              Ledger(data.ledgerPrefix),
              data.token,
              data.expectedPath
            )
          }
      case RotateKeyWithSeed.eventType =>
        RotateKeyWithSeed.deserializeData(event)
          .foreach{ data =>
            endorserPort.rotateKeyWithSeed(
              data.endorserId,
              data.token,
              data.expectedPath
            )
          }
      case _ => logUnknownEventType(topic, event)
    }
  }

  def processRequestEndorsementTopic(topic: String, event: CloudEvent, eventType: String): Unit= {
    eventType match {
      case RequestEndorsement.eventType =>
        RequestEndorsement.deserializeData(event)
          .foreach{ data =>
            endorsementPort.requestEndorsement(
              data.txnRef,
              data.ledgerPrefix,
              Some(event.getSource).map(_.toString))
          }
      case _ => logUnknownEventType(topic, event)
    }
  }

  def processEndorsementTopic(topic: String, event: CloudEvent, eventType: String): Unit= {
    eventType match {
      case EndorsementTransactionValidated.eventType =>
        EndorsementTransactionValidated.deserializeData(event)
          .foreach{ data =>
            adapters.endorserMap.get(data.ledgerPrefix)
              // Send to an uninitialized endorser if we can find an endorser for this endorsement
              .orElse{
                logger.info(s"No known endorser for '${data.ledgerPrefix}' prefix")
                Some("UNKNOWN_ENDORSER")
              }
              .foreach { endorserId =>
                endorserPort.prepareTransaction(
                  endorserId,
                  data.txnRef,
                  data.ledgerPrefix,
                  data.endorsementId,
                  data.requestSource
                )
              }
          }
      case _ => logUnknownEventType(topic, event)
    }
  }

  def processEndorserTopic(topic: String, event: CloudEvent, eventType: String): Unit= {
    eventType match {
      case TransactionPrepared.eventType =>
        TransactionPrepared.deserializeData(event)
          .foreach{data =>
            endorsementPort.holdPreparedTransaction(data.endorsementId, data.txnRef)
            publisherPort.submitTransaction(Ledger(data.ledgerPrefix), data.endorsementId, data.txnRef)
          }
      case _ => logUnknownEventType(topic, event)
    }
  }

  def processPublisherTopic(topic: String, event: CloudEvent, eventType: String): Unit= {
    eventType match {
      case PublisherTransactionWritten.eventType =>
        PublisherTransactionWritten.deserializeData(event)
          .foreach{ data =>
            endorsementPort.completeEndorsement(data.endorsementId, Results.fromEventCode(data.result))
          }
      case PublisherWriteFailed.eventType =>
        PublisherWriteFailed.deserializeData(event)
          .foreach{ data =>
            endorsementPort.completeEndorsement(data.endorsementId, Results.fromEventCode(data.result))
          }
      case _ => logUnknownEventType(topic, event)
    }
  }

  def deserializeEvent(topic: String, eventBytes: Array[Byte]): Try[CloudEvent]  = {
    Event.deserializeEvent(eventBytes)
      .recoverWith(
        logTryFailure(s"Unhandled event on topic '$topic' because it was un-deserializable")
                     (logger, level = Level.WARN)
      )
  }
}

object ApplicationEventHandler {

  def apply(adapters: Adapters): ApplicationEventHandler = new ApplicationEventHandler(adapters)


  val logger: Logger = Logger(classOf[ApplicationEventHandler])

  def logUnknownTopic(topic: String, event: CloudEvent): Unit = {
    logger.debug(s"Dropping event from unknown topic '$topic' -- eventType: '${event.getType}")
  }

  def logUnknownEventType(topic: String, event: CloudEvent): Unit = {
    logger.debug(s"Dropping unknown event type '${event.getType} in topic '$topic'")
  }
}