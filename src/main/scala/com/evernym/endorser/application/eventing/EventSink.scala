package com.evernym.endorser.application.eventing

import akka.actor.typed.ActorSystem
import com.evernym.endorser.application.initialization.Startable
import com.evernym.endorser.domain.ports.EventPublisherPort
import com.typesafe.config.Config

import scala.concurrent.{ExecutionContext, Future}

trait EventSink extends Startable{
  override def start(config: Config)
           (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[EventSink]

  def name(): String = "Event Sink"

  def eventPublisherAdapter: EventPublisherPort

}
