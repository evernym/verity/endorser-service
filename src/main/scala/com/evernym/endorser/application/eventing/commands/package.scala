package com.evernym.endorser.application.eventing

import com.evernym.endorser.domain.events.Event.EventTopic

package object commands {
  val endorserCommandTopic: EventTopic = "private.cmd.ssi.endorser"
}
