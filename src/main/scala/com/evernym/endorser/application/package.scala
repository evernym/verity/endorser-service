package com.evernym.endorser

import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, EntityContext, EntityTypeKey}
import akka.persistence.typed.PersistenceId

import java.util.UUID

package object application {
  def asPersistenceId[M](ctx: EntityContext[M]): PersistenceId = {
    PersistenceId(ctx.entityTypeKey.name, ctx.entityId)
  }

  def newId(): String = UUID.randomUUID().toString

  def tellShardedEntity[T](id: String,
                           command: T)
                          (implicit sharding: ClusterSharding,
                           entityKey: EntityTypeKey[T]): Unit = {
    sharding.entityRefFor(entityKey, id) ! command
  }
}
