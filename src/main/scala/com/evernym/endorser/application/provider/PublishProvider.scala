package com.evernym.endorser.application.provider

import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort

trait PublishProvider {
  def provide(ledgerPrefix: String): PublishTransactionPort

}
