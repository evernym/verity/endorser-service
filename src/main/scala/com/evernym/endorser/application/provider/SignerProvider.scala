package com.evernym.endorser.application.provider

import akka.cluster.sharding.ShardRegion.EntityId
import com.evernym.endorser.domain.aggregate.endorser.ports.SignerPort

trait SignerProvider {
  def provide(entityId: EntityId): SignerPort
}
