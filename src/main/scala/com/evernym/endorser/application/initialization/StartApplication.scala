package com.evernym.endorser.application.initialization

import akka.actor.typed.ActorSystem
import akka.actor.{Extension, ExtensionId}
import akka.management.cluster.bootstrap.ClusterBootstrap
import akka.management.scaladsl.AkkaManagement
import com.evernym.endorser.application.adapter.{Adapters, EndorsementApplicationAdapter, EndorserApplicationAdapter, PublisherApplicationAdapter}
import com.evernym.endorser.application.discovery.discover
import com.evernym.endorser.application.eventing.{ApplicationEventHandler, EventSink, EventSource}
import com.evernym.endorser.application.initialization.StartApplication._
import com.evernym.endorser.application.provider.{PublishProvider, SignerProvider}
import com.evernym.endorser.application.resolver.VaultSeedResolverPort
import com.evernym.endorser.domain.aggregate.endorser.ports.TransactionStoragePort
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.ports.TransactionResolverPort
import com.evernym.endorser.util.TryUtils.getOrThrow
import com.typesafe.config.Config

import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters._
import scala.util.Try

class StartApplication extends Startable {
  override def start(config: Config)(implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {
    startExtensionIfEnabled(AkkaManagement, "akka.management.http.enabled")(_.start())
    startExtensionIfEnabled(ClusterBootstrap, "akka.management.cluster.bootstrap.enabled")(_.start())

    getOrThrow(discoverEventSink()).start(config)
    .map{ sink =>

      val transactionResolverAdapter: TransactionResolverPort = getOrThrow(discoverTransactionResolverPort())
      val hostTransactionAdapter: TransactionStoragePort = getOrThrow(discoverHostTransactionPort())
      val vaultSeedResolver: VaultSeedResolverPort = getOrThrow(discoverVaultSeedResolver())
      val publishProvider: PublishProvider = getOrThrow(discoverPublishProvider())
      val signerProvider: SignerProvider = getOrThrow(discoverSignerProvider())


      Adapters (
        EndorsementApplicationAdapter(
          actorSys,
          transactionResolverAdapter,
          sink.eventPublisherAdapter
        ),
        EndorserApplicationAdapter(
          actorSys,
          transactionResolverAdapter,
          signerProvider,
          sink.eventPublisherAdapter,
          hostTransactionAdapter,
          vaultSeedResolver
        ),
        PublisherApplicationAdapter(
          actorSys,
          publishProvider,
          transactionResolverAdapter,
          sink.eventPublisherAdapter
        )
      )
    }
    .map{ adapters =>

      val endorsers = config.getConfigList("endorser.endorsers")
        .asScala
        .map { conf =>
          val ledgerPrefix = conf.getString("ledger-prefix")
          val ledger = Ledger(ledgerPrefix)
          val id = conf.getString("id")
          adapters.endorser.createEndorser(id, ledger)
          ledgerPrefix -> id
        }.toMap

      adapters.copy(endorserMap = adapters.endorserMap ++ endorsers)
    }
    .flatMap { adapters =>
      getOrThrow(discoverEventSource()).start(ApplicationEventHandler(adapters))
    }
  }

  override def name(): String = "App Layer Wiring"
}

object StartApplication {
  def startExtensionIfEnabled[T <: Extension](t: ExtensionId[T], confPath: String)
                                             (start: T => Unit)
                                             (implicit actorSys: ActorSystem[_]): Unit = {
    val isEnabled = Try(
      actorSys.settings.config.getBoolean(confPath)
    )
    .getOrElse(false)

    if (isEnabled) {
      start(t.get(actorSys))
    }
  }

  def apply(): StartApplication = new StartApplication()

  def discoverEventSource()(implicit actorSys: ActorSystem[_]): Try[EventSource] = {
    discover[EventSource]("endorser.event.event-source")
  }

  def discoverEventSink()(implicit actorSys: ActorSystem[_]): Try[EventSink] = {
    discover[EventSink]("endorser.event.event-sink")
  }

  def discoverTransactionResolverPort()(implicit actorSys: ActorSystem[_]): Try[TransactionResolverPort] = {
    discover[TransactionResolverPort]("endorser.resolver.transaction-resolver")
  }

  def discoverHostTransactionPort()(implicit actorSys: ActorSystem[_]): Try[TransactionStoragePort] = {
    discover[TransactionStoragePort]("endorser.transaction-host")
  }

  def discoverVaultSeedResolver()(implicit actorSys: ActorSystem[_]): Try[VaultSeedResolverPort] = {
    discover[VaultSeedResolverPort]("endorser.resolver.vault-seed-resolver")
  }

  def discoverPublishProvider()(implicit actorSys: ActorSystem[_]): Try[PublishProvider] = {
    discover[PublishProvider]("endorser.publish.provider")
  }

  def discoverSignerProvider()(implicit actorSys: ActorSystem[_]): Try[SignerProvider] = {
    discover[SignerProvider]("endorser.signer.provider")
  }
}
