package com.evernym.endorser.application.initialization

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import com.evernym.endorser.application.health.BuiltVersion.version
import com.typesafe.config.Config
import com.typesafe.scalalogging.Logger

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}


object StarterGuardian {
  def apply(s: Seq[Startable]): Behavior[String] = Behaviors.setup{ implicit context =>
    implicit val system: ActorSystem[_] = context.system

    val logger: Logger = Logger(context.log)
    Runtime.getRuntime.addShutdownHook(stopHook(logger))

    implicit val executionContext: ExecutionContextExecutor = context.system.executionContext

    logger.info("Application starting...")

    checkVersion(system.settings.config, logger)

    logger.info("Base Actor System is started")

    val futures = Future.sequence(s.map(i => {
      startOne(i, logger)
    }))

    futures.onComplete({
      case Success(_) =>
        Thread.sleep(1000)
        logger.info("Application is started.")
      case Failure(errors) =>
        logger.error(s"Failed to start Application - $errors")
        logger.error(s"Shutting down because of failure to start")

        Thread.sleep(100) //Give a moment for the asyc logger to write the log messages

        sys.exit(-1)
    })

    Await.result(futures, Duration.Inf)

    Behaviors.receiveMessage { _ =>
      Behaviors.same[String]
    }
  }

  private def startOne(s: Startable, logger: Logger)(implicit system: ActorSystem[_]): Future[Any] = {
    implicit val executionContext: ExecutionContextExecutor = system.executionContext

    logger.info(s"Starting ${s.name()} ...")
    val fut = s.start(system.settings.config)
    fut.onComplete({
      case Success(result) => logger.info(s"Started ${s.name()} -- $result")
      case _ => ()
    })
    fut
  }

  def stopHook(logger: Logger)(implicit system: ActorSystem[_]): Thread = new Thread() {
    override def run(): Unit = {
      implicit val executionContext: ExecutionContextExecutor = system.executionContext

      logger.info("Stopping Application ...")
      system.terminate()
      val stopping = system.whenTerminated
      stopping.onComplete ({
        case Success(_) =>
          logger.info("Application is stopped.")
        case Failure(errors) =>
          logger.error(s"Failed to stop - $errors")
          logger.error(s"Shutting down")
      })
      Await.result(stopping, Duration.Inf)
    }
  }

  def checkVersion(appConf: Config, logger: Logger): Unit = {
    val confVersion = Try(appConf.getString("endorser.version"))
    val codeVersion = Option(version).map(_.toString)
    val versionsAgree = codeVersion.contains(confVersion.getOrElse(""))

    if (versionsAgree) {
      logger.info(s"Application Version: ${codeVersion.getOrElse("")}")
    }
    else {
      logger.warn(s"Application versions don't agree -- code version [${codeVersion.getOrElse("")}] - " +
        s"conf version [${confVersion.getOrElse("")}]")
    }
  }
}