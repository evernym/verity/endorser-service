package com.evernym.endorser.application.initialization

import akka.actor.typed.ActorSystem
import com.typesafe.config.Config

import scala.concurrent.{ExecutionContext, Future}

trait Startable {

  def start(config: Config)
           (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any]

  def name(): String
}
