package com.evernym.endorser.application.adapter

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityTypeKey}
import com.evernym.endorser.application.port.PublisherPort
import com.evernym.endorser.application.provider.PublishProvider
import com.evernym.endorser.application.tellShardedEntity
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort}
import com.evernym.endorser.domain.process.publisher.PublisherBehavior
import com.evernym.endorser.domain.process.publisher.PublisherBehavior.{SubmitTransactionRef, entityKey}
import com.typesafe.scalalogging.Logger

import scala.concurrent.ExecutionContextExecutor

class PublisherApplicationAdapter(system: ActorSystem[_],
                                  ledgerTransactionPublishingProvider: PublishProvider,
                                  transactionResolver: TransactionResolverPort,
                                  eventPublisher: EventPublisherPort)
  extends PublisherPort{

  val logger:Logger = Logger(classOf[EndorsementApplicationAdapter])

  implicit val sys: ActorSystem[_] = system
  implicit val executionContext: ExecutionContextExecutor = system.executionContext
  implicit val shardedEntityKey: EntityTypeKey[PublisherBehavior.Commands] = entityKey

  implicit val sharding: ClusterSharding = ClusterSharding(system)
  sharding.init(
    Entity(typeKey = entityKey) { ctx =>
      PublisherBehavior(
        ctx.entityId,
        ledgerTransactionPublishingProvider.provide(ctx.entityId),
        transactionResolver,
        eventPublisher
      )
    }
  )

  override def submitTransaction(ledger: Ledger, endorsementId: String, txnRef: String): Unit = {
    tellShardedEntity(
      ledger.ledgerPrefix,
      SubmitTransactionRef(endorsementId, txnRef)
    )
  }
}

object PublisherApplicationAdapter {
  def apply(system: ActorSystem[_],
            ledgerTransactionPublishingProvider: PublishProvider,
            transactionResolver: TransactionResolverPort,
            eventPublisher: EventPublisherPort): PublisherApplicationAdapter =
    new PublisherApplicationAdapter(system, ledgerTransactionPublishingProvider, transactionResolver, eventPublisher)
}
