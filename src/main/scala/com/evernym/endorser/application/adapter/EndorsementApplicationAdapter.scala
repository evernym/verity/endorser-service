package com.evernym.endorser.application.adapter

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityTypeKey}
import com.evernym.endorser.application.port.EndorsementPort
import com.evernym.endorser.application.{asPersistenceId, newId, tellShardedEntity}
import com.evernym.endorser.domain.aggregate.endorsement.Commands.{CompleteTransaction, EndorsementCommand, HoldPreparedTransactionRef, ValidateTransactionRef}
import com.evernym.endorser.domain.aggregate.endorsement.EndorsementBehavior.{EndorsementId, entityKey}
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.domain.aggregate.endorsement.{EndorsementBehavior, TransactionRef}
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort}
import com.typesafe.scalalogging.Logger

import scala.concurrent.ExecutionContextExecutor

class EndorsementApplicationAdapter(system: ActorSystem[_],
                                    transactionResolver: TransactionResolverPort,
                                    eventPublisher: EventPublisherPort) extends EndorsementPort {

  val logger:Logger = Logger(classOf[EndorsementApplicationAdapter])


  implicit val sys: ActorSystem[_] = system
  implicit val executionContext: ExecutionContextExecutor = system.executionContext
  implicit val shardedEntityKey: EntityTypeKey[EndorsementCommand] = entityKey

  implicit val sharding: ClusterSharding = ClusterSharding(system)
  sharding.init(
    Entity(typeKey = entityKey) { ctx =>
      EndorsementBehavior(asPersistenceId(ctx), transactionResolver, eventPublisher)
    }
  )

  override def requestEndorsement(txnRef: TransactionRef,
                                  ledgerPrefix: String,
                                  requestSource: Option[String] = None): Unit = {
    tellShardedEntity(newId(), ValidateTransactionRef(txnRef, ledgerPrefix, requestSource))
  }

  override def holdPreparedTransaction(endorsementId: EndorsementId,
                                       txnRef: TransactionRef): Unit = {
    tellShardedEntity(endorsementId, HoldPreparedTransactionRef(txnRef))
  }

  override def completeEndorsement(endorsementId: EndorsementId, result: Result): Unit = {
    tellShardedEntity(endorsementId, CompleteTransaction(result))
  }
}

object EndorsementApplicationAdapter {
  val logger:Logger = Logger(classOf[EndorsementApplicationAdapter])

  def apply(system: ActorSystem[_],
            transactionResolver: TransactionResolverPort,
            eventPublisher: EventPublisherPort): EndorsementApplicationAdapter =
    new EndorsementApplicationAdapter(
      system,
      transactionResolver,
      eventPublisher
    )
}