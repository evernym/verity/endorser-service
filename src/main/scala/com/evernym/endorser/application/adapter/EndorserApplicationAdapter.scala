package com.evernym.endorser.application.adapter

import akka.actor.typed.ActorSystem
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityTypeKey}
import com.evernym.endorser.application.port.EndorserPort
import com.evernym.endorser.application.provider.SignerProvider
import com.evernym.endorser.application.resolver.VaultSeedResolverPort
import com.evernym.endorser.application.{asPersistenceId, tellShardedEntity}
import com.evernym.endorser.domain.aggregate.endorser.Commands._
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior.entityKey
import com.evernym.endorser.domain.aggregate.endorser.ports.TransactionStoragePort
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort}
import com.typesafe.scalalogging.Logger

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success, Try}

class EndorserApplicationAdapter(system: ActorSystem[_],
                                 transactionResolver: TransactionResolverPort,
                                 signerProvider: SignerProvider,
                                 eventPublisher: EventPublisherPort,
                                 hoster: TransactionStoragePort,
                                 vaultSeedResolver: VaultSeedResolverPort) extends EndorserPort {

  val logger:Logger = Logger(classOf[EndorserApplicationAdapter])

  implicit val executionContext: ExecutionContextExecutor = system.executionContext
  implicit val shardedEntityKey: EntityTypeKey[EndorserCommand] = entityKey

  implicit val sharding: ClusterSharding = ClusterSharding(system)
  sharding.init(
    Entity(typeKey = entityKey) { ctx =>
      EndorserBehavior(
        asPersistenceId(ctx),
        eventPublisher,
        signerProvider.provide(ctx.entityId),
        transactionResolver,
        hoster
      )
    }
  )

  override def createEndorser(endorserId: String, leger: Ledger): Unit = {
    tellShardedEntity(
      endorserId,
      CreateEndorser()
    )
  }

  override def createEndorserDID(endorserId: String, leger: Ledger): Unit = {
    tellShardedEntity(
      endorserId,
      CreateEndorserDID(leger.ledgerPrefix)
    )
  }

  override def createEndorserDIDWithSeed(endorserId: String, leger: Ledger, seedToken: String, seedPath: String): Unit = {
    vaultSeedResolver.resolve(seedToken, seedPath)
      .map{ seed =>
        CreateEndorserDIDWithSeed(leger.ledgerPrefix, seed.seed)
      }
      .onComplete{
        case Success(cmd) => tellShardedEntity(endorserId, cmd)
        case Failure(e) =>
          logger.error("Failed to resolve SEED from token -- THIS SHOULD be investigated")
          logger.error("Error from vault seed resolver", e)
      }
  }

  override def rotateKeyWithSeed(endorserId: String, seedToken: String, seedPath: String): Unit = {
    vaultSeedResolver.resolve(seedToken, seedPath)
      .map{ seed =>
        RotateKeyWithSeed(seed.seed)
      }
      .onComplete{
        case Success(cmd) => tellShardedEntity(endorserId, cmd)
        case Failure(e) =>
          logger.error("Failed to resolve SEED from token -- THIS SHOULD be investigated")
          logger.error("Error from vault seed resolver", e)
      }
  }

  override def activateEndorser(endorserId: String, leger: Ledger): Unit = {
    tellShardedEntity(
      endorserId,
      ActivateEndorser()
    )
  }

  override def deactivateEndorser(endorserId: String): Unit = {
    tellShardedEntity(
      endorserId,
      DeactivateEndorser()
    )
  }

  override def prepareTransaction(endorserId: String,
                                  txnRef: String,
                                  ledgerPrefix: String,
                                  endorsementId: String,
                                  requestSource: Option[String]): Unit = {
    tellShardedEntity(
      endorserId,
      PrepareTransactionRef(txnRef, ledgerPrefix, endorsementId, requestSource)
    )
  }
}

object EndorserApplicationAdapter {
  val logger:Logger = Logger(classOf[EndorsementApplicationAdapter])


  def apply(system: ActorSystem[_],
            transactionResolver: TransactionResolverPort,
            signerProvider: SignerProvider,
            eventPublisher: EventPublisherPort,
            hoster: TransactionStoragePort,
            vaultSeedResolver: VaultSeedResolverPort): EndorserApplicationAdapter =
    new EndorserApplicationAdapter(system, transactionResolver, signerProvider, eventPublisher, hoster, vaultSeedResolver)

  def tellEndorser(id: String)
                  (command: Try[EndorserCommand])
                  (implicit sharding: ClusterSharding,
                   entityKey: EntityTypeKey[EndorserCommand]): Unit = {
    command match {
      case Success(cmd) => tellShardedEntity(id, cmd)
      case Failure(e) =>
        logger.warn(s"Transaction ref CANNOT resolved -- ${e.getMessage}")
    }
  }
}
