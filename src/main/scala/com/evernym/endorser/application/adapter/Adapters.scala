package com.evernym.endorser.application.adapter

import com.evernym.endorser.application.port.{EndorsementPort, EndorserPort, PublisherPort}

final case class Adapters(endorsement: EndorsementPort,
                          endorser: EndorserPort,
                          publisher: PublisherPort,
                          endorserMap: Map[String, String] = Map.empty)