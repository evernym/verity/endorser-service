package com.evernym.endorser.application

import akka.actor.typed.ActorSystem
import com.evernym.endorser.application.health.HealthApplicationAdapter
import com.evernym.endorser.application.initialization.{StartApplication, StarterGuardian}
import com.evernym.endorser.infrastructure.rest.v1.V1Service
import com.evernym.endorser.infrastructure.sharedlib.VdrTools
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.Logger

import scala.util.Try

object Main extends App {
  private val logger = Logger(Main.getClass)
  logger.info("Enter main method to start application")

  val startable = Seq(
    VdrTools(),
    V1Service(HealthApplicationAdapter()),
    StartApplication()
  )

  val starter = Try{ConfigFactory.load()}
    .map { config =>
//      println(config.root().render())
      ActorSystem(StarterGuardian(startable), "Endorser_Service", config)
    }
    .recover {
      case e:Throwable =>
        logger.error(s"Unable to load configuration -- ${e.getMessage}", e)
        throw e
    }
  logger.info("Leaving main method")
  Thread.sleep(1000) // Give a second to flush logs before leaving
}
