package com.evernym.endorser.application.port

import com.evernym.endorser.domain.aggregate.endorsement.EndorsementBehavior.EndorsementId
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result

trait EndorsementPort {
  def requestEndorsement(txnRef: TransactionRef,
                         ledgerPrefix: String,
                         requestSource: Option[String] = None): Unit

  def holdPreparedTransaction(endorsementId: EndorsementId,
                              txnRef: TransactionRef): Unit

  def completeEndorsement(endorsementId: EndorsementId,
                          result: Result): Unit
}
