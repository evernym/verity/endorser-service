package com.evernym.endorser.application.port
import com.evernym.endorser.domain.ledger.Ledger

trait EndorserPort {
  def createEndorser(endorserId: String, leger: Ledger): Unit

  def createEndorserDID(endorserId: String, leger: Ledger): Unit

  def createEndorserDIDWithSeed(endorserId: String, leger: Ledger, seedToken: String, seedPath: String): Unit

  def rotateKeyWithSeed(endorserId: String, seedToken: String, seedPath: String): Unit

  def activateEndorser(endorserId: String, leger: Ledger): Unit

  def deactivateEndorser(endorserId: String): Unit

  def prepareTransaction(endorserId: String,
                         txnRef: String,
                         ledgerPrefix: String,
                         endorsementId: String,
                         requestSource: Option[String]): Unit


}
