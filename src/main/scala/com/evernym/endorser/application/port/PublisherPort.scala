package com.evernym.endorser.application.port
import com.evernym.endorser.domain.ledger.Ledger

trait PublisherPort {
  def submitTransaction(ledger: Ledger, endorsementId: String, txnRef: String): Unit
}
