package com.evernym.endorser.domain.aggregate.endorser.stateHandlers

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.Effect
import com.evernym.endorser.domain.aggregate.endorser.Commands.{CompleteDIDCreation, FailDIDCreation}
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior.logger
import com.evernym.endorser.domain.aggregate.endorser.ports.SignerPort
import com.evernym.endorser.domain.aggregate.endorser.valueObjects.PreparedTransaction.InvalidEndorserStateCode
import com.evernym.endorser.domain.aggregate.endorser.{Commands, DomainEvents, Events, States}
import com.evernym.endorser.domain.chain
import com.evernym.endorser.util.LoggerUtil.idStr

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

object CreatedHandler extends CommandHandler {

  def handleCommand(id: PersistenceId,
                    ctx: ActorContext[Commands.EndorserCommand],
                    signer: SignerPort,
                    state: States.Created,
                    command: Commands.EndorserCommand)
                   (implicit domainEvents: DomainEvents) : Effect[Events.EndorserEvents, States.EndorserState] = {
    implicit val executionContext: ExecutionContextExecutor = ctx.executionContext

    command match {
      case cmd: Commands.CreateEndorserDID =>
        Effect.none
        .thenRun { s: States.EndorserState =>
          logger.info(s"[${idStr(id)}] Began Endorser DID creation.")
          ctx.pipeToSelf(signer.newDID()) {
            case Success(endorserDID) => CompleteDIDCreation(endorserDID, cmd.ledgerPrefix)
            case Failure(exception) => FailDIDCreation(exception.toString)
          }
        }
      case cmd: Commands.CreateEndorserDIDWithSeed =>
        Effect.none
        .thenRun { s: States.EndorserState =>
          logger.info(s"[${idStr(id)}] Began Endorser DID creation.")
          ctx.pipeToSelf(signer.newDID(Some(cmd.seed))) {
            case Success(endorserDID) => CompleteDIDCreation(endorserDID, cmd.ledgerPrefix, fromSeed = true)
            case Failure(exception) => FailDIDCreation(exception.toString)
          }
        }
      case cmd: Commands.CompleteDIDCreation =>
        Effect.persist(
          Seq(Events.EndorserDIDCreated(cmd.endorserDID, cmd.ledgerPrefix))
          ++
          addIf(
            cmd.fromSeed,
            Events.EndorserActivated() // If we gen DID from seed, auto activate endorser
          )
        )
        .thenRun {
          case s: States.Active => // Send event if Endorser is now active
            chain(publishEndorserDIDCreatedEvent, publishEndorserActivatedEvent)(s)
            logger.info(s"[${idStr(id)}] Endorser DID Created: ${cmd.endorserDID}")
            logger.info(s"[${idStr(id)}] Endorser activated")
          case s =>
            publishEndorserDIDCreatedEvent(s)
            logger.info(s"[${idStr(id)}] Endorser DID Created: ${cmd.endorserDID}")
        }
      case cmd: Commands.FailDIDCreation =>
        Effect.persist(
          Events.EndorserDIDCreationFailed()
        )
        .thenRun(_ => logger.error(s"[${idStr(id)}] Endorser DID Creation failed with exception ${cmd.reason}"))
      case cmd: Commands.PrepareTransactionRef =>
        Effect.persist(Events.TransactionPreparationFailed(InvalidEndorserStateCode))
          .thenRun(publishTransactionNotPreparedEvent(InvalidEndorserStateCode, cmd.endorsementId))
          .thenRun { s =>
            logger.error(s"[${idStr(id)}] Unable to prepare transaction while NOT in active state. " +
              s"(currently in '${s.getClass.getSimpleName}')")
          }
      case cmd: Commands.PrepareTransaction =>
        Effect.persist(Events.TransactionPreparationFailed(InvalidEndorserStateCode))
        .thenRun(publishTransactionNotPreparedEvent(InvalidEndorserStateCode, cmd.endorsementId))
        .thenRun { s =>
          logger.error(s"[${idStr(id)}] Unable to prepare transaction while NOT in active state. " +
            s"(currently in '${s.getClass.getSimpleName}')")
        }
      case cmd: Commands.EndorserCommand =>
        handleUnmatchedCommand(cmd, state, id)
    }
  }
}
