package com.evernym.endorser.domain.aggregate.endorser

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{Behavior, LogOptions, PostStop, PreRestart}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{EventSourcedBehavior, RetentionCriteria}
import com.evernym.endorser.domain.aggregate.endorser.ports.{SignerPort, TransactionStoragePort}
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort}
import com.typesafe.scalalogging.Logger
import org.slf4j.event.Level

object EndorserBehavior {
  implicit val logger:Logger = Logger(EndorserBehavior.getClass)

  val entityKey: EntityTypeKey[Commands.EndorserCommand] =
    EntityTypeKey[Commands.EndorserCommand]("Endorser")

  def apply(persistenceId: PersistenceId,
            publishEvent: EventPublisherPort,
            signer: SignerPort,
            txnResolver: TransactionResolverPort,
            hoster: TransactionStoragePort): Behavior[Commands.EndorserCommand] =
    Behaviors.logMessages(LogOptions().withLevel(Level.INFO), Behaviors.setup { context =>
      EventSourcedBehavior.apply(
        persistenceId,
        States.Empty,
        Commands.handler(persistenceId, context)(publishEvent, signer, txnResolver, hoster),
        Events.handler(persistenceId)
      )
        .withRetention(RetentionCriteria.snapshotEvery(200, 2))
        .receiveSignal{
          case (_, PostStop) =>
            logger.info("Endorser Stopped")
            signer.close()
            Behaviors.same
          case (_, PreRestart) =>
            logger.info("Pre-Restart signal received, restarting actor")
            signer.close()
            Behaviors.same
        }
    })
}
