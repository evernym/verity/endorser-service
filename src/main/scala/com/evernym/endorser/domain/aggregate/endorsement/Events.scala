package com.evernym.endorser.domain.aggregate.endorsement

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.PersistenceId
import com.evernym.endorser.domain.aggregate.endorsement.States.EndorsementState
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction.Invalid
import com.evernym.endorser.domain.serialization.CborSerializable
import com.evernym.endorser.util.LoggerUtil.{className, idStr}

object Events {
  sealed trait EndorsementEvents extends CborSerializable

  def toRequestSourceStored(source: Option[RequestSource]): Seq[RequestSourceStored] =
    source.map(Events.RequestSourceStored).toSeq

  final case class StartTimeStored(time: Long) extends EndorsementEvents
  final case class RequestSourceStored(requestSource: RequestSource) extends EndorsementEvents
  final case class ValidTransactionStored(txn: Transaction) extends EndorsementEvents
  final case class PreparedTransactionStored(preparedTxn: Transaction) extends EndorsementEvents
  final case class EndorsementCompleted(result: Result) extends EndorsementEvents
  final case class InvalidEndorsementCompleted(txn: Transaction, reason: Invalid) extends EndorsementEvents

  def handler(id: PersistenceId, ctx: ActorContext[Commands.EndorsementCommand])(state: States.EndorsementState, event: Events.EndorsementEvents): States.EndorsementState = {
    (state, event) match {
      case (s: EndorsementState, StartTimeStored(time)) =>
        s match {
          case s: States.Empty                      => s.copy(created = time)
          case s: States.ValidTransaction           => s.copy(created = time)
          case s: States.ValidPreparedTransaction   => s.copy(created = time)
          case s: States.CompleteValidTransaction   => s.copy(created = time)
          case s: States.CompleteInvalidTransaction => s.copy(created = time)
          case s: States.CompleteEmptyTransaction   => s.copy(created = time)
        }
      case (s: EndorsementState, RequestSourceStored(requestSource)) =>
        s match {
          case s: States.Empty                      => s.copy(requestSource = Option(requestSource))
          case s: States.ValidTransaction           => s.copy(requestSource = Option(requestSource))
          case s: States.ValidPreparedTransaction   => s.copy(requestSource = Option(requestSource))
          case s: States.CompleteValidTransaction   => s.copy(requestSource = Option(requestSource))
          case s: States.CompleteInvalidTransaction => s.copy(requestSource = Option(requestSource))
          case s: States.CompleteEmptyTransaction   => s.copy(requestSource = Option(requestSource))
        }
      case (States.Empty(requestSource, created), Events.ValidTransactionStored(txn)) =>
        States.ValidTransaction(
          txn,
          requestSource,
          created
        )
      case (States.Empty(requestSource, created), Events.InvalidEndorsementCompleted(txn, reason)) =>
        States.CompleteInvalidTransaction(
          txn,
          requestSource,
          reason,
          created
        )
      case (States.Empty(requestSource, created), Events.EndorsementCompleted(result)) =>
        States.CompleteEmptyTransaction(
          requestSource,
          result,
          created
        )
      case (States.ValidTransaction(txn, requestSource, created), Events.PreparedTransactionStored(preparedTxn)) =>
        States.ValidPreparedTransaction(
          txn,
          preparedTxn,
          requestSource,
          created
        )
      case (States.ValidTransaction(txn, requestSource, created), Events.EndorsementCompleted(result)) =>
        States.CompleteValidTransaction(
          txn,
          None,
          requestSource,
          result,
          created
        )
      case (States.ValidPreparedTransaction(txn, preparedTxn, requestSource, created), Events.EndorsementCompleted(result)) =>
        States.CompleteValidTransaction(
          txn,
          Some(preparedTxn),
          requestSource,
          result,
          created
        )
      case (s: States.EndorsementState, e: Events.EndorsementEvents) =>
        ctx.log.error(s"[${idStr(id)}] Event ${className(e)} is rejected because handler for state: ${className(s)}" +
          s" is not defined")
        state
    }
  }
}
