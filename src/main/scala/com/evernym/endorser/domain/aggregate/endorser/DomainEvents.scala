package com.evernym.endorser.domain.aggregate.endorser

import akka.persistence.typed.PersistenceId
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorser.DomainEvents.source
import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.events.Event
import com.evernym.endorser.domain.events.endorser._
import com.evernym.endorser.domain.ports.EventPublisherPort
import com.evernym.endorser.domain.{DID, Finished}
import com.evernym.endorser.util.LoggerUtil.logDomainEvent
import com.typesafe.scalalogging.Logger
import io.cloudevents.CloudEvent

import java.net.URI
import scala.concurrent.{ExecutionContext, Future}

class DomainEvents(endorserId: PersistenceId, producer: EventPublisherPort)(implicit executor: ExecutionContext) {
  val logger: Logger = Logger(classOf[DomainEvents])

  private def produceEvent(topic: String, event: CloudEvent): Future[Finished] =
    producer.publishEvent(
      topic,
      Event.serializeEvent(event),
      event.getSource,
      event.getType,
      event.getId,
    ).andThen(logDomainEvent(endorserId.id, logger))

  def produceTransactionPrepared(endorsementId: String,
                                 requestSource: Option[String],
                                 preparedTxnRef: TransactionRef,
                                 ledgerPrefix: String,
                                 endorserDid: Option[DID],
                                 submitterDid: DID): Future[Finished] = {
    produceEvent(
      TransactionPrepared.eventTopic,
      TransactionPrepared.build(
        source(endorserId),
        TransactionPrepared.DTO(requestSource, endorsementId, preparedTxnRef, ledgerPrefix, endorserDid.map(_.did), submitterDid.did)
      )
    )
  }

  def produceTransactionNotPrepared(endorsementId: String,
                                    requestSource: Option[String],
                                    reason: Code): Future[Finished] = {
    produceEvent(
      TransactionNotPrepared.eventTopic,
      TransactionNotPrepared.build(
        source(endorserId),
        TransactionNotPrepared.DTO(requestSource, endorsementId, reason.toEventCode)
      )
    )

  }

  def produceEndorserCreated(): Future[Finished] = {
    produceEvent(
      EndorserCreated.eventTopic,
      EndorserCreated.build(
        source(endorserId),
        EndorserCreated.DTO()
      )
    )
  }

  def produceEndorserDIDCreated(endorserDID: DID,
                                ledgerPrefix: String): Future[Finished] = {
    produceEvent(
      EndorserDIDGenerated.eventTopic,
      EndorserDIDGenerated.build(
        source(endorserId),
        EndorserDIDGenerated.DTO(endorserDID.did, ledgerPrefix)
      )
    )
  }

  def produceEndorserKeyRotated(newVerkey: String): Future[Finished] = {
    produceEvent(
      EndorserKeyRotated.eventTopic,
      EndorserKeyRotated.build(
        source(endorserId),
        EndorserKeyRotated.DTO(newVerkey)
      )
    )
  }

  def produceEndorserActivated(endorserDID: DID,
                               ledgerPrefix: String): Future[Finished] = {
    produceEvent(
      EndorserActivated.eventTopic,
      EndorserActivated.build(
        source(endorserId),
        EndorserActivated.DTO(endorserDID.did, ledgerPrefix)
      )
    )
  }

  def produceEndorserDeactivated(endorserDID: DID,
                                 ledgerPrefix: String): Future[Finished] = {
    produceEvent(
      EndorserDeactivated.eventTopic,
      EndorserDeactivated.build(
        source(endorserId),
        EndorserDeactivated.DTO(endorserDID.did, ledgerPrefix)
      )
    )
  }


}

object DomainEvents {
  def apply(endorserId: PersistenceId, producer: EventPublisherPort)(implicit executor: ExecutionContext): DomainEvents =
    new DomainEvents(endorserId, producer)

  def source(id: PersistenceId): URI = new URI(s"event-source://v1:ssi:endorser/${id.entityId}")
}
