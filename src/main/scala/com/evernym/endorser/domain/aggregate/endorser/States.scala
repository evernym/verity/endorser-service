package com.evernym.endorser.domain.aggregate.endorser

import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.did.DID
import com.evernym.endorser.domain.serialization.CborSerializable
import com.fasterxml.jackson.annotation.JsonIgnore

object States {

  sealed trait EndorserState extends CborSerializable

  final case object Empty extends EndorserState

  final case class Created() extends EndorserState

  final case class Active(endorserDID: String, ledgerPrefix: String) extends EndorserState {
    @JsonIgnore
    def did: DID = DID.fromString(endorserDID, Option(ledgerPrefix))
  }

  final case class Inactive(endorserDID: String, ledgerPrefix: String) extends EndorserState {
    @JsonIgnore
    def did: DID = DID.fromString(endorserDID, Option(ledgerPrefix))
  }
}
