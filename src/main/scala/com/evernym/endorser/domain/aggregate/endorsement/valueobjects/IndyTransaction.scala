package com.evernym.endorser.domain.aggregate.endorsement.valueobjects

import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.IndyTransaction._
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.IndyTransactionCodes.{invalidEndorser, invalidJson, invalidMissingEndorser, invalidTransactionType}
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction.{Invalid, Valid}
import com.evernym.endorser.domain.aggregate.endorsement.{TransactionRef, abbreviation}
import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.codes.HasCodes
import com.evernym.endorser.domain.did.DID
import com.evernym.endorser.util.JsonUtil.jsonNode
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.JsonNode

import scala.util.{Failure, Success, Try}

final case class IndyTransaction(transactionStr: String,
                                 transactionRef: Option[TransactionRef],
                                 ledgerPrefix: String
                                ) extends Transaction {
  @JsonIgnore
  private val node: Try[JsonNode] = jsonNode(transactionStr)

  override def endorserDID: Option[DID] = {
    node
      .map(_.get("endorser").asText())
      .toOption
      .map(s => DID.fromString(s, Option(ledgerPrefix)))
  }

  override def submitterDID: DID = {
    node
      .map(_.get("identifier").asText())
      .toOption
      .map(s => DID.fromString(s, Option(ledgerPrefix)))
      .getOrElse(throw new Exception("SubmitterDID not found in TXN"))
  }



  override def validateTransaction(): Either[Transaction.Invalid, Transaction.Valid] = {
    isValidJson
      .flatMap(_ => isValidEndorserDID )
      .flatMap(_ => isValidTransactionType )
  }

  def isValidJson: Either[Transaction.Invalid, Transaction.Valid] = {
    node
      .toEither
      .map(_ => Valid)
      .left.map {
      case _: JsonProcessingException => invalidJson
    }
  }

  def isValidEndorserDID: Either[Transaction.Invalid, Transaction.Valid] = {
    endorserDID
      .map(isValidIndyDID)
      .map{b => if(b) Right(Valid) else Left(invalidEndorser)}
      .getOrElse(Left(invalidMissingEndorser))
  }

  def isValidTransactionType: Either[Transaction.Invalid, Transaction.Valid] = {
    typeNum match {
      case Success(1) => Right(Valid)
      case Success(101) => Right(Valid)
      case Success(102) => Right(Valid)
      case Success(i) => Left(invalidTransactionType(Some(s"Unsupported Transaction Type (${typeNumToName(i)})")))
      case Failure(exception) => Left(invalidTransactionType(Some("Transaction type number is NOT parsable")))
    }
  }

  def typeNum: Try[Int] = node.map(_.get("operation").get("type").asText().toInt)

  override def typeName: String = {
    typeNum
      .map(typeNumToName).getOrElse("Transaction Type Not Defined")
  }
}

object IndyTransactionCodes extends HasCodes {
  override def topic: String = "INDY-TXN"

  def descriptions(code: Code): String = {
    code.code match {
      case 1 => "Invalid JSON"
      case 2 => "Invalid - missing Endorser"
      case 3 => "Unsupported Transaction Type"
      case 4 => "Invalid - Endorser DID"
    }
  }

  def invalidJson: Invalid = Invalid(Code(abbreviation, topic, 1))
  def invalidMissingEndorser: Invalid = Invalid(Code(abbreviation, topic, 2))
  def invalidTransactionType(desc: Option[String] = None): Invalid = Invalid(Code(abbreviation, topic, 3, desc = desc))
  def invalidEndorser: Invalid = Invalid(Code(abbreviation, topic, 4))
}

object IndyTransaction {
  def typeNumToName(num: Int): String = {
    num match {
      case 0 => "NODE"
      case 1 => "DID (NYM)"
      case 3 => "GET_TXN"
      case 4 => "TXN_AUTHR_AGRMT"
      case 5 => "TXN_AUTHR_AGRMT_AML"
      case 6 => "GET_TXN_AUTHR_AGRMT"
      case 7 => "GET_TXN_AUTHR_AGRMT_AML"
      case 100 => "ATTRIB"
      case 101 => "SCHEMA"
      case 104 => "GET_ATTR"
      case 105 => "GET_NYM"
      case 107 => "GET_SCHEMA"
      case 108 => "GET_CRED_DEF"
      case 102 => "CRED_DEF"
      case 109 => "POOL_UPGRADE"
      case 111 => "POOL_CONFIG"
      case 113 => "REVOC_REG_DEF"
      case 114 => "REVOC_REG_ENTRY"
      case 115 => "GET_REVOC_REG_DEF"
      case 116 => "GET_REVOC_REG"
      case 117 => "GET_REVOC_REG_DELTA"
      case 118 => "POOL_RESTART"
      case 119 => "GET_VALIDATOR_INFO"
      case 120 => "AUTH_RULE"
      case 121 => "GET_AUTH_RULE"
      case 122 => "AUTH_RULES"
      case i => s"UNKNOWN_TXN_TYPE (#$i)"
    }
  }

  def isValidIndyDID(did: DID): Boolean = {
    !did.did.isBlank
  }
}