package com.evernym.endorser.domain.aggregate.endorsement.valueobjects

import com.evernym.endorser.domain.aggregate.endorsement.abbreviation
import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.codes.{Codes, EventCode, HasCodes}
import com.fasterxml.jackson.annotation.{JsonIgnore, JsonSubTypes, JsonTypeInfo}

import scala.util.chaining.scalaUtilChainingOps

object Results extends HasCodes {
  override def topic: String = "RES"

  def fromEventCode(eventCode: EventCode): Result = {
    val desc = eventCode.descr
    Codes(eventCode.code)
      .pipe { c =>
        if (c.aggregate == abbreviation && c.topic == topic) {
          c.code match {
            case 1 => writtenToVDR
            case 2 => rejectedByVDR
            case 3 => txnRefNotFound
            case 4 => preparedTxnRefNotFound
            case 5 => unknownResult
            case 6 => unknownVDR
            case 7 => blankTxn
            case _ => unknownResult
          }
        } else unknownResult
      }
      .pipe {
        case SuccessfulEndorsement(code) => SuccessfulEndorsement(code.copy(desc = desc))
        case FailedEndorsement(code) => FailedEndorsement(code.copy(desc = desc))
      }
  }

  def descriptions(code: Code): String = {
    code.code match {
      case 1 => "Transaction Written to VDR (normally a ledger)"
      case 2 => "Transaction was rejected by VDR (normally a ledger)"
      case 3 => "Transaction reference was not found"
      case 4 => "Prepared transaction reference was not found"
      case 5 => "Unknown result" // Most likely from a parsing error
      case 6 => "Request for endorsement to a ledger that is not known" // Most likely from a parsing error
      case 7 => "Blank transaction (likely because ref was not resolved)" // Most likely from a parsing error
      case i => s"Unknown Code -- $i"
    }
  }

  @JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
  @JsonSubTypes(
    Array(
      new JsonSubTypes.Type(value = classOf[SuccessfulEndorsement], name = "SuccessfulWrite"),
      new JsonSubTypes.Type(value = classOf[FailedEndorsement], name = "FailedWrite")
    )
  )
  sealed trait Result{
    val code: Code

    @JsonIgnore
    def toEventCode: EventCode = code.toEventCode
  }

  final case class SuccessfulEndorsement(code: Code) extends Result
  final case class FailedEndorsement(code: Code) extends Result

  val writtenToVDR: Result = SuccessfulEndorsement(Code(abbreviation, topic, 1))
  val rejectedByVDR: Result = FailedEndorsement(Code(abbreviation, topic, 2))
  def rejectedByVDRWithException(e: Throwable): Result = FailedEndorsement(
    Code(abbreviation, topic, 2, Some(s"VDR threw execution -- ${e.getMessage}"))
  )

  val txnRefNotFound: Result = FailedEndorsement(Code(abbreviation, topic, 3))
  val preparedTxnRefNotFound: Result = FailedEndorsement(Code(abbreviation, topic, 4))

  val unknownResult: Result = FailedEndorsement(Code(abbreviation, topic, 5))

  val unknownVDR: Result = FailedEndorsement(Code(abbreviation, topic, 6))
  val blankTxn: Result = FailedEndorsement(Code(abbreviation, topic, 7))

  def unexpectedException(ex: Throwable): Result = FailedEndorsement(
    Code(abbreviation, topic, 3, Some(s"Unexpected exception: ${ex.getMessage}"))
  )
}
