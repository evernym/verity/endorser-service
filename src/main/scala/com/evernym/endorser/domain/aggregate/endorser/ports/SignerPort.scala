package com.evernym.endorser.domain.aggregate.endorser.ports

import com.evernym.endorser.domain.{DID, Finished}

import scala.concurrent.Future

trait SignerPort {
  def prepareTxn(transaction: String, ledgerPrefix: String, endorserDid: Option[DID] = None): Future[String]
  def newDID(seed: Option[String] = None): Future[String]
  def rotateKeyWithSeed(did: String, seed: String): Future[String]
  def close(): Finished
}
