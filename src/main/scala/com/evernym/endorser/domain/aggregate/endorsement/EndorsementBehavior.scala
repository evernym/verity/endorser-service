package com.evernym.endorser.domain.aggregate.endorsement

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.EventSourcedBehavior
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort}

object EndorsementBehavior {
  type EndorsementId = String

  val entityKey: EntityTypeKey[Commands.EndorsementCommand] =
    EntityTypeKey[Commands.EndorsementCommand]("Endorsement")

  def apply(persistenceId: PersistenceId,
            txnResolver: TransactionResolverPort,
            eventPublisher: EventPublisherPort): Behavior[Commands.EndorsementCommand] =
    Behaviors.setup { context =>
      EventSourcedBehavior.apply(
        persistenceId,
        States.Empty(),
        Commands.handler(persistenceId, context)(eventPublisher, txnResolver),
        Events.handler(persistenceId, context)
      )
    }

}