package com.evernym.endorser.domain.aggregate.endorsement.valueobjects

import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction._
import com.evernym.endorser.domain.codes.Codes.Code
import com.fasterxml.jackson.annotation.{JsonSubTypes, JsonTypeInfo}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
  Array(
    new JsonSubTypes.Type(value = classOf[IndyTransaction], name = "IndyTransaction")
  )
)
trait Transaction {
  def transactionStr: String
  def transactionRef: Option[TransactionRef]
  def validateTransaction(): Either[Invalid, Valid]
  def submitterDID: DID
  def endorserDID: Option[DID]
  def ledgerPrefix: String
  def typeName: String
}

object Transaction {


  sealed trait Valid
  final case object Valid extends Valid

  final case class Invalid(code: Code)

  def apply(txn: String, txnRef: Option[String], ledgerPrefix: String): Transaction = {
    IndyTransaction(txn, txnRef, ledgerPrefix)
  }
}