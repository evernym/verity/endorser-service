package com.evernym.endorser.domain.aggregate.endorsement

import akka.persistence.typed.PersistenceId
import com.evernym.endorser.domain.aggregate.endorsement.DomainEvents.source
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.{FailedEndorsement, Result}
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction.Invalid
import com.evernym.endorser.domain.events.Event
import com.evernym.endorser.domain.events.endorsement.{EndorsementComplete, EndorsementPreparedTransactionHeld, EndorsementTransactionRejected, EndorsementTransactionValidated}
import com.evernym.endorser.domain.ports.EventPublisherPort
import com.evernym.endorser.domain.{DID, Finished}
import com.evernym.endorser.util.LoggerUtil.logDomainEvent
import com.typesafe.scalalogging.Logger
import io.cloudevents.CloudEvent

import java.net.URI
import scala.concurrent.{ExecutionContext, Future}

class DomainEvents(endorsementId: PersistenceId, producer: EventPublisherPort)(implicit executor: ExecutionContext) {
  val logger: Logger = Logger(classOf[DomainEvents])

  def produceEvent(topic: String, event: CloudEvent): Future[Finished] =
    producer.publishEvent(
      topic,
      Event.serializeEvent(event),
      event.getSource,
      event.getType,
      event.getId,
    ).andThen(logDomainEvent(endorsementId.id, logger))

  def produceTransactionValidated(state: States.EndorsementState): Future[Finished] = {
    state match {
      case States.ValidTransaction(txn, requestSource, _) =>
        produceTransactionValidated(
          txn.transactionRef.getOrElse(""),  // TODO decide if transactionRef is really optional
          requestSource,
          txn.ledgerPrefix,
          txn.endorserDID,
          txn.submitterDID
        )
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorsementTransactionValidated)
    }
  }

  private def produceTransactionValidated(txnRef: TransactionRef,
                                  requestSource: Option[RequestSource],
                                  ledgerPrefix: String,
                                  endorserDid: Option[DID],
                                  submitterDid: DID): Future[Finished] = {
    produceEvent(
      EndorsementTransactionValidated.eventTopic,
      EndorsementTransactionValidated.build(
        source(endorsementId),
        EndorsementTransactionValidated.DTO(endorsementId.entityId, txnRef, requestSource, ledgerPrefix, endorserDid.map(_.did), submitterDid.did)
      )
    )
  }

  def produceTransactionRejected(state: States.EndorsementState): Future[Finished] = {
    state match {
      case States.CompleteInvalidTransaction(txn, requestSource, reason, _) =>
        produceTransactionRejected(
          txn.transactionRef.getOrElse(""),  // TODO decide if transactionRef is really optional
          requestSource,
          reason
        )
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorsementTransactionRejected)
    }
  }

  private def produceTransactionRejected(txnRef: TransactionRef,
                                 requestSource: Option[RequestSource],
                                 reason: Invalid): Future[Finished] = {
    produceEvent(
      EndorsementTransactionRejected.eventTopic,
      EndorsementTransactionRejected.build(
        source(endorsementId),
        EndorsementTransactionRejected.DTO(endorsementId.entityId, txnRef, requestSource, reason.code.toEventCode)
      )
    )
  }

  def producePreparedTransactionHeld(state: States.EndorsementState): Future[Finished] = {
    state match {
      case States.ValidPreparedTransaction(_, _, requestSource, _) =>
        producePreparedTransactionHeld(
          requestSource
        )
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorsementPreparedTransactionHeld)
    }
  }

  private def producePreparedTransactionHeld(requestSource: Option[RequestSource]): Future[Finished] = {
    produceEvent(
      EndorsementPreparedTransactionHeld.eventTopic,
      EndorsementPreparedTransactionHeld.build(
        source(endorsementId),
        EndorsementPreparedTransactionHeld.DTO(endorsementId.entityId, requestSource)
      )
    )
  }

  def produceEndorsementComplete(state: States.EndorsementState): Future[Finished] = {
    state match {
      case States.CompleteValidTransaction(txn, _, requestSource, result, _) =>
        produceEndorsementComplete(requestSource, Some(txn.submitterDID), result)
      case States.CompleteInvalidTransaction(_, requestSource, reason, _) =>
        produceEndorsementComplete(requestSource, None, FailedEndorsement(reason.code))
      case States.CompleteEmptyTransaction(requestSource, reason, _) =>
        produceEndorsementComplete(requestSource, None, FailedEndorsement(reason.code))
      case s =>
        // Event only makes sense for Complete Transaction state
        invalidProductionState(s, EndorsementComplete)
    }
  }

  private def produceEndorsementComplete(requestSource: Option[RequestSource],
                                 submitterDid: Option[DID],
                                 result: Result): Future[Finished] = {
    produceEvent(
      EndorsementComplete.eventTopic,
      EndorsementComplete.build(
        source(endorsementId),
        EndorsementComplete.DTO(endorsementId.entityId, requestSource, submitterDid.map(_.did), result.code.toEventCode)
      )
    )
  }

  private def invalidProductionState(state: States.EndorsementState, eventType: Event): Future[Finished] = {
    Future.failed(new Exception(s"State '${state.getClass.getSimpleName}' is not compatible " +
      s"with this event '${eventType.eventType}'"))
  }
}

object DomainEvents {
  def apply(endorsementId: PersistenceId, producer: EventPublisherPort)(implicit executor: ExecutionContext): DomainEvents =
    new DomainEvents(endorsementId, producer)

  private def source(id: PersistenceId): URI = new URI(s"event-source://v1:ssi:endorsement/${id.entityId}")
}
