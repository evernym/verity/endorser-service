package com.evernym.endorser.domain.aggregate.endorser

import akka.persistence.typed.PersistenceId
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior.logger
import com.evernym.endorser.domain.aggregate.endorser.valueObjects.Transaction
import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.serialization.CborSerializable
import com.evernym.endorser.util.LoggerUtil.className

object Events {
  sealed trait EndorserEvents extends CborSerializable

  final case class TransactionPrepared(preparedTxn: Transaction) extends EndorserEvents
  final case class TransactionPreparationFailed(reason: Code) extends EndorserEvents
  final case class EndorserCreated() extends EndorserEvents
  final case class EndorserDIDCreated(endorserDID: String, ledgerPrefix: String) extends EndorserEvents
  final case class EndorserDIDCreationFailed() extends EndorserEvents
  final case class EndorserKeyRotated(newVerkey: String) extends EndorserEvents
  final case class EndorserActivated() extends EndorserEvents
  final case class EndorserDeactivated() extends EndorserEvents

  def handler(persistenceId: PersistenceId)(state: States.EndorserState, event: Events.EndorserEvents): States.EndorserState = {
    (state, event) match {
      case (s: States.Active, _: Events.TransactionPrepared) => States.Active(s.endorserDID, s.ledgerPrefix)
      case (s: States.Active, _: Events.TransactionPreparationFailed) => States.Active(s.endorserDID, s.ledgerPrefix)
      case (States.Empty, _: Events.EndorserCreated) => States.Created()
      case (_: States.Created, e: Events.EndorserDIDCreationFailed) => States.Created()
      case (_: States.Created, e: Events.EndorserDIDCreated) => States.Inactive(e.endorserDID, e.ledgerPrefix)
      case (s: States.Active, _: Events.EndorserDeactivated) => States.Inactive(s.endorserDID, s.ledgerPrefix)
      case (s: States.Active, _: Events.EndorserKeyRotated) => s
      case (s: States.Inactive, _: Events.EndorserActivated) => States.Active(s.endorserDID, s.ledgerPrefix)
      case (s: States.Inactive, _: Events.TransactionPreparationFailed) => States.Inactive(s.endorserDID, s.ledgerPrefix)
      case (s: States.EndorserState, e: Events.EndorserEvents) =>
        logger.error(s"[$persistenceId] Event ${className(e)} is rejected because handler for state: ${className(s)}" +
          s" is not defined")
        state
    }
  }
}
