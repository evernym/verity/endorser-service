package com.evernym.endorser.domain.aggregate

import akka.actor.typed.scaladsl.ActorContext
import com.evernym.endorser.domain.aggregate.endorsement.States.EndorsementState
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.util.MetricUtil.{Tags, incrementCount, recordTime}
import org.slf4j.Logger

import scala.util.Try

package object endorsement {
  val abbreviation = "ENDT"

  type RequestSource = String
  type TransactionRef = String


  object BillingLogs {
    def logBillableInfo(givenResult: Result, logger: Logger)(currentState: EndorsementState): Unit = {
      /*
    This log message is not a domain concern and should not be handled here but should be handled by
    another bounded context that consumes EndorsementComplete events. But since we don't have that bounded
    context and we need to customer usage for billing we log this message. But this must be a non-dependent
    side-effect that should be removable without effect the domain business logic
     */
      currentState match {
        case States.CompleteValidTransaction(txn, preparedTxn, requestSource, result, _)
          if result.code.isCode(Results.writtenToVDR.code) =>
          logger.info(
            s"[BILLING INFO] transaction written to ledger"   +
              s" -- endorser: ${txn.endorserDID.getOrElse("")}" +
              s" -- ledger: ${preparedTxn.map(_.ledgerPrefix).getOrElse("")}" +
              s" -- transaction type: ${txn.typeName}"   +
              s" -- requestedBy: ${requestSource.getOrElse("")}"
          )
        case States.CompleteValidTransaction(txn, preparedTxn, requestSource, result, _) =>
          logger.info(s"Un-billable completed endorsements -- result: $result " +
            s"-- state: ${currentState.getClass.getSimpleName}" +
            s"-- requestedBy: ${requestSource.getOrElse("")}")
        case _ =>
          logger.info(s"Un-billable completed endorsements -- result: $givenResult -- state: ${currentState.getClass.getSimpleName}")
      }
    }
  }

  object MetricOps {
    val requestedCounterMetricName = "ssi.endorsement.requested"
    val completionLatencyMetricName = "ssi.endorsement.completed-latency"
    val completionCounterMetricName = "ssi.endorsement.completed"


    def incrementRequested(state: EndorsementState)(implicit ctx: ActorContext[_]): Unit = {
      incrementCount(requestedCounterMetricName, tags = metricsTags(state))
    }

    def incrementCompleted(state: EndorsementState)(implicit ctx: ActorContext[_]): Unit = {
      incrementCount(completionCounterMetricName, tags = metricsTags(state))
    }

    def recordLatency(state: EndorsementState)(implicit ctx: ActorContext[_]): Unit = {
      recordTime(completionLatencyMetricName, state.created, System.nanoTime(), tags = metricsTags(state))
    }

    def metricsTags(state: EndorsementState): Tags = {
      val t: Try[Tags] = Try{
        state match {
          case States.Empty(_, _) => Map.empty
          case States.ValidTransaction(txn, _, _) => Map("ledger-prefix" -> txn.ledgerPrefix)
          case States.ValidPreparedTransaction(txn, _, _, _) => Map("ledger-prefix" -> txn.ledgerPrefix)
          case States.CompleteEmptyTransaction(_, result, _) => Map("result" -> result.code.toString)
          case States.CompleteValidTransaction(txn, _, _, result, _) =>
            Map(
              "result" -> result.code.toString,
              "ledger-prefix" -> txn.ledgerPrefix
            )
          case States.CompleteInvalidTransaction(txn, _, result, _) =>
            Map(
              "result" -> result.code.toString,
              "ledger-prefix" -> txn.ledgerPrefix
            )
        }
      }
      t.getOrElse(Map.empty)
    }
  }
}
