package com.evernym.endorser.domain.aggregate.endorser.stateHandlers

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.Effect
import com.evernym.endorser.domain.aggregate.endorser.Commands.{TransactionPreparationComplete, TransactionPreparationFailed}
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior.logger
import com.evernym.endorser.domain.aggregate.endorser.ports.{SignerPort, TransactionStoragePort}
import com.evernym.endorser.domain.aggregate.endorser.valueObjects.PreparedTransaction
import com.evernym.endorser.domain.aggregate.endorser.valueObjects.PreparedTransaction.{genericPreparationFailedCode, transactionRefNotFound}
import com.evernym.endorser.domain.aggregate.endorser.{Commands, DomainEvents, Events, States}
import com.evernym.endorser.domain.ports.TransactionResolverPort
import com.evernym.endorser.util.LoggerUtil.idStr

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

object ActiveHandler extends CommandHandler {
  @SuppressWarnings(Array("VariableShadowing"))
  def handleCommand(id: PersistenceId,
                    ctx: ActorContext[Commands.EndorserCommand],
                    signer: SignerPort,
                    resolver: TransactionResolverPort,
                    hoster: TransactionStoragePort,
                    state: States.Active,
                    command: Commands.EndorserCommand)
                   (implicit executionContext: ExecutionContextExecutor, domain:DomainEvents): Effect[Events.EndorserEvents, States.EndorserState] = {
    command match {
      case cmd: Commands.PrepareTransactionRef =>
        Effect.none
          .thenRun{ _ =>
            ctx.pipeToSelf(resolver.resolve(cmd.txnRef)){
              case Success(value) => Commands.PrepareTransaction(
                value,
                cmd.ledgerPrefix,
                cmd.endorsementId,
                cmd.requestSource
              )
              case Failure(e) =>
                ctx.log.warn(s"[${idStr(id)}] Transaction ref CANNOT resolved -- ${e.getMessage}")

                Commands.TransactionPreparationFailed(
                  transactionRefNotFound.copy(desc = Some(e.getMessage)), // Not sure why scapegoat think this is shadowing
                  cmd.endorsementId,
                  cmd.requestSource
                )
            }
          }
      case cmd: Commands.PrepareTransaction =>
        val future = signer.prepareTxn(cmd.txn, cmd.ledgerPrefix, Some(state.did))
          .flatMap{ txn =>
            hoster.store(txn)
              .map( (_, txn) )
          }
        Effect.none
          .thenRun { _ =>
          ctx.pipeToSelf(future) {
            case Success(preparedTxn) =>
              logger.info(s"[${idStr(id)}] Preparing Txn for endorsementId: '${cmd.endorsementId}'")
              TransactionPreparationComplete(
                preparedTxn._2,
                Option(preparedTxn._1),
                cmd.ledgerPrefix,
                cmd.endorsementId,
                cmd.requestSource
              )
            case Failure(exception) =>
              logger.warn(s"[${idStr(id)}] Preparing Txn FAILED for endorsementId: '${cmd.endorsementId}' that was " +
                s"required by '${cmd.requestSource.getOrElse("unknown")}' -- ${exception.getMessage}")
              TransactionPreparationFailed(
                genericPreparationFailedCode, // We should consider having a more specific code
                cmd.endorsementId,
                cmd.requestSource
              )
          }
        }
      case cmd: Commands.TransactionPreparationComplete =>
        val preparedTxn = PreparedTransaction(cmd.preparedTxn, cmd.preparedTxnRef, cmd.ledgerPrefix)
        Effect
          .persist(Events.TransactionPrepared(preparedTxn))
          .thenRun(publishTransactionPreparedEvent(preparedTxn, cmd.endorsementId, cmd.requestSource))
          .thenRun { _ => logger.info(s"[${idStr(id)}] Prepared Txn for endorsementId '${cmd.endorsementId}'")}
      case cmd: Commands.TransactionPreparationFailed =>
        Effect
          .persist(Events.TransactionPreparationFailed(cmd.reason))
          .thenRun(publishTransactionNotPreparedEvent(cmd.reason, cmd.endorsementId, cmd.requestSource))
          .thenRun { _ => logger.warn(s"[${idStr(id)}] Transaction preparation " +
            s"failed for endorsementId '${cmd.endorsementId}' with reason: ${cmd.reason}") }
      case Commands.RotateKeyWithSeed(seed) =>
        Effect.none
          .thenRun {
            case s: States.Active =>
              ctx.pipeToSelf(signer.rotateKeyWithSeed(s.did.unqualifiedId, seed)) {
                case Success(verKey) =>
                  ctx.log.info(s"rotating key with seed for did: ${s.did}")
                  Commands.CompleteKeyRotation(verKey)
                case Failure(e) =>
                  ctx.log.warn(s"rotating key with seed for did:'${s.did}' failed -- ${e.getMessage}")
                  Commands.FailKeyRotation(e.getMessage)
              }
            case s: States.EndorserState =>
              ctx.log.warn(s"key rotation is only supported in the Active State (currently in ${s.getClass.getSimpleName}")
          }
      case Commands.CompleteKeyRotation(newVerkey) =>
        Effect
          .persist(Events.EndorserKeyRotated(newVerkey))
          .thenRun(
            publishEndorserKeyRotated(newVerkey)
          )
          .thenRun( _ => ctx.log.info(s"rotation complete with new verkey: '$newVerkey'"))
      case Commands.FailKeyRotation(reason) =>
        Effect
          .none
          .thenRun( _ => ctx.log.warn(s"rotation failed -- $reason"))
      case _: Commands.DeactivateEndorser =>
        Effect
          .persist(Events.EndorserDeactivated())
          .thenRun(publishEndorserDeactivatedEvent)
          .thenRun { _ => logger.info(s"[${idStr(id)}] Endorser deactivated")}
      case cmd: Commands.EndorserCommand =>
        handleUnmatchedCommand(cmd, state, id)
    }
  }
}
