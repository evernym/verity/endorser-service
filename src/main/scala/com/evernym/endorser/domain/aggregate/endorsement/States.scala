package com.evernym.endorser.domain.aggregate.endorsement

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction.Invalid
import com.evernym.endorser.domain.serialization.CborSerializable

object States {

  sealed trait EndorsementState extends CborSerializable {
    def created: Long
  }
  sealed trait CompleteState

  final case class Empty(requestSource: Option[RequestSource] = None, created: Long = 0) extends EndorsementState

  final case class ValidTransaction(txn: Transaction,
                                    requestSource: Option[RequestSource],
                                    created: Long
                                   ) extends EndorsementState

  final case class ValidPreparedTransaction(txn: Transaction,
                                            preparedTxn: Transaction,
                                            requestSource: Option[RequestSource],
                                            created: Long
                                           ) extends EndorsementState

  final case class CompleteValidTransaction(txn: Transaction,
                                            preparedTxn: Option[Transaction],
                                            requestSource: Option[RequestSource],
                                            result: Result,
                                            created: Long
                                           ) extends EndorsementState with CompleteState

  final case class CompleteEmptyTransaction(requestSource: Option[RequestSource],
                                            result: Result,
                                            created: Long
                                           ) extends EndorsementState with CompleteState

  final case class CompleteInvalidTransaction(txn: Transaction,
                                              requestSource: Option[RequestSource],
                                              reason: Invalid,
                                              created: Long
                                             ) extends EndorsementState with CompleteState
}
