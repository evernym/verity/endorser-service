package com.evernym.endorser.domain.aggregate.endorser.valueObjects
import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.fasterxml.jackson.annotation.{JsonSubTypes, JsonTypeInfo}

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes(
  Array(
    new JsonSubTypes.Type(value = classOf[PreparedTransaction], name = "PreparedTransaction")
  )
)
trait Transaction {
  def transactionStr: String
  def transactionRef: Option[TransactionRef]
  def ledgerPrefix: String
  def submitterDID: DID
  def endorserDID: Option[DID]
}

object Transaction {
  def apply(txn: String, txnRef: Option[TransactionRef], ledgerPrefix: String): Transaction = {
    PreparedTransaction(txn, txnRef, ledgerPrefix)
  }
}