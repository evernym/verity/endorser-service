package com.evernym.endorser.domain.aggregate.endorser.stateHandlers

import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.Effect
import com.evernym.endorser.domain.Finished
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior.logger
import com.evernym.endorser.domain.aggregate.endorser.valueObjects.PreparedTransaction
import com.evernym.endorser.domain.aggregate.endorser.valueObjects.PreparedTransaction.InvalidEndorserStateCode
import com.evernym.endorser.domain.aggregate.endorser.{Commands, DomainEvents, Events, States}
import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.events.Event
import com.evernym.endorser.domain.events.endorser._
import com.evernym.endorser.util.LoggerUtil.{className, idStr}

import scala.concurrent.Future

trait CommandHandler {

//  def resolveTransaction(command: Commands.PrepareTransactionRef, resolver: TransactionResolverPort)
//                        (implicit ctx: ActorContext[Commands.EndorserCommand],
//                         id: PersistenceId): Effect[Events.EndorserEvents, States.EndorserState] = {
//    Effect.none
//      .thenRun{ _ =>
//        ctx.pipeToSelf(resolver.resolve(command.txnRef)){
//          case Success(value) => Commands.PrepareTransaction(
//            value,
//            command.ledgerPrefix,
//            command.endorsementId,
//            command.requestSource
//          )
//          case Failure(e) =>
//            ctx.log.warn(s"[${idStr(id)}] Transaction ref CANNOT resolved -- ${e.getMessage}")
//            Commands.TransactionPreparationFailed(
//              transactionRefNotFound.copy(desc = Some(e.getMessage)),
//              command.endorsementId,
//              command.requestSource
//            )
//        }
//      }
//  }

  // used to define common behavior for commands shared by many state handlers
  def handleUnmatchedCommand(command: Commands.EndorserCommand, state: States.EndorserState, id: PersistenceId)
                            (implicit domainEvents: DomainEvents): Effect[Events.EndorserEvents, States.EndorserState] = {
    command match {
      case cmd: Commands.CreateEndorser =>
        logger.debug("NO OP -- Endorser is already created")
        Effect.none
      case cmd: Commands.PrepareTransaction =>
        Effect
          .persist(Events.TransactionPreparationFailed(InvalidEndorserStateCode))
          .thenRun { s: States.EndorserState =>
            publishTransactionNotPreparedEvent(InvalidEndorserStateCode, cmd.endorsementId)(s)
            logger.error(s"[${idStr(id)}] Unable to prepare transaction: ${cmd.txn}, invalid state for PrepareTransaction command.")
          }
      case _: Commands.EndorserCommand =>
        logger.warn(s"[${idStr(id)}] Command '${className(command)}' rejected because handler for " +
          s"state: ${className(state)} is not defined")
        Effect.none
    }
  }

  def publishEndorserCreatedEvent(state: States.EndorserState)
                                 (implicit events: DomainEvents): Future[Finished] = {
    state match {
      case _: States.Created =>
        events.produceEndorserCreated()
      // Event only makes sense for Create state
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorserCreated)
    }
  }

  def publishEndorserDIDCreatedEvent(state: States.EndorserState)
                                    (implicit events: DomainEvents): Future[Finished] = {
    state match {
      case s: States.Inactive =>
        events.produceEndorserDIDCreated(s.did, s.ledgerPrefix)
      // Event only makes sense for Create state
      case s: States.Active => // Publish after automatically going active from CreateEndorserDIDWithSeed cmd
        events.produceEndorserDIDCreated(s.did, s.ledgerPrefix)
      // Event only makes sense for Create state
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorserDIDGenerated)
    }
  }

  def publishEndorserKeyRotated(newVerkey: String)(state: States.EndorserState)
                               (implicit events: DomainEvents): Future[Finished] = {
    state match {
      case _: States.Active =>
        events.produceEndorserKeyRotated(newVerkey)
      // Event only makes sense for Create state
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorserCreated)
    }
  }

  def publishEndorserActivatedEvent(state: States.EndorserState)
                                   (implicit events: DomainEvents): Future[Finished] = {
    state match {
      case s: States.Active =>
        events.produceEndorserActivated(s.did, s.ledgerPrefix)
      // Event only makes sense for PreparedTransaction state
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorserActivated)
    }
  }

  def publishTransactionPreparedEvent(transaction: PreparedTransaction,
                                      endorsementId: String,
                                      eventSource: Option[String] = None)
                                     (state: States.EndorserState)
                                     (implicit events: DomainEvents): Future[Finished] = {
    state match {
      case s: States.Active =>
        events.produceTransactionPrepared(
          endorsementId,
          eventSource,
          transaction.transactionRef.getOrElse(""), // TODO decide if transactionRef is really optional
          transaction.ledgerPrefix,
          Option(s.did),
          transaction.submitterDID
        )
      // Event only makes sense for Active state
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, TransactionPrepared)
    }
  }

  def publishTransactionNotPreparedEvent(reason: Code,
                                         endorsementId: String,
                                         requestSource: Option[String] = None)
                                        (state: States.EndorserState)
                                        (implicit events: DomainEvents): Future[Finished] = {
    state match {
      case _: States.Active =>
        events.produceTransactionNotPrepared(endorsementId, requestSource, reason)
      case s =>
        events.produceTransactionNotPrepared(endorsementId, requestSource, reason)
        invalidProductionState(s, TransactionNotPrepared)
    }
  }

  def publishEndorserDeactivatedEvent(state: States.EndorserState)
                                     (implicit events: DomainEvents): Future[Finished] = {
    state match {
      case s: States.Inactive =>
        events.produceEndorserDeactivated(s.did, s.ledgerPrefix)
      // Event only makes sense for Inactive state
      case s =>
        // Event only makes sense for CompleteValidTransaction state
        invalidProductionState(s, EndorserDeactivated)
    }
  }

  private def invalidProductionState(state: States.EndorserState, eventType: Event): Future[Finished] = {
    Future.failed(new Exception(s"State '${state.getClass.getSimpleName}' is not compatible " +
      s"with this event '${eventType.eventType}'"))
  }

  def addIf[T](condition: => Boolean, obj: T): Seq[T] = {
    if(condition) {
      Seq(obj)
    }else {Seq.empty}
  }
}
