package com.evernym.endorser.domain.aggregate.endorser.valueObjects

import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.aggregate.endorsement.{TransactionRef, abbreviation}
import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.codes.HasCodes
import com.evernym.endorser.domain.did.DID
import com.evernym.endorser.util.JsonUtil.jsonNode
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.JsonNode

import scala.util.Try

final case class PreparedTransaction(transactionStr: String,
                                     transactionRef: Option[TransactionRef],
                                     ledgerPrefix: String) extends Transaction {
  @JsonIgnore
  private val node: Try[JsonNode] = jsonNode(transactionStr)

  override def endorserDID: Option[DID] = {
    node
      .map(_.get("endorser").asText())
      .map(DID.fromString(_))
      .toOption
  }

  override def submitterDID: DID = {
    node
      .map(_.get("identifier").asText())
      .map(DID.fromString(_, Option(ledgerPrefix)))
      .toOption
      .getOrElse(throw new Exception("Submitter DID not found in TXN"))
  }
}

object PreparedTransaction extends HasCodes {
  override def topic: String = "PREP"
  val genericPreparationFailedCode: Code = Code(abbreviation, topic, 1)
  val InvalidEndorserStateCode: Code = Code(abbreviation, topic, 2)
  val preparationFailedAndInvalidStateCode: Code = Code(abbreviation, topic, 3)
  val transactionRefNotFound: Code = Code(abbreviation, topic, 4)

  def descriptions(code: Code): String = {
    code.code match {
      case 4 => "Transaction failed to resolve"
      case 3 => "Transaction Preparation failed, and endorser was also in invalid state upon receiving failure command"
      case 2 => "Endorser not in Active state, cannot prepare Transaction"
      case 1 => "Generic Preparation Failed Code"
      case i => s"Unknown Code -- $i"
    }
  }
}