package com.evernym.endorser.domain.aggregate.endorser

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.Effect
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorser.ports.{SignerPort, TransactionStoragePort}
import com.evernym.endorser.domain.aggregate.endorser.stateHandlers._
import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort}
import com.evernym.endorser.domain.serialization.CborSerializable

import scala.concurrent.ExecutionContextExecutor

object Commands {

  sealed trait EndorserCommand extends CborSerializable

  final case class PrepareTransactionRef(txnRef: TransactionRef,
                                         ledgerPrefix : String,
                                         endorsementId: String,
                                         requestSource: Option[String]) extends EndorserCommand

  final case class PrepareTransaction(txn: String,
                                      ledgerPrefix : String,
                                      endorsementId: String,
                                      requestSource: Option[String]) extends EndorserCommand

  final case class CreateEndorser() extends EndorserCommand

  final case class CreateEndorserDID(ledgerPrefix: String) extends EndorserCommand
  final case class CreateEndorserDIDWithSeed(ledgerPrefix: String, seed: String) extends EndorserCommand
  final case class RotateKeyWithSeed(seed: String) extends EndorserCommand

  final case class ActivateEndorser() extends EndorserCommand

  final case class DeactivateEndorser() extends EndorserCommand

  // These case classes are private because these commands are for internal actor use only, they should not be called at the application level
  final case class TransactionPreparationComplete(preparedTxn: String,
                                                  preparedTxnRef: Option[TransactionRef],
                                                  ledgerPrefix: String,
                                                  endorsementId: String,
                                                  requestSource: Option[String]) extends EndorserCommand

  final case class TransactionPreparationFailed(reason: Code,
                                                endorsementId: String,
                                                requestSource: Option[String]) extends EndorserCommand

  final case class CompleteDIDCreation(endorserDID: String, ledgerPrefix: String, fromSeed: Boolean = false) extends EndorserCommand

  final case class FailDIDCreation(reason: String) extends EndorserCommand
  final case class CompleteKeyRotation(verkey: String) extends EndorserCommand
  final case class FailKeyRotation(reason: String) extends EndorserCommand

  def handler(id: PersistenceId,
              ctx: ActorContext[Commands.EndorserCommand])
             (publishEvent: EventPublisherPort,
              signer: SignerPort,
              txnResolver: TransactionResolverPort,
              hoster: TransactionStoragePort)
             (state: States.EndorserState,
              cmd: Commands.EndorserCommand): Effect[Events.EndorserEvents, States.EndorserState] = {
    implicit val executionContext: ExecutionContextExecutor = ctx.executionContext
    implicit val domain: DomainEvents = DomainEvents(id, publishEvent)
    state match {
      case States.Empty =>
        EmptyHandler.handleCommand(id, States.Empty, cmd)
      case s: States.Created =>
        CreatedHandler.handleCommand(id, ctx, signer, s, cmd)
      case s: States.Active =>
        ActiveHandler.handleCommand(id, ctx, signer, txnResolver, hoster, s, cmd)
      case s: States.Inactive =>
        InactiveHandler.handleCommand(id, s, cmd)
    }
  }
}
