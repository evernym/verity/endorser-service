package com.evernym.endorser.domain.aggregate.endorser.ports

import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef

import scala.concurrent.Future

trait TransactionStoragePort {
  def store(transactionStr: String): Future[TransactionRef]
}
