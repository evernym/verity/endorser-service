package com.evernym.endorser.domain.aggregate.endorser.stateHandlers

import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.Effect
import com.evernym.endorser.domain.aggregate.endorser.EndorserBehavior.logger
import com.evernym.endorser.domain.aggregate.endorser.valueObjects.PreparedTransaction.InvalidEndorserStateCode
import com.evernym.endorser.domain.aggregate.endorser.{Commands, DomainEvents, Events, States}
import com.evernym.endorser.util.LoggerUtil.idStr

object EmptyHandler extends CommandHandler {

  def handleCommand(id: PersistenceId,
                    state: States.EndorserState,
                    command: Commands.EndorserCommand)
                   (implicit domain:DomainEvents): Effect[Events.EndorserEvents, States.EndorserState] = {
    command match {
      case _: Commands.CreateEndorser =>
        Effect.persist(Events.EndorserCreated())
          .thenRun(publishEndorserCreatedEvent)
          .thenRun { _ => logger.info(s"[${idStr(id)}] Endorser is created")}
      case cmd: Commands.PrepareTransactionRef =>
        Effect.persist(Events.TransactionPreparationFailed(InvalidEndorserStateCode))
          .thenRun(publishTransactionNotPreparedEvent(InvalidEndorserStateCode, cmd.endorsementId))
          .thenRun { s => logger.error(s"[${idStr(id)}] Unable to prepare transaction while NOT in active state. " +
            s"(currently in '${s.getClass.getSimpleName}')")
          }
      case cmd: Commands.PrepareTransaction =>
        Effect.persist(Events.TransactionPreparationFailed(InvalidEndorserStateCode))
          .thenRun(publishTransactionNotPreparedEvent(InvalidEndorserStateCode, cmd.endorsementId))
          .thenRun { s => logger.error(s"[${idStr(id)}] Unable to prepare transaction while NOT in active state. " +
              s"(currently in '${s.getClass.getSimpleName}')")
          }
      case cmd: Commands.EndorserCommand =>
        handleUnmatchedCommand(cmd, state, id)
    }
  }
}
