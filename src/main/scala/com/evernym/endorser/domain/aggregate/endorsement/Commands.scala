package com.evernym.endorser.domain.aggregate.endorsement

import akka.actor.typed.scaladsl.ActorContext
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.Effect
import com.evernym.endorser.domain.aggregate.endorsement.BillingLogs.logBillableInfo
import com.evernym.endorser.domain.aggregate.endorsement.MetricOps.{incrementCompleted, incrementRequested, recordLatency}
import com.evernym.endorser.domain.aggregate.endorsement.States.EndorsementState
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.{Result, txnRefNotFound}
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction
import com.evernym.endorser.domain.chain
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort}
import com.evernym.endorser.domain.serialization.CborSerializable
import com.evernym.endorser.util.LoggerUtil.{className, idStr}

import scala.concurrent.ExecutionContextExecutor
import scala.util.{Failure, Success}

object Commands {
  sealed trait EndorsementCommand extends CborSerializable

  final case class ValidateTransactionRef(txnRef: TransactionRef,
                                          ledgerPrefix: String,
                                          requestSource: Option[RequestSource] = None) extends EndorsementCommand

  final case class ValidateTransaction(txn: String,
                                       txnRef: Option[TransactionRef],
                                       ledgerPrefix: String,
                                       requestSource: Option[RequestSource] = None) extends EndorsementCommand

  final case class HoldPreparedTransactionRef(preparedTxnRef: TransactionRef) extends EndorsementCommand

  final case class HoldPreparedTransaction(preparedTxn: String,
                                           preparedTxnRef: Option[TransactionRef]) extends EndorsementCommand

  final case class CompleteTransaction(result: Result, requestSource: Option[RequestSource] = None) extends EndorsementCommand

  def handler(id: PersistenceId,
              ctx: ActorContext[Commands.EndorsementCommand])
             (eventPublisher: EventPublisherPort,
              txnResolver: TransactionResolverPort)
             (state: States.EndorsementState,
              command: Commands.EndorsementCommand): Effect[Events.EndorsementEvents, States.EndorsementState] = {
    implicit val executionContext: ExecutionContextExecutor = ctx.executionContext
    implicit val domainEvents: DomainEvents = DomainEvents(id, eventPublisher)
    implicit val persistenceId: PersistenceId = id
    implicit val actorContext: ActorContext[EndorsementCommand] = ctx

    (state, command) match {
      case (_, Commands.ValidateTransactionRef(txnRef, ledgerPrefix, requestSource)) =>
        Effect.none
        .thenRun(
          resolveTransaction(txnResolver, txnRef, requestSource)(
            ValidateTransaction(_, Some(txnRef), ledgerPrefix, requestSource)
          )
        )
      case (_, Commands.HoldPreparedTransactionRef(txnRef)) =>
        Effect.none
        .thenRun(
          resolveTransaction(txnResolver, txnRef)(
            HoldPreparedTransaction(_, Some(txnRef))
          )
        )
      case (States.Empty(_, _), Commands.ValidateTransaction(txn, txnRef, ledgerPrefix, source)) =>
        val txnValueObj: Transaction = Transaction(txn, txnRef, ledgerPrefix)
        txnValueObj.validateTransaction() match {
          case Right(_) =>
            Effect.persist(
              Events.toRequestSourceStored(source)
              :+
              Events.StartTimeStored(System.nanoTime())
              :+
              Events.ValidTransactionStored(txnValueObj)
            )
            .thenRun(domainEvents.produceTransactionValidated)
            .thenRun(incrementRequested)
            .thenRun(_ => ctx.log.info(s"[${idStr(id)}] Endorsement is started"))
          case Left(invalid) =>
            Effect.persist(
              Events.toRequestSourceStored(source)
              :+
              Events.StartTimeStored(System.nanoTime())
              :+
              Events.InvalidEndorsementCompleted(txnValueObj, invalid)
            )
            .thenRun(chain(domainEvents.produceTransactionRejected, domainEvents.produceEndorsementComplete))
            .thenRun(incrementRequested)
            .thenRun(_ => ctx.log.info(s"[${idStr(id)}] Endorsement's Transaction was rejected"))
        }
      case (States.Empty(_, _), Commands.CompleteTransaction(result, source)) =>
        Effect.persist(
          Events.toRequestSourceStored(source)
          :+
          Events.StartTimeStored(System.nanoTime())
          :+
          Events.EndorsementCompleted(result)
        )
        .thenRun(domainEvents.produceEndorsementComplete)
        .thenRun(recordLatency)
        .thenRun(incrementCompleted)
      case (States.ValidTransaction(txn, _, _), Commands.HoldPreparedTransaction(preparedTxn, preparedTxnRef)) =>
        Effect.persist(
          Events.PreparedTransactionStored(
            Transaction(preparedTxn, preparedTxnRef, txn.ledgerPrefix)
          )
        )
        .thenRun(domainEvents.producePreparedTransactionHeld)
        .thenRun(_ => ctx.log.info(s"[${idStr(id)}] Prepared Transaction is held"))
      case (States.ValidTransaction(_, _, _), Commands.CompleteTransaction(result, _)) =>
        Effect.persist(
          Events.EndorsementCompleted(result)
        )
        .thenRun(domainEvents.produceEndorsementComplete)
        .thenRun{ _ =>
          ctx.log.warn(s"[${idStr(id)}] Endorsement was completed without receiving a prepared transactions")
          ctx.log.info(s"[${idStr(id)}] Endorsement is complete with '${result.code}' result code")
        }
        .thenRun(logBillableInfo(result, ctx.log))
        .thenRun(recordLatency)
        .thenRun(incrementCompleted)
      case (States.ValidPreparedTransaction(txn, _, _, _), Commands.CompleteTransaction(result, _)) =>
        Effect.persist(
          Events.EndorsementCompleted(result)
        )
        .thenRun(domainEvents.produceEndorsementComplete)
        .thenRun{ _ => ctx.log.info(s"[${idStr(id)}] Endorsement is complete with '${result.code}' result code") }
        .thenRun(logBillableInfo(result, ctx.log))
        .thenRun(recordLatency)
        .thenRun(incrementCompleted)
      case (s: States.EndorsementState, cmd: Commands.EndorsementCommand) =>
        s match {
//          case _: States.CompleteState =>
//            ctx.log.warn(s"[${idStr(id)}] Command '${className(cmd)}' rejected because Endorsement is in a complete state")
          case _ =>
            ctx.log.warn(s"[${idStr(id)}] Command '${className(cmd)}' rejected because handler for " +
              s"state: ${className(s)} is not defined")
        }
        Effect.none
    }
  }

  @SuppressWarnings(Array("UnusedMethodParameter")) // state parameter is required to use it with .thenRun
  def resolveTransaction(resolver: TransactionResolverPort, ref: String, requestSource: Option[RequestSource] = None)
                        (toCmd: String => EndorsementCommand)
                        (state: EndorsementState)
                        (implicit ctx: ActorContext[EndorsementCommand], id: PersistenceId): Unit = {
    ctx.pipeToSelf(resolver.resolve(ref)){
      case Success(value) => toCmd(value)
      case Failure(e) =>
        ctx.log.warn(s"[${idStr(id)}] Transaction ref CANNOT resolved -- ${e.getMessage}")
        CompleteTransaction(txnRefNotFound, requestSource)
    }
  }
}
