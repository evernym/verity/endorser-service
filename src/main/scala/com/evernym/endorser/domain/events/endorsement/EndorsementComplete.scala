package com.evernym.endorser.domain.events.endorsement

import com.evernym.endorser.domain.aggregate.endorsement.EndorsementBehavior.EndorsementId
import com.evernym.endorser.domain.aggregate.endorsement.RequestSource
import com.evernym.endorser.domain.codes.EventCode
import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import com.evernym.endorser.domain.events.{Event, endorsementTopic}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object EndorsementComplete extends Event {
  val eventType: EventType = "endorsement.endorsement-complete.v1"
  val eventTopic: EventTopic = endorsementTopic

  final case class DTO(endorsementId: EndorsementId,
                       requestSource: Option[RequestSource],
                       submitterDID: Option[String],
                       result: EventCode)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}
