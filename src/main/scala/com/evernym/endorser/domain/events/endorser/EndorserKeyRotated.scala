package com.evernym.endorser.domain.events.endorser

import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import com.evernym.endorser.domain.events.{Event, endorserTopic}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object EndorserKeyRotated extends Event {
  val eventType: EventType = "endorser.endorser-key-rotated.v1"
  val eventTopic: EventTopic = endorserTopic

  final case class DTO(newverkey: String)

  def build[T](source: URI, dto: DTO): CloudEvent =
    super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}
