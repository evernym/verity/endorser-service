package com.evernym.endorser.domain.events.publisher

import com.evernym.endorser.domain.codes.EventCode
import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import com.evernym.endorser.domain.events.{Event, publisherTopic}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object PublisherWriteFailed extends Event {
  val eventType: EventType = "publisher.transaction-write-failed.v1"
  val eventTopic: EventTopic = publisherTopic

  final case class DTO(endorsementId: String, result: EventCode)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}
