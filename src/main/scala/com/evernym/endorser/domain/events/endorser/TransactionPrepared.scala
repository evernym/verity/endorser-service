package com.evernym.endorser.domain.events.endorser

import com.evernym.endorser.domain.aggregate.endorsement.EndorsementBehavior.EndorsementId
import com.evernym.endorser.domain.aggregate.endorsement.{RequestSource, TransactionRef}
import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import com.evernym.endorser.domain.events.{Event, endorserTopic}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object TransactionPrepared extends Event {
  val eventType: EventType = "endorser.transaction-prepared.v1"
  val eventTopic: EventTopic = endorserTopic

  final case class DTO(requestSource: Option[RequestSource],
                       endorsementId: EndorsementId,
                       txnRef: TransactionRef,
                       ledgerPrefix: String,
                       endorserDid: Option[String],
                       submitterDid: String)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}
