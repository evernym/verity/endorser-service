package com.evernym.endorser.domain

package object events {
  val endorsementTopic = "public.event.ssi.endorsement"
  val endorserTopic = "public.event.ssi.endorser"
  val publisherTopic = "public.event.ssi.publisher"
  val requestEndorsementTopic: String = "public.request.endorsement"
}
