package com.evernym.endorser.domain.events.request

import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.events.Event.EventType
import com.evernym.endorser.domain.events.{Event, requestEndorsementTopic}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object RequestEndorsement extends Event {
  val eventType: EventType = "request.endorsement.v1"
  val eventTopic: String = requestEndorsementTopic

  final case class DTO(txnRef: TransactionRef, ledgerPrefix: String)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}