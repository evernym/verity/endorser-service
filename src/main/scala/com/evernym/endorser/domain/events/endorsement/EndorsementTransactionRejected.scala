package com.evernym.endorser.domain.events.endorsement

import com.evernym.endorser.domain.aggregate.endorsement.EndorsementBehavior.EndorsementId
import com.evernym.endorser.domain.aggregate.endorsement.{RequestSource, TransactionRef}
import com.evernym.endorser.domain.codes.EventCode
import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import com.evernym.endorser.domain.events.{Event, endorsementTopic}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object EndorsementTransactionRejected extends Event {
  val eventType: EventType = "endorsement.transaction-rejected.v1"
  val eventTopic: EventTopic = endorsementTopic

  final case class DTO(endorsementId: EndorsementId, txnRef: TransactionRef, requestSource: Option[RequestSource], reason: EventCode)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}
