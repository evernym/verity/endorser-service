package com.evernym.endorser.domain.events

import com.evernym.endorser.util.LoggerUtil.logTryFailure
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper, PropertyNamingStrategy}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.scalalogging.Logger
import io.cloudevents.CloudEvent
import io.cloudevents.core.CloudEventUtils.mapData
import io.cloudevents.core.builder.CloudEventBuilder
import io.cloudevents.core.format.EventFormat
import io.cloudevents.core.provider.EventFormatProvider
import io.cloudevents.jackson.{JsonFormat, PojoCloudEventDataMapper}
import org.slf4j.event.Level

import java.net.URI
import java.time.OffsetDateTime.now
import java.time.ZoneId
import java.util.UUID
import scala.util.Try

abstract class Event {
  val logger: Logger = Logger(this.getClass)

  def eventType: String

  protected val jacksonMapper: ObjectMapper = new ObjectMapper()
    .registerModule(DefaultScalaModule)
    // This is to match cloud event naming convention
    .setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE)
    // Allows unknown properties
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    // No Nulls for required fields
    .configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, true)

  protected def build[T](source: URI, dto: T): CloudEvent = {
    val data = jacksonMapper.writeValueAsBytes(dto)

    CloudEventBuilder.v1()
      .withId(UUID.randomUUID().toString)
      .withType(eventType)
      .withSource(source)
      .withData("application/json",data)
      .withTime(now(ZoneId.of("UTC")))
      .build()
  }

  @SuppressWarnings(Array("AsInstanceOf"))
  protected def deserializeData[T](event: CloudEvent, classType: Class[_]): Try[T] = {
    Try {
      mapData(
        event,
        PojoCloudEventDataMapper.from(jacksonMapper, classType)
      ).getValue.asInstanceOf[T]
    }
    .recoverWith(logTryFailure(s"Unable to parse cloud event data block")(logger, level = Level.WARN))
  }
}

object Event {
  val logger: Logger = Logger(Event.getClass)
  type EventType = String
  type EventTopic = String

  protected val jsonFormat: EventFormat = EventFormatProvider
    .getInstance
    .resolveFormat(JsonFormat.CONTENT_TYPE)

  def serializeEvent(event: CloudEvent): Array[Byte] =
    jsonFormat.serialize(event)

  private def throwableToLoggable(throwable: Throwable): String =
    s"${throwable.getClass.getSimpleName}" +
      s"${Option(throwable.getCause).map(x => s" caused by ${x.getClass.getSimpleName}")
        .getOrElse("")}"


  def deserializeEvent(bytes: Array[Byte]): Try[CloudEvent] =
    Try(jsonFormat.deserialize(bytes))
      .recoverWith(
        logTryFailure(s"Unable to parse bytes into cloud event")
                     (logger, level = Level.WARN, throwableMessage = Some(throwableToLoggable))
      )
}
