package com.evernym.endorser.domain.events.endorser

import com.evernym.endorser.domain.events.Event.{EventTopic, EventType}
import com.evernym.endorser.domain.events.{Event, endorserTopic}
import io.cloudevents.CloudEvent

import java.net.URI
import scala.util.Try

object EndorserDeactivated extends Event {
  val eventType: EventType = "endorser.endorser-deactivated.v1"
  val eventTopic: EventTopic = endorserTopic

  final case class DTO(endorserDid: String,
                       ledgerPrefix: String)

  def build[T](source: URI, dto: DTO): CloudEvent = super.build(source, dto)

  def deserializeData(event: CloudEvent): Try[DTO] = super.deserializeData(event, classOf[DTO])
}
