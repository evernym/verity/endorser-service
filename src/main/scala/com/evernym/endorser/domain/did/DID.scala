package com.evernym.endorser.domain.did

import com.evernym.endorser.util.OptionUtil.blankOption

import scala.util.chaining.scalaUtilChainingOps

/*
Lightweight DID implementation that don't deal with DID Doc or other more complex DID use-case
 */
trait DID {
  def prefix: String
  def unqualifiedId: String
  def did: String
  def toString: String
}

final case class InvalidDIDException(message: String) extends Exception(message: String)

object DID {
  final private case class DIDImpl(unqualifiedId: String, prefix: String) extends DID {
    override def did: String = this.toString
    override def toString: String = s"$prefix:$unqualifiedId"
  }

  def fromString(didStr: String, ledgerPrefix: Option[String] = None): DID = {
    splitPrefix(didStr)
      .pipe(x => x.copy(_1 = x._1.orElse(ledgerPrefix))) // replace found prefix with given prefix
      .pipe {
        case (None, None) => throw InvalidDIDException(s"DID cannot be parsed from '$didStr' with given prefix $ledgerPrefix")
        case (Some(prefix), Some(id)) => DIDImpl(id, prefix)
        case (None, Some(id)) => throw InvalidDIDException(s"Unqualified DID '$didStr' with no given prefix")
        case (Some(prefix), None) => throw InvalidDIDException(s"No id for DID '$didStr', only prefix '$prefix' found")
      }
  }

  def splitPrefix(didStr: String): (Option[String], Option[String]) = {
    Option(didStr)
      .map{str =>
        str.lastIndexOf(':') match {
          case -1 => // Prefix Not found
            (None, blankOption(str))
          case 0 => // Prefix Not Found
            (None, blankOption(str.substring(0+1)))
          case i if i == str.length => // Only prefix found
            (Some(str.substring(0, i)), None)
          case i if i > 0 => // Prefix Found
            (Some(str.substring(0, i)), blankOption(str.substring(i+1)))
        }
      }
      .getOrElse((None, None))
  }
}