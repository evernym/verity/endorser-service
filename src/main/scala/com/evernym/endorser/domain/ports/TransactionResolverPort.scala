package com.evernym.endorser.domain.ports

import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef

import scala.concurrent.Future

class UnresolvableRefException(msg: String) extends Exception(msg)

trait TransactionResolverPort {
  def resolve(ref: TransactionRef): Future[String]

}
