package com.evernym.endorser.domain.ports

import com.evernym.endorser.domain.Finished

import java.net.URI
import scala.concurrent.Future

trait EventPublisherPort {
  def publishEvent(topic: String, event: Array[Byte], eventSource: URI, eventType: String, eventId: String): Future[Finished]
}
