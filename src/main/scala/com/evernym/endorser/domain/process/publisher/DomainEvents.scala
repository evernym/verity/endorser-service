package com.evernym.endorser.domain.process.publisher

import akka.cluster.sharding.ShardRegion.EntityId
import com.evernym.endorser.domain.Finished
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.domain.events.Event
import com.evernym.endorser.domain.events.publisher.{PublisherTransactionWritten, PublisherWriteFailed}
import com.evernym.endorser.domain.ports.EventPublisherPort
import io.cloudevents.CloudEvent

import java.net.URI
import scala.concurrent.Future

class DomainEvents(producer: EventPublisherPort)  {
  def produceEvent(topic: String, event: CloudEvent): Future[Finished] =
    producer.publishEvent(
      topic,
      Event.serializeEvent(event),
      event.getSource,
      event.getType,
      event.getId,
    )
  def publishTransactionWritten(publisherId: EntityId, endorsementId: String, result: Result): Future[Finished] = {
    produceEvent(
      PublisherTransactionWritten.eventTopic,
      PublisherTransactionWritten.build(
        source(publisherId),
        PublisherTransactionWritten.DTO(endorsementId, result.code.toEventCode)
      )
    )
  }

  def publishTransactionWriteFailed(publisherId: EntityId, endorsementId: String, result: Result): Future[Finished] = {
    produceEvent(
      PublisherWriteFailed.eventTopic,
      PublisherWriteFailed.build(
        source(publisherId),
        PublisherWriteFailed.DTO(endorsementId, result.code.toEventCode)
      )
    )
  }

  private def source(id: EntityId): URI = new URI(s"event-source://v1:ssi:publisher/$id")
}

object DomainEvents {
  def apply(publisher: EventPublisherPort): DomainEvents = new DomainEvents(publisher)
}
