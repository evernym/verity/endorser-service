package com.evernym.endorser.domain.process.publisher.valueobjects

import com.evernym.endorser.domain.codes.Codes.Code
import com.evernym.endorser.domain.codes.HasCodes
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.abbreviation

final case class Status(ledger: Ledger, code: Code)

object Status extends HasCodes{
  override def topic: String = "LDGR-STAT"

  def descriptions(code: Code): String = {
    code.code match {
      case 1 => "Connected to Ledger"
      case 2 => "Disconnected to Ledger"
      case 3 => "Unknown status for Ledger"
    }
  }

  val connected: Code = Code(abbreviation, topic, 1)
  val disconnected: Code = Code(abbreviation, topic, 2)
  val unknown: Code = Code(abbreviation, topic, 2)
}