package com.evernym.endorser.domain.process.publisher.ports

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.valueobjects.Status

import scala.concurrent.Future

trait PublishTransactionPort {
  def publishTransaction(txn: String): Future[Result]

  def connectionStatus(): Status

  def ledger: Ledger
}