package com.evernym.endorser.domain.process.publisher

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.cluster.sharding.ShardRegion.EntityId
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.{FailedEndorsement, Result, SuccessfulEndorsement}
import com.evernym.endorser.domain.ports.{EventPublisherPort, TransactionResolverPort, UnresolvableRefException}
import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort
import com.evernym.endorser.domain.process.publisher.valueobjects.Status
import com.evernym.endorser.domain.serialization.CborSerializable
import com.evernym.endorser.util.OptionUtil.blankOption

import scala.util.{Failure, Success}

object PublisherBehavior {
  val entityKey: EntityTypeKey[Commands] =
    EntityTypeKey[Commands]("Publisher")

  sealed trait Commands extends CborSerializable

  final case class ConnectionStatus(replyTo: ActorRef[Status]) extends Commands
  final case class SubmitTransactionRef(endorsementId: String, txnRef: TransactionRef) extends Commands
  final case class SubmitTransaction(endorsementId: String, txn: String) extends Commands
  protected final case class CompleteTransaction(endorsementId: String, result: Result) extends Commands
  protected final case class CompleteFailedTransaction(endorsementId: String, result: Result) extends Commands


  def apply(entityId: EntityId,
            txnPublish: PublishTransactionPort,
            txnResolver: TransactionResolverPort,
            eventPublisher: EventPublisherPort): Behavior[Commands] =
    Behaviors.setup { ctx =>
      val domainEvents = DomainEvents(eventPublisher)

      Behaviors.receiveMessage{ msg =>
        msg match {
          case ConnectionStatus(replyTo) =>
            replyTo ! txnPublish.connectionStatus()
          case SubmitTransactionRef(endorsementId, txnRef) =>
            ctx.pipeToSelf(txnResolver.resolve(txnRef)) {
              case Success(txn) => SubmitTransaction(endorsementId, txn)
              case Failure(exception) => exceptionToCmd(endorsementId, exception)
            }
          case SubmitTransaction(endorsementId, txn) =>
            blankOption(txn) match {
              case Some(t) =>
                ctx.pipeToSelf(txnPublish.publishTransaction(txn)) {
                  case Success(s: SuccessfulEndorsement)    =>  completeResult(endorsementId, s)
                  case Success(f: FailedEndorsement)        =>  completeResult(endorsementId, f)
                  case Failure(exception) =>  exceptionToCmd(endorsementId, exception)
                }
              case None => // blank transaction
                ctx.log.warn("Given a blank transaction -- unable to process it")
                domainEvents.publishTransactionWriteFailed(entityId, endorsementId, Results.blankTxn)
            }
          case CompleteTransaction(endorsementId, result) =>
            domainEvents.publishTransactionWritten(entityId, endorsementId, result)
          case CompleteFailedTransaction(endorsementId, result) =>
            domainEvents.publishTransactionWriteFailed(entityId, endorsementId, result)
        }

        Behaviors.same
      }
    }

  def completeResult(endorsementId: String, result: SuccessfulEndorsement): CompleteTransaction =
    CompleteTransaction(endorsementId, result)

  def completeResult(endorsementId: String, result: FailedEndorsement): CompleteFailedTransaction =
    CompleteFailedTransaction(endorsementId, result)

  def exceptionToCmd(endorsementId: String, ex: Throwable): CompleteFailedTransaction = {
    ex match {
      case e: UnresolvableRefException => CompleteFailedTransaction(endorsementId, Results.txnRefNotFound)
      case e => CompleteFailedTransaction(endorsementId, Results.unexpectedException(ex))
    }
  }
}
