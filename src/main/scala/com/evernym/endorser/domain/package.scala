package com.evernym.endorser

import scala.concurrent.{ExecutionContext, Future}

package object domain {
  type DID = did.DID

  final case class Finished(domainEventType: String)

  def chain[S](first: S => Future[Finished], second: S => Future[Finished])
              (state: S)
              (implicit executor: ExecutionContext): Future[Finished] = {
    first(state).flatMap(_ => second(state))
  }
}

