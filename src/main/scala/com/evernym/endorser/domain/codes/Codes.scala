package com.evernym.endorser.domain.codes

import com.fasterxml.jackson.annotation.JsonIgnore

import scala.collection.mutable
import scala.util.Try
import scala.util.chaining.scalaUtilChainingOps


object Codes {
  val delimiter = "_"
  val unparsableCode: Code = Code("CODE", "ERR", 1)

  final case class Code(aggregate: String, topic: String, code: Int, desc: Option[String] = None) {

    @JsonIgnore
    def description: String = desc.getOrElse(codeDescription(this))

    @JsonIgnore
    def isCode(cmpCode: Code): Boolean = equalCode(this, cmpCode)

    override def toString: String = {
      s"$aggregate$delimiter$topic$delimiter${"%04d".format(code)}"
    }

    @JsonIgnore
    def toEventCode: EventCode = EventCode(
      this.toString,
      Option(description)
    )
  }

  def apply(codeStr: String, descr: Option[String] = None): Code = {
    Try {
      codeStr
        .trim
        .split(delimiter)
        .pipe{ parts => Code(parts(0), parts(1), parts(2).toInt, descr)}
    }.getOrElse(unparsableCode)
  }

  def equalCode(code1: Code, code2: Code): Boolean = {
    code1.copy(desc = None) == code2.copy(desc = None)
  }

  private val codeDescMap: mutable.Map[String, Code => String] = mutable.Map.empty

  def registerCodes(codeTopic: String, toString: Code => String): Unit = {
    if (codeDescMap.contains(codeTopic)) {
      throw new RuntimeException("Duplicate Code Topics")
    }
    else {
      codeDescMap.put(codeTopic, toString)
    }
  }

  def codeDescription(code: Code): String =
    codeDescMap.get(code.topic).map(_(code)).getOrElse(s"Unknown Code -- ${code.toString}")
}
