package com.evernym.endorser.domain.codes

import com.evernym.endorser.domain.codes.Codes.Code

trait HasCodes {
  def topic: String
  def descriptions(code: Code): String

  Codes.registerCodes(topic, descriptions)
}
