package com.evernym.endorser.domain.codes

final case class EventCode(code: String, descr: Option[String] = None)