package com.evernym.endorser.util

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.node.{ContainerNode, MissingNode, NullNode}
import com.fasterxml.jackson.databind.{JsonNode, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import scala.util.Try

object JsonUtil {
  private val jacksonMapper = new ObjectMapper()
    .registerModule(DefaultScalaModule)
    .setSerializationInclusion(Include.NON_ABSENT)

  def jsonNode(json: String): Try[JsonNode] = {
    Try(jacksonMapper.readTree(json))
  }

  def jsonExtractString(json: String, path: String): Option[String] = {
    Try(jacksonMapper.readTree(json))
      .toOption
      .flatMap{tree =>
        tree.at(path) match {
          case _: MissingNode => None
          case _: NullNode => None
          case _: ContainerNode[_] => None
          case n =>
            val t = n
            Try(n.asText()).toOption
        }
      }
  }

  def simpleMapToJson(map: Map[String, Any]): String = {
    jacksonMapper.writeValueAsString(map)
  }

  def simpleObjectToJson(obj: Any): String = {
    jacksonMapper.writeValueAsString(obj)
  }
}
