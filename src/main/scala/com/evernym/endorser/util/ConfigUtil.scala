package com.evernym.endorser.util

import com.typesafe.scalalogging.Logger

import scala.util.{Failure, Success, Try}

object ConfigUtil {
  private val logger: Logger = Logger(ConfigUtil.getClass)

  def default[T](defaultValue: T)(tryToGetIt: => T): T = {
    Try {
      tryToGetIt
    } match {
      case Success(value) => value
      case Failure(exception) =>
        logger.debug(s"Fullback to default '$defaultValue' -- ${exception.getMessage}")
        defaultValue
    }
  }

}
