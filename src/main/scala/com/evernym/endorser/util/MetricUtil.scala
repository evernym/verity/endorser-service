package com.evernym.endorser.util

import akka.actor.typed.scaladsl.ActorContext
import com.lightbend.cinnamon.akka.typed.CinnamonMetrics

object MetricUtil {
  type Tags = Map[String, String]

  def recordTime(name: String, startTime: Long, endTime: Long, tags: Map[String, String] = Map.empty)
                (implicit ctx: ActorContext[_]): Unit = {
    CinnamonMetrics(ctx)
      .createRecorder(name, tags)
      .record(endTime - startTime)
  }

  def incrementCount(name: String, count: Long = 1L, tags: Map[String, String] = Map.empty)
                    (implicit ctx: ActorContext[_]): Unit = {
    CinnamonMetrics(ctx)
      .createCounter(name, tags)
      .increment(count)
  }
}
