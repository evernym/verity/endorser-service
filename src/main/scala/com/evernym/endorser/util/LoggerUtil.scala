package com.evernym.endorser.util

import akka.persistence.typed.PersistenceId
import com.evernym.endorser.domain.Finished
import com.typesafe.scalalogging.{Logger => ScalaLogger}
import org.slf4j.event.Level
import org.slf4j.{Logger => Slf4jLogger}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object LoggerUtil {
  def idStr(persistenceId: PersistenceId): String = persistenceId.id
  def className(o: Any): String = o.getClass.getSimpleName

  /*
  Care should be given since the breaks the macro protection by Scala Logging to not create string for log messages
  that will not be logged.
   */
  def logTryFailure[U](msg: => String)
                      (logger: ScalaLogger,
                       level: Level = Level.ERROR,
                       throwableMessage: Option[Throwable => String] = None
                      ): PartialFunction[Throwable, Try[U]] = {
    case e: Throwable =>
      logToScalaLoggerLevel(
        logger,
        msg,
        level,
        Some(e),
        throwableMessage
          .map(_ (e))
      )
      Failure(e)
  }

  /*
  Care should be given since the breaks the macro protection by Scala Logging to not create string for log messages
  that will not be logged.
   */
  def logFutureFailure[U](msg: => String)(logger: ScalaLogger, level: Level = Level.ERROR): PartialFunction[Throwable, Future[U]] = {
    case e: Throwable =>
      logToScalaLoggerLevel(logger, msg, level, Some(e))
      Future.failed(e)
  }

  private def logToScalaLoggerLevel(logger: ScalaLogger,
                                    msg: String,
                                    level: Level,
                                    throwable: Option[Throwable] = None,
                                    throwableMessage: Option[String] = None
                                   ): Unit = {
    level match {
      case Level.TRACE =>
        Option(throwable).flatten match {
          case Some(e) => throwableMessage match {
            case Some(tmsg) => logger.trace(s"$msg -- $tmsg")
            case _ => logger.trace(msg, e)
          }
          case None => logger.trace(msg)
        }
      case Level.DEBUG =>
        Option(throwable).flatten match {
          case Some(e) => throwableMessage match {
            case Some(tmsg) => logger.debug(s"$msg -- $tmsg")
            case _ => logger.debug(msg, e)
          }
          case None => logger.debug(msg)
        }
      case Level.INFO =>
        Option(throwable).flatten match {
          case Some(e) => throwableMessage match {
            case Some(tmsg) => logger.info(s"$msg -- $tmsg")
            case _ => logger.info(msg, e)
          }
          case None => logger.info(msg)
        }
      case Level.WARN =>
        Option(throwable).flatten match {
          case Some(e) => throwableMessage match {
            case Some(tmsg) => logger.warn(s"$msg -- $tmsg")
            case _ => logger.warn(msg, e)
          }
          case None => logger.warn(msg)
        }
      case Level.ERROR =>
        Option(throwable).flatten match {
          case Some(e) => throwableMessage match {
            case Some(tmsg) => logger.error(s"$msg -- $tmsg")
            case _ => logger.error(msg, e)
          }
          case None => logger.error(msg)
        }
    }
  }

  def logToSlf4LoggerLevel(logger: Slf4jLogger, msg: String, level: Level, exception: Option[Throwable] = None): Unit = {
    level match {
      case Level.TRACE => doLog(msg, logger.trace, logger.trace, exception)
      case Level.DEBUG => doLog(msg, logger.debug, logger.debug, exception)
      case Level.INFO => doLog(msg, logger.info, logger.info, exception)
      case Level.WARN => doLog(msg, logger.warn, logger.warn, exception)
      case Level.ERROR => doLog(msg, logger.error, logger.error, exception)
    }
  }

  private def doLog(msg: String, loggingFuc: String => Unit, loggingExceptionFuc: (String, Throwable) => Unit, exception: Option[Throwable]): Unit = {
    Option(exception).flatten match {
      case Some(e) => loggingExceptionFuc(msg, e)
      case None => loggingFuc(msg)
    }
  }

  def logDomainEvent(id: String, logger: ScalaLogger): PartialFunction[Try[Finished], Unit] = {
    case Success(done) =>
      logger.info(s"[$id] Done sending domain event - ${done.domainEventType}")
    case Failure(errors) =>
      logger.error(s"[$id] Failed to send domain event - $errors")
  }

}
