package com.evernym.endorser.util

import scala.util.{Failure, Success, Try}

object TryUtils {
  def getOrThrow[T](t: Try[T]): T = t match {
    case Success(v) => v
    case Failure(e) => throw e
  }

}
