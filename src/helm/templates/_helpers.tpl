{{/*
Expand the name of the chart.
*/}}
{{- define "app.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "app.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Values.service .Values.env | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end }}

{{/*
Vault injection template
*/}}
{{- define "app.vaultInjectTemplate" -}}
{{`{{ with secret "`}}{{ .Secret }}{{`" -}}`}}
{{`{{ range $k, $v := .Data.data -}}
  export {{ $k }}='{{ $v }}'
{{ end }}
{{- end }}`}}
{{- end }}
