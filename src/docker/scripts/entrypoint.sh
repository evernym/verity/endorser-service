#! /bin/bash

trap stop_sig SIGINT SIGTERM SIGQUIT SIGABRT

APP_NAME="endorser"
APP_CONFIG_DIR="/etc/$APP_NAME/$APP_NAME-application"
BIN_DIR="/usr/lib/$APP_NAME-application"

function stop_sig {
    echo "Caught a shutdown signal..."
    if [ -n "$PID" ] ; then
        echo "Signaling $APP_NAME to stop gracefully"
        kill -TERM "$PID"
    fi
}

# Generate a list of the vault injected files for the app, and ensure that
# app-overrides is always sourced last if it exists
VAULT_SECRETS=($( ls /vault/secrets/app-* | sed 's/.*\/app-overrides//' ))
VAULT_SECRETS+=( "/vault/secrets/app-overrides" )

# Source secret files injected from Vault (if they exist)
for f in "${VAULT_SECRETS[@]}" ; do
    if [ -f "$f" ]; then
        # shellcheck disable=SC1090
        source "$f"
    fi
done

# Start Application
/usr/bin/java \
    -javaagent:/usr/lib/endorser-application/cinnamon-agent.jar \
    -Dlogback.statusListenerClass=ch.qos.logback.core.status.OnConsoleStatusListener \
    -cp $APP_CONFIG_DIR:$APP_CONFIG_DIR/config-map:$BIN_DIR/$APP_NAME-assembly.jar \
    com.evernym.$APP_NAME.application.Main &
PID=$!

wait $PID
trap - SIGINT SIGTERM SIGQUIT SIGABRT
wait $PID
