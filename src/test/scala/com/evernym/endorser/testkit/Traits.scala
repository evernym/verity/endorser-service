package com.evernym.endorser.testkit

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import com.typesafe.config.Config

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

object Traits {
  trait ObjectTester {
    def test[Obj](s: Obj)(f: Obj => Unit = noOp(_)): Obj = {
      f(s)
      s
    }
  }

  trait TestActorSystem {
    def withActorSystem(config: Config)(f: ActorSystem[_] => Unit): Unit = {
      val as: ActorSystem[_] = ActorSystem(Behaviors.empty[Any], "TEST_AS", config)
      try {
        f(as)
      }
      finally {
        as.terminate()
        Await.result(as.whenTerminated, 10 seconds)
      }
    }
  }
}
