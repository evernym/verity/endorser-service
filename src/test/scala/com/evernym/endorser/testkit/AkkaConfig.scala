package com.evernym.endorser.testkit

import com.typesafe.config.{Config, ConfigFactory}

object AkkaConfig {
  def withoutClustering(wrappedConfig: Config = ConfigFactory.empty()): Config = {
    ConfigFactory.parseString("""
    akka.actor.provider = local
    """).withFallback(wrappedConfig)
  }

  def withSalts(wrappedConfig: Config = ConfigFactory.empty()): Config = {
    ConfigFactory.parseString("""
    endorser.secret.wallet-key-salt = "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW"
    """).withFallback(wrappedConfig)
  }
}
