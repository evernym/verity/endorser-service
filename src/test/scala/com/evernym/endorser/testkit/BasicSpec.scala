package com.evernym.endorser.testkit

import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should._

trait BasicSpec extends AnyFreeSpecLike with Matchers