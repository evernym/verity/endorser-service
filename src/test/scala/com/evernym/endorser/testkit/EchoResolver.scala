package com.evernym.endorser.testkit

import com.evernym.endorser.domain.aggregate.endorsement.TransactionRef
import com.evernym.endorser.domain.ports.TransactionResolverPort

import scala.concurrent.Future

object EchoResolver extends TransactionResolverPort{
  override def resolve(ref: TransactionRef): Future[String] = Future.successful(ref)
}
