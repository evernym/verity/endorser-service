package com.evernym.endorser.testkit

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.Behavior
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit.CommandResult
import com.evernym.endorser.testkit.AkkaConfig.{withSalts, withoutClustering}

import scala.reflect.ClassTag

abstract class EventSourcedTestKitHelper[Command, Event, State]
  extends ScalaTestWithActorTestKit(
    withSalts(withoutClustering(EventSourcedBehaviorTestKit.config))
  )
//    with BeforeAndAfterEach
    {

  def testBehavior: Behavior[Command]

  implicit lazy val kit: EventSourcedBehaviorTestKit[Command, Event, State] = {
    EventSourcedBehaviorTestKit[Command, Event, State](system ,testBehavior)
  }

//  override def beforeEach(): Unit = {
//    super.beforeEach()
//    kit.clear()
//  }

  def send(command: Command)
          (implicit kit: EventSourcedBehaviorTestKit[Command, Event, State]): CommandResultWrapper[Command, Event, State] = {
    new CommandResultWrapper(kit.runCommand(command))
  }
}

class CommandResultWrapper[Command, Event, State](val cmdResult: CommandResult[Command, Event, State]) {
  def eventOfType[E <: Event: ClassTag]: E = cmdResult.eventOfType[E]

  def expectEventOf[E <: Event: ClassTag](f: E => Unit = noOp(_)): CommandResultWrapper[Command, Event, State] = {
    val resultEvent = cmdResult.eventOfType[E]
    f(resultEvent)
    this
  }

  def stateOfType[S <: State: ClassTag]: S = cmdResult.stateOfType[S]

  def expectStateOf[S <: State: ClassTag](f: S => Unit = noOp(_)): CommandResultWrapper[Command, Event, State] = {
    val resultState = cmdResult.stateOfType[S]
    f(resultState)
    this
  }
}
