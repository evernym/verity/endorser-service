package com.evernym.endorser.testkit

import com.fasterxml.jackson.databind.{JsonNode, ObjectMapper}
import org.scalatest.Assertions
import org.scalatest.compatible.Assertion
import org.scalatest.exceptions.TestFailedException

trait JsonCompare extends Assertions {
  private val jacksonMapper = new ObjectMapper()

  def assertJsonEqual(expected: String, observed: String): Assertion = {
    val node1: JsonNode = jacksonMapper.readTree(expected)
    val node2: JsonNode = jacksonMapper.readTree(observed)
    try {
      assert(node1.equals(node2))
    }
    catch {
      case e: TestFailedException =>
        println("Expected JSON: ")
        println(expected)
        println("Observed JSON: ")
        println(observed)

        throw e
      case e: Throwable => throw e
    }
  }
}
