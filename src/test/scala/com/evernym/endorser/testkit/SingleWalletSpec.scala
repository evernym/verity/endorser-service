package com.evernym.endorser.testkit

import com.evernym.endorser.infrastructure.sharedlib.VdrTools
import com.evernym.vdrtools.wallet.Wallet
import org.scalatest.BeforeAndAfterAll
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

trait SingleWalletSpec extends BasicSpec with BeforeAndAfterAll {
  val vdrToolsAvailable: Boolean = Await.result(
    VdrTools()
      .initialize()
      .recover{case _ => false},
    5 seconds
  )

  protected val WALLET = "Wallet1"
  protected val TYPE = "default"
  protected val WALLET_CONFIG: String = "{ \"id\":\"" + WALLET + "\", \"storage_type\":\"" + TYPE + "\"}"
  protected val WALLET_CREDENTIALS: String = "{\"key\":\"8dvfYSt5d1taSd6yJdpjq4emkwsPDDLYxkNFysFD2cZY\", \"key_derivation_method\":\"RAW\"}"

  lazy val wallet: Wallet = {
      Wallet.createWallet(WALLET_CONFIG, WALLET_CREDENTIALS).get
      Wallet.openWallet(WALLET_CONFIG, WALLET_CREDENTIALS).get
  }

  override protected def beforeAll(): Unit = {
    try{
      Wallet.deleteWallet(WALLET_CONFIG, WALLET_CREDENTIALS).get()
    } catch {
      case e: Throwable => // do nothing
    }
  }

  override protected def afterAll(): Unit = {
    if(vdrToolsAvailable){
      wallet.closeWallet.get
      Wallet.deleteWallet(WALLET_CONFIG, WALLET_CREDENTIALS).get
    }
  }
}