package com.evernym.endorser

package object testkit {
  //noinspection ScalaUnusedSymbol the noOp requires a certain method signature
  def noOp(a: Any): Unit = ()
}
