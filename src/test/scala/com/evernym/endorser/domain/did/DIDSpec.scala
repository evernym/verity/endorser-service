package com.evernym.endorser.domain.did

import org.scalatest.OptionValues
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class DIDSpec extends AnyFreeSpec with Matchers with OptionValues {

  "parsing" - {
    "can parse a fully qualified did without prefix" in {
      val t = DID.fromString("did:indy:sovrin:builder:63MGv4YcFok2zJCXBsuY6D")
      t.unqualifiedId shouldBe "63MGv4YcFok2zJCXBsuY6D"
      t.prefix shouldBe "did:indy:sovrin:builder"
      t.did shouldBe "did:indy:sovrin:builder:63MGv4YcFok2zJCXBsuY6D"
      t.did shouldBe t.toString

      val t2 = DID.fromString("did:indy:sovrin:Nvq1m5UgGY7mNVUjNB7G55")
      t2.unqualifiedId shouldBe "Nvq1m5UgGY7mNVUjNB7G55"
      t2.prefix shouldBe "did:indy:sovrin"
      t2.did shouldBe "did:indy:sovrin:Nvq1m5UgGY7mNVUjNB7G55"
    }

    "throws expected exceptions" in {
      assertThrows[InvalidDIDException] (DID.fromString(""))
      assertThrows[InvalidDIDException] (DID.fromString("Nvq1m5UgGY7mNVUjNB7G55"))
      assertThrows[InvalidDIDException] (DID.fromString("did:indy:sovrin:"))
      assertThrows[InvalidDIDException] (DID.fromString(null))
    }

    "splitPrefix" - {
      "can split a fully qualified DID" in {
        DID.splitPrefix("did:indy:sovrin:builder:63MGv4YcFok2zJCXBsuY6D") shouldBe
          (Some("did:indy:sovrin:builder"), Some("63MGv4YcFok2zJCXBsuY6D"))
      }
      "can split only prefix" in {
        DID.splitPrefix("did:indy:sovrin:builder:") shouldBe
          (Some("did:indy:sovrin:builder"), None)
      }

      "can split unqualified DID" in {
        DID.splitPrefix("63MGv4YcFok2zJCXBsuY6D") shouldBe
          (None, Some("63MGv4YcFok2zJCXBsuY6D"))

        DID.splitPrefix(":63MGv4YcFok2zJCXBsuY6D") shouldBe
          (None, Some("63MGv4YcFok2zJCXBsuY6D"))
      }
      "can split edge cases" in {
        DID.splitPrefix(":") shouldBe (None, None)
        DID.splitPrefix(null) shouldBe (None, None)
        DID.splitPrefix("") shouldBe (None, None)
      }
    }
  }

}
