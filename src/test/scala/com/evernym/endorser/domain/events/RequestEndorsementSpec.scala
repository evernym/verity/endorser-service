package com.evernym.endorser.domain.events

import com.evernym.endorser.domain.events.request.RequestEndorsement
import com.evernym.endorser.domain.events.request.RequestEndorsement.eventType
import io.cloudevents.core.builder.CloudEventBuilder
import io.cloudevents.rw.CloudEventRWException
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import java.net.URI
import java.time.OffsetDateTime.now
import java.time.ZoneId
import java.util.UUID

class RequestEndorsementSpec extends AnyFreeSpec with Matchers {
  val exampleSource: URI = URI.create("http://example.com")

  "can create a useful event" in {
    val data = RequestEndorsement.DTO("https://pastebin.com/raw/E7WEEsc4", "did:sov")
    val event = RequestEndorsement.build(exampleSource, data)
    event.getType shouldBe RequestEndorsement.eventType
  }

  "raises useful exception if invalid DTO" in {
    val event = CloudEventBuilder.v1()
      .withId(UUID.randomUUID().toString)
      .withType(eventType)
      .withSource(exampleSource)
      .withData("application/json","{\"a\":\"b\"}".getBytes())
      .withTime(now(ZoneId.of("UTC")))
      .build()

    assertThrows[CloudEventRWException] {
      RequestEndorsement.deserializeData(event).get
    }
  }

}
