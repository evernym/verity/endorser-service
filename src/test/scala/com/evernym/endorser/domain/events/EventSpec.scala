package com.evernym.endorser.domain.events

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.events.endorsement.EndorsementComplete
import com.evernym.endorser.testkit.JsonCompare
import org.scalatest.freespec.AnyFreeSpec

import java.net.URI

class EventSpec extends AnyFreeSpec with JsonCompare {

  "serializeEvent" in {
    val expectedJson = """{
                         |   "specversion":"1.0",
                         |   "source":"http://www.example.com",
                         |   "type":"endorsement.endorsement-complete.v1",
                         |   "datacontenttype":"application/json",
                         |   "data":{
                         |      "endorsementid": "c97144fb-d348-422e-bc9b-ee1893f8bad1",
                         |      "requestsource":"temp:foo",
                         |      "submitterdid":"35428J9E36DGJgyfULLFNC",
                         |      "result":{
                         |         "code":"ENDT_RES_0001",
                         |         "descr":"Transaction Written to VDR (normally a ledger)"
                         |      }
                         |   }
                         |}""".stripMargin

    val testEvent = EndorsementComplete.build(
      new URI("http://www.example.com"),
      EndorsementComplete.DTO("c97144fb-d348-422e-bc9b-ee1893f8bad1", Some("temp:foo"), Some("35428J9E36DGJgyfULLFNC"),Results.writtenToVDR.toEventCode)
    )
    val s = new String(Event.serializeEvent(testEvent))
      .replaceFirst(""""time":".*?"?,""" , "") // strip time because it is not deterministic
      .replaceFirst(""""id":".*?"?,""" , "") // strip id because it is not deterministic

    assertJsonEqual(s, expectedJson)
  }

}
