package com.evernym.endorser.domain.aggregate.endorser

import akka.actor.testkit.typed.scaladsl.LogCapturing
import akka.actor.typed.Behavior
import akka.persistence.typed.PersistenceId
import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.aggregate.TestObjects._
import com.evernym.endorser.domain.aggregate.endorser.ports._
import com.evernym.endorser.domain.events.endorser.{EndorserCreated, EndorserDIDGenerated, EndorserKeyRotated, TransactionNotPrepared, TransactionPrepared}
import com.evernym.endorser.domain.testkit.MockEventPublisher
import com.evernym.endorser.testkit.{EchoResolver, EventSourcedTestKitHelper}
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchersSugar.{argMatching, argThat, eqTo}
import org.mockito.MockitoSugar
import org.scalatest.concurrent.Eventually
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, OptionValues}

import java.net.URI
import scala.concurrent.Future
import scala.language.reflectiveCalls

class EndorserBehaviorSpec
  extends EventSourcedTestKitHelper[Commands.EndorserCommand, Events.EndorserEvents, States.EndorserState]
    with AnyFreeSpecLike
    with BeforeAndAfterEach
    with LogCapturing
    with MockitoSugar
    with OptionValues
    with Eventually
    with BeforeAndAfterAll
    with MockEventPublisher {

  private val signerAdapter = mock[SignerPort]
  private val mockHoster = mock[TransactionStoragePort]

  private val staticHosterUrl = "http://example.com/Sf8sE"

  private val persistenceId = PersistenceId("Endorser", "2349873")

  lazy val testBehavior: Behavior[Commands.EndorserCommand] =
    EndorserBehavior(
      persistenceId,
      mockPublishEvent,
      signerAdapter,
      EchoResolver,
      mockHoster
    )

  def resetMock(): Unit = {
    resetEventPublisherMock()
    reset(signerAdapter)
    reset(mockHoster)

    when(signerAdapter.newDID())
      .thenReturn(Future.successful(endorserDid.did))

    when(signerAdapter.newDID(
      any[Option[String]]
    )).thenReturn(Future.successful(endorserDid.did))

    when(signerAdapter.newDID(
      Some("delay")
    )).thenAnswer({
      Thread.sleep(3000)
      Future.successful(endorserDid.did)
    })

    when(signerAdapter.rotateKeyWithSeed(
      any[String],
      any[String]
    )).thenAnswer({
      Future.successful("GfWXLeCfU8Hg74V7ZjegdbYK3ACxPCUk1VwXjqZg7TvM")
    })

    when(signerAdapter.prepareTxn(
      any[String],
      any[String],
      any[Option[DID]]
    )).thenAnswer[String, String]({ (txn, _) =>
      if (txn == validIndyTxn) {
        Future.successful(validIndyTxn)
      } else {
        Future.failed(new Exception("Unable to prepare transaction"))
      }
    })

    when(mockHoster.store(any[String]))
      .thenReturn(Future.successful(staticHosterUrl))
  }

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    kit.clear()
    resetMock()
  }

  "Endorser Aggregate" - {
    "should start with empty state" in {
      kit.getState() shouldBe States.Empty
      kit.restart().state shouldBe kit.getState()
    }

    "should create endorser" - {
      "should transition to Create state after endorser created" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
      }
    }

    "Should activate endorser" - {
      "Should create endorser DID" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
      }

      "Should transition to 'Active' state after 'activate endorser' command" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
      }

      "Should store endorser DID and ledger prefix in state" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .expectStateOf[States.Active] { state =>
            state.endorserDID shouldBe endorserDid.did
            state.ledgerPrefix shouldBe builderNetPrefix
          }
      }
    }

    "Should activate endorser when creating DID from seed" - {
      "should generate DID from seed" in {
        val seed = "673e371096e1c649972e504db3b6dee884e2c3353403c136d49fe51cf45ad2bd"
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDIDWithSeed(builderNetPrefix, seed))
        eventually {
          kit.getState() shouldBe a[States.Active]
        }

        verify(signerAdapter, atLeastOnce).newDID(Some(seed))
      }
    }

    "should prepare a transaction" - {
      "should stay in Active state after transaction is prepared" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
        send(Commands.PrepareTransaction(validIndyTxn, builderNetPrefix, endorsementId, None))
          .stateOfType[States.Active]
      }

      "should handle resolving transaction from a reference" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
        send(Commands.PrepareTransactionRef(validIndyTxn, builderNetPrefix, endorsementId, None))
          .stateOfType[States.Active]

        eventually {
          verify(mockPublishEvent, atLeastOnce).publishEvent(
            eqTo(TransactionPrepared.eventTopic),
            argThat(
              testDTO(TransactionPrepared.deserializeData){ dto =>
                dto.endorserDid.value shouldBe endorserDid.did
              }
            ),
            any[URI],
            any[String],
            any[String]
          )
        }
      }
    }
    "Should rotate key" - {
      "Should move to new verkey" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]

        send(Commands.RotateKeyWithSeed("test-seed"))
          .stateOfType[States.Active]

        eventually {
          verify(mockPublishEvent, atLeastOnce).publishEvent(
            eqTo(EndorserKeyRotated.eventTopic),
            argThat(
              testDTO(EndorserKeyRotated.deserializeData){ dto =>
                dto.newverkey shouldBe "GfWXLeCfU8Hg74V7ZjegdbYK3ACxPCUk1VwXjqZg7TvM"
              }
            ),
            any[URI],
            argMatching{case EndorserKeyRotated.eventType => },
            any[String]
          )
        }
      }

    }
    "Should deactivate" - {
      "Should transition to Inactive state after deactivation command" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
        send(Commands.DeactivateEndorser())
          .expectStateOf[States.Inactive] { state =>
            state.endorserDID shouldBe endorserDid.did
          }
      }

      "Should Activate from Inactive State" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
        send(Commands.DeactivateEndorser())
          .expectStateOf[States.Inactive] { state =>
            state.endorserDID shouldBe endorserDid.did
          }
        send(Commands.ActivateEndorser())
          .expectStateOf[States.Active] { state =>
            state.endorserDID shouldBe endorserDid.did
          }
      }
    }

    "Should avoid state transitions when issued incompatible commands" - {
      "Should remain in created state if activation command is sent before endorser did is created" in {
        send(Commands.CreateEndorser())
        send(Commands.ActivateEndorser())
          .stateOfType[States.Created]
      }

      "Should remain in empty state if createDID command is sent before endorser creation" in {
        send(Commands.CreateEndorserDID(builderNetPrefix))
        kit.getState() shouldBe States.Empty
      }

      "Should remain in active state if activate command is sent repeatedly" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
      }

      "Should remain in deactivated state if deactivate command is sent repeatedly" in {
        send(Commands.CreateEndorser())
          .stateOfType[States.Created]
        send(Commands.CreateEndorserDID(builderNetPrefix))
        eventually {
          kit.getState() shouldBe a[States.Inactive]
        }
        send(Commands.ActivateEndorser())
          .stateOfType[States.Active]
        send(Commands.DeactivateEndorser())
          .expectStateOf[States.Inactive] { state =>
            state.endorserDID shouldBe endorserDid.did
          }
        send(Commands.DeactivateEndorser())
          .expectStateOf[States.Inactive] { state =>
            state.endorserDID shouldBe endorserDid.did
          }
      }
    }

    "should publish domain event 'Endorser Created' after endorser is created" in {

      send(
        Commands.CreateEndorser()
      )

      verify(mockPublishEvent, atLeastOnce).publishEvent(
        eqTo(EndorserCreated.eventTopic),
        argThat(matchesType(EndorserCreated.eventType)),
        any[URI],
        any[String],
        any[String]
      )
    }

    "should publish domain event 'Endorser DID Created' after endorser did is generated" in {

      send(
        Commands.CreateEndorser()
      )

      send(
        Commands.CreateEndorserDID(builderNetPrefix)
      )

      eventually{
        kit.getState() shouldBe a[States.Inactive]
      }

      verify(mockPublishEvent, atLeastOnce).publishEvent(
        eqTo(EndorserDIDGenerated.eventTopic),
        argThat(matchesDTO(
          EndorserDIDGenerated.DTO(
            endorserDid.did,
            builderNetPrefix,
          ),
          EndorserDIDGenerated.deserializeData
        )),
        any[URI],
        any[String],
        any[String]
      )
    }

    "should publish domain event 'Transaction Preparation Failed' if transaction preparation fails" in {
      val requesterSource = Some("protocol://FqvqDaGvQDeczjR29ohLFa/34c7e231-5784-4248-a40b-3b6c1dae2819")

      send(Commands.CreateEndorser())
      send(Commands.CreateEndorserDID(builderNetPrefix))
      send(Commands.ActivateEndorser())
      send(
        Commands.PrepareTransaction(
          validIndyTxn2,
          builderNetPrefix,
          endorsementId,
          requesterSource
        )
      )
        .stateOfType[States.Active]

      eventually {
        verify(mockPublishEvent, atLeastOnce).publishEvent(
          eqTo(TransactionNotPrepared.eventTopic),
          argThat(matchesType(TransactionNotPrepared.eventType)),
          any[URI],
          any[String],
          any[String]
        )
      }
    }

    "should publish domain event 'Transaction Prepared' when transaction prepared" in {
      val requesterSource = Some("protocol://FqvqDaGvQDeczjR29ohLFa/34c7e231-5784-4248-a40b-3b6c1dae2819")

      send(Commands.CreateEndorser())
      send(Commands.CreateEndorserDID(builderNetPrefix))
      send(Commands.ActivateEndorser())
      send(
        Commands.PrepareTransaction(
          validIndyTxn,
          builderNetPrefix,
          endorsementId,
          requesterSource
        )
      )


      eventually {
        verify(mockPublishEvent, atLeastOnce).publishEvent(
          eqTo(TransactionPrepared.eventTopic),
          argThat(
            testDTO(TransactionPrepared.deserializeData){ dto =>
              dto.endorserDid.value shouldBe endorserDid.did
            }
          ),
          any[URI],
          any[String],
          any[String]
        )
      }
    }

    "should publish domain event 'Transaction Not Prepared' when transaction preparation command is received while inactive" in {
      val requesterSource = Some("protocol://FqvqDaGvQDeczjR29ohLFa/34c7e231-5784-4248-a40b-3b6c1dae2819")

      send(Commands.CreateEndorser())
      send(Commands.CreateEndorserDID(builderNetPrefix))
      send(Commands.ActivateEndorser())
      send(Commands.DeactivateEndorser())
        .stateOfType[States.Inactive]
      send(
        Commands.PrepareTransaction(
          validIndyTxn,
          builderNetPrefix,
          endorsementId,
          requesterSource
        )
      )

      eventually {
        verify(mockPublishEvent, atLeastOnce).publishEvent(
          eqTo(TransactionNotPrepared.eventTopic),
          argThat(matchesType(TransactionNotPrepared.eventType)),
          any[URI],
          any[String],
          any[String]
        )
      }
    }
    "should publish domain event 'Transaction Preparation Failed' when complete transaction preparations command is received after deactivating" in {
      val requesterSource = Some("protocol://FqvqDaGvQDeczjR29ohLFa/34c7e231-5784-4248-a40b-3b6c1dae2819")

      send(Commands.CreateEndorser())
      send(Commands.CreateEndorserDID(builderNetPrefix))
      send(Commands.ActivateEndorser())
      send(
        Commands.PrepareTransaction(
          "delayedSuccess",
          builderNetPrefix,
          endorsementId,
          requesterSource
        )
      )
      send(Commands.DeactivateEndorser())

      eventually {
        eventually {
          verify(mockPublishEvent, atLeastOnce).publishEvent(
            eqTo(TransactionNotPrepared.eventTopic),
            argThat(matchesType(TransactionNotPrepared.eventType)),
            any[URI],
            any[String],
            any[String]
          )
        }
      }

    }
    "Should call close when actor is restarted" in {
      kit.restart()

      verify(signerAdapter, atLeastOnce).close()
    }
  }
}
