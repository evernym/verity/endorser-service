package com.evernym.endorser.domain.aggregate

import com.evernym.endorser.domain.DID
import com.evernym.endorser.domain.did.DID

object TestObjects {
  val endorsementId = "8fc5d3b7-70cf-4fe7-9489-f41af82de0f2"

  val builderNetPrefix = "did:indy:sovrin:builder"

  val submitterDid: DID = DID.fromString("did:indy:sovrin:builder:Pw3547NjqCEF94iPM43T46")
  val unqualifiedSubmitterDid: String = "Pw3547NjqCEF94iPM43T46"

  val endorserDid: DID = DID.fromString("did:indy:sovrin:builder:P9prvGXKFvCdajeXfBivi4")
  val unqualifiedEndorserDid: String = "P9prvGXKFvCdajeXfBivi4"

  val endorserDid2: DID = DID.fromString("did:indy:sovrin:builder:2fSJLv7CErChuAtysJBvaF")

  val validIndyTxn: String =
    """{
      |   "endorser":"P9prvGXKFvCdajeXfBivi4",
      |   "identifier":"Pw3547NjqCEF94iPM43T46",
      |   "operation":{
      |      "dest":"ND6fMqptp2MsVSZmRSbPR8",
      |      "type":"1",
      |      "verkey":"CZSp1LFDezCAPMAij8eGaGpP3oYUjYWzwN5XctNV6EJC"
      |   },
      |   "protocolVersion":2,
      |   "reqId":1646167413258037924,
      |   "signature":"2fGDmoGwcQ6FR6hRFpLB8xdKhPJYJA2g77fjC7nkzKeSZUtbBrShqKzVZLjun7At5CuU5TMzRKtMJ1opyyGp6h6o"
      |}""".stripMargin

  val validIndyTxn2: String =
    """{
      |   "endorser":"2fSJLv7CErChuAtysJBvaF",
      |   "identifier":"XU1EwgFHFjKcckHc8sbHsE",
      |   "operation":{
      |      "dest":"KiJ9NacXWNftnbJpnr2UWM",
      |      "type":"1",
      |      "verkey":"CZSp1LFDezCAPMAij8eGaGpP3oYUjYWzwN5XctNV6EJC"
      |   },
      |   "protocolVersion":2,
      |   "reqId":1646167413258037924,
      |   "signature":"2fGDmoGwcQ6FR6hRFpLB8xdKhPJYJA2g77fjC7nkzKeSZUtbBrShqKzVZLjun7At5CuU5TMzRKtMJ1opyyGp6h6o"
      |}""".stripMargin


  val validIndyTxnWithEndorsement: String = validIndyTxn
  val validIndyTxnWithEndorsement2: String = validIndyTxn2

  val invalidJSONIndyTxn: String =
    """{
      |   "endorser:"P9prvGXKFvCdajeXfBivi4",
      |   "identifier":"Pw3547NjqCEF94iPM43T46"
      |   "operation":{
      |      "dest":"ND6fMqptp2MsVSZmRSbPR8",
      |      "type":"1",
      |      "verkey":"CZSp1LFDezCAPMAij8eGaGpP3oYUjYWzwN5XctNV6EJC"
      |   },
      |   "protocolVersion":2,
      |   "reqId":1646167413258037924,
      |   "signature":"2fGDmoGwcQ6FR6hRFpLB8xdKhPJYJA2g77fjC7nkzKeSZUtbBrShqKzVZLjun7At5CuU5TMzRKtMJ1opyyGp6h6o"
      |}""".stripMargin

  val invalidWithoutEndorserIndyTxn: String =
    """{
      |   "identifier":"Pw3547NjqCEF94iPM43T46",
      |   "operation":{
      |      "dest":"ND6fMqptp2MsVSZmRSbPR8",
      |      "type":"1",
      |      "verkey":"CZSp1LFDezCAPMAij8eGaGpP3oYUjYWzwN5XctNV6EJC"
      |   },
      |   "protocolVersion":2,
      |   "reqId":1646167413258037924,
      |   "signature":"2fGDmoGwcQ6FR6hRFpLB8xdKhPJYJA2g77fjC7nkzKeSZUtbBrShqKzVZLjun7At5CuU5TMzRKtMJ1opyyGp6h6o"
      |}""".stripMargin

  val invalidWithUnsupportedTransactionType: String =
    """{
      |   "endorser":"2fSJLv7CErChuAtysJBvaF",
      |   "identifier":"XU1EwgFHFjKcckHc8sbHsE",
      |   "operation":{
      |      "dest":"KiJ9NacXWNftnbJpnr2UWM",
      |      "type":"NOT_A_NUMBER",
      |      "verkey":"CZSp1LFDezCAPMAij8eGaGpP3oYUjYWzwN5XctNV6EJC"
      |   },
      |   "protocolVersion":2,
      |   "reqId":1646167413258037924,
      |   "signature":"2fGDmoGwcQ6FR6hRFpLB8xdKhPJYJA2g77fjC7nkzKeSZUtbBrShqKzVZLjun7At5CuU5TMzRKtMJ1opyyGp6h6o"
      |}""".stripMargin

  val invalidWithUnsupportedTransactionType2: String =
    """{
      |   "endorser":"2fSJLv7CErChuAtysJBvaF",
      |   "identifier":"XU1EwgFHFjKcckHc8sbHsE",
      |   "operation":{
      |      "dest":"KiJ9NacXWNftnbJpnr2UWM",
      |      "type":"5664",
      |      "verkey":"CZSp1LFDezCAPMAij8eGaGpP3oYUjYWzwN5XctNV6EJC"
      |   },
      |   "protocolVersion":2,
      |   "reqId":1646167413258037924,
      |   "signature":"2fGDmoGwcQ6FR6hRFpLB8xdKhPJYJA2g77fjC7nkzKeSZUtbBrShqKzVZLjun7At5CuU5TMzRKtMJ1opyyGp6h6o"
      |}""".stripMargin
}
