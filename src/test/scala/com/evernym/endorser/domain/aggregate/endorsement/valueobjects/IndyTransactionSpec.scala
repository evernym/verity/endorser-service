package com.evernym.endorser.domain.aggregate.endorsement.valueobjects

import com.evernym.endorser.domain.aggregate.TestObjects._
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.IndyTransaction.isValidIndyDID
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.IndyTransactionCodes.{invalidJson, invalidMissingEndorser, invalidTransactionType}
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Transaction.Valid
import com.evernym.endorser.domain.did.DID
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.{EitherValues, OptionValues}

class IndyTransactionSpec
  extends AnyFreeSpec
    with Matchers
    with EitherValues
    with OptionValues {

  "IndyTransaction" - {
    "invalid JSON is invalid" in {
      IndyTransaction(invalidJSONIndyTxn, None, builderNetPrefix)
        .validateTransaction().left.value shouldBe invalidJson
    }

    "valid JSON is valid" in {
      IndyTransaction(validIndyTxn, None, builderNetPrefix)
        .validateTransaction().value shouldBe Valid
    }

    "valid JSON has endorser" in {
      IndyTransaction(validIndyTxn, None, builderNetPrefix).endorserDID.value shouldBe endorserDid
    }

    "invalid JSON has NO endorser" in {
      IndyTransaction(invalidWithoutEndorserIndyTxn, None, builderNetPrefix)
        .validateTransaction().left.value shouldBe invalidMissingEndorser
    }

    "invalid transaction type" in {
      IndyTransaction(invalidWithUnsupportedTransactionType, None, builderNetPrefix)
        .validateTransaction().left.value.code.toString shouldBe invalidTransactionType(None).code.toString

      IndyTransaction(invalidWithUnsupportedTransactionType2, None, builderNetPrefix)
        .validateTransaction().left.value.code.toString shouldBe invalidTransactionType(None).code.toString
    }


    "typeName" - {
      "extract DID (Nym)" in {
        IndyTransaction(validIndyTxn, None, builderNetPrefix).typeName shouldBe "DID (NYM)"
      }

      "extract Schema" in {
        IndyTransaction(
          validIndyTxn.replace(""""type":"1"""", """"type":"101""""),
          None,
          builderNetPrefix
        ).typeName shouldBe "SCHEMA"
      }

      "extract Cred Def" in {
        IndyTransaction(
          validIndyTxn.replace(""""type":"1"""", """"type":"102""""),
          None,
          builderNetPrefix
        ).typeName shouldBe "CRED_DEF"
      }

      "handle unknown type" in {
        IndyTransaction(
          validIndyTxn.replace(""""type":"1"""", """"type":"554648""""),
          None,
          builderNetPrefix
        ).typeName shouldBe "UNKNOWN_TXN_TYPE (#554648)"
      }

      "handle no type" in {
        IndyTransaction(
          validIndyTxn.replace(""""type":"1",""", ""),
          None,
          builderNetPrefix
        ).typeName shouldBe "Transaction Type Not Defined"
      }

      "handle invalid json" in {
        IndyTransaction(
          invalidJSONIndyTxn,
          None,
          builderNetPrefix
        ).typeName shouldBe "Transaction Type Not Defined"
      }
    }
    "isValidIndyDID" - {
      "blank DID is not valid" in {
        val blankDID: DID = new DID {
          override def prefix: String = ""

          override def unqualifiedId: String = ""

          override def did: String = ""
        }

        isValidIndyDID(blankDID) shouldBe false
      }
    }
  }
}
