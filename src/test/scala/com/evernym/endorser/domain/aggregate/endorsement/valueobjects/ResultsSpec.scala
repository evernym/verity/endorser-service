package com.evernym.endorser.domain.aggregate.endorsement.valueobjects

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results._
import com.evernym.endorser.domain.codes.EventCode
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class ResultsSpec extends AnyFreeSpec with Matchers {
  "fromEventCode" - {
    "can convert valid EventCode" in {
      Results.fromEventCode(EventCode("ENDT_RES_0001")) shouldBe writtenToVDR
    }

    "can convert invalid EventCode to Unknown" in {
      Results.fromEventCode(EventCode("ENDT_FOO_0001")) shouldBe unknownResult
      Results.fromEventCode(EventCode(null)) shouldBe unknownResult
      Results.fromEventCode(EventCode("")) shouldBe unknownResult
      Results.fromEventCode(EventCode("ENDT-RES-0001")) shouldBe unknownResult
    }

    "should maintain desc if available" in {
      Results.fromEventCode(EventCode("ENDT_FOO_0001")).code.description shouldBe unknownResult.code.description
      Results.fromEventCode(EventCode("ENDT_FOO_0001", Some("Foo"))).code.description shouldBe "Foo"
    }
  }
}
