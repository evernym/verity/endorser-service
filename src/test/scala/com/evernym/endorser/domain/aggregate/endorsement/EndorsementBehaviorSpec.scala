package com.evernym.endorser.domain.aggregate.endorsement

import akka.actor.testkit.typed.scaladsl.LogCapturing
import akka.actor.typed.Behavior
import akka.persistence.typed.PersistenceId
import com.evernym.endorser.domain.aggregate.TestObjects._
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results._
import com.evernym.endorser.domain.events.endorsement.{EndorsementComplete, EndorsementTransactionRejected, EndorsementTransactionValidated}
import com.evernym.endorser.domain.testkit.MockEventPublisher
import com.evernym.endorser.testkit.{EchoResolver, EventSourcedTestKitHelper}
import com.typesafe.scalalogging.Logger
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchersSugar.{argThat, eqTo}
import org.mockito.MockitoSugar
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.{BeforeAndAfterEach, OptionValues}

import java.net.URI
import scala.language.reflectiveCalls

class EndorsementBehaviorSpec
    extends EventSourcedTestKitHelper[Commands.EndorsementCommand, Events.EndorsementEvents, States.EndorsementState]
    with AnyFreeSpecLike
    with BeforeAndAfterEach
    with LogCapturing
    with MockitoSugar
    with MockEventPublisher

    with OptionValues {

  val logger: Logger = Logger(EndorsementBehavior.getClass)

  private val persistenceId = PersistenceId("Endorsement", "1234566")
  def testBehavior: Behavior[Commands.EndorsementCommand] = {
    EndorsementBehavior(
      persistenceId,
      EchoResolver,
      mockPublishEvent
    )
  }


  override def beforeEach(): Unit = {
    super.beforeEach()
    kit.clear()
    resetEventPublisherMock()
  }

  "Endorsement Aggregate" - {
    "should start with empty state" in {
      kit.getState() shouldBe States.Empty()
      kit.restart().state shouldBe kit.getState()
    }

    "should validate a valid transaction" - {
      "should transition to ValidTransaction for a valid transaction" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix, Some("foo")))
          .expectStateOf[States.ValidTransaction]{
            state =>
              state.txn.transactionStr shouldBe validIndyTxn
              state.requestSource.value shouldBe "foo"

          }
      }

      "should store transaction in state" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .expectStateOf[States.ValidTransaction] {
            state =>
              state.txn.transactionStr shouldBe validIndyTxn
              state.txn.ledgerPrefix shouldBe builderNetPrefix
              state.requestSource shouldBe None
          }
      }
    }

    "should NOT validate a invalid transaction" - {
      "should validate txn is JSON object for indy transactions" in {
        send(Commands.ValidateTransaction(invalidJSONIndyTxn, None, builderNetPrefix))
          .expectStateOf[States.CompleteInvalidTransaction] {
            state =>
              state.txn.transactionStr shouldBe invalidJSONIndyTxn
              state.reason.code.description shouldBe "Invalid JSON"
          }

      }
    }

    "should hold prepared transaction if its a valid transaction" - {
      "should transition to ValidPreparedTransaction for valid prepared transaction" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement, None))
          .expectStateOf[States.ValidPreparedTransaction] {
            state =>
              state.txn.transactionStr shouldBe validIndyTxn
              state.txn.ledgerPrefix shouldBe builderNetPrefix

              state.preparedTxn.transactionStr shouldBe validIndyTxnWithEndorsement
              state.preparedTxn.ledgerPrefix shouldBe builderNetPrefix
          }
      }
    }

    "should NOT hold prepared transaction if its a invalid transaction" in {
      send(Commands.ValidateTransaction(invalidJSONIndyTxn, None, builderNetPrefix))
        .stateOfType[States.CompleteInvalidTransaction]

      send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement, None))
        .stateOfType[States.CompleteInvalidTransaction]
    }

    "should complete endorsement if valid prepared transaction" - {
      "should transition to CompleteValidTransaction" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix, Some("foo")))
          .stateOfType[States.ValidTransaction]

        send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement, None))
          .stateOfType[States.ValidPreparedTransaction]

        send(Commands.CompleteTransaction(writtenToVDR))
          .expectStateOf[States.CompleteValidTransaction] { state =>
            state.txn.transactionStr shouldBe validIndyTxn
            state.txn.transactionRef shouldBe None
            state.preparedTxn.value.transactionStr shouldBe validIndyTxnWithEndorsement
            state.preparedTxn.value.transactionRef shouldBe None
            state.result shouldBe writtenToVDR
            state.requestSource.value shouldBe "foo"
          }

        // state should NOT have mutated
        val finalState = kit.getState()
        kit.restart().state shouldBe finalState
      }

      "should maintain transactions" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement, None))
          .stateOfType[States.ValidPreparedTransaction]

        val lastState = send(Commands.CompleteTransaction(writtenToVDR))
          .expectStateOf[States.CompleteValidTransaction] {
            state =>
              state.txn.transactionStr shouldBe validIndyTxn
              state.txn.ledgerPrefix shouldBe builderNetPrefix

              state.preparedTxn.value.transactionStr shouldBe validIndyTxnWithEndorsement
              state.preparedTxn.value.ledgerPrefix shouldBe builderNetPrefix
          }
          .stateOfType[States.CompleteValidTransaction]

        kit.restart().state shouldBe lastState
      }

      "should handle transaction ref to CompleteValidTransaction" in {
        send(Commands.ValidateTransactionRef(validIndyTxn, builderNetPrefix, Some("foo")))

        eventually {
          kit.getState() shouldBe an[States.ValidTransaction]
        }

        send(Commands.HoldPreparedTransactionRef(validIndyTxnWithEndorsement))

        eventually {
          kit.getState() shouldBe an[States.ValidPreparedTransaction]
        }

        send(Commands.CompleteTransaction(writtenToVDR))
          .expectStateOf[States.CompleteValidTransaction] { state =>
            state.txn.transactionStr shouldBe validIndyTxn
            state.txn.transactionRef.isDefined shouldBe true
            state.preparedTxn.value.transactionStr shouldBe validIndyTxnWithEndorsement
            state.preparedTxn.value.transactionRef.isDefined shouldBe true
            state.result shouldBe writtenToVDR
            state.requestSource.value shouldBe "foo"
          }

        // state should NOT have mutated
        val finalState = kit.getState()
        kit.restart().state shouldBe finalState
      }
    }

    "should handle completing an un-prepared endorsement if valid transaction" - {
      "should transition to CompleteValidTransaction" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        send(Commands.CompleteTransaction(writtenToVDR))
          .stateOfType[States.CompleteValidTransaction]
      }

      "should maintain transactions" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        send(Commands.CompleteTransaction(writtenToVDR))
          .expectStateOf[States.CompleteValidTransaction] {
            state =>
              state.txn.transactionStr shouldBe validIndyTxn
              state.txn.ledgerPrefix shouldBe builderNetPrefix

              state.preparedTxn shouldBe None

              state.result shouldBe writtenToVDR
              state.result.code.description should not be empty
          }
      }
    }
    "should handle completing transaction from empty state" in {
      send(Commands.CompleteTransaction(writtenToVDR))
        .expectStateOf[States.CompleteEmptyTransaction] {
          state =>
            state.result shouldBe writtenToVDR
            state.result.code.description should not be empty
        }
    }
  }

  "Domain Events" - {
    "should publish domain event 'Transaction Validated' if valid transaction" in {
      val requesterSource = Some("protocol://FqvqDaGvQDeczjR29ohLFa/34c7e231-5784-4248-a40b-3b6c1dae2819")

      send(
        Commands.ValidateTransaction(
          validIndyTxn,
          None,
          builderNetPrefix,
          requesterSource
        )
      ).expectStateOf[States.ValidTransaction] {
          state =>
            state.requestSource.value shouldBe requesterSource.value
      }

      verify(mockPublishEvent, only).publishEvent(
        eqTo(EndorsementTransactionValidated.eventTopic),
        argThat(matchesType(EndorsementTransactionValidated.eventType)),
        any[URI],
        any[String],
        any[String]
      )
    }

    "should publish domain event 'Endorsement Complete' if valid transaction completes" in {
      val requesterSource = Some("protocol://FqvqDaGvQDeczjR29ohLFa/34c7e231-5784-4248-a40b-3b6c1dae2819")

      send(
        Commands.ValidateTransaction(
          validIndyTxn,
          None,
          builderNetPrefix,
          requesterSource
        )
      )

      verify(mockPublishEvent, atLeastOnce).publishEvent(
        eqTo(EndorsementTransactionValidated.eventTopic),
        argThat(matchesDTO(
          EndorsementTransactionValidated.DTO(
            persistenceId.entityId,
            "",
            requesterSource,
            builderNetPrefix,
            Some(endorserDid.did),
            submitterDid.did
          ),
          EndorsementTransactionValidated.deserializeData
        )),
        any[URI],
        any[String],
        any[String]
      )

      send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement, None))

      send(Commands.CompleteTransaction(writtenToVDR))
        .stateOfType[States.CompleteValidTransaction]


      verify(mockPublishEvent, atLeastOnce).publishEvent(
        eqTo(EndorsementComplete.eventTopic),
        argThat(matchesDTO(
          EndorsementComplete.DTO(
            persistenceId.entityId,
            requesterSource,
            Some(submitterDid.did),
            writtenToVDR.code.toEventCode
          ),
          EndorsementComplete.deserializeData
        )),
        any[URI],
        any[String],
        any[String]
      )
    }

    "should publish domain event 'Transaction Rejected' if invalid transaction" in {
      send(Commands.ValidateTransaction(invalidJSONIndyTxn, None, builderNetPrefix))

      verify(mockPublishEvent, atLeastOnce).publishEvent(
        eqTo(EndorsementTransactionRejected.eventTopic),
        argThat(matchesType(EndorsementTransactionRejected.eventType)),
        any[URI],
        any[String],
        any[String]
      )

      verify(mockPublishEvent, atLeastOnce).publishEvent(
        eqTo(EndorsementComplete.eventTopic),
        argThat(matchesType(EndorsementComplete.eventType)),
        any[URI],
        any[String],
        any[String]
      )
    }
  }
  "Rainy Cases" - {
    "repeating commands" - {
      "Command to validate txn is sent after already validating txn" in {
        val state1 = send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        val state2 = send(Commands.ValidateTransaction(validIndyTxn2, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        // state should NOT have mutated
        state1 shouldBe state2
      }

      "Command to hold prepared txn is sent after already holding a prepared txn" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        val state1 = send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement, None))
          .stateOfType[States.ValidPreparedTransaction]

        val state2 = send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement2, None))
          .stateOfType[States.ValidPreparedTransaction]

        // state should NOT have mutated
        state1 shouldBe state2
        state2.preparedTxn.transactionStr should not be validIndyTxnWithEndorsement2
      }

      "Command to complete endorsement is sent after the endorsement has already been complete" in {
        send(Commands.ValidateTransaction(validIndyTxn, None, builderNetPrefix))
          .stateOfType[States.ValidTransaction]

        send(Commands.HoldPreparedTransaction(validIndyTxnWithEndorsement, None))
          .stateOfType[States.ValidPreparedTransaction]

        val state1 = send(Commands.CompleteTransaction(writtenToVDR))
          .stateOfType[States.CompleteValidTransaction]

        val state2 = send(Commands.CompleteTransaction(rejectedByVDR))
          .stateOfType[States.CompleteValidTransaction]

        // state should NOT have mutated
        state1 shouldBe state2
        state2.result should not be rejectedByVDR
      }
    }
  }
}
