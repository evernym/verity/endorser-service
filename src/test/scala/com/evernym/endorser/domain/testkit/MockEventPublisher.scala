package com.evernym.endorser.domain.testkit

import com.evernym.endorser.domain.Finished
import com.evernym.endorser.domain.events.Event
import com.evernym.endorser.domain.ports.EventPublisherPort
import io.cloudevents.CloudEvent
import org.mockito.ArgumentMatchersSugar.any
import org.mockito.{ArgumentMatcher, MockitoSugar}

import java.net.URI
import scala.concurrent.Future
import scala.util.Try

trait MockEventPublisher extends MockitoSugar {
  val mockPublishEvent: EventPublisherPort = mock[EventPublisherPort]

  def resetEventPublisherMock(): Unit = {
    reset(mockPublishEvent)

    when(mockPublishEvent.publishEvent(
      any[String],
      any[Array[Byte]],
      any[URI],
      any[String],
      any[String]
    )).thenReturn(Future.successful(Finished("event")))
  }

  def matchesType(eventType: String): ArgumentMatcher[Array[Byte]] =
    (argument: Array[Byte]) =>
      Event.deserializeEvent(argument).get.getType == eventType

  def matchesDTO[T](dto: T, deserializeData: CloudEvent => Try[T]): ArgumentMatcher[Array[Byte]] =
    (argument: Array[Byte]) => deserializeData(Event.deserializeEvent(argument).get).map(_ == dto).getOrElse(false)

  def testDTO[T](deserializeData: CloudEvent => Try[T])(testAnonymousFunction: T => Unit): ArgumentMatcher[Array[Byte]] = {
    (argument: Array[Byte]) =>
      deserializeData(Event.deserializeEvent(argument).get)
        .map { dto =>
          testAnonymousFunction(dto)
          true // expecting exceptions to be thrown if function failed (use will 'shouldBe')
        }.getOrElse(false)
  }
}
