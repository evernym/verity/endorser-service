package com.evernym.endorser.domain.codes

import com.evernym.endorser.domain.codes.Codes.{Code, unparsableCode}
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class CodesSpec extends AnyFreeSpec with Matchers {

  "can parse a valid code string" in {
    Codes("FOO_BAR_0001") shouldBe Code("FOO", "BAR", 1)
    Codes("FOO_BAR_0001", Some("DESR")) shouldBe Code("FOO", "BAR", 1, Some("DESR"))
  }

  "can parse invalid code string" in {
    Codes("FOO-BAR-0001") shouldBe unparsableCode
    Codes("FOO_BAR_0B01") shouldBe unparsableCode
    Codes(null) shouldBe unparsableCode
    Codes("") shouldBe unparsableCode
  }

}
