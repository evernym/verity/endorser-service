package com.evernym.endorser.domain.process.publisher

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort
import com.evernym.endorser.domain.process.publisher.valueobjects.Status
import com.evernym.endorser.domain.process.publisher.valueobjects.Status.connected

import scala.concurrent.Future

object TestObjects {
  val endorsementId = "4509dece-ec79-4ea5-8c73-69e113492c74"

  val builderNetPrefix = "did:indy:sovrin:builder"


  val alwaysConnected: PublishTransactionPort = new PublishTransactionPort {
    override def connectionStatus(): Status = Status(Ledger(builderNetPrefix), connected)

    override def ledger: Ledger = throw new NotImplementedError

    override def publishTransaction(txn: String): Future[Result] = throw new NotImplementedError
  }


  val validIndyTxn: String =
    """{
      |   "endorser":"P9prvGXKFvCdajeXfBivi4",
      |   "identifier":"Pw3547NjqCEF94iPM43T46",
      |   "operation":{
      |      "dest":"ND6fMqptp2MsVSZmRSbPR8",
      |      "type":"1",
      |      "verkey":"CZSp1LFDezCAPMAij8eGaGpP3oYUjYWzwN5XctNV6EJC"
      |   },
      |   "protocolVersion":2,
      |   "reqId":1646167413258037924,
      |   "signature":"2fGDmoGwcQ6FR6hRFpLB8xdKhPJYJA2g77fjC7nkzKeSZUtbBrShqKzVZLjun7At5CuU5TMzRKtMJ1opyyGp6h6o"
      |}""".stripMargin
}
