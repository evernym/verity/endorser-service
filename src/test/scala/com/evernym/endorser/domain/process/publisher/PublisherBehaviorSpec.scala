package com.evernym.endorser.domain.process.publisher

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.{rejectedByVDR, writtenToVDR}
import com.evernym.endorser.domain.events.publisher.{PublisherTransactionWritten, PublisherWriteFailed}
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.domain.process.publisher.PublisherBehavior._
import com.evernym.endorser.domain.process.publisher.TestObjects.{alwaysConnected, builderNetPrefix, endorsementId, validIndyTxn}
import com.evernym.endorser.domain.process.publisher.ports.PublishTransactionPort
import com.evernym.endorser.domain.process.publisher.valueobjects.Status
import com.evernym.endorser.domain.process.publisher.valueobjects.Status.connected
import com.evernym.endorser.domain.testkit.MockEventPublisher
import com.evernym.endorser.infrastructure.noop.NoOpResolver
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import com.evernym.endorser.testkit.EchoResolver
import com.evernym.endorser.testkit.Traits.ObjectTester
import org.mockito.ArgumentMatchersSugar.{any, argThat, eqTo}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.freespec.AnyFreeSpecLike

import java.net.URI
import scala.concurrent.Future

class PublisherBehaviorSpec
  extends ScalaTestWithActorTestKit(withoutClustering())
    with AnyFreeSpecLike
    with BeforeAndAfterEach
    with ObjectTester
    with MockEventPublisher {

  private val mockPublishTransactionPort = mock[PublishTransactionPort]
  def resetPublishTransactionMock(): Unit = {
    reset(mockPublishTransactionPort)

    when(mockPublishTransactionPort.ledger)
      .thenReturn(Ledger(builderNetPrefix))
    when(mockPublishTransactionPort.publishTransaction(any[String]))
      .thenReturn(Future.successful(writtenToVDR))
  }

  override def beforeEach(): Unit = {
    super.beforeEach()
    resetEventPublisherMock()
    resetPublishTransactionMock()
  }

  val entityId = "did:test"

  "Publisher behavior" - {
    "should ask for connection status" in {
      val replyProbe = createTestProbe[Status]()
      val underTest = spawn(
        PublisherBehavior(entityId, alwaysConnected, NoOpResolver, null)
      )
      underTest ! ConnectionStatus(replyProbe.ref)
      test(replyProbe.expectMessageType[Status].code){ code =>
        code shouldBe connected
        code.aggregate shouldBe abbreviation
      }
    }

    "should publish transaction" in {
      spawn(
        PublisherBehavior(entityId, mockPublishTransactionPort, NoOpResolver, mockPublishEvent)
      ) ! SubmitTransaction(endorsementId, validIndyTxn)

      eventually {
        verify(mockPublishTransactionPort, atLeastOnce).publishTransaction(validIndyTxn)

        verify(mockPublishEvent, only).publishEvent(
          eqTo(PublisherTransactionWritten.eventTopic),
          argThat(matchesType(PublisherTransactionWritten.eventType)),
          any[URI],
          any[String],
          any[String]
        )
      }
    }

    "should publish transaction from reference" in {
      spawn(
        PublisherBehavior(entityId, mockPublishTransactionPort, EchoResolver, mockPublishEvent)
      ) ! SubmitTransactionRef(endorsementId, validIndyTxn)

      eventually {
        verify(mockPublishTransactionPort, atLeastOnce).publishTransaction(validIndyTxn)

        verify(mockPublishEvent, only).publishEvent(
          eqTo(PublisherTransactionWritten.eventTopic),
          argThat(matchesType(PublisherTransactionWritten.eventType)),
          any[URI],
          any[String],
          any[String]
        )
      }
    }

    "should handle failed write" in {
      when(mockPublishTransactionPort.publishTransaction(any[String])).thenReturn(
        Future.successful(rejectedByVDR)
      )

      spawn(
        PublisherBehavior(entityId, mockPublishTransactionPort, NoOpResolver, mockPublishEvent)
      ) ! SubmitTransaction(endorsementId, validIndyTxn)

      eventually {
        verify(mockPublishTransactionPort, atLeastOnce).publishTransaction(validIndyTxn)

        verify(mockPublishEvent, only).publishEvent(
          eqTo(PublisherWriteFailed.eventTopic),
          argThat(matchesDTO(
            PublisherWriteFailed.DTO(
              endorsementId,
              rejectedByVDR.code.toEventCode
            ),
            PublisherWriteFailed.deserializeData
          )),
          any[URI],
          any[String],
          any[String]
        )
      }
    }
    "should fail on blank transaction" in {
      spawn(
        PublisherBehavior(entityId, mockPublishTransactionPort, NoOpResolver, mockPublishEvent)
      ) ! SubmitTransaction(endorsementId, "  ")

      eventually {

        verify(mockPublishEvent, only).publishEvent(
          eqTo(PublisherWriteFailed.eventTopic),
          argThat(matchesType(PublisherWriteFailed.eventType)),
          any[URI],
          any[String],
          any[String]
        )
      }
    }
    "should handle unexpected exception" in {
      val expectedException = new Exception("Foo")
      when(mockPublishTransactionPort.publishTransaction(any[String])).thenReturn(
        Future.failed(expectedException)
      )

      spawn(
        PublisherBehavior(entityId, mockPublishTransactionPort, NoOpResolver, mockPublishEvent)
      ) ! SubmitTransaction(endorsementId, validIndyTxn)

      eventually {
        verify(mockPublishTransactionPort, atLeastOnce).publishTransaction(validIndyTxn)

        verify(mockPublishEvent, only).publishEvent(
          eqTo(PublisherWriteFailed.eventTopic),
          argThat(matchesDTO(
            PublisherWriteFailed.DTO(
              endorsementId,
              Results.unexpectedException(expectedException).code.toEventCode
            ),
            PublisherWriteFailed.deserializeData
          )),
          any[URI],
          any[String],
          any[String]
        )
      }
    }
  }
}
