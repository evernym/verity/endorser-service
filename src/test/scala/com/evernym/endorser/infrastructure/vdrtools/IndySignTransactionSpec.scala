package com.evernym.endorser.infrastructure.vdrtools

import com.evernym.endorser.infrastructure.sharedlib.VdrTools.vdrToolsAvailable
import com.evernym.endorser.testkit.SingleWalletSpec
import com.evernym.vdrtools.did.{Did, DidJSONParameters}
import com.evernym.vdrtools.ledger.Ledger

class IndySignTransactionSpec extends SingleWalletSpec {
  "VDR Tools should produce valid signed transaction" in {
    assume(vdrToolsAvailable)

    val msg = "{\n" +
      "                \"reqId\":1496822211362017764,\n" +
      "                \"identifier\":\"GJ1SzoWzavQYfNL9XkaJdrQejfztN4XqdsiV4ct3LXKL\",\n" +
      "                \"operation\":{\n" +
      "                    \"type\":\"1\",\n" +
      "                    \"dest\":\"VsKV7grR1BUE29mG2Fm2kX\",\n" +
      "                    \"verkey\":\"GjZWsBLgZCR18aL468JAT7w9CZRiBnpxUPPgyQxh4voa\"\n" +
      "                }\n" +
      "            }"
    val TRUSTEE_SEED = "000000000000000000000000Trustee1"

    val TRUSTEE_IDENTITY_JSON = new DidJSONParameters.CreateAndStoreMyDidJSONParameter(null, TRUSTEE_SEED, null, null).toJson

    val expectedSignature = "\"signature\":\"65hzs4nsdQsTUqLCLy2qisbKLfwYKZSWoyh1C6CU59p5pfG3EHQXGAsjW4Qw4QdwkrvjSgQuyv8qyABcXRBznFKW\""

    val result = Did.createAndStoreMyDid(wallet, TRUSTEE_IDENTITY_JSON).get
    val did = result.getDid

    val signedMessage = Ledger.signRequest(wallet, did, msg).get

    signedMessage should include (expectedSignature)
  }
}