package com.evernym.endorser.infrastructure.ledger

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.infrastructure.ledger.IndyPublishTransactionAdapter.{buildLedgerConfig, responseToResult}
import com.evernym.endorser.infrastructure.sharedlib.VdrTools
import com.evernym.endorser.testkit.JsonCompare
import com.typesafe.config.ConfigFactory
import org.scalatest.TryValues
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps

class IndyPublishTransactionAdapterSpec
  extends AnyFreeSpec
    with Matchers
    with TryValues
    with JsonCompare {
  val vdrToolsAvailable: Boolean = Await.result(
    VdrTools()
      .initialize()
      .recover{case _ => false},
    5 seconds
  )

//  val configStr =
//    """
//      |ledger = {
//      |"did_test" = {
//      |    type = "indy"
//      |    did-aliases = []
//      |    genesis-txn-endpoint = ""
//      |    genesis-txn-file = "/tmp/genesis.txt"
//      |    config {
//      |    connection-manager-open-timeout = 80
//      |    timeout = 20
//      |    extended-timeout = 60
//      |    conn-limit = 5
//      |    conn-active-timeout = 5
//      |    }
//      |}
//      |}
//      |""".stripMargin
//
//  val config = ConfigFactory.parseString(configStr)
//
//  val txn = """{"identifier":"Th7MpTaRZVRYnPiabds81Y","operation":{"dest":"Udndvn1MVeDBjcdaekGr3L","role":"101","type":"1","verkey":"G4aFbioHotiXE7XEpLbxzFt3HN8qyzrKpEVtSUKt4BXq"},"protocolVersion":2,"reqId":1649097673158144221,"signature":"MWhS431xuSiQKtG1fAgp3D7fHRLnmHZU2Y3UiJfKBuLH5VnsPwQxdS7TW124vr1bW22dCmH2FVnoX6WHH3w9Hbw","taaAcceptance":{"mechanism":"wallet_agreement","taaDigest":"a0ab0aada7582d4d211bf9355f36482e5cb33eeb46502a71e6cc7fea57bb8305","time":1649030400}}"""
////  val txn = """{"identifier":"Th7MpTaRZVRYnPiabds81Y","operation":{"dest":"54oL2zee3znZKTtngSfn9C","role":"101","type":"1","verkey":"3DX4hKCjwseBYKs7GnJ1D3Ny8xeqA9jCGAD3Yubb6RdH"},"protocolVersion":2,"reqId":1649102951609846007,"signature":"5J9k6i1mFtc2jutA9uCWEv3MK1qbcoizGnPVJFBdEjdkmX2VnX6JgZUmPKYg84otMv226gKKdhCYwNEh71WBcU9u","taaAcceptance":{"mechanism":"wallet_agreement","taaDigest":"a0ab0aada7582d4d211bf9355f36482e5cb33eeb46502a71e6cc7fea57bb8305","time":1649030400}}"""
//
//  val system = ActorSystem(Behaviors.empty, "test")
//
//  val fut = new IndyPublishTransactionAdapter("did:test", config.getConfig("ledger"), system)
//    .publishTransaction(txn)
//
//  val t = Await.result(fut, 10000 seconds)
//  val t2 = t.code
//  val t3 = 0

  "buildLedgerConfig" - {
    "should handle no config" in {
      assertJsonEqual(
        """{
          |   "timeout":20,
          |   "extended_timeout":60,
          |   "conn_limit":20,
          |   "conn_active_timeout":20
          |}""".stripMargin,
          buildLedgerConfig("foo", ConfigFactory.empty()).success.value
      )
    }

    "should pull correct values from config" in {
      val config =
        """
          |{
          |    "did_test" = {
          |        config {
          |            connection-manager-open-timeout = 13
          |            timeout = 24
          |            extended-timeout = 59
          |            conn-limit = 75
          |            conn-active-timeout = 95
          |        }
          |    }
          |}
          |""".stripMargin
      assertJsonEqual(
        """{
          |   "timeout":24,
          |   "extended_timeout":59,
          |   "conn_limit":75,
          |   "conn_active_timeout":95
          |}""".stripMargin,
        buildLedgerConfig("did:test", ConfigFactory.parseString(config).getConfig("did_test")).success.value
      )
    }
  }


  "responseToResult" - {
    "should be able handle a successful response" in {
      Await.result(responseToResult("did:test")(
        """{
          |   "result":{
          |      "txnMetadata":{
          |         "txnTime":1649098032,
          |         "seqNo":16,
          |         "txnId":"d90e7e9538b51d283fe2947cd1771cdec177b0f8fe34ef1254e2ab8657ba0cdc"
          |      },
          |      "txn":{
          |         "type":"1",
          |         "metadata":{
          |            "reqId":1649097673158144221,
          |            "taaAcceptance":{
          |               "time":1649030400,
          |               "mechanism":"wallet_agreement",
          |               "taaDigest":"a0ab0aada7582d4d211bf9355f36482e5cb33eeb46502a71e6cc7fea57bb8305"
          |            },
          |            "payloadDigest":"3c74673ebdfcdac41bf6e20d91d56837e8974146bf70c075c787cd1e433aa38d",
          |            "digest":"d09ff5e365241bb3c4b858a1798c191152cc3e808c630098c0cc59105ae18605",
          |            "from":"Th7MpTaRZVRYnPiabds81Y"
          |         },
          |         "data":{
          |            "role":"101",
          |            "dest":"Udndvn1MVeDBjcdaekGr3L",
          |            "verkey":"G4aFbioHotiXE7XEpLbxzFt3HN8qyzrKpEVtSUKt4BXq"
          |         },
          |         "protocolVersion":2
          |      },
          |      "reqSignature":{
          |         "type":"ED25519",
          |         "values":[
          |            {
          |               "value":"MWhS431xuSiQKtG1fAgp3D7fHRLnmHZU2Y3UiJfKBuLH5VnsPwQxdS7TW124vr1bW22dCmH2FVnoX6WHH3w9Hbw",
          |               "from":"Th7MpTaRZVRYnPiabds81Y"
          |            }
          |         ]
          |      },
          |      "ver":"1",
          |      "auditPath":[
          |         "5LxFb798wNXz5Qrt4Kyj3Aj6eEPuL8rWcGuJFu8HodMr",
          |         "7bA3fb5czDeweVpMAqWJderLf3Cu6LoNJS2BMEMnLyTd",
          |         "79rZyNQnYG1Vpm8ZX8A6bCKxMA3W4hDGv8yJKMerzXaA",
          |         "DNHM372JZJoGcxdHdmsj3QSSiomyeZux6ssJXxAJqyvd"
          |      ],
          |      "rootHash":"J7sxY5iTjU3nL8Rm2F8xKzZDichXhkm4Dnsg4zZ21ukY"
          |   },
          |   "op":"REPLY"
          |}""".stripMargin
      ), 10 seconds) shouldBe Results.writtenToVDR
    }
  }
}
