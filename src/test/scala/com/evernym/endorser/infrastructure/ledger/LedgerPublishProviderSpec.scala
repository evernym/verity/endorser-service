package com.evernym.endorser.infrastructure.ledger

import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.process.publisher.valueobjects.Status.unknown
import com.evernym.endorser.infrastructure.ledger.LedgerPublishProvider._
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import com.evernym.endorser.testkit.Traits.{ObjectTester, TestActorSystem}
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.OptionValues
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class LedgerPublishProviderSpec
  extends AnyFreeSpec
  with Matchers
  with OptionValues
  with TestActorSystem
  with ObjectTester {
  val config: Config = ConfigFactory.parseString(
    """
      |foo.bar {}
      |""".stripMargin
  )

  "normalizeLedgerName" - {
    "should convert ':' to '_'" in {
      normalizeLedgerName("did:test") shouldBe "did_test"
    }

    "should leave other char alone" in {
      normalizeLedgerName("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz") shouldBe
        "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
    }

    "should handle null" in {
      normalizeLedgerName(null) shouldBe ""
    }

    "should handle empty" in {
      normalizeLedgerName("") shouldBe ""
    }

  }

  "extractLedgerType" - {
    "non found in config should return None" in {
      extractLedgerType("did:foo", config) shouldBe None
    }

    "undefined type should return None" in {
      val conf = ConfigFactory.parseString(
        """
          | did_foo = {}
          |""".stripMargin
      )
      extractLedgerType("did:foo", conf) shouldBe None
    }

    "type should return with relevant config" in {
      val conf = ConfigFactory.parseString(
        """
          | did_foo.type = "KNOWN_TYPE"
          | did_foo.test = "BAR"
          |""".stripMargin
      )

      test(extractLedgerType("did:foo", conf).value){ extractedType =>
        extractedType.typeString shouldBe "KNOWN_TYPE"
        extractedType.config.getString("test") shouldBe "BAR"
      }
    }

  }

  "unknownVDRAdaptor" - {
    test(unknownVDRAdapter("did:foo")) {adapter =>
      adapter.ledger.ledgerPrefix shouldBe "did:foo"
      adapter.connectionStatus().code shouldBe unknown
      Await.result(adapter.publishTransaction("HI"), 1 second) shouldBe Results.unknownVDR
    }
  }

  "resolveLedgerConfig" - {
    val configStr = """endorser.ledger {
                   |  "did_indy_sovrin_builder" = {
                   |    type = "indy"
                   |    genesis-txn-file = "/etc/endorser/endorser-application/config-map/sovrin-builder-net-genesis.txn"
                   |    config {
                   |      connection-manager-open-timeout = 360
                   |      timeout = 120
                   |      extended-timeout = 360
                   |      conn-limit = 5
                   |      conn-active-timeout = 5
                   |    }
                   |  }
                   |}""".stripMargin

    "should resolve to ledger configs" in {
      val config = ConfigFactory.parseString(configStr)
      resolveLedgerConfig(config).getConfig("did_indy_sovrin_builder" ) shouldBe an [Config]
      resolveLedgerConfig(config).getString("did_indy_sovrin_builder.type" ) shouldBe "indy"
    }

    "should resolve to empty config object if 'endorser.ledger' is not found" in {
      val config = ConfigFactory.parseString("foo.bar = bar")
      resolveLedgerConfig(config).isEmpty shouldBe true
    }
  }

  "providerAdapter" - {
    "returns the unknown adaptor if ledger is not in config" in {
      withActorSystem(withoutClustering(config)) { as =>
        Await.result(
          providerAdapter("did:foo", as.settings.config, as)
            .publishTransaction("BAR"), 1 second
        ) shouldBe Results.unknownVDR
      }
    }

    "returns the unknown adaptor if ledger is not indy" in {
      val conf = ConfigFactory.parseString(
        """
          | did_foo.type = "KNOWN_TYPE"
          | did_foo.test = "BAR"
          |""".stripMargin
      )
      withActorSystem(withoutClustering(config)) { as =>
        Await.result(
          providerAdapter("did:foo", conf, as)
            .publishTransaction("BAR"), 1 second
        ) shouldBe Results.unknownVDR
      }
    }
  }


}
