package com.evernym.endorser.infrastructure.db

import akka.Done
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.persistence.jdbc.testkit.scaladsl.SchemaUtils
import com.evernym.endorser.infrastructure.db.AkkaPersistenceJdbcSpec.basicConfig
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.freespec.AnyFreeSpecLike

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

class AkkaPersistenceJdbcSpec
  extends ScalaTestWithActorTestKit(basicConfig)
  with AnyFreeSpecLike {

  override protected def afterAll(): Unit = {
    super.afterAll()
    system.terminate()
  }

  "Akka JDBC Persistence should should be able to build schema" in {
    Await.result(SchemaUtils.createIfNotExists(), 10 seconds) shouldBe Done
  }
}

object AkkaPersistenceJdbcSpec {
  val basicConfig: Config = {
    val configStr: String = """akka {
                              |  persistence {
                              |    journal {
                              |      plugin = "jdbc-journal"
                              |      // Enable the line below to automatically start the journal when the actorsystem is started
                              |      // auto-start-journals = ["jdbc-journal"]
                              |    }
                              |    snapshot-store {
                              |      plugin = "jdbc-snapshot-store"
                              |      // Enable the line below to automatically start the snapshot-store when the actorsystem is started
                              |      // auto-start-snapshot-stores = ["jdbc-snapshot-store"]
                              |    }
                              |  }
                              |}
                              |
                              |jdbc-journal {
                              |  slick = ${slick}
                              |}
                              |
                              |# the akka-persistence-snapshot-store in use
                              |jdbc-snapshot-store {
                              |  slick = ${slick}
                              |}
                              |
                              |# the akka-persistence-query provider in use
                              |jdbc-read-journal {
                              |  slick = ${slick}
                              |}
                              |
                              |# the akka-persistence-jdbc provider in use for durable state store
                              |jdbc-durable-state-store {
                              |  slick = ${slick}
                              |}
                              |
                              |slick {
                              |  profile = "slick.jdbc.H2Profile$"
                              |  db {
                              |    url = "jdbc:h2:mem:test-database;DATABASE_TO_UPPER=false;"
                              |    user = "root"
                              |    password = "root"
                              |    driver = "org.h2.Driver"
                              |    numThreads = 5
                              |    maxConnections = 5
                              |    minConnections = 1
                              |  }
                              |}""".stripMargin
    val c = ConfigFactory.parseString(configStr).resolve()
    withoutClustering(c)
  }
}