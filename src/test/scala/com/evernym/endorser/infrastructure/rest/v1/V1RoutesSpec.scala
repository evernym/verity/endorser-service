package com.evernym.endorser.infrastructure.rest.v1

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.evernym.endorser.application.health.MockHealthAdapter
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import com.typesafe.config.Config
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class V1RoutesSpec
  extends AnyFreeSpec
    with Matchers
    with ScalatestRouteTest {

  override def testConfig: Config = withoutClustering(super.testConfig)

  "Health Check" - {
    "should respond to get" in {
      Get("/v1/health") ~> V1Routes.userRoutes(MockHealthAdapter) ~> check {
        responseAs[String] should include( "1.2.33.bea9b536064f65e6c12af8ffa8ee2490c18d3807")
      }
    }
  }
}