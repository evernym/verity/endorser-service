//package com.evernym.endorser.infrastructure.kafka
//
//import com.evernym.endorser.infrastructure.eventing.kafka.ConsumerSettingsProvider
//import com.evernym.endorser.testkit.BasicSpec
//import com.typesafe.config.ConfigException.Missing
//import com.typesafe.config.ConfigFactory
//
//class ConsumerSettingsProviderSpec
//  extends BasicSpec {
//
//  "ProducerSettingsProvider" - {
//
//    "when constructed without sufficient configs" - {
//      "should fail" in {
//        intercept[Missing] {
//          ConsumerSettingsProvider(ConfigFactory.empty)
//        }
//      }
//    }
//
//    "when constructed with sufficient configs" - {
//      "should pass" in {
//        val defaultAkkaKafkaConfig = ConfigFactory.load().withOnlyPath("akka.kafka")
//
//        val endorserKafkaConfig = ConfigFactory.parseString(
//          """
//            | endorser.kafka = ${akka.kafka} {
//            |   consumer = ${akka.kafka.consumer} {
//            |     kafka-clients = ${akka.kafka.consumer.kafka-clients} {
//            |       bootstrap.servers = "testkafka"
//            |       client.id = "endorser"
//            |     }
//            |     topics = [ DEMO_EVENTS ]
//            |
//            |     resolve-timeout = 3 seconds
//            |
//            |     parallelism = 100
//            |   }
//            | }
//            |""".stripMargin
//        )
//
//        val settingsProvider = ConsumerSettingsProvider(endorserKafkaConfig.withFallback(defaultAkkaKafkaConfig).resolve())
//
//        val kafkaProducerSettings = settingsProvider.kafkaConsumerSettings()
//        kafkaProducerSettings.properties shouldBe Map(
//          "bootstrap.servers"   -> "testkafka",
//          "client.id"           -> "endorser",
//          "enable.auto.commit"  -> "false",
//        )
//      }
//    }
//  }
//}
