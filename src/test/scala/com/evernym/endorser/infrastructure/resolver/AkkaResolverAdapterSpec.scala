package com.evernym.endorser.infrastructure.resolver

import akka.http.scaladsl.model.StatusCodes.{NotFound, OK}
import akka.http.scaladsl.model.{HttpEntity, HttpResponse}
import com.evernym.endorser.domain.ports.UnresolvableRefException
import com.evernym.endorser.infrastructure.resolver.AkkaResolverAdapter.mapResponse
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class AkkaResolverAdapterSpec extends AnyFreeSpec with Matchers {
  "mapResponse" - {
    "should map 404 throw exception" in {
      val testResp = HttpResponse(NotFound)
      assertThrows[UnresolvableRefException] {
        mapResponse("example.com")(testResp)
      }
    }

    "should map 200 to entity" in {
      val testResp = HttpResponse(OK)
      mapResponse("example.com")(testResp) shouldBe HttpEntity.Empty
    }
  }

}
