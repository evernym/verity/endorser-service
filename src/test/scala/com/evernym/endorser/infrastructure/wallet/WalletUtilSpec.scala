package com.evernym.endorser.infrastructure.wallet

import com.evernym.endorser.application.wallet.{CacheSettings, WalletUtil}
import com.evernym.endorser.infrastructure.sharedlib.VdrTools
import com.evernym.endorser.testkit.JsonCompare
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps

class WalletUtilSpec
  extends AnyFreeSpecLike
  with JsonCompare {
  val vdrToolsAvailable: Boolean = Await.result(
    VdrTools()
      .initialize()
      .recover{case _ => false},
    5 seconds
  )

  val mysqlConfig: Config = {
    ConfigFactory.parseString(
      s"""
         |wallet-type = "mysql"
         |wallet-db-storage {
         |  connection-limit = 20
         |  read-host-ip = "1.1.1.1"
         |  write-host-ip = "2.2.2.2"
         |  host-port = "3306"
         |  credentials-username = "user"
         |  credentials-password = "password"
         |  db-name = "wallet"
         |}
         |""".stripMargin)
  }
  val defaultConfig: Config = {
    ConfigFactory.parseString(
    s"""
       |wallet-type = "default"
       |""".stripMargin)
  }

  "Wallet Util" - {
    "Should have proper default config values" in {
      val testConfig = WalletUtil.buildWalletConfig(defaultConfig)
      assertJsonEqual(
        s"""{
           |"id": "test",
           |"storage_type": "default"
           |}""".stripMargin,
        testConfig.buildConfig("test")
      )

      assertJsonEqual(
        s"""{
           |"key": "1234",
           |"key_derivation_method":"RAW"
           |}""".stripMargin,
        testConfig.buildCredentials("1234", "RAW")
      )
    }

    "Should have proper values for mysql wallet config" in {
      val testConfig = WalletUtil.buildWalletConfig(mysqlConfig)
      val storageConfigs =
        s"""{
           |"db_name":"wallet",
           |"read_host":"1.1.1.1",
           |"write_host":"2.2.2.2",
           |"port": 3306,
           |"connection_limit": 20
         }""".stripMargin

      val walletConfig = s"""{
         |"id": "test",
         |"storage_type": "mysql",
         |"storage_config": $storageConfigs,
         |"cache": {"size":10,"entities":["vdrtools::Did","vdrtools::Key","indy::Did","indy::Key"]}
       }""".stripMargin

      assertJsonEqual(
        walletConfig,
        testConfig.buildConfig("test")
      )

      assertJsonEqual(
        s"""{
           |"key": "1234",
           |"storage_credentials": {"user": "user", "pass": "password"},
           |"key_derivation_method":"RAW"
           |}""".stripMargin,
        testConfig.buildCredentials("1234", "RAW")
      )
    }

    "Should derive correct wallet key seed" in {
      val salt = "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW"
      val entityId = "42330461-2c6d-430b-af53-dbc4b7aa448e"

      WalletUtil.deriveWalletKeySeed(salt, entityId) shouldBe "78b8ad3ab10a77d1d01511043afe04157108329fb1ca9dd3646cb8c21481953b"
    }

    "Should derive correct wallet key" in {
      assume(vdrToolsAvailable)
      val salt = "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW"
      val entityId = "42330461-2c6d-430b-af53-dbc4b7aa448e"

      val seed = WalletUtil.deriveWalletKeySeed(salt, entityId)

      WalletUtil.deriveWalletKey(seed) shouldBe "BTgVBdCas5tRiPubWNHxkmgNhJyFsXrB4mYUmr3WNSpN"
    }
  }

  "CacheSettings" - {
    "can make JSON" in {
      assertJsonEqual(
        """{"size":10,"entities":["vdrtools::Did","vdrtools::Key","indy::Did","indy::Key"]}""",
        CacheSettings(10).toJson
      )
    }
  }
}
