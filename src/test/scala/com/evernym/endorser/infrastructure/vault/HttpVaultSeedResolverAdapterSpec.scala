package com.evernym.endorser.infrastructure.vault

import akka.http.scaladsl.model.StatusCodes
import com.evernym.endorser.application.resolver.Seed
import com.evernym.endorser.infrastructure.vault.HttpVaultSeedResolverAdapter.extractSeed
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import scala.language.postfixOps


class HttpVaultSeedResolverAdapterSpec extends AnyFreeSpec with Matchers {

//  "test against actual vault server" in {
//    import akka.actor.typed.ActorSystem
//    import akka.actor.typed.scaladsl.Behaviors
//    import com.typesafe.config.ConfigFactory
//
//    import scala.concurrent.Await
//    import scala.concurrent.duration._
//
////    val vaultEndpoint = "http://0.0.0.0:8200"
//    val vaultEndpoint = "https://vault.corp.evernym.com:8200"
//    val config = ConfigFactory.parseString(s"""resolver.vault-address = "$vaultEndpoint" """)
//
//    val system = ActorSystem(Behaviors.empty[Any], "test", config)
//    val t = HttpVaultSeedResolverAdapter("resolver", system).resolve(
//      "hvs.CAESIDEFDvomTNSXxKT3itCdWPQE5DuIOYZsS06JElMd7kR3Gh4KHGh2cy4wTGVQVXFBb1NtcmEzSUdVYjNRbW1uR1Q",
//      "sharedpass/data/dev/temp")
//    val t2 = Await.result(t, 10 seconds)
//    println(t2)
//  }


  "extractSeed" - {
    "should extract from Vault KVM v1" in {
      val response =
        """
          |{
          |  "request_id": "eca8826d-3cf0-1790-d058-14faac100df1",
          |  "lease_id": "",
          |  "renewable": false,
          |  "lease_duration": 2764800,
          |  "data": {
          |    "did": "GtxY2ZasN6doGutU12im2X",
          |    "seed": "Foo"
          |  },
          |  "wrap_info": null,
          |  "warnings": null,
          |  "auth": null
          |}
          |""".stripMargin

      extractSeed((StatusCodes.OK, response)) shouldBe Seed("Foo")
    }

    "should extract from Vault KVM v2" in {
      val response =
        """
          |{
          |  "request_id": "99460438-172a-2988-cd62-b51edd256485",
          |  "lease_id": "",
          |  "renewable": false,
          |  "lease_duration": 0,
          |  "data": {
          |    "data": {
          |      "seed": "Foo"
          |    },
          |    "metadata": {
          |      "created_time": "2022-04-05T16:24:08.444432804Z",
          |      "custom_metadata": null,
          |      "deletion_time": "",
          |      "destroyed": false,
          |      "version": 2
          |    }
          |  },
          |  "wrap_info": null,
          |  "warnings": null,
          |  "auth": null
          |}
          |""".stripMargin

      extractSeed((StatusCodes.OK, response)) shouldBe Seed("Foo")
    }

    "should throw execution if un-parsable" in {
      val response =
        """
          |{
          |  "request_id": "99460438-172a-2988-cd62-b51edd256485",
          |  "lease_id": "",
          |  "renewable": false,
          |  "lease_duration": 0,
          |  "data": {
          |    "OTHER_KEY": {
          |      "seed": "Foo"
          |    },
          |    "metadata": {
          |      "created_time": "2022-04-05T16:24:08.444432804Z",
          |      "custom_metadata": null,
          |      "deletion_time": "",
          |      "destroyed": false,
          |      "version": 2
          |    }
          |  },
          |  "wrap_info": null,
          |  "warnings": null,
          |  "auth": null
          |}
          |""".stripMargin

      the [UnresolvableSeedException] thrownBy extractSeed((StatusCodes.OK, response))

      the [UnresolvableSeedException] thrownBy extractSeed((StatusCodes.OK, "{}"))
      the [UnresolvableSeedException] thrownBy extractSeed((StatusCodes.OK, null))
    }
  }
}
