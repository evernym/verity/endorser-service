package com.evernym.endorser.application.health

object MockHealthAdapter extends HealthPort {
  override def report: HealthReport = HealthReport(version = Version(
    "1",
    "2",
    "33",
    "bea9b536064f65e6c12af8ffa8ee2490c18d3807"
  ).toString)
}
