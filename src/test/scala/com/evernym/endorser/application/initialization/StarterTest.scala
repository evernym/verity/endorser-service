// This needs to be refactor since I move Start to be a Guardian Actor
//
//
// package com.evernym.endorser.application.initialization
//
//
//import akka.actor.typed.ActorSystem
//import com.typesafe.config.{Config, ConfigFactory}
//import org.scalatest.freespec.AnyFreeSpec
//
//import scala.concurrent.{ExecutionContext, Future}
//
//class StarterTest extends AnyFreeSpec {
//  "Starter" - {
//    "must start service" in {
//      val mockService = new Startable {
//        override def name(): String = "Mock Service"
//
//        override def start(config: Config)
//                          (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {
//          Future {
//            "HI"
//          }
//        }
//      }
//
//      val s:Starter = Starter(ConfigFactory.load())
//      try {
//        s.start(Seq(mockService))
//      }
//      finally {
//        s.stop()
//      }
//
//    }
//    "must start multiple services" in {
//      val mockService = new Startable {
//        override def name(): String = "Mock Service"
//
//        override def start(config: Config)
//                          (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {
//          Future {
//            "FOO"
//          }
//        }
//      }
//
//      val mockService2 = new Startable {
//        override def name(): String = "Mock Service"
//
//        override def start(config: Config)
//                          (implicit actorSys: ActorSystem[_], execution: ExecutionContext): Future[Any] = {
//          Future {
//            "BAR"
//          }
//        }
//      }
//
//      val r:Starter = Starter(ConfigFactory.load())
//      try {
//        r.start(Seq(mockService, mockService2))
//      }
//      finally {
//        r.stop()
//      }
//    }
//  }
//
//}
