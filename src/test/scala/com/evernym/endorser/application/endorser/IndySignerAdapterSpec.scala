package com.evernym.endorser.application.endorser

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.cluster.sharding.ShardRegion.EntityId
import akka.persistence.typed.PersistenceId
import com.evernym.endorser.application.wallet.{WalletConfig, WalletUtil}
import com.evernym.endorser.domain.aggregate.TestObjects.{builderNetPrefix, endorserDid, validIndyTxn}
import com.evernym.endorser.domain.did.DID
import com.evernym.endorser.infrastructure.crypto.IndySignerAdapter
import com.evernym.endorser.infrastructure.sharedlib.VdrTools
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import com.evernym.endorser.util.JsonUtil
import com.evernym.vdrtools.wallet.Wallet
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.Logger
import org.mockito.scalatest.MockitoSugar
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, OptionValues}

import scala.collection.mutable
import scala.compat.java8.FutureConverters.CompletionStageOps
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.language.postfixOps


class IndySignerAdapterSpec
  extends ScalaTestWithActorTestKit(withoutClustering(ConfigFactory.parseString(
    s"""
       |endorser.secret.wallet-key-salt = "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW"
       |""".stripMargin)))
    with AnyFreeSpecLike
    with MockitoSugar
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with OptionValues {
  val vdrToolsAvailable: Boolean = Await.result(
    VdrTools()
      .initialize()
      .recover{case _ => false},
    5 seconds
  )

  val logger: Logger = Logger(IndySignerAdapter.getClass)

  def padLeftZeros(inputString: String, length: Int): String = {
    if (inputString.length >= length) return inputString
    val sb = new mutable.StringBuilder
    while ( {
      sb.length < length - inputString.length
    }) sb.append('0')
    sb.append(inputString)
    sb.toString
  }

  def generateWalletKey(entityId: EntityId): String = {
    Await.result(Wallet.generateWalletKey(s"{\"seed\":\"${padLeftZeros(entityId, 32)}\"}").toScala, Duration(5, SECONDS))
  }

  private val persistenceId = PersistenceId("Endorser", "2349873")
  private val persistenceId2 = PersistenceId("Endorser", "124892374")

  lazy val walletConfig1: WalletConfig = WalletUtil.buildWalletConfig(ConfigFactory.parseString(
    s"""
       |endorser.secret.wallet-key-salt = "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW"
       |wallet-type = "default"
       |""".stripMargin))
  lazy val WALLET_CREDENTIALS: String = walletConfig1.buildCredentials(
    WalletUtil.deriveWalletKey(
      WalletUtil.deriveWalletKeySeed(
        "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW",
        persistenceId.entityId
      )
    ),
    "RAW"
  )
  lazy val WALLET_CONFIG: String = walletConfig1.buildConfig(persistenceId.entityId)

  lazy val walletConfig2: WalletConfig = WalletUtil.buildWalletConfig(ConfigFactory.parseString(
    s"""
       |endorser.secret.wallet-key-salt = "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW"
       |wallet-type = "default"
       |""".stripMargin))
  lazy val WALLET_CREDENTIALS2: String = walletConfig2.buildCredentials(
    WalletUtil.deriveWalletKey(
      WalletUtil.deriveWalletKeySeed(
        "W8hJfONJ766gGMNqjSX8exOj1cMRlsKCZLT4y1d9tm94oKOR7C2gwR337qTVeM4QJifguezpLEGz3zTZjVTTGta9bQFRpWu79CAXOn4XDVzcYMKK9jL4jjKW",
        persistenceId2.entityId
      )
    ),
    "RAW"
  )

  lazy val WALLET_CONFIG2: String = walletConfig2.buildConfig(persistenceId2.entityId)

  lazy val adapter1: IndySignerAdapter = IndySignerAdapter(testKit.system, persistenceId.entityId, walletConfig1)

  override protected def beforeEach(): Unit = {
    super.beforeEach()

    if(vdrToolsAvailable) {
      Await.result(Wallet.deleteWallet(WALLET_CONFIG, WALLET_CREDENTIALS).toScala
        .recover({
          case e: Throwable => logger.error("Wallet cleanup failed with exception: ${e.getMessage}")
        }), Duration(3, SECONDS))
      Await.result(Wallet.deleteWallet(WALLET_CONFIG2, WALLET_CREDENTIALS2).toScala
        .recover({
          case e: Throwable => logger.error("Wallet cleanup failed with exception: ${e.getMessage}")
        }), Duration(3, SECONDS))
    }

  }

  override protected def afterAll(): Unit = {
    super.afterAll()

    if(vdrToolsAvailable) {
      Await.result(Wallet.deleteWallet(WALLET_CONFIG, WALLET_CREDENTIALS).toScala
        .recover({
          case e: Throwable => logger.error("Wallet cleanup failed with exception: ${e.getMessage}")
        }), Duration(3, SECONDS))
      Await.result(Wallet.deleteWallet(WALLET_CONFIG2, WALLET_CREDENTIALS2).toScala
        .recover({
          case e: Throwable => logger.error("Wallet cleanup failed with exception: ${e.getMessage}")
        }), Duration(3, SECONDS))
    }

  }

  "VDR Tools Adapter" - {
    "Should create and store a new DID" in {
      assume(vdrToolsAvailable)

      val TRUSTEE_SEED = "00000000000000000000000000000My1"

      Await.result(adapter1.newDID(Some(TRUSTEE_SEED)), 10 seconds) shouldBe "VsKV7grR1BUE29mG2Fm2kX"
    }

    "Should rotate key correctly" in {

      val txnToSign =
        """
          |{
          |   "endorser":"BGBwvBSkdqq7WBYhfdPHeh",
          |   "identifier":"7PiPMw2ARD2AurqtyL5Gbi",
          |   "operation":{
          |      "data":{
          |         "attr_names":[
          |            "firstName",
          |            "lastName"
          |         ],
          |         "name":"test1",
          |         "version":"0.1"
          |      },
          |      "type":"101"
          |   },
          |   "protocolVersion":2,
          |   "reqId":1655246171074977107,
          |   "signature":"9DUvFNm8vVxPSqen7vS9Ev8x6SvMJ1mfWkHzvC5P7hbpeGJpZkLbRDmss6mAAJYKfkZ5wfsC4YkwtzJHm9ctY3K"
          |}
          |""".stripMargin

      assume(vdrToolsAvailable)
      val TRUSTEE_SEED = "8a1c10cb8af97fbb2d4ac08706658fd5bbf476bd5db831a6f5df044c690356ce"
      Await.result(adapter1.newDID(Some(TRUSTEE_SEED)), 10 seconds) shouldBe "BGBwvBSkdqq7WBYhfdPHeh"

      for (_ <- 1 to 10) { // Make sure thing don't change as we repetitive sign the same txn
        val preparedTxn = Await.result(
          adapter1.prepareTxn(
            txnToSign,
            builderNetPrefix,
            Some(DID.fromString("BGBwvBSkdqq7WBYhfdPHeh", Some(builderNetPrefix)))),
          Duration.Inf
        )

        JsonUtil.jsonExtractString(preparedTxn, "/signatures/BGBwvBSkdqq7WBYhfdPHeh").value shouldBe
          "4F7YfzaUZnerYdcLP2NR8KDNaeRpT7CMyqRLgq6G1b5aEQ4Nn9pkpNpX4fM5UbfnXgwZVnBJhLX9Emg9o7GcZcTe"
      }


      Await.result(
        adapter1.rotateKeyWithSeed(
          "BGBwvBSkdqq7WBYhfdPHeh",
          "8aabd3c09b44c778bb1138e364765cd51afe213dda2a0a59f94876cbb7596fb0"
        ),
        10 seconds
      ) shouldBe "HE8cuddmwNGkqAfmLvh3MW91JdW35rS8EC1QbFFEUSEd"

      val preparedTxn2 = Await.result(
        adapter1.prepareTxn(
          txnToSign,
          builderNetPrefix,
          Some(DID.fromString("BGBwvBSkdqq7WBYhfdPHeh", Some(builderNetPrefix)))),
        Duration.Inf
      )

      JsonUtil.jsonExtractString(preparedTxn2, "/signatures/BGBwvBSkdqq7WBYhfdPHeh").value shouldBe
        "2YcjAv97u9DWf7iM8iTkZPQ5V639YAkqfR9dJ3h9EEgQ7ip5rcdgVmcMJpEhiQZdUhRMew67X1oLfsegZTFA8aBw"
    }
    "Should lazily open wallet on newDid function call" in {
      assume(vdrToolsAvailable)

      val adapter2 = IndySignerAdapter(testKit.system, persistenceId2.entityId, walletConfig2)

      try {
        // wallet opening should fail because it will only be created on use
        val wallet = Wallet.openWallet(WALLET_CONFIG2, WALLET_CREDENTIALS2).get()
        // be sure and close wallet just in case so it doesn't interfere with other tests
        wallet.close()
      } catch {
        case e: Throwable => // We expect a WalletNotFoundException
          if (!e.toString.contains("WalletNotFoundException")) {
            fail(e.getMessage)
          }
      }

      // Should open wallet on function call
      Await.result(adapter2.newDID(), Duration.Inf)

      try {
        // wallet opening should fail because wallet is now open
        Wallet.openWallet(WALLET_CONFIG2, WALLET_CREDENTIALS2).get()
        fail("Wallet opened successfully when it should already have been opened. Expected WalletAlreadyOpenedException")
      } catch {
        case e: Throwable => e.getMessage should include("WalletAlreadyOpenedException")
      }

      // close wallet to avoid impacting other tests
      adapter2.close()
    }

    "Should lazily open wallet on prepareTxn function call" in {
      assume(vdrToolsAvailable)

      val adapter2 = IndySignerAdapter(testKit.system, persistenceId2.entityId, walletConfig2)

      try {
        // wallet opening should fail because it will only be created on use
        val wallet = Wallet.openWallet(WALLET_CONFIG2, WALLET_CREDENTIALS2).get()
        // be sure and close wallet just in case so it doesn't interfere with other tests
        wallet.close()
      } catch {
        case e: Throwable => // We expect a WalletNotFoundException
          if (!e.toString.contains("WalletNotFoundException")) {
            fail(e.getMessage)
          }
      }

      // Should open wallet on function call
      try {   // This will probably fail but we don't care, we're only testing the initialization of the wallet
        Await.result(adapter2.prepareTxn(validIndyTxn, builderNetPrefix, Some(endorserDid)), Duration.Inf)
      } catch {
        case e: Throwable => // Do nothing
      }

      try {
        // wallet opening should fail because wallet is now open
        Wallet.openWallet(WALLET_CONFIG2, WALLET_CREDENTIALS2).get()
        fail("Wallet opened successfully when it should already have been opened. Expected WalletAlreadyOpenedException")
      } catch {
        case e: Throwable => e.getMessage should include("WalletAlreadyOpenedException")
      }

      // close wallet to avoid impacting other tests
      adapter2.close()
    }
    "Should close endorser wallet on close command" in {
      assume(vdrToolsAvailable)

      // call some function so wallet will initialize
      adapter1.newDID()

      adapter1.close()

      try {
        // wallet opening should succeed now because wallet has been closed
        val wallet = Wallet.openWallet(WALLET_CONFIG, WALLET_CREDENTIALS).get()
        // be sure and close wallet so it doesn't interfere with other tests
        wallet.close()
      } catch {
        case e: Throwable =>
          fail(e.getMessage)
      }
    }
  }
}
