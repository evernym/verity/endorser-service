package com.evernym.endorser.application.endorser

import akka.actor.testkit.typed.scaladsl.LogCapturing
import akka.actor.typed.{ActorSystem, Behavior}
import akka.persistence.typed.PersistenceId
import com.evernym.endorser.application.endorser.CurrentEndorsersBehavior.EndorserId
import com.evernym.endorser.domain.ledger.Ledger
import com.evernym.endorser.testkit.EventSourcedTestKitHelper
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.{BeforeAndAfterEach, OptionValues}

class CurrentEndorsersBehaviorSpec
  extends EventSourcedTestKitHelper[Commands, Events, CurrentEndorsers]
  with AnyFreeSpecLike
  with LogCapturing
  with BeforeAndAfterEach
  with OptionValues {

  val mockRouter: Router = new Router {
    override def sendToEndorser(cmd: Commands.RouteToActiveEndorser,
                                endorserId: EndorserId)
                               (implicit system: ActorSystem[_]): Unit = {}
  }
  private val persistenceId = PersistenceId("CurrentEndorsers", "1234566")
  override def testBehavior: Behavior[Commands] = CurrentEndorsersBehavior(persistenceId, mockRouter)

  override def beforeEach(): Unit = {
    super.beforeEach()
    kit.clear()
  }

  "should start with empty map" in {
    kit.getState().endorsers shouldBe empty
    kit.restart().state shouldBe kit.getState()

  }

  "should add single endorser" in {
    send(
      Commands.AddActiveEndorser(
        "123",
        Ledger("did:sov:main"),
        None
      )
    ).expectStateOf[CurrentEndorsers] { s =>
      s.endorsers.size shouldBe 1
    }

  }

  "should add single endorser multiple times" in {
    send(
      Commands.AddActiveEndorser(
        "123",
        Ledger("did:sov:main"),
        None
      )
    )

    send(
      Commands.AddActiveEndorser(
        "123",
        Ledger("did:sov:main"),
        None
      )
    )

    send(
      Commands.AddActiveEndorser(
        "123",
        Ledger("did:sov:main"),
        None
      )
    ).expectStateOf[CurrentEndorsers] { s =>
      s.endorsers.size shouldBe 1
      s.endorsers.get("did:sov:main").value shouldBe "123"
    }

  }

  "should add and remove single endorser" in {
    send(
      Commands.AddActiveEndorser(
        "123",
        Ledger("did:sov:main"),
        None
      )
    )
    send(
      Commands.RemoveInactiveEndorser(
        "123"
      )
    ).expectStateOf[CurrentEndorsers] { s =>
      s.endorsers.size shouldBe 0
    }

  }

  "should remove a single endorser" in {
    send(
      Commands.AddActiveEndorser(
        "123",
        Ledger("did:sov:main"),
        None
      )
    )

    send(
      Commands.AddActiveEndorser(
        "432",
        Ledger("did:sov:main2"),
        None
      )
    )

    send(
      Commands.RemoveInactiveEndorser(
        "123"
      )
    ).expectStateOf[CurrentEndorsers] { s =>
      s.endorsers.size shouldBe 1
      s.endorsers.exists(e => e._1 == "432" && e._2 == "did:sov:main2")
    }
  }
}
