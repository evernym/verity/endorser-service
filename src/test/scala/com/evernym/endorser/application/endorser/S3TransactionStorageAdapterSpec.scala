package com.evernym.endorser.application.endorser

import akka.actor.ClassicActorSystemProvider
import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.stream.alpakka.s3.BucketAccess
import akka.stream.alpakka.s3.BucketAccess.{AccessGranted, NotExists}
import akka.stream.alpakka.s3.scaladsl.S3
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.evernym.endorser.application.endorser.S3TransactionStorageAdapter
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.BeforeAndAfterAll

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.{Duration, SECONDS}

class S3TransactionStorageAdapterSpec
    extends ScalaTestWithActorTestKit(withoutClustering(ConfigFactory.parseString(
      """
        |endorser.db.s3 {
        |  bucket-name = "testBucket"
        |  id-length = 11
        |}
        |
        |alpakka.s3 {
        |
        |  buffer = "memory"
        |
        |  aws {
        |    credentials {
        |      provider = static
        |
        |      access-key-id = "accessKey1"
        |      access-key-id = ${?S3_ACCESS_KEY_ID}
        |
        |      secret-access-key = "verySecretKey1"
        |      secret-access-key = ${?S3_SECRET_KEY}
        |    }
        |
        |    region {
        |      provider = static
        |      default-region = "us-west-2"
        |    }
        |  }
        |  //TODO: Move to access-style instead of path-style-access
        |  // The 'access-style=path' is used because at the moment this test in Gitlab CI fails with virtual host path-style.
        |  // Docker can't resolve links with subdomains like http://bucket.containerName:port without additional configuration which is not a priority now.
        |  access-style = path
        |  endpoint-url = "http://localhost:8000"
        |}
        |""".stripMargin).resolve()))
    with AnyFreeSpecLike
    with BeforeAndAfterAll {

  val bucketName = "testBucket"
  implicit val ec: ExecutionContext = testKit.system.executionContext

//  override def beforeAll() = {
//    val ba = Await.result(S3.checkIfBucketExists(bucketName), Duration.Inf)
//    ba match {
//      case NotExists => Await.result(S3.makeBucket(bucketName), Duration.Inf)
//      case _ =>
//    }
//  }

  val storageAdapter: S3TransactionStorageAdapter = new S3TransactionStorageAdapter("endorser", testKit.system)

  "S3 transaction storage adapter" - {
    "Should read in correct configs" in {
      storageAdapter.bucketName shouldBe bucketName
      storageAdapter.idLength shouldBe 11
    }
//    "should return transaction ref of correct length" in {
//      storageAdapter.store("test").map(transactionRef =>
//        transactionRef.length shouldBe 11)
//    }
//
//    "object should be resolvable" in {
//      storageAdapter.store("test").map(ref => {
//        val s3File: Source[Option[(Source[ByteString, _], _)], _] = S3.download(bucketName, ref)
//
//        val Some((data: Source[ByteString, _], _)) =
//          s3File.runWith(Sink.head).futureValue
//
//        data.map(_.utf8String).runWith(Sink.head).map(txn =>
//          txn shouldBe "test"
//        )
//      })
//    }
  }
}
