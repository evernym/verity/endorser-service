package com.evernym.endorser.application.discovery

import akka.actor.typed.ActorSystem
import com.evernym.endorser.testkit.Traits.TestActorSystem
import com.typesafe.config.ConfigException.Missing
import com.typesafe.config.ConfigFactory
import org.scalatest.TryValues
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import scala.language.postfixOps

trait HiTrait {
  def hi(): Unit
}
case class ProviderImpl(configPath: String, as: ActorSystem[_]) extends HiTrait {
  override def hi(): Unit = println(s"HI with path: $configPath")
}

class packageSpec
  extends AnyFreeSpec
    with Matchers
    with TryValues
    with TestActorSystem {
  "discover" - {
    "discoverClassName" in {
      discoverClassName(ConfigFactory.empty(), "test.provider.giver")
        .failed.success.value shouldBe an [Missing]

      val c =
        """
          |test.giver = "test.provider.giver"
          |test.provider.giver.class: "com.test.giver.GiverImpl" """.stripMargin

      discoverClassName(ConfigFactory.parseString(c), "test.giver")
        .success.value shouldBe ("test.provider.giver", "com.test.giver.GiverImpl")
    }

    "discoverClass with concrete type" in {
      val config = ConfigFactory.parseString(
        s"""
           |test.giver = "test.provider.giver"
           |test.provider.giver.class: "${classOf[ProviderImpl].getCanonicalName}" """.stripMargin
      )
      withActorSystem(config) { implicit as =>
        discover[ProviderImpl]("test.giver").success.value shouldBe a [ProviderImpl]
      }
    }

    "discoverClass with trait" in {
      val config = ConfigFactory.parseString(
        s"""
           |test.giver = "test.provider.giver"
           |test.provider.giver.class: "${classOf[ProviderImpl].getCanonicalName}" """.stripMargin
      )

      withActorSystem(config) { implicit as =>
        discover[HiTrait]("test.giver").success.value shouldBe a [HiTrait]
      }
    }
  }
}
