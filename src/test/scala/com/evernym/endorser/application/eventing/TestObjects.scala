package com.evernym.endorser.application.eventing

object TestObjects {

  val validRequestEndorsement: Array[Byte] = """{
    |   "specversion":"1.0",
    |   "id":"fc98f673-cb73-43ae-9f1e-2eaba70b3055",
    |   "source":"http://example.com",
    |   "type":"request.endorsement.v1",
    |   "datacontenttype":"application/json",
    |   "time":"2022-03-22T17:56:50.342059Z",
    |   "data":{
    |      "txnref":"https://pastebin.com/raw/c6Bue6L6",
    |      "ledgerprefix":"did:sov"
    |   }
    |}
    |""".stripMargin.getBytes

  val validTransactionWritten: Array[Byte] ="""
    |{
    |   "specversion":"1.0",
    |   "id":"fc98f673-cb73-43ae-9f1e-2eaba70b3055",
    |   "source":"http://example.com",
    |   "type":"publisher.transaction-written.v1",
    |   "datacontenttype":"application/json",
    |   "time":"2022-03-22T17:56:50.342059Z",
    |   "data":{
    |      "endorsementid":"fd9a00af-fd3f-46b0-8893-af24aab1fae6",
    |      "result": {
    |         "code": "ENDT_RES_0001",
    |         "descr": "foo"
    |       }
    |   }
    |}
    |""".stripMargin.getBytes

  val invalidJsonEvent: Array[Byte] ="""
    |{
    |   "specversion":"1.0",
    |   "id":"fc98f673-cb73-43ae-9f1e-2eaba70b3055",
    |   "source":"http://example.com",
    |   "type":"publisher.transaction-written.v1",
    |   "datacontenttype":"application/json",
    |   "time":"2022-03-22T17:56:50.342059Z",
    |   "data":{
    |      "endorsementid":"fd9a00af-fd3f-46b0-8893-af24aab1fae6",
    |      "result": {
    |         "code": "ENDT_RES_0001",
    |         "descr": "foo"
    |       }
    |
    |}
    |""".stripMargin.getBytes

  val unknownEventType: Array[Byte] ="""
    |{
    |   "specversion":"1.0",
    |   "id":"fc98f673-cb73-43ae-9f1e-2eaba70b3055",
    |   "source":"http://example.com",
    |   "type":"not.a.valid.event.type.that.is.known",
    |   "datacontenttype":"application/json",
    |   "time":"2022-03-22T17:56:50.342059Z",
    |   "data":{
    |      "endorsementid":"fd9a00af-fd3f-46b0-8893-af24aab1fae6",
    |      "result": {
    |         "code": "ENDT_RES_0001",
    |         "descr": "foo"
    |       }
    |   }
    |}
    |""".stripMargin.getBytes

  val corruptDTO: Array[Byte] = """{
                     |   "specversion":"1.0",
                     |   "id":"5d348a2e-c995-4587-b104-3d8be976ef3f",
                     |   "source":"test:test",
                     |   "type":"publisher.transaction-written.v1",
                     |   "datacontenttype":"application/json",
                     |   "time":"2022-04-11T21:00:35.146689Z",
                     |   "data":{
                     |      "endorsementid": 122
                     |   }
                     |}""".stripMargin.getBytes()
}
