package com.evernym.endorser.application.eventing

import com.evernym.endorser.application.adapter.Adapters
import com.evernym.endorser.application.eventing.TestObjects._
import com.evernym.endorser.application.eventing.commands.{CreateDIDFromWrappedSeed, RotateKeyWithSeed}
import com.evernym.endorser.application.port.{EndorsementPort, EndorserPort, PublisherPort}
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results
import com.evernym.endorser.domain.aggregate.endorsement.valueobjects.Results.Result
import com.evernym.endorser.domain.events._
import com.evernym.endorser.domain.events.endorsement.EndorsementTransactionValidated
import com.evernym.endorser.domain.events.endorser.TransactionPrepared
import com.evernym.endorser.domain.ledger.Ledger
import io.cloudevents.CloudEvent
import org.mockito.ArgumentMatchersSugar._
import org.mockito.MockitoSugar
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import java.net.URI

class ApplicationEventHandlerSpec
  extends AnyFreeSpec
  with Matchers
  with MockitoSugar {

  def defaultAdapters: Adapters = {
    Adapters(mock[EndorsementPort], mock[EndorserPort], mock[PublisherPort])
  }


  "EndorsementPort" - {
    "EndorsementRequest event triggers requestEndorsement" in {
      val mock = defaultAdapters
      ApplicationEventHandler(mock).process(requestEndorsementTopic, validRequestEndorsement)

      verify(mock.endorsement, only).requestEndorsement(
        "https://pastebin.com/raw/c6Bue6L6",
        "did:sov",
        Some("http://example.com")
      )
    }

    "TransactionPrepared event triggers holdPreparedTransaction" in {
      val mock = defaultAdapters
      val event = TransactionPrepared.build(
        new URI("test:test"),
        TransactionPrepared.DTO(
          None,
          "fd9a00af-fd3f-46b0-8893-af24aab1fae6",
          "https://pastebin.com/raw/c6Bue6L6",
          "did:sov",
          None,
          "did:foo:Tw5yj73GEcwYXvPhE1xNDq"
        )
      )
      ApplicationEventHandler(mock).process(endorserTopic, Event.serializeEvent(event))

      verify(mock.endorsement, only).holdPreparedTransaction(
        "fd9a00af-fd3f-46b0-8893-af24aab1fae6",
        "https://pastebin.com/raw/c6Bue6L6"
      )
    }

    "PublisherTransactionWritten event triggers completeEndorsement" in {
      val mock = defaultAdapters
      ApplicationEventHandler(mock).process(publisherTopic, validTransactionWritten)

      verify(mock.endorsement, only).completeEndorsement(
        eqTo("fd9a00af-fd3f-46b0-8893-af24aab1fae6"),
        argThat { x: Result => x.toString == Results.writtenToVDR.toString }
      )
    }

    "CreateDIDFromWrappedSeed event triggers createEndorserDIDWithSeed" in {
      val mock = defaultAdapters
      ApplicationEventHandler(mock).process(
        CreateDIDFromWrappedSeed.eventTopic,
        Event.serializeEvent(
          CreateDIDFromWrappedSeed.build(
            new URI("http://example.com"),
            CreateDIDFromWrappedSeed.DTO(
              "29c6dd06-e507-488f-97df-b6f17d7818d8",
              "did:test",
              "token",
              "expectedPath"
            )
          )
        )
      )

      verify(mock.endorser, only).createEndorserDIDWithSeed(
        "29c6dd06-e507-488f-97df-b6f17d7818d8",
        Ledger("did:test"),
        "token",
        "expectedPath"
      )
    }

    "RotateKeyWithSeed event triggers createEndorserDIDWithSeed" in {
      val mock = defaultAdapters
      ApplicationEventHandler(mock).process(
        CreateDIDFromWrappedSeed.eventTopic,
        Event.serializeEvent(
          RotateKeyWithSeed.build(
            new URI("http://example.com"),
            RotateKeyWithSeed.DTO(
              "e6e6374a-5988-4309-a8c0-9c4c424ee29a",
              "token-str",
              "expectedPath given"
            )
          )
        )
      )

      verify(mock.endorser, only).rotateKeyWithSeed(
        "e6e6374a-5988-4309-a8c0-9c4c424ee29a",
        "token-str",
        "expectedPath given"
      )
    }

    "rainy day cases" - {
      "empty string does not call methods" in {
        val mock = defaultAdapters
        ApplicationEventHandler(mock).process(publisherTopic, "".getBytes)

        verifyZeroInteractions(mock.endorsement)
      }

      "invalid JSON string does not call methods" in {
        val mock = defaultAdapters
        ApplicationEventHandler(mock).process(publisherTopic, invalidJsonEvent)

        verifyZeroInteractions(mock.endorsement)
      }

      "invalid event data structure for a event type should not call methods" in {
        val mock = defaultAdapters
        ApplicationEventHandler(mock).process(publisherTopic, corruptDTO)

        verifyZeroInteractions(mock.endorsement)
      }

      "unknown endorser when handing EndorsementTransactionValidated" in {
        val mock = defaultAdapters
        val event: CloudEvent = EndorsementTransactionValidated.build(
          new URI("test:test"),
          EndorsementTransactionValidated.DTO(
            "064892ce-6b27-419d-b32c-365fe78b8b0f",
            "example.com/foo",
            None,
            "did:not-specified",
            None,
            "did:foo:Tw5yj73GEcwYXvPhE1xNDq"
          )
        )
        ApplicationEventHandler(mock).process(EndorsementTransactionValidated.eventTopic, Event.serializeEvent(event))
        verify(mock.endorser, atLeastOnce).prepareTransaction(
          "UNKNOWN_ENDORSER",
          "example.com/foo",
          "did:not-specified",
          "064892ce-6b27-419d-b32c-365fe78b8b0f",
          None
        )
      }

      "unknown event type does not call methods" in {
        val mock = defaultAdapters
        ApplicationEventHandler(mock).process(publisherTopic, unknownEventType)

        verifyZeroInteractions(mock.endorsement)
      }

      "unknown topic does not call methods" in {
        val mock = defaultAdapters
        ApplicationEventHandler(mock).process("unknown.and.invalid.topic", validTransactionWritten)

        verifyZeroInteractions(mock.endorsement)
      }
    }
  }
}
