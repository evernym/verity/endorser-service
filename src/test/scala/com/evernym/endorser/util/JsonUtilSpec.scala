package com.evernym.endorser.util

import com.evernym.endorser.util.JsonUtil.jsonExtractString
import org.scalatest.OptionValues
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class JsonUtilSpec extends AnyFreeSpec with Matchers with OptionValues{
  
  "can extract text" in {
    val jsonTxt = """
      |{
      |  "request_id": "fcf7de1e-125a-d896-4379-cd455110b6b8",
      |  "lease_id": "",
      |  "renewable": false,
      |  "lease_duration": 0,
      |  "data": {
      |    "creation_path": "secret/data/hello",
      |    "creation_time": "2022-03-30T16:18:34.920210499Z",
      |    "creation_ttl": 1200
      |  },
      |  "wrap_info": null,
      |  "warnings": null,
      |  "auth": null
      |}
      |""".stripMargin

    jsonExtractString(jsonTxt, "/data/creation_path").value shouldBe "secret/data/hello"
  }

  "should return None if it don't exist" in {
    val jsonTxt = """
                    |{
                    |  "request_id": "fcf7de1e-125a-d896-4379-cd455110b6b8",
                    |  "lease_id": "",
                    |  "renewable": false,
                    |  "lease_duration": 0,
                    |  "data": {
                    |    "creation_path": "secret/data/hello",
                    |    "creation_time": "2022-03-30T16:18:34.920210499Z",
                    |    "creation_ttl": 1200
                    |  },
                    |  "wrap_info": null,
                    |  "warnings": null,
                    |  "auth": null
                    |}
                    |""".stripMargin

    jsonExtractString(jsonTxt, "/foo/bar") shouldBe None
  }

  "should return None if path the object" in {
    val jsonTxt = """
                    |{
                    |  "request_id": "fcf7de1e-125a-d896-4379-cd455110b6b8",
                    |  "lease_id": "",
                    |  "renewable": false,
                    |  "lease_duration": 0,
                    |  "data": {
                    |    "creation_path": "secret/data/hello",
                    |    "creation_time": "2022-03-30T16:18:34.920210499Z",
                    |    "creation_ttl": 1200
                    |  },
                    |  "wrap_info": null,
                    |  "warnings": null,
                    |  "auth": null
                    |}
                    |""".stripMargin

    jsonExtractString(jsonTxt, "/data") shouldBe None
  }

}
