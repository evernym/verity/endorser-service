package com.evernym.endorser.util

import com.evernym.endorser.util.TryUtils.getOrThrow
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import scala.util.Try


class TryUtilsSpec extends AnyFreeSpec with Matchers {

  "Should throw exception for failed try" in {
    assertThrows[Exception]{
      getOrThrow(Try(throw new Exception("foo")))
    }
  }

  "Should NOT throw exception successful try " in {
    getOrThrow(Try("foo")) shouldBe "foo"
  }

  "Should throw exception successful try " in {
    val t:String = getOrThrow(Try(null))
    t shouldBe null
  }

}
