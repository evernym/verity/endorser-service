package com.evernym.endorser.sample.akka.actor

import akka.actor.testkit.typed.scaladsl.{ScalaTestWithActorTestKit, TestProbe}
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit
import akka.persistence.testkit.scaladsl.EventSourcedBehaviorTestKit.CommandResult
import akka.persistence.typed.PersistenceId
import com.evernym.endorser.sample.akka.actor.CountingEcho.{Count, CountIncremented, Ping, Pong}
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import org.scalatest.BeforeAndAfterEach
import org.scalatest.freespec.AnyFreeSpecLike

import scala.language.postfixOps

class AkkaTypedEventSourcedSampleSpec
  extends ScalaTestWithActorTestKit (withoutClustering(EventSourcedBehaviorTestKit.config))
  with AnyFreeSpecLike
  with BeforeAndAfterEach {

  private val eventSourcedTestKit =
    EventSourcedBehaviorTestKit[CountingEcho.Ping, CountingEcho.CountIncremented, CountingEcho.Count](
      system,
      CountingEcho(
        PersistenceId("CountingEcho", "1234566")
      )
    )

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    eventSourcedTestKit.clear()
  }

  "CountingEcho" - {
    "should count an Ping Requests" in {
      val replyProbe: TestProbe[Pong] = createTestProbe[Pong]()
      val result: CommandResult[Ping, CountIncremented, Count] = eventSourcedTestKit.runCommand(
        Ping("HI", replyProbe.ref)
      )
      replyProbe.expectMessage(Pong("HI"))
      result.event shouldBe CountIncremented()
      result.stateOfType[Count].count shouldBe 1

      val result2: CommandResult[Ping, CountIncremented, Count] = eventSourcedTestKit.runCommand(
        Ping("FOO BAR", replyProbe.ref)
      )
      replyProbe.expectMessage(Pong("FOO BAR"))
      result2.event shouldBe CountIncremented()
      result2.stateOfType[Count].count shouldBe 2
    }
  }

}
