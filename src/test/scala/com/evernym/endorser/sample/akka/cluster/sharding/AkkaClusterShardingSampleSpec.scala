package com.evernym.endorser.sample.akka.cluster.sharding

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import akka.actor.typed.ActorRef
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityTypeKey}
import akka.persistence.typed.PersistenceId
import akka.util.Timeout
import com.evernym.endorser.sample.akka.actor.CountingEcho
import com.evernym.endorser.sample.akka.actor.CountingEcho.Ping
import com.evernym.endorser.sample.akka.cluster.sharding.AkkaClusterShardingSampleSpec.basicConfig
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.freespec.AnyFreeSpecLike

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

class AkkaClusterShardingSampleSpec
  extends ScalaTestWithActorTestKit(basicConfig)
    with AnyFreeSpecLike {

  val sharding: ClusterSharding = ClusterSharding(system)

  val countingEchoTypeKey: EntityTypeKey[Ping] =
    EntityTypeKey[CountingEcho.Ping]("CountingPing")

  sharding.init(Entity(typeKey = countingEchoTypeKey) { entityContext =>
    CountingEcho(PersistenceId(entityContext.entityTypeKey.name, entityContext.entityId))
  })

  private implicit val askTimeout: Timeout = Timeout(5 seconds)

  def askPing(message: String)(rtnRef: ActorRef[CountingEcho.Pong]): CountingEcho.Ping = {
    CountingEcho.Ping(message, rtnRef)
  }

  "Should be able to ask sharded Echo" in {
    val entityRef = sharding.entityRefFor(countingEchoTypeKey, "testId")
    val greeting = entityRef ? askPing("HI")

    val pong = Await.result(greeting, askTimeout.duration)
    pong.message shouldBe "HI"
  }
}


object AkkaClusterShardingSampleSpec {
  val basicConfig:Config = {
    ConfigFactory.parseString(
      """
        |akka.actor.provider = cluster
        |akka.remote.artery.canonical.hostname = 127.0.0.1
        |akka.cluster.seed-nodes = ["akka://AkkaClusterShardingSampleSpec@127.0.0.1:25520"]
        |
        |akka.cluster.downing-provider-class = "akka.cluster.sbr.SplitBrainResolverProvider"
        |akka.cluster.log-info-verbose = off
        |akka.cluster.log-info = off
        |""".stripMargin)
  }
}