package com.evernym.endorser.sample.akka.actor

import akka.actor.testkit.typed.scaladsl.TestProbe
import akka.actor.typed.Behavior
import akka.persistence.typed.PersistenceId
import com.evernym.endorser.sample.akka.actor.CountingEcho.{Count, CountIncremented, Ping, Pong}
import com.evernym.endorser.testkit.EventSourcedTestKitHelper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.freespec.AnyFreeSpecLike

class AkkaTypedEventSourcedSampleSpec2
  extends EventSourcedTestKitHelper[CountingEcho.Ping, CountingEcho.CountIncremented, CountingEcho.Count]
  with AnyFreeSpecLike
  with BeforeAndAfterEach {


  override def beforeEach(): Unit = {
    super.beforeEach()
    kit.clear()
  }

  override def testBehavior: Behavior[CountingEcho.Ping] =
    CountingEcho(
      PersistenceId("CountingEcho", "1234566")
    )


  "CountingEcho" - {
    "should count an Ping Requests" in {
      val replyProbe: TestProbe[Pong] = createTestProbe[Pong]()

      send(Ping("HI", replyProbe.ref))
        .expectEventOf[CountIncremented]()
        .expectStateOf[Count] {
          state =>
            replyProbe.expectMessage(Pong("HI"))
            state.count shouldBe 1
        }

      send(Ping("FOO BAR", replyProbe.ref))
        .expectEventOf[CountIncremented]()
        .expectStateOf[Count] {
          state =>
            replyProbe.expectMessage(Pong("FOO BAR"))
            state.count shouldBe 2
        }
    }
  }
}
