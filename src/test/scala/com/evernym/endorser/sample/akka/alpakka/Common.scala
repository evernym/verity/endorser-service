package com.evernym.endorser.sample.akka.alpakka

object Common {
  val bootstrapServer = "pkc-ymrq7.us-east-2.aws.confluent.cloud:9092"
  val providedUserName: Option[String] = None
//  val providedUserName: Option[String] = Some("")

  val providedApiKey: Option[String] = None
  //  val providedApiKey: Option[String] = Some("")
}
