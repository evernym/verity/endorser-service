package com.evernym.endorser.sample.akka.alpakka

import akka.Done
import akka.actor.typed
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.scaladsl.Source
import com.evernym.endorser.sample.akka.alpakka.Common.{bootstrapServer, providedApiKey, providedUserName}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object EventProducerPOC extends App {
  val username = providedUserName.getOrElse {
    print("Enter username: ")
    scala.io.StdIn.readLine()
  }

  val apiKey = providedApiKey.getOrElse {
    print("Enter password: ")
    scala.io.StdIn.readLine()
  }

  implicit val logger: Logger = Logger(this.getClass)

  def runKafkaProducer(implicit system: typed.ActorSystem[_]): Future[Done] = {
    val topic = "DEMO_EVENTS"

    println("Running Kafka Consumer")

    val done: Future[Done] =
      Source(Seq(
        """
          |{
          |	"ordertime": 1497014222380,
          |	"orderid": 18,
          |	"itemid": "Item_184",
          |	"address": {
          |		"city": "Mountain View",
          |		"state": "CA",
          |		"zipcode": 94041
          |	}
          |}
          |""".stripMargin))
        .map(value => {
          val record = new ProducerRecord[String, String](topic, "18", value)
          println(s"Producing Event: ${record.toString}")
          record
        })
        .runWith(Producer.plainSink(getKafkaProducerSettings(system)))

    done
  }

  private def getKafkaProducerSettings(system: typed.ActorSystem[_]): ProducerSettings[String, String] = {
    // configure Kafka consumer
    val config = system.settings.config.getConfig("akka.kafka.producer")
    ProducerSettings(config, new StringSerializer, new StringSerializer)
  }

  val config: Config = {
    ConfigFactory.parseString(
      s"""
         |akka {
         |  remote.artery.canonical.port = 25520
         |  kafka.producer {
         |    discovery-method = akka.discovery
         |    kafka-clients {
         |    # Required connection configs for Kafka producer, consumer, and admin
         |    bootstrap.servers="$bootstrapServer"
         |    security.protocol=SASL_SSL
         |    sasl.jaas.config="org.apache.kafka.common.security.plain.PlainLoginModule   required username='$username'   password='$apiKey'";
         |    sasl.mechanism=PLAIN
         |    # Required for correctness in Apache Kafka clients prior to 2.6
         |    client.dns.lookup=use_all_dns_ips
         |
         |    # Best practice for higher availability in Apache Kafka clients prior to 3.0
         |    session.timeout.ms=45000
         |
         |    # Best practice for Kafka producer to prevent data loss
         |    acks=all
         |    }
         |  }
         |}
         |""".stripMargin)
  }

  println("Application starting...")

  private final def load[T](msg: String)(f: => T): T = {
    println(s"$msg ... started")
    val rtn = f
    println(s"$msg ... complete")
    rtn
  }

  implicit val system: ActorSystem[_] = load("Creating Actor System")({
    ActorSystem(Behaviors.empty[Any], "Endorser_Service", config)
  })

  val control = runKafkaProducer(system)

  control onComplete {
    case Success(Done) =>
      println("Events Produced")
      system.terminate()
    case Failure(exception) =>
      println(exception)
  }
}
