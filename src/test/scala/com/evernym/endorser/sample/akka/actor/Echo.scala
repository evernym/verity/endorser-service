package com.evernym.endorser.sample.akka.actor

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.persistence.typed.PersistenceId
import akka.persistence.typed.scaladsl.{Effect, EventSourcedBehavior, ReplyEffect}
import com.evernym.endorser.domain.serialization.CborSerializable

object Echo {
  case class Ping(message: String, response: ActorRef[Pong])
  case class Pong(message: String)

  def apply(): Behavior[Ping] = Behaviors.receiveMessage {
    case Ping(m, replyTo) =>
      replyTo ! Pong(m)
      Behaviors.same
  }
}

object CountingEcho {
  case class Ping(message: String, response: ActorRef[Pong]) extends CborSerializable
  case class Pong(message: String)

  case class Count(count: Int) extends CborSerializable {
    def applyEvent(increment: CountIncremented): Count = {
      assert(increment != null)
      this.copy(count = this.count + 1)
    }
  }

  case class CountIncremented() extends CborSerializable

  def commandHandler(state: Count, command: Ping): ReplyEffect[CountIncremented, Count] = {
    assert(state != null)
    Effect.persist(CountIncremented()).thenReply(command.response)(_ => Pong(command.message))
  }

  def apply(persistenceId: PersistenceId): Behavior[Ping] = EventSourcedBehavior.withEnforcedReplies(
    persistenceId,
    Count(0),
    commandHandler,
    (count: Count, event: CountIncremented) => count.applyEvent(event)
  )
}