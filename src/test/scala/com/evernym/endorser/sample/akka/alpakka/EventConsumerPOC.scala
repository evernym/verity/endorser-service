package com.evernym.endorser.sample.akka.alpakka

import akka.Done
import akka.actor.typed
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.kafka.scaladsl.{Committer, Consumer}
import akka.kafka.{CommitterSettings, ConsumerSettings, Subscriptions}
import akka.stream.scaladsl.Sink
import com.evernym.endorser.sample.akka.alpakka.Common.{bootstrapServer, providedApiKey, providedUserName}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.Logger
import org.apache.kafka.common.serialization.StringDeserializer

object EventConsumerPOC extends App {
  val username = providedUserName.getOrElse {
    print("Enter username: ")
    scala.io.StdIn.readLine()
  }

  val apiKey = providedApiKey.getOrElse {
    print("Enter password: ")
    scala.io.StdIn.readLine()
  }

  implicit val logger: Logger = Logger(this.getClass)

  def runKafkaConsumer(implicit system: typed.ActorSystem[_]): Consumer.DrainingControl[Done] = {
    val topic = "DEMO_EVENTS"

    println("Running Kafka Consumer")

    Consumer.sourceWithOffsetContext(getKafkaConsumerSettings(system), Subscriptions.topics(topic))
      .map { consumerRecord =>
        println(s"Received Event: ${consumerRecord.toString}")
      }.via(Committer.flowWithOffsetContext(CommitterSettings(system)))
      .toMat(Sink.ignore)(Consumer.DrainingControl.apply)
      .run()
  }

  private def getKafkaConsumerSettings(system: typed.ActorSystem[_]): ConsumerSettings[String, String] = {
    // configure Kafka consumer
    val config = system.settings.config.getConfig("akka.kafka.consumer")
    ConsumerSettings(config, new StringDeserializer, new StringDeserializer).withGroupId("CONSUMER_POC")
  }

  val config: Config = {
    ConfigFactory.parseString(
      s"""
         |akka {
         |  remote.artery.canonical.port = 25520
         |  kafka.consumer {
         |    discovery-method = akka.discovery
         |    kafka-clients {
         |    # Required connection configs for Kafka producer, consumer, and admin
         |    bootstrap.servers="$bootstrapServer"
         |    security.protocol=SASL_SSL
         |    sasl.jaas.config="org.apache.kafka.common.security.plain.PlainLoginModule   required username='$username'   password='$apiKey'";
         |    sasl.mechanism=PLAIN
         |    # Required for correctness in Apache Kafka clients prior to 2.6
         |    client.dns.lookup=use_all_dns_ips
         |
         |    # Best practice for higher availability in Apache Kafka clients prior to 3.0
         |    session.timeout.ms=45000
         |
         |    # Best practice for Kafka producer to prevent data loss
         |    acks=all
         |    }
         |  }
         |}
         |""".stripMargin)
  }

  println("Application starting...")

  private final def load[T](msg: String)(f: => T): T = {
    println(s"$msg ... started")
    val rtn = f
    println(s"$msg ... complete")
    rtn
  }

  implicit val system: ActorSystem[_] = load("Creating Actor System")({
    ActorSystem(Behaviors.empty[Any], "Endorser_Service", config)
  })

  runKafkaConsumer(system)
}
