package com.evernym.endorser.sample.akka.http.client

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import com.evernym.endorser.testkit.Traits.TestActorSystem
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.{BeforeAndAfter, Ignore}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

@Ignore // This is sample code but we don't want to depend on external resources during tests
class AkkaHttpClientSampleSpec
  extends AnyFreeSpec
    with BeforeAndAfter
    with TestActorSystem {

  "Try to retrieve 'example.com'" in {
      withActorSystem(withoutClustering()) { implicit as =>
        val resp = Await.result(Http().singleRequest(HttpRequest(uri = "http://www.example.com")), 10 seconds)

        println(resp.headers)
        println(resp.protocol)
        println(resp.status)
        println(Unmarshal(resp.entity).to[String])
      }
  }
}
