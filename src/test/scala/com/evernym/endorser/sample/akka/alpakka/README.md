# Proof Of Concepts

## Purpose
These two apps function as POC's for Alpakka Kafka event production and consumption. 

## Running
To run either application you will need an API Key and API Secret. These can be generated
in the Confluent UI. The API Key is the username and the API secret is the password. 

The Event producer should automatically connect to our TEAM1 Kafka instance and produce events
to the DEMO_EVENTS topic, with a sequence of values that read "Hello World! This is an event DEMO!".
No action by the user is required and the events should be viewable in the Confluent UI. 

The Event Consumer POC runs the same way as the producer POC, but in order to see it working
some events will need to be created. This can be done either by running the Event Producer POC,
or by manually creating events in the DEMO_EVENTS topic in Confluent UI. 
