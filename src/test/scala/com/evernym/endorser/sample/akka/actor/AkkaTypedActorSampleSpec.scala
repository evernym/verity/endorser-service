package com.evernym.endorser.sample.akka.actor

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit
import com.evernym.endorser.sample.akka.actor.Echo.{Ping, Pong}
import com.evernym.endorser.testkit.AkkaConfig.withoutClustering
import org.scalatest.freespec.AnyFreeSpecLike

class AkkaTypedActorSampleSpec
  extends ScalaTestWithActorTestKit(withoutClustering())
    with AnyFreeSpecLike {

  "An Actor should be testable" in {
          val replyProbe = createTestProbe[Pong]()
          val underTest = spawn(Echo())
          underTest ! Ping("HI", replyProbe.ref)
          replyProbe.expectMessage(Pong("HI"))
  }
}