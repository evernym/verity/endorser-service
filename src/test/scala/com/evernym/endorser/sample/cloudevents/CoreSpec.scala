package com.evernym.endorser.sample.cloudevents

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.scalalogging.Logger
import io.cloudevents.CloudEvent
import io.cloudevents.core.CloudEventUtils.mapData
import io.cloudevents.core.builder.CloudEventBuilder
import io.cloudevents.core.provider.EventFormatProvider
import io.cloudevents.jackson.{JsonFormat, PojoCloudEventDataMapper}
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import java.net.URI
import java.time.OffsetDateTime.now
import java.time.ZoneId

case class SimpleCaseClass(a: String)

class CoreSpec extends AnyFreeSpec with Matchers {
  val logger: Logger = Logger(this.getClass)

  val jacksonMapper: ObjectMapper = new ObjectMapper()
    .registerModule(DefaultScalaModule)

  val event: CloudEvent = CloudEventBuilder.v1()
    .withId("000")
    .withType("example.demo")
    .withSource(URI.create("http://example.com"))
    .withData("application/json","{\"a\":\"b\"}".getBytes())
    .withTime(now(ZoneId.of("UTC")))
    .withExtension("evernym", 112)
    .build()

  "Core Cloud Event Spec" - {
    "should be able to build event with builder" in {
      event.getId shouldBe "000"
    }

    "should be able to serialize to JSON" in {
      val serialized = EventFormatProvider
        .getInstance
        .resolveFormat(JsonFormat.CONTENT_TYPE)
        .serialize(event)

      logger.info(new String(serialized))
      val tree = jacksonMapper.readTree(new String(serialized))
      tree.get("id").asText() shouldBe "000"
    }

    "should handle CaseClass serialization" in {
      val dataObj = mapData(
        event,
        PojoCloudEventDataMapper.from(jacksonMapper, classOf[SimpleCaseClass])
      ).getValue

      dataObj shouldBe SimpleCaseClass(a="b")
    }

    "should handle CaseClass deserialization from raw bytes" in {
      val serialized = EventFormatProvider
        .getInstance
        .resolveFormat(JsonFormat.CONTENT_TYPE)
        .serialize(event)

      val deserdeEvent = EventFormatProvider
        .getInstance
        .resolveFormat(JsonFormat.CONTENT_TYPE)
        .deserialize(serialized)

      deserdeEvent.getData

      logger.info(deserdeEvent.toString)
    }

    "should handle deserialization from string" in {
      val eventStr =
        """
          |{
          |   "specversion":"1.0",
          |   "id":"9ecae451-2d9a-4b45-b160-d0fbabf58bed",
          |   "source":"admin:devin-fisher",
          |   "type":"endorser.create-endorser-did.v1",
          |   "datacontenttype":"application/json",
          |   "time":"2022-05-27T21:46:12+00:00",
          |   "data":{
          |      "endorserid":"ec6810c2-8877-427c-a54d-b363315e3c19",
          |      "ledgerprefix":"did:indy:sovrin:builder",
          |      "token":"s.OqkudjVfDJ2P0mvxPsU9pOPi",
          |      "expectedpath":"sharedpass/data/dev/temp"
          |   }
          |}
          |""".stripMargin

      val deserdeEvent = EventFormatProvider
        .getInstance
        .resolveFormat(JsonFormat.CONTENT_TYPE)
        .deserialize(eventStr.getBytes)

      logger.info(deserdeEvent.toString)
    }
  }
}
