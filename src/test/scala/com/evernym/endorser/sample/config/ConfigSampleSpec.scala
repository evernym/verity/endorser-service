package com.evernym.endorser.sample.config

import com.typesafe.config.ConfigFactory
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import scala.util.chaining.scalaUtilChainingOps

class ConfigSampleSpec extends AnyFreeSpec with Matchers {
  "sample of += operator" in {
    val sampleConfig = """
        |foo {
        |  bar = "test"
        |  a += ${foo.bar}
        |  a += ${?foo.notthere}
        |}
        |""".stripMargin

    ConfigFactory
      .parseString(sampleConfig).resolve()
      .tap { config =>
        config.getConfig("foo")
        config.getList("foo.a").unwrapped() should contain ("test")
        config.getList("foo.a").size() shouldBe 1
      }
  }

}
