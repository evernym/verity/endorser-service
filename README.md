# Endorser

## Purpose
`Endorser` is a microservice intended to handle endorsing transactions on a variety of Verifiable Data Registries. Initially only Indy based ledgers
will be supported, with Cheqd support a possibility at a later date. 

## Roadmap
Issues related to the development of this service can be tracked here: https://evernym.atlassian.net/browse/VE-1436

## Development Environment

### OS Requirements
`Endorser` is being developed and tested on Ubuntu 20.04.

### Scala and SBT

`endorser` is written in scala and built using SBT. Scala is installed with SBT, so only SBT needs to be installed.

**Install (Ubuntu):**

See instructions on [sbt website](https://www.scala-sbt.org/download.html).

### Lightbend Integration
This project uses Lightbend Telemetry for Metrics reporting. This requires the use of a lightbend commercial token. 

**Use Lightbend Commercial**:
* Get your credential token form Lightbend at: https://www.lightbend.com/account/lightbend-platform/credentials
* Put the credential token in a local file at $HOME/.sbt/lightbend_cred.txt
    * Note: $HOME/.sbt/lightbend_cred.txt should only have the credential token and **NOT** full URL as shown in the above page. Should look like `lln2VuiZnVOumJ6RK_U0OMZ1S_IPGWgBda82Iy87hfCtFyEu` (Not a real token)

**Intellij Exclusion**

SBT will automatically exclude the Lightbend Object when the credential token is not available **BUT** Intellij is not so smart. It most must be manually excluded. This can be easily done via this setting in Intellij -- `File | Settings | Build, Execution, Deployment | Compiler | Excludes`


## Development Tasks

### Compile
The following `sbt` task will compile the main `endorser` project and its associated tests.

```shell
sbt compile test:compile
```

### Unit Tests

```sbt test```

### Integration Tests
Integration tests are planned but not yet built.

## Development Process
All developers are expected to do the following:

1. Keep the default branch (i.e. main) stable
2. Follow TDD principles to produce sufficient test coverage
3. When collaborating with other team members, prepend 'wip-' to a branch name if
   a branch is not ready to be vetted/tested by GitLab CI/CD pipelines. Doing so
   allows developers to push to origin w/o needlessly consuming resources and
   knowingly producing failed jobs.
4. Get new and current tests (unit and integration) passing before submitting a merge request
5. Have at least one peer review merge requests (MR)
6. Once an MR has been merged, and artifacts have been published, upgrade the
   team environment and assign another team member to test the changes using the
   team environment

## Terraform
The pipeline has its own IAM user for creating resources in AWS, it (and other pipeline pre-reqs) are created via terraform, but executed manually by someone from the TE team. See [Manual IAM Readme](deployment/manual_tf_bootstrap/README.md)

### Manual Terraform/terragrunt local execution
There are times when it is useful for a member of TE etc... to execute a terragrunt plan/apply locally. 

Pre-requisites for local execution:
1. Install the vault binary somewhere in your `$PATH`
1. Install terraform somewhere in your `$PATH`
1. Install terragrunt somewhere in your `$PATH`

Below are some basic instructions for accomplishing this:
(Make you sure you set CI_ENVIRONMENT_NAME and the vault login username correctly before executing)

First Set up your shell:
```
vault login -method=ldap -no-print username=dan.farnsworth  # Replace username with correct vault username
export CI_ENVIRONMENT_NAME=staging/staging                  # Replace with correct gitlab environment

export VAULT_TOKEN=$(cat ~/.vault-token)
export VAULT_ADDR=https://vault.corp.evernym.com:8200
export TF_VAR_gitlab_environment_name=$CI_ENVIRONMENT_NAME
export CONFLUENT_API_KEY=$(vault kv get -field=ENDORSER__KAFKA__COMMON__USER_NAME kubernetes_secrets/eks-${CI_ENVIRONMENT_NAME}/endorser/app-confluent)
export CONFLUENT_API_SECRET=$(vault kv get -field=ENDORSER__KAFKA__COMMON__PASSWORD kubernetes_secrets/eks-${CI_ENVIRONMENT_NAME}/endorser/app-confluent)
export CONFLUENT_CLUSTER_ID=$(vault kv get -field=CONFLUENT_CLUSTER_ID kubernetes_secrets/eks-${CI_ENVIRONMENT_NAME}/endorser/app-confluent)
export CONFLUENT_CLUSTER_HTTP_ENDPOINT=$(vault kv get -field=CONFLUENT_CLUSTER_HTTP_ENDPOINT kubernetes_secrets/eks-${CI_ENVIRONMENT_NAME}/endorser/app-confluent)
```

No you should be able to change into the terraform directory of the environment you want, and execute terragrunt
```
cd deployment/live/staging/staging/terraform
terragrunt plan
```
