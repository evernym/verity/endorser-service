# Events

The following documents the **Domain Events** produced by Bounding Context of this microservice. This document was 
handcrafted because we are still exploring more automated and maintainable options but this document should become 
obsolete as we finalize how we want to do schema documentation of our **Domain Events**.

## Naming Convention
### Topics
`<scope>.<classifier>.<domain>.<aggragate>`
* `scope` `public|private` - Defines the consumable scope of the topic.
  * `public` - consumable by any service in outside the scope of the producer's bounded context (or microservice)
  * `private` - only consumable by services within the producer's bounded context (or microservice)
* `classifier` `event` - classification of the type of data in the topic
  * `event` - are domain events as defined by the DDD model, intended to express factual events about the modeled system
  * `request` - Async request from one system to another
  * `cmd` - direct commands to an aggregate
  * TBD - there will likely be other classifier types, but we are not defining any here
* `domain` - named domain that contains the producer that is producing to the topic
* `aggragate` - named aggregate that is producing to the topic

### Event 
`<aggragate>.<event-name>.<version>`
* `aggragate` - named aggregate that is producing to the topic
* `event-name` - domain name of event
* `version` - 

## Cloud Events Conformance
Spec: [v1.0.2](https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md)

Required Fields:
* `id` : UUID of the event
* `source` : identifier for producer
  * TBD the URI schema
* `specversion`: `1.0`
* `type`: conform to above naming convention
* `dataschema` : TBD
* `time`: time of occurrence
* `datacontenttype`:`application/json`
* `data` : JSON with fields that are constrained to the datatypes defined by `Cloud Events` (a subset of JSON's datatypes)

### Versioning
Changes allow without version change:
1. Adding new fields (consumers should gracefully handle unexpected (new) fields)

## TOPIC: `public.event.ssi.endorsement`

### EVENT: `endorsement.transaction-validated.v1`
#### Sample
```json
{
  "specversion":"1.0",
  "id":"9e18d9c0-0f49-4d75-b4f1-cde189b9c4a0",
  "source":"URI<TBD>",
  "type":"endorsement.transaction-validated.v1",
  "datacontenttype":"application/json",
  "time":"2022-03-29T20:18:33.32033Z",
  "data":{
    "endorsementid":"04b3f166-3f43-46f6-95f7-ad5630b4b5af",
    "txnref":"https://pastebin.com/raw/c6Bue6L6",
    "requestsource":"URI<TBD>",
    "ledgerprefix":"did:test",
    "endorserdid":"did:test:P9prvGXKFvCdajeXfBivi4",
    "submitterdid":"did:test:Pw3547NjqCEF94iPM43T46"
  }
}
```
### EVENT: `endorsement.transaction-rejected.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"1c471f31-0d33-4f18-944c-a9be42f01d9e",
   "source":"URI<TBD>",
   "type":"endorsement.transaction-rejected.v1",
   "time":"2022-03-18T22:44:49.642336Z",
   "dataschema":"URI<TBD>",
   "datacontenttype":"application/json",
   "data": {
      "endorsementid":"04b3f166-3f43-46f6-95f7-ad5630b4b5af",
      "txnref":"https://pastebin.com/raw/c6Bue6L6",
      "requestsource":"URI<TBD>",
      "reason":{
        "code":"ENDT_INDY-TXN_0001",
        "descr":"Invalid JSON"
      }
   }
}
```

### EVENT: `endorsement.prepared-transaction-held.v1`
#### Sample 
```json
{
   "specversion":"1.0",
   "id":"2e0a11c9-c144-453f-aa1b-6c6374fd1fd2",
   "source":"URI<TBD>",
   "type":"endorsement.prepared-transaction-held.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T17:45:15.560071Z",
   "data":{
      "endorsementid":"27d5dbc3-8808-42aa-89a8-c5f26333bbc2",
      "requestsource":"URI<TBD>"
   }
}
```
### EVENT: `endorsement.endorsement-complete.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"1c471f31-0d33-4f18-944c-a9be42f01d9e",
   "source":"URI<TBD>",
   "type":"endorsement.endorsement-complete.v1",
   "time":"2022-03-18T22:44:49.642336Z",
   "dataschema":"URI<TBD>",
   "datacontenttype":"application/json",
   "data":{
      "endorsementid":"04b3f166-3f43-46f6-95f7-ad5630b4b5af",
      "requestsource":"URI<TBD>",
      "submitterdid":"did:indy:sov-main:D6CyGGDHPFofPVEx4RY6K7",
      "result":{
         "code":"ENDT-RES-0001",
         "descr":"Transaction Written to VDR (normally a ledger)"
      }
   }
}
```

## TOPIC: `public.event.ssi.endorser`
### EVENT: `endorser.transaction-prepared.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"5e22d900-7916-48ab-98ab-ff55c618e6e8",
   "source":"URI<TBD>",
   "type":"endorser.transaction-prepared.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T20:06:01.672798Z",
   "data":{
      "requestsource":"URI<TBD>",
      "endorsementid":"e5df85b3-7786-4e0d-86a5-c8f0ab42cb03",
      "txnref":"https://pastebin.com/raw/c6Bue6L6",
      "ledgerprefix":"did:test",
      "endorserdid":"did:sov:9kDbgfz2x1haHhVJHb8ksd",
      "submitterdid":"did:sov:7Ym44grAtPVANy91L9sVyc"
   }
}
```
### EVENT: `endorser.transaction-not-prepared.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"5e22d900-7916-48ab-98ab-ff55c618e6e8",
   "source":"URI<TBD>",
   "type":"endorser.transaction-prepared.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T20:06:01.672798Z",
   "data":{
      "requestsource":"URI<TBD>",
      "endorsementid":"e5df85b3-7786-4e0d-86a5-c8f0ab42cb03",
       "reason":{
         "code":"ENDT_INDY-TXN_0001",
         "descr":"Invalid JSON"
       }
   }
}
```
### EVENT: `endorser.endorser-created.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"3ccc2e95-c75e-4d94-b10e-b0478c83afc6",
   "source":"URI<TBD>",
   "type":"endorser.endorser-created.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T20:16:29.94333Z",
   "data":{}
}
```
### EVENT: `endorser.endorser-did-generated.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"0aad1d02-c9e3-4325-9317-d149936b4566",
   "source":"URI<TBD>",
   "type":"endorser.endorser-did-generated.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-28T22:29:31.312002Z",
   "data":{
      "endorserdid":"did:sov:9kDbgfz2x1haHhVJHb8ksd",
      "ledgerprefix":"did:test"
   }
}
```
### EVENT: `endorser.endorser-key-rotated.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"0aad1d02-c9e3-4325-9317-d149936b4566",
   "source":"URI<TBD>",
   "type":"endorser.endorser-key-rotated.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-28T22:29:31.312002Z",
   "data":{
     "newverkey": "GfWXLeCfU8Hg74V7ZjegdbYK3ACxPCUk1VwXjqZg7TvM"
   }
}
```
### EVENT: `endorser.endorser-activated.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"6c38415b-5600-494b-9fd2-6e6bde1d3447",
   "source":"URI<TBD>",
   "type":"endorser.endorser-activated.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T17:00:37.428237Z",
   "data":{
      "endorserdid":"did:sov:9kDbgfz2x1haHhVJHb8ksd",
      "ledgerprefix":"did:test"
   }
}
```
### EVENT: `endorser.endorser-deactivated.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"6c38415b-5600-494b-9fd2-6e6bde1d3447",
   "source":"URI<TBD>",
   "type":"endorser.endorser-deactivated.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T17:00:37.428237Z",
   "data":{
      "endorserdid":"did:sov:9kDbgfz2x1haHhVJHb8ksd",
      "ledgerprefix":"did:test"
   }
}
```

## TOPIC: `public.event.ssi.publisher`
### EVENT: `publisher.transaction-written.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"1e9e131e-690a-478a-99da-4c6141979af0",
   "source":"URI<TBD>",
   "type":"publisher.transaction-written.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T20:06:31.247887Z",
   "data":{
      "endorsementid":"d73cfd20-a7c6-490d-9385-adc74147dc5f",
      "result":{
         "code":"ENDT_RES_0001",
         "descr":"Transaction Written to VDR (normally a ledger)"
      }
   }
}
```
### EVENT: `publisher.transaction-write-failed.v1`
#### Sample
```json
{
   "specversion":"1.0",
   "id":"1e9e131e-690a-478a-99da-4c6141979af0",
   "source":"URI<TBD>",
   "type":"publisher.transaction-written.v1",
   "datacontenttype":"application/json",
   "time":"2022-03-29T20:06:31.247887Z",
   "data":{
      "endorsementid":"d73cfd20-a7c6-490d-9385-adc74147dc5f",
      "result":{
         "code":"ENDT_RES_0002",
         "descr":"Transaction was rejected by VDR (normally a ledger)"
      }
   }
}
```