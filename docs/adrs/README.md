## Architectural Decisions
This is the definitive repository for global Architectural Decisions (ADs). All such decisions _shall be_ recorded as Architectural Design Records (ADRs) and approved by the Architecture Council.
### Instructions
To propose a new global AD, the process is as follows:
1. Create a branch following the following scheme: `[firstname].[lastname]_yymmdd_adr-[title-of-adr-wiith-dashes]`.
2. Copy the ADR template and fill in all required information. The file name shall be `NNNN-title-with-dashes.md` with `NNNN` being the next available consecutive number between `0001` and `9999` and currently not assigned to an ADR.
3. Submit an MR against `main` with any needed explanation included as comments.
4. The Architecture Council will review, comment, and accept/reject the MR.


## Index
### Approved
