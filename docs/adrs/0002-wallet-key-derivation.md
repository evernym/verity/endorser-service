# Wallet Key Derivation

* Status: approved
* Deciders: Project Lead (Devin Fisher), Lead Crypto Engineer (Brent Zundel)
* Date: 2022-03-17

## Context
`vdrtools` wallets require a key when opening/creating that is used to encrypt the content of the wallet. Since
`endorser`'s will need keys in a wallet that they can use to sign the transactions being endorsed, `endorser`'s will
need a deterministic derivable key. This key needs to be both secure and manageable. 

## Decision
Derive the key from two parts:
1. Part from application configuration -- wallet key derivation **salt**
   1. MUST be held in Vault
   2. MUST be generated with cryptographically secure random function generator
   3. MUST be 128 bits long
2. Part from persisted entity -- **entity-ID**

### Hashing schema

sha256(sha256(salt) + sha256(entity-id))

### Intention
The scheme will require both access to run application data (held in running docker container memory and hashicorp vault)
and persisted data. This means that dumping the database would not give you access to the both pieces of data to derive 
the wallet key. Similarly, only having application data does not give you access but that innately true since the 
encrypted wallet data is held in the database anyway.

### Considerations
* The entity-id is fairly public value 
  * contained in events sent between microservices through the event-bus
  * contained in other aggregates
  * contained in logs (or other observability measurements)