# Program Fundamentals

* Status: approved
* Deciders: Project Lead (Devin Fisher)
* Date: 2022-02-24

## Context
Initial considerations for the endorser service. These fundamentals will guild the initialization and coding of this
service.

## Decision


### Architecture Patterns
1. Reactive Manifesto
    1. Event Driven Architecture
    3. Domain-drive Design (DDD)
    4. Microservice
2. Hexagonal Architecture (ports and adapters)
3. Pipeline First
   1. Deployment
   2. Observability
   3. Testability
4. Event Sourced
5. Command Query Responsibility Segregation (CQRS)

###Language
**Scala**

### Stack 
* Akka Runtime (framework)
  * Akka Sharded Cluster
* Akka Persistence
  * Using JDBC database event and snapshot journal (mysql or PostgreSQL)
* Typesafe Config
* Logback Logger
* Lightbend Telemetry (metrics and APM)
* Alpakka Kafka
* VDR Tools


## Decision Considerations
### Architecture Considerations
Most of these patterns have been discussed and advocated for by the Architecture Council. This program will provide
the first opportunity to build a new microservice since taking to heart the architectural principles espoused by these
patterns. As such, this program is seen as an opportunity vet these patterns in a real microservice that has limited 
scope. So, heavy emphases is being place on these patterns (Reactive, Hexagonal and Pipeline First).

Event Sourced and CQRS are seen as good practice, and although they may not be required and/or needed for the scope of 
the program, vetting them in this program will provide value for other future programs and are well-supported by the 
proposed stack (see Stack Considerations).

### Language Considerations
The choice for scala was fairly pragmatic and fell on two considerations:
1. The language is know and productive for the team
2. The language allows use of the Akka Runtime (see Stack Considerations for analysis)

### Stack Considerations
Akka Pros:
1. Akka is designed for the Reactive Manifesto
   1. Nativity message driven -- actors respond to messages
   2. Nativity event driven -- see alpakka-kafka
   3. Nativity resilient -- supervisor allow resilient exception handling
   4. Nativity elasticity -- cluster provides the ability grow and constrict
2. Actor Pattern is well suited for DDD implementations (clear abstraction and encapsulation)
3. Akka persistence is a fairly simple implementation of CQRS
4. Akka has native support for Kafka
5. We have Lightbend Commercial Support for Akka.

Cons:
1. Akka is not traditionally stateless
2. Requires expertise about akka
3. Akka maybe heavy for this small and limited microservice
4. Akka can be hard to debug and deploy