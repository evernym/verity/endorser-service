# Endorser Seed Passing

* Status: approved
* Deciders: Project Lead (Devin Fisher), Lead Crypto Engineer (Brent Zundel)
* Date: 2022-03-17

## Context
The initial set of Verifiable Data Registries (VDR)s targeted by the Endorser Service are the set of Sovrin Foundation 
Networks (BuilderNet, StagingNet, MainNet). The Sovrin Foundation management and cost structure for these networks are 
such that product has required that our organization only have one endorser DID/key pair per VDR network across all of 
our deployment environments (team1, devrc, staging, demo, prod). This means that the single DID/key pair that is setup for 
VDR network (ex. Sovrin MainNet) must exist and be identical on multiple deployments of the Endorser Service. This means
that these DID/key pairs (one per network) must somehow be shared between deployments. That leads to the question about 
how to securely and accurately share these secrets.


## Decision
1. Hold SEEDS in HashiCorp Vault
2. Pass SEEDS using HashiCorp Vault's Response Wrapping
   1. See: https://www.vaultproject.io/docs/concepts/response-wrapping
3. Response Wrapping Token is passed via HTTP REST API or System Event (via Kafka)

### Work Flow
1. Human with access to the SEED in the Vault and Event Bus (confluent)
   1. Access SEED via wrapping API setting the TTL (time to live) to something reasonable (5 minutes)
   2. Post CMD Event with the token, TTL, create time and lookup Path
2. Endorser Service
   1. Receives CMD Event with token, TTL, create time and lookup path
   2. Token Validation (see: https://www.vaultproject.io/docs/concepts/response-wrapping#response-wrapping-token-validation)
   3. Unwrap Token
   4. Pass Command to the appropriate Endorser Aggregate
   5. Endorser Activates after successfully creating DID in wallet
