## Add Useful tools
`apt update ; apt install -y vim mysql-client jq`

## Mysql Database
Requires:
* Mysql Client

Connect:

`source /vault/secrets/app-database`

`mysql -u$ENDORSER__DB__MYSQL__USER -p$ENDORSER__DB__MYSQL__PASSWORD -h $ENDORSER__DB__MYSQL__WRITE_HOST`

### Usefully Queries

**All Events**

```
SELECT 
ordering, 
persistence_id, 
sequence_number, 
writer, 
write_timestamp, 
adapter_manifest,  
event_ser_id, 
event_ser_manifest
FROM event_journal
```

**Events from an Actor**

```
SELECT 
ordering, 
persistence_id, 
sequence_number, 
writer, 
write_timestamp, 
adapter_manifest,  
event_ser_id, 
event_ser_manifest
FROM event_journal
WHERE persistence_id="<CHANGE_ME>"
```

**List of persistent Actors**

```
SELECT DISTINCT
persistence_id
FROM event_journal
```

## Current state of cluster 
Requires: 
* jq

`curl -s http://$(cat /etc/hostname):8558/cluster/members | jq`

## Health Check
`curl -s http://$(cat /etc/hostname):8089/v1/health | jq`

## Get POD Java process Environment Variables
`cat /proc/$(pidof java)/environ | tr '\0' '\n'`