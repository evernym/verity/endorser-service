## Endorser Command Topic
All commands for endorser must go to the following topic:

`private.cmd.ssi.endorser`

## Provision Endorser Command

```json
{
   "specversion":"1.0",
   "id":"<ADD_ID>",
   "source":"admin:<ADD_NAME>",
   "type":"endorser.create-endorser-did.v1",
   "datacontenttype":"application/json",
   "time":"<ADD_TIME>",
   "data":{
      "endorserid":"<ADD_ENDORSER_UUID>",
      "ledgerprefix":"<ADD_LEDGER_PREFIX>",
      "token":"<ADD_VAULT_WRAPPED_TOKEN>",
      "expectedpath":"<ADD_VAULT_PATH>"
   }
}
```

Fields to populate:
* `ADD_ID` -- give the message a new UUID
* `ADD_NAME` -- put operator name for auditability (e.g. `devin-fisher`)
* `ADD_TIME` -- a RFC 3339 timestamp (see: https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#time)
  * On Linux: `date -u --iso-8601=s`
* `ADD_ENDORSER_UUID` -- the UUID for the targeted Endorser Actor
* `ADD_LEDGER_PREFIX` -- the ledger prefix for the targeted Endorser Actor
* `ADD_VAULT_WRAPPED_TOKEN` -- the wrapped secret from value that has SEED that the endoerser will use to generate its DID and key
* `ADD_VAULT_PATH` -- the path of the wrapped secret (path can be looked up using `/v1/sys/wrapping/lookup` in vault API)

> **NOTE** The value path **MUST** have a key name `seed` and that key **MUST** have the needed seed. 


## Rotate Endorser Key

````json
{
   "specversion":"1.0",
   "id":"<ADD_ID>",
   "source":"admin:<ADD_NAME>",
   "type":"endorser.rotate-key-with-seed.v1",
   "datacontenttype":"application/json",
   "time":"<ADD_TIME>",
   "data":{
      "endorserid":"<ADD_ENDORSER_UUID>",
      "token":"<ADD_VAULT_WRAPPED_TOKEN>",
      "expectedpath":"<ADD_VAULT_PATH>"
   }
}
````

Fields to populate:
* `ADD_ID` -- give the message a new UUID
* `ADD_NAME` -- put operator name for auditability (e.g. `devin-fisher`)
* `ADD_TIME` -- a RFC 3339 timestamp (see: https://github.com/cloudevents/spec/blob/v1.0.2/cloudevents/spec.md#time)
  * On Linux: `date -u --iso-8601=s`
* `ADD_ENDORSER_UUID` -- the UUID for the targeted Endorser Actor
* `ADD_VAULT_WRAPPED_TOKEN` -- the wrapped secret from value that has SEED that the endoerser will use to generate its DID and key
* `ADD_VAULT_PATH` -- the path of the wrapped secret (path can be looked up using `/v1/sys/wrapping/lookup` in vault API)
