import DevEnvironmentTasks.envOsCheck
import sbt.Keys.{streams, target, update}
import sbt.internal.util.ManagedLogger
import sbt.{Def, _}

import java.io.File
import java.nio.file.{Files, Path, Paths}
import scala.sys.process._
import scala.util.{Failure, Try}


object SharedLibrary {
  def init: Seq[Def.Setting[_]] = {
    Seq(
      sharedLibrariesDir := target.value.toPath.resolve(dir).resolve(libsSubDir).toFile,
      sharedLibraries := Seq.empty,
      updateSharedLibraries := defaultUpdateSharedLibraries(
        sharedLibraries.value,
        target.value.toPath.resolve(dir),
        streams.value.log,
        envOsCheck.value
      ),
      update := update.dependsOn(updateSharedLibraries).value,
    )
  }

  val sharedLibraries = taskKey[Seq[Lib]]("List native shared Libraries")
  val sharedLibrariesDir = settingKey[File]("Location for Shared Libraries")
  val updateSharedLibraries = taskKey[Unit]("Update Shared Libraries")


  val managedSharedLibTrigger: String = "MANAGE_SHARED_LIBS"
  /*
  This code expects and requires a debian package mangers is on the system (dpkg and apt-get)
  It also expects that the required repos for given share libraries are available
   */
  sealed trait Lib {
    def packageName: String
    def packageVersion(os: Ubuntu): String
    def libraryName: String
  }

  // For cases where the library and package share the same name
  case class StandardLib(packageName: String, givenVersion: String) extends Lib {
    override def packageVersion(os: Ubuntu): String = modifyVer(os, packageName, givenVersion)
    val libraryName: String = s"$packageName.so"
  }

  case class NonMatchingLib(packageName: String, givenVersion: String, libraryName: String ) extends Lib {
    override def packageVersion(os: Ubuntu): String = givenVersion
  }

  case class NonMatchingDistLib(packageName: String, packageBaseVersion: String, libraryName: String ) extends Lib {
    override def packageVersion(os: Ubuntu): String = s"$packageBaseVersion-${os.codeName}"
  }

  val dir = "shared-libs"
  val packageSubDir = "pkg"
  val libsSubDir = "libs"

  def defaultUpdateSharedLibraries(libs: Seq[Lib],
                                   shareLibTarget: Path,
                                   logger: ManagedLogger,
                                   osType: Option[OS]): Unit = {
    osType match {
      case Some(u: Ubuntu) =>
        if(!u.toolsCheck) {
          throw new Exception("This task requires tools that are not available")
        }

        // downloads and unpacks given shared libraries
        libs.foreach { l =>
          downloadSharedLibrary(
            l.packageName,
            l.libraryName,
            l.packageVersion(u),
            shareLibTarget,
            logger
          )
        }

        // very simple check of dependencies
        // if library is not libindy, expects it only dependency to be libindy (this is true as of now)
        // if the library is libindy, check that its dependencies are on the box
        libs.foreach{ l =>
          checkDeps(
            l.packageName,
            l.packageVersion(u),
            shareLibTarget
          )
        }
      case Some(m: MacOS) =>
        println("Unable to update shared libraries for Mac OS")
      case None =>
        println("Unable to update shared libraries for an Unknown OS")
    }
  }

  private def modifyVer(os: Ubuntu, pkgName: String, pkgVer: String) = {
    pkgName match {
      case "libvdrtools" => s"$pkgVer-${os.codeName}"
      case _ => pkgVer
    }
  }

  private val depsLineRegex = "Depends: (.*)".r()

  private def checkDeps(packageName: String, packageVersion: String, dest: Path): Unit = {
    val deps = findDeps(packageName, packageVersion, dest)
    packageName match {
      case "libvdrtools" =>
        deps.foreach{ d =>
          try {
            Seq(
              "dpkg",
              "-l",
              d.trim
            ).!!
          }
          catch {
            case e:RuntimeException => throw new Exception(s"Dependency '$d' NOT found for libindy", e)
          }
        }
      case _ =>
        if(!(deps.length == 1 && deps.head.contains("libindy"))) {
          throw new Exception(s"'$packageName' has a dependency that is not libindy -- currently not allowed")
        }
    }
  }

  /*
  Pulling dependencies from package
   */
  private def findDeps(packageName: String, packageVersion: String, dest: Path): Seq[String] = {
    val packageDest = dest.resolve(packageSubDir)
    val controlFile = Process(
      Seq(
        "dpkg-deb",
        "-I",
        s"${packageName}_${packageVersion}_amd64.deb"
      ),
      packageDest.toFile
    )
    .!!

    depsLineRegex
      .findFirstMatchIn(controlFile)
      .map(_.group(1))
      .getOrElse(throw new Exception(s"Unable to check dependencies for $packageName"))
      .split(",")
      .toSeq
  }


  private def logExceptions(logger: ManagedLogger)(f: => Any): Unit = {
    try {
      f
    }
    catch {
      case e: Exception =>
        logger.error(e.toString)
        e.printStackTrace()
        throw e
    }

  }

  /*
  Downloads package via 'apt', unpackage the package and extract target library
   */
  private def downloadSharedLibrary(packageName: String,
                            libName: String,
                            packageVersion: String,
                            dest: Path,
                            logger: ManagedLogger): Unit = logExceptions(logger) {
    val packageDest = dest.resolve(packageSubDir)
    val packageUnpackedDest = packageDest.resolve(s"$packageName-$packageVersion-unpacked")
    val libDest = dest.resolve(libsSubDir)
    val libVersionedDest = libDest.resolve(s"$libName.$packageVersion")
    if (libVersionedDest.toFile.exists()) {
      logger.debug(s"The lib '$libName' in the '$packageName' for version '$packageVersion' already exists and has already been downloaded.")
    } else {
      logger.info(s"Downloading '$libName' from the package '$packageName' at version '$packageVersion' via Apt")
      logger.debug(s"Targeting '$packageDest' for downloaded package")

      logger.debug(s"Making needed directories in '$dest'")
      makeDir(dest)
      makeDir(packageDest)
      makeDir(packageUnpackedDest)
      makeDir(libDest)

      logger.info("Update apt repositories")
      val updateOut = Try {
        Process(
          Seq(
            "apt-get",
            "update",
          )
        ).!!
      }

      updateOut
      .recoverWith{
        case e: Throwable =>
          logger.warn("Unable to update Apt repositories")
          Failure(e)
      }
      .foreach{ x:String =>
        logger.debug(x)
      }





      logger.debug("Downloading package via 'apt-get'")
      val downloadOut = Process(
        Seq(
          "apt-get",
          "download",
          s"$packageName=$packageVersion"
        ),
        packageDest.toFile
      ).!!
      logger.debug(downloadOut)

      logger.debug("Unzipping package via 'dpkg-deb'")
      Process(
        Seq(
          "dpkg-deb",
          "-x",
          s"${packageName}_${packageVersion}_amd64.deb",
          packageUnpackedDest.toString
        ), packageDest.toFile
      ).!!

      val foundLibFilePath = Process(
        Seq(
          "find",
          packageUnpackedDest.toString,
          "-name",
          s"$libName*")
      )
      .!!
      .trim

      if(foundLibFilePath.isEmpty) {
        throw new Exception(s"Unable to find '$libName' in $packageName for $packageVersion -- cannot continue")
      }

      val libFilePath = Paths.get(foundLibFilePath)


      logger.debug(s"Coping found library to versioned file --'${libVersionedDest.toString}'")
      Files.copy(libFilePath, libVersionedDest)
    }


    val commonFileDest = libDest.resolve(libName)
    logger.debug(s"Coping library to file --'${commonFileDest.toString}'")
    Files.deleteIfExists(commonFileDest)
    Files.copy(libVersionedDest,commonFileDest)
  }

  private def makeDir(dir: Path): Unit = {
    val dirFile = dir.toFile
    if(dirFile.exists()) {
      if(!dirFile.isDirectory){
        throw new Exception(s"'$dir' MUST be a directory")
      }
    }
    else {
      if(!dir.toFile.mkdirs()){
        throw new Exception(s"Unable to create directors for '$dir'")
      }
    }
  }
}
