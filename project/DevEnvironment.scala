import sbt.Keys.sbtVersion
import sbt.{Def, taskKey}

import java.io.IOException
import scala.io.AnsiColor._
import scala.language.postfixOps
import scala.math.max
import scala.sys.process._
import scala.util.matching.Regex
import scala.util.{Failure, Success, Try}

object Need {
  sealed trait NeedScope

  case object Core extends NeedScope
  case object Integration extends NeedScope
  case object IntegrationSDK extends NeedScope
}

object DevEnvironmentTasks {
  //noinspection TypeAnnotation
  def init: Seq[Def.Setting[_]] = {
    Seq(
      envFullCheck := {
        val jdkVer = jdkExpectedVersion.value
        val sbtVer = sbtVersion.value
        val repos = envRepos.value
        DevEnvironment.fullCheck(repos, jdkVer, sbtVer)
      },
      envOsCheck := {
        DevEnvironment.checkOS()
      }
    )
  }
  val envFullCheck = taskKey[Boolean]("Check for required/optional environment elements")
  val envOsCheck= taskKey[Option[OS]]("Setup environment elements")
  val envNativeLibCheck = taskKey[Boolean]("Check for native libraries")
  val envRepos = taskKey[Seq[DevEnvironment.DebianRepo]]("Repos need for the native libs")
  val jdkExpectedVersion = taskKey[String]("Expected version of JDK (not enforced)")
  val agentJars = taskKey[Seq[String]]("Agent jars")
}

//noinspection NameBooleanParameters
object DevEnvironment {
  import DevEnvironmentUtil._

  case class DebianRepo(repo: String, release: String, components: String, keyFingerPrint: String, keyLoc: String) {
    override def toString: String = s"$repo $release $components"
  }

  def fullCheck(repos: Seq[DebianRepo], jdkVer: String, sbtVer: String): Boolean = {
    val os = checkOS()
    Seq(
      os.isDefined,
      checkCertificate(),
      checkScala(jdkVer, sbtVer),
      checkAptRepos(os, repos).getOrElse(false),
      checkDocker()
    ).forall(identity)
  }

  def checkOS(indent: Int = 2): Option[OS] = {
    printCheckHeading("OS", Need.Core)
    val os = sys.props.get("os.name")

    val osType:Option[OS] = os match {
      case Some("Linux") =>
        printDetection("OS", "Linux", true, indent)
        run("cat /etc/os-release")
          .map { content =>
            if(content.contains("ID=ubuntu") || content.contains("ID_LIKE=ubuntu")) {
              content
                .linesIterator
                .find(_.startsWith("UBUNTU_CODENAME="))
                .map{ x =>
                  x.splitAt(x.indexOf("=")+1)._2
                }
                .map(Ubuntu(_, false))
            }
            else {
              None
            }
          }
          .getOrElse(None)
      case Some("Mac OS X") =>
        printDetection("OS", "Mac OS", true, indent)
        Some(MacOS())
      case _ =>
        print((" " * indent) + YELLOW_B + "Automatic setup actions require Ubuntu" + RESET)
        print((" " * indent) + YELLOW_B + "Other Detections will still be attempt" + RESET)
        None
    }

    printDetection("Distribution", osType, osType.isDefined, indent)

    val toolsCheck = osType match {
      case Some(_: Ubuntu) =>
        val dpkgVer = run("dpkg --version")
        val aptVer = run("apt-get -v")
        Seq(
          printDetection("dpkg", dpkgVer, hasVer(dpkgVer), indent),
          printDetection("apt", aptVer, hasVer(aptVer), indent)
        ).forall(identity)
      case _ => true
    }

    osType match {
      case Some(u:Ubuntu) => Some(u.copy(toolsCheck=toolsCheck))
      case o => o
    }
  }

  def checkCertificate(indent: Int = 2): Boolean = {
    printCheckHeading("Certificate", Need.Core)
    val canCurlGitlabReqCert = run("curl -s -o /dev/null https://gitlab.corp.evernym.com/")
    val canCurlGitlabWithoutCert = canCurlGitlabReqCert.transform(
      _ => Success(""), // No need to rerun if successful with cert
      _ => run("curl -k -s -o /dev/null https://gitlab.corp.evernym.com/")
    )

    val rtn = if(canCurlGitlabReqCert.isSuccess) {
      printDetection("Certificate", "", true, indent)
    }
    else if (canCurlGitlabReqCert.isFailure && canCurlGitlabWithoutCert.isFailure) {
      printDetection("Certificate", "Unable to reach evernym gitlab (vpn maybe?)", false, indent)
    }
    else if (canCurlGitlabReqCert.isFailure && canCurlGitlabWithoutCert.isSuccess) {
      printDetection("Certificate", "Missing Evernym certificate", false, indent)
    }
    else {
      printDetection("Certificate", "Unable check certificate", false, indent)
    }

    if (!rtn) println(s"""${" "*(indent+2)} [INSTALL CERT] -- see: https://docs.google.com/document/d/1XnKbaF1CYLa2MXcwZv1KexXokGfU4EaT-yyNpki_5j0""")

    rtn
  }

  def checkScala(jdkVer: String, sbtVer: String, indent: Int = 2): Boolean = {
    printCheckHeading("Scala", Need.Core)
    val checkJdkVer = run("javac -version")
    printDetection("JDK", checkJdkVer, eqVer(checkJdkVer, jdkVer.toDouble), indent)

    printDetection("SBT", sbtVer, hasVer(Try(sbtVer)), indent)
  }

  def checkAptRepos(os: Option[OS], repos: Seq[DebianRepo], indent: Int = 2): Option[Boolean] = {
    printCheckHeading("Debian Repos", Need.Core)
    os match {
      case Some(u: Ubuntu) =>
        Some(
          repos
            .map(u.checkRepo(_, indent))
            .forall(_.getOrElse(false))
        )
      case Some(m: MacOS) =>
        Some(
          repos
            .map(m.checkRepo(_, indent))
            .forall(_.getOrElse(false))
        )
      case _ =>
        println("Unable to check Native Libs for a Non-Ubuntu OS")
        None
    }
  }

  def checkDocker(indent: Int = 2): Boolean = {
    printCheckHeading("Docker", Need.Integration)
    val ver = run("docker --version")
    printDetection("Docker", ver, hasVer(ver), indent)
  }
}

sealed trait OS {
  def checkRepo(repo: DevEnvironment.DebianRepo, indent: Int = 2): Option[Boolean]
  def checkPkg(packageName: String, packageVersion: Option[String], reason: Option[String], indent: Int = 2): Option[Boolean]
}

case class MacOS() extends OS {
  override def checkRepo(repo: DevEnvironment.DebianRepo, indent: Int): Option[Boolean] = None

  override def checkPkg(packageName: String, packageVersion: Option[String], reason: Option[String], indent: Int): Option[Boolean] = None
}

case class Ubuntu(codeName: String, toolsCheck: Boolean) extends OS {
  import DevEnvironment.DebianRepo
  import DevEnvironmentUtil._
  val version: String = codeName match {
    case "focal" => "20.04"
  }

  override def checkRepo(repo: DebianRepo, indent: Int = 2): Option[Boolean] = {
    val fingers = run("apt-key finger")
    val hasRepoKey = printDetection(
      "Repo Signing Key",
      repo.keyFingerPrint,
      fingers.map(_.contains(repo.keyFingerPrint)).getOrElse(false),
      indent
    )

    if (!hasRepoKey) println(s"""${" "*(indent+2)} [INSTALL KEY] -- curl ${repo.keyLoc} | apt-key add -""")

    val repos = run("grep -rhE ^deb /etc/apt/")
      .map(_.linesIterator.toList)
      .map(_.map(_.trim.substring(4)))

    val hasEvernymRep = repos.toOption.exists(_.contains(repo.toString))
    val hasRepo = printDetection(
      s"Debian Repos - $repo",
      if(hasEvernymRep) "found" else "missing",
      hasEvernymRep,
      indent
    )
    if (!hasRepo) println(s"""${" "*(indent+2)} [INSTALL REPO] -- sudo add-apt-repository "deb $repo"""")

    Some(hasRepo && hasRepoKey)
  }

  override def checkPkg(packageName: String, packageVersion: Option[String], reason: Option[String], indent: Int = 2): Option[Boolean] = {
    val lines = run(s"dpkg -s $packageName")
      .map(_.linesIterator.toSeq)

    val statusInstalled = lines
      .map(_.exists(_.matches("Status:.*installed")))

    val installedVersion = lines
      .map(_.find(_.startsWith("Version:")))
      .flatMap { o =>
        Try(o.getOrElse(throw new Exception("Unable to find Version of package")))
      }
      .map { version =>
        packageVersion match {
          case Some(v: String) => (version, version.contains(v))
          case None => (version, true)
        }
      }

    val isValidInstall = for(
      status <- statusInstalled;
      version <- installedVersion
    ) yield status && version._2

    val reasonStr = reason.map(r=>s" (for $r)").getOrElse("")
    val rtn = printDetection(
      s"$packageName$reasonStr",
      installedVersion.map(_._1).getOrElse(""),
      isValidInstall.getOrElse(false),
      indent
    )
    if(!rtn) {
      val pkgStr = s"$packageName${packageVersion.map("="+_).getOrElse("")}"
      println(s"${" "*(indent+2)} [INSTALL PACKAGE] -- sudo apt install $pkgStr")
    }
    Some(rtn)
  }
}

object DevEnvironmentUtil {
  def printDetection(lookingFor: Any, found: Any, isOk: => Boolean, indent: Int): Boolean = {
    val MAX_LINE_SIZE = 130

    val isOkBool = isOk
    val indentStr = " " * indent
    val foundStr = {
      val s = found match {
        case Success(s: String) => s
        case Failure(t: Throwable) => t.getMessage
        case Some(s) => s.toString
        case s => s.toString
      }
      s.linesIterator.mkString(" -- ").replaceAll("\\s", " ")
    }

    val detectionStr = s"${MAGENTA}Detected $lookingFor:$RESET "

    val status = if (isOkBool) {
      s"$GREEN[OK]$RESET"
    } else {
      s"$RED[BAD]$RESET"
    }

    val str = {
      val requiredLength = indentStr.length + detectionStr.length + status.length
      val finalFoundStr = if(requiredLength + foundStr.length > MAX_LINE_SIZE) {
        foundStr.substring(0, MAX_LINE_SIZE-requiredLength)
      } else {
        foundStr
      }

      val mainStr = s"$indentStr$detectionStr$finalFoundStr"
      val pad = max(MAX_LINE_SIZE - (mainStr.length+status.length-2), 2)
      s"$mainStr ${"-"*pad} $status"
    }
    println(str)
    isOkBool
  }

  def printCheckHeading(checking: String, need: Need.NeedScope): Unit = {
    val needStr = need match {
      case Need.Core =>  s"$BLUE[${Need.Core}]$RESET"
      case Need.Integration =>  s"$YELLOW[${Need.Integration}]$RESET"
      case Need.IntegrationSDK =>  s"$YELLOW[${Need.IntegrationSDK}]$RESET"
    }

    val str = s"Checking $checking: $needStr"

    Thread.sleep(1) // give a little time to flush stdout
    println(str)
  }

  def run(cmd: String): Try[String] = {
    Try {
      val outBuffer = new StringBuffer

      val log: ProcessLogger = new ProcessLogger {
        override def out(s: => String): Unit = {
          outBuffer.append(s)
          outBuffer.append("\n")
        }
        override def err(s: => String): Unit = out(s)
        override def buffer[T](f: => T): T = f
      }

      val returnCode = cmd ! log
      if(returnCode == 0) {
        outBuffer.toString
      }
      else {
        throw new Exception(s"cmd failed with $returnCode -- ${outBuffer.toString}")
      }

    }
      .transform(Success(_), {
        case e: IOException if e.getMessage.contains("No such file or directory") =>
          Failure(new Exception("command not found"))
        case e =>
          Failure(e)
      })
  }

  def eqVer(text: Try[String], ver: Double): Boolean = {
    text
      .map(findVer)
      .map(_.contains(ver))
      .getOrElse(false)
  }

  def hasVer(text: Try[String]): Boolean = {
    text
      .map(findVer)
      .map(_.isDefined)
      .getOrElse(false)
  }

  val FLOATING_POINT_VERSION_PAT: Regex = "(\\d+\\.\\d+)".r

  def findVer(text:String): Option[Double] = {
    FLOATING_POINT_VERSION_PAT.findFirstIn(text).map(_.toDouble)
  }
}
