import sbt._
import sbtassembly.MergeStrategy

import java.io.File
import java.nio.file.Files
import scala.language.postfixOps

object Utils {
  /*
  There are several reference.conf files from different libraries. This strategy merges them together by
  concatenating them together. But we want the reference.conf from the application to be concatenated last
  so it can override other configuration settings.
   */
  def referenceConfMerge(): MergeStrategy = new MergeStrategy {
    val name = "referenceConfMerge"
    def apply(tempDir: File, path: String, files: Seq[File]): Either[String, Seq[(File, String)]] = {
      MergeStrategy.concat.apply(
        tempDir,
        path,
        files
          .partition { x =>
            val allBytes = new String(Files.readAllBytes(x.toPath))
            val substrLen = if (allBytes.length < 200) allBytes.length else 200
            allBytes
              .substring(0, substrLen)
              .contains("Application Reference Config File")
          }
          .some
          .map { x =>
            if (x._1.length != 1) throw new Exception(s"Did not find application reference.conf -- MUST EXIST (${x._1.length})")
            x._2 ++ x._1
          }
          .getOrElse(throw new Exception("Unable to merge reference.conf file"))
      )
    }
  }
}
